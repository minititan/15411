	.file	"../tests/struct-arrays.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVL	$5, %eax
	MOVL	$5, %r15d
	CMPL	$0, %r15d
	JL L1
	JMP L10
L10:
	MOVL	$6, %eax
	MOVL	$6, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	$5, 0(%rax)
	MOVQ	$4, %rsi
	ADDQ	%rax, %rsi
	MOVQ	%rsi, %rax
	MOVQ	%rsi, %rax
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L7
	JMP L1
L7:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L8
	JMP L1
L8:
	MOVL	$0, %eax
	MOVQ	%rsi, %rax
	ADDQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %edi
	MOVQ	%rsi, %rax
	MOVL	$4, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$4, %r15d
	CMPL	%eax, %r15d
	JL L5
	JMP L1
L5:
	MOVL	$4, %r15d
	CMPL	$0, %r15d
	JGE L6
	JMP L1
L6:
	MOVL	$16, %eax
	MOVQ	%rsi, %rax
	ADDQ	$16, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	%eax, %edi
	JE L3
	JMP L4
L3:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L4:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
