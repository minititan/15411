	.file	"../tests/return02.l4"
	.text
	.global	_c0_merge_opt
_c0_merge_opt:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVQ	%rdi, %rbx
	MOVQ	%rsi, %rbp
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdx
	MOVQ	%rdx, %rax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	$0, %eax
	JNE L5
	JMP L6
L5:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	$0, %eax
	JNE L3
	JMP L4
L6:
	JMP L4
L3:
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rsi
	ADDQ	$4, %rsi
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$4, %rax
	MOVL	0(%rax), %edi
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$4, %rax
	MOVL	0(%rax), %eax
	ADDL	%edi, %eax
	MOVL	%eax, 0(%rsi)
	JMP L7
L4:
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVL	$0, 0(%rax)
	JMP L7
L7:
	MOVQ	%rdx, %rax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_virtual_fold
_c0_virtual_fold:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L9:
	MOVQ	%rdi, %r12
	MOVL	%esi, %ebx
	CMPL	$0, %ebx
	JE L10
	JMP L11
L10:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L11:
	CMPL	$1, %ebx
	JE L12
	JMP L13
L12:
	MOVQ	%r12, %rax
	MOVL	$0, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L24
	JMP L8
L24:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L25
	JMP L8
L25:
	MOVL	$0, %eax
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$4, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L13:
	MOVQ	%r12, %rax
	MOVL	$0, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L22
	JMP L8
L22:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L23
	JMP L8
L23:
	MOVL	$0, %eax
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rdi
	MOVQ	%rdi, %rax
	MOVL	$1, %eax
	JMP L37
L37:
	MOVQ	$1, %rbp
	MOVQ	%rdi, %rdi
	JMP L19
L19:
	CMPL	%ebx, %ebp
	JL L17
	JMP L18
L17:
	MOVQ	%r12, %rax
	MOVL	%ebp, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	CMPL	%eax, %ebp
	JL L20
	JMP L8
L20:
	CMPL	$0, %ebp
	JGE L21
	JMP L8
L21:
	MOVL	%ebp, %eax
	IMULL	$8, %eax
	ADDQ	%r12, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rdi, %rdi
	MOVQ	%rax, %rsi
	ANDL	$0, %eax
	CALL	_c0_merge_opt
	MOVQ	%rax, %rsi
	MOVQ	%rsi, %rax
	MOVQ	%rsi, %rax
	MOVL	%ebp, %edi
	ADDL	$1, %edi
	MOVL	%edi, %eax
	JMP L38
L38:
	MOVQ	%rdi, %rbp
	MOVQ	%rsi, %rdi
	JMP L19
L18:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	$0, %eax
	JNE L14
	JMP L15
L14:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$4, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L15:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L8:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L29:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %r12
	MOVQ	%r12, %rax
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rbx
	MOVQ	%rbx, %rax
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rbp
	MOVQ	%rbp, %rax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$4, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$4, %rax
	MOVL	$2, 0(%rax)
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$4, %rax
	MOVL	$3, 0(%rax)
	MOVL	$3, %eax
	MOVL	$3, %r15d
	CMPL	$0, %r15d
	JL L28
	JMP L36
L36:
	MOVL	$4, %eax
	MOVL	$4, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	$3, 0(%rax)
	MOVQ	$8, %rdi
	ADDQ	%rax, %rdi
	MOVQ	%rdi, %rax
	MOVQ	%rdi, %rax
	MOVL	$0, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L34
	JMP L28
L34:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L35
	JMP L28
L35:
	MOVL	$0, %eax
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVQ	%r12, 0(%rax)
	MOVQ	%rdi, %rax
	MOVL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L32
	JMP L28
L32:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L33
	JMP L28
L33:
	MOVL	$8, %eax
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	%rbx, 0(%rax)
	MOVQ	%rdi, %rax
	MOVL	$2, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L30
	JMP L28
L30:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L31
	JMP L28
L31:
	MOVL	$16, %eax
	MOVQ	%rdi, %rax
	ADDQ	$16, %rax
	MOVQ	%rbp, 0(%rax)
	MOVQ	%rdi, %rdi
	MOVL	$3, %esi
	ANDL	$0, %eax
	CALL	_c0_virtual_fold
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L28:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
