	.file	"../tests/exception02.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVL	$5000, %eax
	MOVL	$5000, %eax
	MOVL	$5000, %r15d
	CMPL	$0, %r15d
	JL L1
	JMP L18
L18:
	MOVL	$5001, %eax
	MOVL	$5001, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVL	$5000, 0(%rdi)
	MOVQ	$4, %r8
	ADDQ	%rdi, %r8
	MOVQ	%r8, %rdi
	MOVL	$0, %edi
	JMP L19
L19:
	MOVQ	$0, %rcx
	JMP L10
L10:
	CMPL	$5000, %ecx
	JL L8
	JMP L9
L8:
	MOVL	$2, %edi
	MOVL	%ecx, %eax
	CLTD
	IDIVL	%edi
	MOVL	%edx, %edi
	CMPL	$0, %edi
	JE L16
	JMP L17
L16:
	JMP L13
L17:
	JMP L14
L13:
	MOVL	$1, %edi
	JMP L20
L20:
	MOVQ	$1, %rsi
	JMP L15
L14:
	MOVL	$0, %edi
	JMP L21
L21:
	MOVQ	$0, %rsi
	JMP L15
L15:
	MOVL	%esi, %edi
	MOVQ	%r8, %rdi
	MOVL	%ecx, %edi
	TESTQ	%r8, %r8
	jz	_c0_call_raise
	MOVL	-4(%r8), %edi
	CMPL	%edi, %ecx
	JL L11
	JMP L1
L11:
	CMPL	$0, %ecx
	JGE L12
	JMP L1
L12:
	MOVL	%ecx, %edi
	IMULL	$4, %edi
	ADDQ	%r8, %rdi
	MOVL	%esi, 0(%rdi)
	MOVL	%ecx, %esi
	ADDL	$1, %esi
	MOVL	%esi, %edi
	JMP L22
L22:
	MOVQ	%rsi, %rcx
	JMP L10
L9:
	MOVQ	%r8, %rax
	MOVL	$50001, %eax
	TESTQ	%r8, %r8
	jz	_c0_call_raise
	MOVL	-4(%r8), %eax
	MOVL	$50001, %r15d
	CMPL	%eax, %r15d
	JL L6
	JMP L1
L6:
	MOVL	$50001, %r15d
	CMPL	$0, %r15d
	JGE L7
	JMP L1
L7:
	MOVL	$200004, %eax
	MOVQ	%r8, %rax
	ADDQ	$200004, %rax
	MOVL	0(%rax), %eax
	CMPL	$0, %eax
	JNE L3
	JMP L4
L3:
	MOVL	$1, %eax
	JMP L23
L23:
	MOVQ	$1, %rax
	JMP L5
L4:
	MOVL	$0, %eax
	JMP L24
L24:
	MOVQ	$0, %rax
	JMP L5
L5:
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
