	.file	"../tests/mytest.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L1:
	MOVL	$4, %ebx
	CMPL	$0, %ebx
	JL L12
	JMP L11
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L11:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rcx
	MOVL	$0, %eax
	JMP L13
L13:
	MOVQ	%rax, %rax
	JMP L10
L10:
	CMPL	$4, %eax
	JL L8
	JMP L9
L8:
	INCL	%eax
	MOVL	%eax, %eax
	JMP L14
L14:
	MOVQ	%rax, %rax
	JMP L10
L9:
	MOVL	$0, %eax
	JMP L15
L15:
	MOVQ	%rax, %rsi
	JMP L4
L4:
	CMPL	$4, %esi
	JL L2
	JMP L3
L2:
	MOVQ	%rcx, %rdx
	MOVL	%esi, %eax
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %edi
	CMPL	%edi, %eax
	JL L5
	JMP L6
L5:
	CMPL	$0, %eax
	JGE L7
	JMP L6
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L7:
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	0(%rax), %eax
	MOVL	%esi, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L16
L16:
	MOVQ	%rax, %rsi
	JMP L4
L3:
	MOVL	$6, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
