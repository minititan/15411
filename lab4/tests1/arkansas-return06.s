	.file	"../tests1/arkansas-return06.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$10, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L23
	JMP L22
L23:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L22:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rbp
	MOVL	$10, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L21
	JMP L20
L21:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L20:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rdx
	MOVL	$0, %eax
	JMP L24
L24:
	MOVQ	%rax, %rcx
	JMP L13
L13:
	CMPL	$10, %ecx
	JL L11
	JMP L12
L11:
	MOVQ	%rbp, %rsi
	MOVL	%ecx, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L17
	JMP L18
L17:
	CMPL	$0, %eax
	JGE L19
	JMP L18
L18:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L19:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	%ecx, 0(%rax)
	MOVQ	%rdx, %rsi
	MOVL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L14
	JMP L15
L14:
	CMPL	$0, %edi
	JGE L16
	JMP L15
L15:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L16:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	$10, %edi
	SUBL	%ecx, %edi
	MOVL	%edi, 0(%rax)
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L25
L25:
	MOVQ	%rax, %rcx
	JMP L13
L12:
	MOVL	$0, %eax
	MOVL	$0, %edi
	JMP L26
L26:
	MOVQ	%rax, %rdx
	MOVQ	%rdi, %rcx
	JMP L4
L4:
	CMPL	$10, %ecx
	JL L2
	JMP L3
L2:
	MOVQ	%r12, %rdi
	MOVL	%ecx, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L8
	JMP L9
L8:
	CMPL	$0, %eax
	JGE L10
	JMP L9
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L10:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %r8d
	MOVQ	%r12, %rdi
	MOVL	%ecx, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L5
	JMP L6
L5:
	CMPL	$0, %esi
	JGE L7
	JMP L6
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L7:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	ADDL	%r8d, %eax
	ADDL	%edx, %eax
	MOVL	%eax, %edi
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L27
L27:
	MOVQ	%rdi, %rdx
	MOVQ	%rax, %rcx
	JMP L4
L3:
	MOVL	%edx, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
