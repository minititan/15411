	.file	"../tests1/rhodeisland-return-binarysearch.l4"
	.text
	.global	_c0_search
_c0_search:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %r9
	MOVL	%esi, %r8d
	MOVL	%edx, %esi
	MOVL	%ecx, %edi
	CMPL	%edi, %esi
	JG L13
	JMP L14
L13:
	JMP L16
L16:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$8, %rsp
	RET
L15:
	JMP L53
L53:
	MOVQ	%rax, %rcx
	MOVQ	%rax, %r11
	MOVQ	%rax, %r10
	MOVQ	%rax, %r8
	JMP L17
L14:
	JMP L54
L54:
	MOVQ	%rdi, %rcx
	MOVQ	%rsi, %r11
	MOVQ	%r8, %r10
	MOVQ	%r9, %r8
	JMP L17
L17:
	MOVL	%ecx, %esi
	SUBL	%r11d, %esi
	MOVL	$2, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	ADDL	%r11d, %edi
	MOVL	%edi, %eax
	MOVQ	%r8, %rax
	MOVL	%edi, %eax
	TESTQ	%r8, %r8
	jz	_c0_call_raise
	MOVL	-4(%r8), %eax
	CMPL	%eax, %edi
	JL L5
	JMP L1
L5:
	CMPL	$0, %edi
	JGE L6
	JMP L1
L6:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%r8, %rax
	MOVL	0(%rax), %eax
	CMPL	%r10d, %eax
	JE L3
	JMP L4
L3:
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
L4:
	MOVQ	%r8, %rax
	MOVL	%edi, %eax
	TESTQ	%r8, %r8
	jz	_c0_call_raise
	MOVL	-4(%r8), %eax
	CMPL	%eax, %edi
	JL L9
	JMP L1
L9:
	CMPL	$0, %edi
	JGE L10
	JMP L1
L10:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%r8, %rax
	MOVL	0(%rax), %eax
	CMPL	%r10d, %eax
	JG L7
	JMP L8
L7:
	MOVL	%edi, %eax
	SUBL	$1, %eax
	MOVQ	%r8, %rdi
	MOVL	%r10d, %esi
	MOVL	%r11d, %edx
	MOVL	%eax, %ecx
	ANDL	$0, %eax
	CALL	_c0_search
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L8:
	MOVL	%edi, %eax
	ADDL	$1, %eax
	MOVQ	%r8, %rdi
	MOVL	%r10d, %esi
	MOVL	%eax, %edx
	MOVL	%ecx, %ecx
	ANDL	$0, %eax
	CALL	_c0_search
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_insertionsort
_c0_insertionsort:
	SUBQ	$8, %rsp
L19:
	MOVQ	%rdi, %r10
	MOVL	%esi, %r9d
	MOVL	$0, %edi
	JMP L55
L55:
	MOVQ	$0, %r8
	JMP L22
L22:
	CMPL	%r9d, %r8d
	JL L20
	JMP L21
L20:
	MOVQ	%r10, %rdi
	MOVL	%r8d, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %r8d
	JL L36
	JMP L18
L36:
	CMPL	$0, %r8d
	JGE L37
	JMP L18
L37:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %ecx
	MOVL	%ecx, %edi
	MOVL	%r8d, %esi
	SUBL	$1, %esi
	MOVL	%esi, %edi
	JMP L56
L56:
	MOVQ	%rsi, %rdx
	JMP L27
L27:
	CMPL	$0, %edx
	JGE L28
	JMP L29
L28:
	MOVQ	%r10, %rdi
	MOVL	%edx, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %edx
	JL L30
	JMP L18
L30:
	CMPL	$0, %edx
	JGE L31
	JMP L18
L31:
	MOVL	%edx, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	CMPL	%ecx, %edi
	JG L25
	JMP L26
L29:
	JMP L26
L25:
	MOVQ	%r10, %rdi
	MOVL	%edx, %edi
	ADDL	$1, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %esi
	CMPL	%esi, %edi
	JL L32
	JMP L18
L32:
	CMPL	$0, %edi
	JGE L33
	JMP L18
L33:
	IMULL	$4, %edi
	MOVQ	%r10, %rsi
	ADDQ	%rdi, %rsi
	MOVQ	%r10, %rdi
	MOVL	%edx, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %edx
	JL L34
	JMP L18
L34:
	CMPL	$0, %edx
	JGE L35
	JMP L18
L35:
	MOVL	%edx, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%edi, 0(%rsi)
	MOVL	%edx, %esi
	SUBL	$1, %esi
	MOVL	%esi, %edi
	JMP L57
L57:
	MOVQ	%rsi, %rdx
	JMP L27
L26:
	MOVQ	%r10, %rdi
	MOVL	%edx, %edi
	ADDL	$1, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %esi
	CMPL	%esi, %edi
	JL L23
	JMP L18
L23:
	CMPL	$0, %edi
	JGE L24
	JMP L18
L24:
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	%ecx, 0(%rdi)
	MOVL	%r8d, %esi
	ADDL	$1, %esi
	MOVL	%esi, %edi
	JMP L58
L58:
	MOVQ	%rsi, %r8
	JMP L22
L21:
	ADDQ	$8, %rsp
	RET
L18:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L39:
	MOVL	$6, %eax
	MOVL	$6, %r15d
	CMPL	$0, %r15d
	JL L38
	JMP L52
L52:
	MOVL	$7, %eax
	MOVL	$7, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	$6, 0(%rax)
	MOVQ	$4, %rbx
	ADDQ	%rax, %rbx
	MOVQ	%rbx, %rax
	MOVQ	%rbx, %rax
	MOVL	$0, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L50
	JMP L38
L50:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L51
	JMP L38
L51:
	MOVL	$0, %eax
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVL	$3, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$1, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L48
	JMP L38
L48:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L49
	JMP L38
L49:
	MOVL	$4, %eax
	MOVQ	%rbx, %rax
	ADDQ	$4, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$2, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L46
	JMP L38
L46:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L47
	JMP L38
L47:
	MOVL	$8, %eax
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVL	$77, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$3, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$3, %r15d
	CMPL	%eax, %r15d
	JL L44
	JMP L38
L44:
	MOVL	$3, %r15d
	CMPL	$0, %r15d
	JGE L45
	JMP L38
L45:
	MOVL	$12, %eax
	MOVQ	%rbx, %rax
	ADDQ	$12, %rax
	MOVL	$12, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$4, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$4, %r15d
	CMPL	%eax, %r15d
	JL L42
	JMP L38
L42:
	MOVL	$4, %r15d
	CMPL	$0, %r15d
	JGE L43
	JMP L38
L43:
	MOVL	$16, %eax
	MOVQ	%rbx, %rax
	ADDQ	$16, %rax
	MOVL	$6, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$5, %eax
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	MOVL	$5, %r15d
	CMPL	%eax, %r15d
	JL L40
	JMP L38
L40:
	MOVL	$5, %r15d
	CMPL	$0, %r15d
	JGE L41
	JMP L38
L41:
	MOVL	$20, %eax
	MOVQ	%rbx, %rax
	ADDQ	$20, %rax
	MOVL	$9, 0(%rax)
	MOVQ	%rbx, %rdi
	MOVL	$6, %esi
	ANDL	$0, %eax
	CALL	_c0_insertionsort
	MOVQ	%rbx, %rdi
	MOVL	$3, %esi
	MOVL	$0, %edx
	MOVL	$5, %ecx
	ANDL	$0, %eax
	CALL	_c0_search
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	RET
L38:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
