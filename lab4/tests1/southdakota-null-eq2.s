	.file	"../tests1/southdakota-null-eq2.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVQ	$0, %r15
	CMPQ	$0, %r15
	JE L3
	JMP L4
L3:
	MOVQ	$0, %r15
	CMPQ	$0, %r15
	JNE L5
	JMP L6
L5:
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
L5:
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
L6:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L3:
	MOVQ	$0, %r15
	CMPQ	$0, %r15
	JNE L5
	JMP L6
L5:
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
L5:
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
L6:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L4:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
