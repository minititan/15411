	.file	"../tests1/california-return-binsearch.l4"
	.text
	.global	_c0_binsearch
_c0_binsearch:
	SUBQ	$8, %rsp
L2:
	MOVL	%edi, %r9d
	MOVQ	%rsi, %r10
	MOVL	%edx, %esi
	MOVL	$0, %edi
	MOVL	%esi, %edi
	JMP L24
L24:
	MOVQ	%rsi, %r8
	MOVQ	$0, %rcx
	JMP L5
L5:
	CMPL	%r8d, %ecx
	JL L3
	JMP L4
L3:
	MOVL	%r8d, %edi
	SUBL	%ecx, %edi
	MOVL	$2, %esi
	MOVL	%edi, %eax
	CLTD
	IDIVL	%esi
	MOVL	%eax, %edi
	MOVL	%ecx, %esi
	ADDL	%edi, %esi
	MOVL	%esi, %edi
	MOVQ	%r10, %rdi
	MOVL	%esi, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %esi
	JL L8
	JMP L1
L8:
	CMPL	$0, %esi
	JGE L9
	JMP L1
L9:
	MOVL	%esi, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	CMPL	%r9d, %edi
	JL L6
	JMP L7
L6:
	MOVL	%esi, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L25
L25:
	MOVQ	%r8, %rsi
	MOVQ	%rdi, %rdi
	JMP L15
L7:
	MOVQ	%r10, %rdi
	MOVL	%esi, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %esi
	JL L12
	JMP L1
L12:
	CMPL	$0, %esi
	JGE L13
	JMP L1
L13:
	MOVL	%esi, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	CMPL	%r9d, %edi
	JG L10
	JMP L11
L10:
	MOVL	%esi, %edi
	JMP L14
L11:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
L14:
	JMP L26
L26:
	MOVQ	%rsi, %rsi
	MOVQ	%rcx, %rdi
	JMP L15
L15:
	JMP L27
L27:
	MOVQ	%rsi, %r8
	MOVQ	%rdi, %rcx
	JMP L5
L4:
	MOVL	$-1, %eax
	MOVL	$-1, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L17:
	MOVL	$20, %eax
	MOVL	$20, %r15d
	CMPL	$0, %r15d
	JL L16
	JMP L23
L23:
	MOVL	$21, %eax
	MOVL	$21, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	$20, 0(%rax)
	MOVQ	$4, %rsi
	ADDQ	%rax, %rsi
	MOVQ	%rsi, %rax
	MOVL	$0, %eax
	JMP L28
L28:
	MOVQ	$0, %rdi
	JMP L20
L20:
	CMPL	$20, %edi
	JL L18
	JMP L19
L18:
	MOVQ	%rsi, %rax
	MOVL	%edi, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L21
	JMP L16
L21:
	CMPL	$0, %edi
	JGE L22
	JMP L16
L22:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	MOVQ	%rsi, %rdx
	ADDQ	%rax, %rdx
	MOVL	%edi, %eax
	ADDL	$1, %eax
	MOVL	%eax, 0(%rdx)
	MOVL	%edi, %eax
	ADDL	$1, %eax
	MOVL	%eax, %edi
	JMP L29
L29:
	MOVQ	%rax, %rdi
	JMP L20
L19:
	MOVL	$5, %edi
	MOVQ	%rsi, %rsi
	MOVL	$20, %edx
	ANDL	$0, %eax
	CALL	_c0_binsearch
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L16:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
