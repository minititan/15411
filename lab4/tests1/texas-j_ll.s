	.file	"../tests1/texas-j_ll.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rbp
	MOVQ	%rbp, %rax
	MOVQ	%rbp, %rdi
	MOVL	$1, %esi
	ANDL	$0, %eax
	CALL	_c0_addToBack
	MOVQ	%rbp, %rdi
	MOVL	$2, %esi
	ANDL	$0, %eax
	CALL	_c0_addToBack
	MOVQ	%rbp, %rdi
	MOVL	$3, %esi
	ANDL	$0, %eax
	CALL	_c0_addToBack
	MOVQ	%rbp, %rdi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	_c0_addToFront
	MOVQ	%rbp, %rdi
	MOVL	$5, %esi
	ANDL	$0, %eax
	CALL	_c0_addToFront
	MOVQ	%rbp, %rdi
	MOVL	$6, %esi
	ANDL	$0, %eax
	CALL	_c0_addToBack
	MOVQ	%rbp, %rdi
	MOVL	$7, %esi
	ANDL	$0, %eax
	CALL	_c0_addToFront
	MOVQ	%rbp, %rdi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	_c0_addToBack
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rbx
	MOVQ	%rbx, %rax
	MOVQ	%rbp, %rdi
	MOVL	$2, %esi
	MOVQ	%rbx, %rdx
	ANDL	$0, %eax
	CALL	_c0_get
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	CMPL	$0, %eax
	JNE L3
	JMP L4
L3:
	MOVL	0(%rbx), %eax
	MOVL	%eax, %edi
	JMP L26
L26:
	MOVQ	%rax, %rax
	JMP L5
L4:
	MOVL	$-1, %eax
	MOVL	$-1, %eax
	JMP L27
L27:
	MOVQ	$-1, %rax
	JMP L5
L5:
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_addToFront
_c0_addToFront:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L7:
	MOVQ	%rdi, %rbp
	MOVL	%esi, %ebx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdx
	MOVQ	%rdx, %rdi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rdi
	ADDQ	$0, %rdi
	MOVL	%ebx, 0(%rdi)
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rsi
	ADDQ	$8, %rsi
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	MOVQ	%rdi, 0(%rsi)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	%rdx, 0(%rdi)
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L6:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_addToBack
_c0_addToBack:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L9:
	MOVQ	%rdi, %rbp
	MOVL	%esi, %ebx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rcx
	MOVQ	%rcx, %rdi
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVQ	%rcx, %rdi
	ADDQ	$0, %rdi
	MOVL	%ebx, 0(%rdi)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	CMPQ	$0, %rdi
	JE L10
	JMP L11
L10:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	%rcx, 0(%rdi)
	JMP L15
L11:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdx
	MOVQ	%rdx, %rdi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rdi
	ADDQ	$8, %rdi
	MOVQ	0(%rdi), %rsi
	MOVQ	%rsi, %rdi
	JMP L28
L28:
	MOVQ	%rsi, %rsi
	MOVQ	%rdx, %rdx
	JMP L14
L14:
	CMPQ	$0, %rsi
	JNE L12
	JMP L13
L12:
	MOVQ	%rsi, %rdi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVQ	%rsi, %rdi
	ADDQ	$8, %rdi
	MOVQ	0(%rdi), %rdi
	MOVQ	%rdi, %rsi
	JMP L29
L29:
	MOVQ	%rdi, %rsi
	MOVQ	%rsi, %rdx
	JMP L14
L13:
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVQ	%rcx, %rsi
	ADDQ	$8, %rsi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rdi
	ADDQ	$8, %rdi
	MOVQ	0(%rdi), %rdi
	MOVQ	%rdi, 0(%rsi)
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rdi
	ADDQ	$8, %rdi
	MOVQ	%rcx, 0(%rdi)
	JMP L15
L15:
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L8:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_get
_c0_get:
	SUBQ	$8, %rsp
L17:
	MOVQ	%rdi, %rax
	MOVL	%esi, %esi
	MOVQ	%rdx, %rdx
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rdi
	MOVQ	%rdi, %rax
	JMP L30
L30:
	MOVQ	%rdi, %rdi
	MOVQ	%rsi, %rax
	JMP L23
L23:
	CMPL	$0, %eax
	JG L24
	JMP L25
L24:
	CMPQ	$0, %rdi
	JNE L21
	JMP L22
L25:
	JMP L22
L21:
	MOVL	%eax, %esi
	SUBL	$1, %esi
	MOVL	%esi, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rdi
	MOVQ	%rdi, %rax
	JMP L31
L31:
	MOVQ	%rdi, %rdi
	MOVQ	%rsi, %rax
	JMP L23
L22:
	CMPQ	$0, %rdi
	JE L18
	JMP L19
L18:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L19:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdx)
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L16:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
