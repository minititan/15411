	.file	"../tests1/idaho-exception-array2.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$2147483647, %ebx
	MOVL	$2, %ebp
	MOVL	$2147483647, %r12d
	CMPL	$0, %r12d
	JL L8
	JMP L7
L8:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L7:
	MOVL	$1, %eax
	ADDL	%r12d, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%r12d, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	MOVL	%ebx, %edi
	ADDL	%ebp, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L2
	JMP L3
L2:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L3:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L4
	JMP L5
L4:
	CMPL	$0, %edi
	JGE L6
	JMP L5
L5:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L6:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$2, 0(%rax)
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
