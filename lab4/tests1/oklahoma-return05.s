	.file	"../tests1/oklahoma-return05.l4"
	.text
	.global	_c0_sum
_c0_sum:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rcx
	MOVL	%esi, %r10d
	MOVL	%edx, %r8d
	MOVL	$0, %eax
	MOVL	$0, %eax
	JMP L153
L153:
	MOVQ	$0, %rdx
	MOVQ	$0, %rdi
	JMP L5
L5:
	CMPL	%r10d, %edx
	JL L3
	JMP L4
L3:
	MOVL	$0, %eax
	JMP L154
L154:
	MOVQ	$0, %r9
	MOVQ	%rdi, %rsi
	JMP L8
L8:
	CMPL	%r8d, %r9d
	JL L6
	JMP L7
L6:
	MOVQ	%rcx, %rax
	MOVL	%edx, %eax
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVL	-8(%rcx), %eax
	CMPL	%eax, %edx
	JL L9
	JMP L1
L9:
	CMPL	$0, %edx
	JGE L10
	JMP L1
L10:
	MOVL	%edx, %eax
	IMULL	$8, %eax
	ADDQ	%rcx, %rax
	MOVQ	0(%rax), %rdi
	MOVL	%r9d, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %r9d
	JL L11
	JMP L1
L11:
	CMPL	$0, %r9d
	JGE L12
	JMP L1
L12:
	MOVL	%r9d, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	ADDL	%eax, %esi
	MOVL	%esi, %eax
	MOVL	%r9d, %edi
	ADDL	$1, %edi
	MOVL	%edi, %eax
	JMP L155
L155:
	MOVQ	%rdi, %r9
	MOVQ	%rsi, %rsi
	JMP L8
L7:
	MOVL	%edx, %edi
	ADDL	$1, %edi
	MOVL	%edi, %eax
	JMP L156
L156:
	MOVQ	%rdi, %rdx
	MOVQ	%rsi, %rdi
	JMP L5
L4:
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_iter
_c0_iter:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$16, %rsp
L14:
	MOVQ	%rdi, 8(%rsp)
	MOVL	%esi, %r12d
	MOVL	%edx, %r13d
	MOVL	$10, %eax
	MOVL	$10, %r15d
	CMPL	$0, %r15d
	JL L13
	JMP L120
L120:
	MOVL	$11, %eax
	MOVL	$11, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVQ	$10, 0(%rdi)
	MOVQ	$8, 0(%rsp)
	ADDQ	%rdi, 0(%rsp)
	MOVQ	0(%rsp), %rdi
	MOVL	$0, %edi
	JMP L157
L157:
	MOVQ	$0, %rbx
	JMP L116
L116:
	CMPL	$10, %ebx
	JL L114
	JMP L115
L114:
	MOVQ	0(%rsp), %rax
	MOVL	%ebx, %eax
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %eax
	CMPL	%eax, %ebx
	JL L117
	JMP L13
L117:
	CMPL	$0, %ebx
	JGE L118
	JMP L13
L118:
	MOVL	%ebx, %eax
	IMULL	$8, %eax
	MOVQ	0(%rsp), %rbp
	ADDQ	%rax, %rbp
	MOVL	$10, %eax
	MOVL	$10, %r15d
	CMPL	$0, %r15d
	JL L13
	JMP L119
L119:
	MOVL	$11, %eax
	MOVL	$11, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVL	$10, 0(%rdi)
	ADDQ	$4, %rdi
	MOVQ	%rdi, 0(%rbp)
	MOVL	%ebx, %esi
	ADDL	$1, %esi
	MOVL	%esi, %edi
	JMP L158
L158:
	MOVQ	%rsi, %rbx
	JMP L116
L115:
	MOVL	$0, %edi
	JMP L159
L159:
	MOVQ	$0, %rbx
	JMP L31
L31:
	CMPL	%r12d, %ebx
	JL L29
	JMP L30
L29:
	MOVL	$0, %edi
	JMP L160
L160:
	MOVQ	$0, %r10
	JMP L34
L34:
	CMPL	%r13d, %r10d
	JL L32
	JMP L33
L32:
	MOVL	%ebx, %edi
	ADDL	%r12d, %edi
	MOVL	%edi, %esi
	SUBL	$1, %esi
	MOVL	%r12d, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%r12d
	MOVL	%edx, %r9d
	MOVL	%r9d, %edi
	MOVL	%ebx, %esi
	ADDL	$1, %esi
	MOVL	%r12d, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%r12d
	MOVL	%edx, %ebp
	MOVL	%ebp, %edi
	MOVL	%r10d, %edi
	ADDL	%r13d, %edi
	MOVL	%edi, %esi
	SUBL	$1, %esi
	MOVL	%r13d, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%r13d
	MOVL	%edx, %r11d
	MOVL	%r11d, %edi
	MOVL	%r10d, %edi
	ADDL	$1, %edi
	MOVL	%r13d, %esi
	MOVL	%edi, %eax
	CLTD
	IDIVL	%r13d
	MOVL	%edx, %r8d
	MOVL	%r8d, %edi
	MOVQ	8(%rsp), %rdi
	MOVL	%r9d, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %r9d
	JL L110
	JMP L13
L110:
	CMPL	$0, %r9d
	JGE L111
	JMP L13
L111:
	MOVL	%r9d, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r11d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r11d
	JL L112
	JMP L13
L112:
	CMPL	$0, %r11d
	JGE L113
	JMP L13
L113:
	MOVL	%r11d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %ecx
	MOVQ	8(%rsp), %rdi
	MOVL	%r9d, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %r9d
	JL L106
	JMP L13
L106:
	CMPL	$0, %r9d
	JGE L107
	JMP L13
L107:
	MOVL	%r9d, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L108
	JMP L13
L108:
	CMPL	$0, %r10d
	JGE L109
	JMP L13
L109:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	ADDL	%edi, %ecx
	MOVQ	8(%rsp), %rdi
	MOVL	%r9d, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %r9d
	JL L102
	JMP L13
L102:
	CMPL	$0, %r9d
	JGE L103
	JMP L13
L103:
	MOVL	%r9d, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r8d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r8d
	JL L104
	JMP L13
L104:
	CMPL	$0, %r8d
	JGE L105
	JMP L13
L105:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%ecx, %esi
	ADDL	%edi, %esi
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L98
	JMP L13
L98:
	CMPL	$0, %ebx
	JGE L99
	JMP L13
L99:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rcx
	MOVL	%r11d, %edi
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVL	-4(%rcx), %edi
	CMPL	%edi, %r11d
	JL L100
	JMP L13
L100:
	CMPL	$0, %r11d
	JGE L101
	JMP L13
L101:
	MOVL	%r11d, %edi
	IMULL	$4, %edi
	ADDQ	%rcx, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%esi, %ecx
	ADDL	%edi, %ecx
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L94
	JMP L13
L94:
	CMPL	$0, %ebx
	JGE L95
	JMP L13
L95:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r8d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r8d
	JL L96
	JMP L13
L96:
	CMPL	$0, %r8d
	JGE L97
	JMP L13
L97:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	ADDL	%edi, %ecx
	MOVQ	8(%rsp), %rdi
	MOVL	%ebp, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebp
	JL L90
	JMP L13
L90:
	CMPL	$0, %ebp
	JGE L91
	JMP L13
L91:
	MOVL	%ebp, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r11d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r11d
	JL L92
	JMP L13
L92:
	CMPL	$0, %r11d
	JGE L93
	JMP L13
L93:
	MOVL	%r11d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%ecx, %esi
	ADDL	%edi, %esi
	MOVQ	8(%rsp), %rdi
	MOVL	%ebp, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebp
	JL L86
	JMP L13
L86:
	CMPL	$0, %ebp
	JGE L87
	JMP L13
L87:
	MOVL	%ebp, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rcx
	MOVL	%r10d, %edi
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVL	-4(%rcx), %edi
	CMPL	%edi, %r10d
	JL L88
	JMP L13
L88:
	CMPL	$0, %r10d
	JGE L89
	JMP L13
L89:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rcx, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%esi, %ecx
	ADDL	%edi, %ecx
	MOVQ	8(%rsp), %rdi
	MOVL	%ebp, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebp
	JL L82
	JMP L13
L82:
	CMPL	$0, %ebp
	JGE L83
	JMP L13
L83:
	MOVL	%ebp, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r8d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r8d
	JL L84
	JMP L13
L84:
	CMPL	$0, %r8d
	JGE L85
	JMP L13
L85:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	ADDL	%edi, %ecx
	MOVL	%ecx, %edi
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L39
	JMP L13
L39:
	CMPL	$0, %ebx
	JGE L40
	JMP L13
L40:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L41
	JMP L13
L41:
	CMPL	$0, %r10d
	JGE L42
	JMP L13
L42:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	CMPL	$0, %edi
	JE L37
	JMP L38
L37:
	CMPL	$3, %ecx
	JE L35
	JMP L36
L38:
	JMP L36
L35:
	MOVQ	0(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L77
	JMP L13
L77:
	CMPL	$0, %ebx
	JGE L78
	JMP L13
L78:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	0(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L79
	JMP L13
L79:
	CMPL	$0, %r10d
	JGE L80
	JMP L13
L80:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	$1, 0(%rdi)
	JMP L81
L36:
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L47
	JMP L13
L47:
	CMPL	$0, %ebx
	JGE L48
	JMP L13
L48:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L49
	JMP L13
L49:
	CMPL	$0, %r10d
	JGE L50
	JMP L13
L50:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	CMPL	$1, %edi
	JE L45
	JMP L46
L45:
	CMPL	$2, %ecx
	JL L43
	JMP L44
L46:
	JMP L44
L43:
	MOVQ	0(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L72
	JMP L13
L72:
	CMPL	$0, %ebx
	JGE L73
	JMP L13
L73:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	0(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L74
	JMP L13
L74:
	CMPL	$0, %r10d
	JGE L75
	JMP L13
L75:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	$0, 0(%rdi)
	JMP L76
L44:
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L55
	JMP L13
L55:
	CMPL	$0, %ebx
	JGE L56
	JMP L13
L56:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L57
	JMP L13
L57:
	CMPL	$0, %r10d
	JGE L58
	JMP L13
L58:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	CMPL	$1, %edi
	JE L53
	JMP L54
L53:
	CMPL	$3, %ecx
	JG L51
	JMP L52
L54:
	JMP L52
L51:
	MOVQ	0(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L67
	JMP L13
L67:
	CMPL	$0, %ebx
	JGE L68
	JMP L13
L68:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	0(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L69
	JMP L13
L69:
	CMPL	$0, %r10d
	JGE L70
	JMP L13
L70:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	$0, 0(%rdi)
	JMP L71
L52:
	MOVQ	0(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L59
	JMP L13
L59:
	CMPL	$0, %ebx
	JGE L60
	JMP L13
L60:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	0(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L61
	JMP L13
L61:
	CMPL	$0, %r10d
	JGE L62
	JMP L13
L62:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	MOVQ	%rsi, %rcx
	ADDQ	%rdi, %rcx
	MOVQ	8(%rsp), %rdi
	MOVL	%ebx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ebx
	JL L63
	JMP L13
L63:
	CMPL	$0, %ebx
	JGE L64
	JMP L13
L64:
	MOVL	%ebx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r10d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r10d
	JL L65
	JMP L13
L65:
	CMPL	$0, %r10d
	JGE L66
	JMP L13
L66:
	MOVL	%r10d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%edi, 0(%rcx)
	JMP L71
L71:
	JMP L76
L76:
	JMP L81
L81:
	MOVL	%r10d, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L161
L161:
	MOVQ	%rdi, %r10
	JMP L34
L33:
	MOVL	%ebx, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L162
L162:
	MOVQ	%rdi, %rbx
	JMP L31
L30:
	MOVL	$0, %edi
	JMP L163
L163:
	MOVQ	$0, %rcx
	JMP L17
L17:
	CMPL	%r12d, %ecx
	JL L15
	JMP L16
L15:
	MOVL	$0, %edi
	JMP L164
L164:
	MOVQ	$0, %r8
	JMP L20
L20:
	CMPL	%r13d, %r8d
	JL L18
	JMP L19
L18:
	MOVQ	8(%rsp), %rdi
	MOVL	%ecx, %edi
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ecx
	JL L21
	JMP L13
L21:
	CMPL	$0, %ecx
	JGE L22
	JMP L13
L22:
	MOVL	%ecx, %edi
	IMULL	$8, %edi
	MOVQ	8(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r8d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r8d
	JL L23
	JMP L13
L23:
	CMPL	$0, %r8d
	JGE L24
	JMP L13
L24:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	MOVQ	%rsi, %rdx
	ADDQ	%rdi, %rdx
	MOVQ	0(%rsp), %rdi
	MOVL	%ecx, %edi
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %r15
	MOVL	-8(%r15), %edi
	CMPL	%edi, %ecx
	JL L25
	JMP L13
L25:
	CMPL	$0, %ecx
	JGE L26
	JMP L13
L26:
	MOVL	%ecx, %edi
	IMULL	$8, %edi
	MOVQ	0(%rsp), %r15
	ADDQ	%r15, %rdi
	MOVQ	0(%rdi), %rsi
	MOVL	%r8d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %r8d
	JL L27
	JMP L13
L27:
	CMPL	$0, %r8d
	JGE L28
	JMP L13
L28:
	MOVL	%r8d, %edi
	IMULL	$4, %edi
	ADDQ	%rsi, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%edi, 0(%rdx)
	MOVL	%r8d, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L165
L165:
	MOVQ	%rdi, %r8
	JMP L20
L19:
	MOVL	%ecx, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L166
L166:
	MOVQ	%rdi, %rcx
	JMP L17
L16:
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L13:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L122:
	MOVL	$10, %eax
	MOVL	$10, %r15d
	CMPL	$0, %r15d
	JL L121
	JMP L152
L152:
	MOVL	$11, %eax
	MOVL	$11, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	$10, 0(%rax)
	MOVQ	$8, %r12
	ADDQ	%rax, %r12
	MOVQ	%r12, %rax
	MOVL	$0, %eax
	JMP L167
L167:
	MOVQ	$0, %rbp
	JMP L148
L148:
	CMPL	$10, %ebp
	JL L146
	JMP L147
L146:
	MOVQ	%r12, %rax
	MOVL	%ebp, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	CMPL	%eax, %ebp
	JL L149
	JMP L121
L149:
	CMPL	$0, %ebp
	JGE L150
	JMP L121
L150:
	MOVL	%ebp, %eax
	IMULL	$8, %eax
	MOVQ	%r12, %rbx
	ADDQ	%rax, %rbx
	MOVL	$10, %eax
	MOVL	$10, %r15d
	CMPL	$0, %r15d
	JL L121
	JMP L151
L151:
	MOVL	$11, %eax
	MOVL	$11, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	$10, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%rbx)
	MOVL	%ebp, %eax
	ADDL	$1, %eax
	MOVL	%eax, %edi
	JMP L168
L168:
	MOVQ	%rax, %rbp
	JMP L148
L147:
	MOVQ	%r12, %rax
	MOVL	$0, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L142
	JMP L121
L142:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L143
	JMP L121
L143:
	MOVL	$0, %eax
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L144
	JMP L121
L144:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L145
	JMP L121
L145:
	MOVL	$4, %eax
	MOVQ	%rdi, %rax
	ADDQ	$4, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%r12, %rax
	MOVL	$1, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L138
	JMP L121
L138:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L139
	JMP L121
L139:
	MOVL	$8, %eax
	MOVQ	%r12, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$2, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L140
	JMP L121
L140:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L141
	JMP L121
L141:
	MOVL	$8, %eax
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%r12, %rax
	MOVL	$2, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L134
	JMP L121
L134:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L135
	JMP L121
L135:
	MOVL	$16, %eax
	MOVQ	%r12, %rax
	ADDQ	$16, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$0, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L136
	JMP L121
L136:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L137
	JMP L121
L137:
	MOVL	$0, %eax
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%r12, %rax
	MOVL	$2, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L130
	JMP L121
L130:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L131
	JMP L121
L131:
	MOVL	$16, %eax
	MOVQ	%r12, %rax
	ADDQ	$16, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L132
	JMP L121
L132:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L133
	JMP L121
L133:
	MOVL	$4, %eax
	MOVQ	%rdi, %rax
	ADDQ	$4, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%r12, %rax
	MOVL	$2, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-8(%r12), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L126
	JMP L121
L126:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L127
	JMP L121
L127:
	MOVL	$16, %eax
	MOVQ	%r12, %rax
	ADDQ	$16, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$2, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L128
	JMP L121
L128:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L129
	JMP L121
L129:
	MOVL	$8, %eax
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVL	$1, 0(%rax)
	MOVL	$0, %eax
	JMP L169
L169:
	MOVQ	$0, %rbx
	JMP L125
L125:
	CMPL	$100, %ebx
	JL L123
	JMP L124
L123:
	MOVQ	%r12, %rdi
	MOVL	$10, %esi
	MOVL	$10, %edx
	ANDL	$0, %eax
	CALL	_c0_iter
	MOVL	%ebx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %edi
	JMP L170
L170:
	MOVQ	%rax, %rbx
	JMP L125
L124:
	MOVQ	%r12, %rdi
	MOVL	$10, %esi
	MOVL	$10, %edx
	ANDL	$0, %eax
	CALL	_c0_sum
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L121:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
