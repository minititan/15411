	.file	"../tests1/vermont-binsearch.l4"
	.text
	.global	_c0_binsearch
_c0_binsearch:
	SUBQ	$8, %rsp
L2:
	MOVL	%edi, %r9d
	MOVQ	%rsi, %r10
	MOVL	%edx, %esi
	MOVL	$0, %edi
	MOVL	%esi, %edi
	JMP L31
L31:
	MOVQ	%rsi, %r8
	MOVQ	$0, %rcx
	JMP L5
L5:
	CMPL	%r8d, %ecx
	JL L3
	JMP L4
L3:
	MOVL	%r8d, %edi
	SUBL	%ecx, %edi
	MOVL	$2, %esi
	MOVL	%edi, %eax
	CLTD
	IDIVL	%esi
	MOVL	%eax, %edi
	MOVL	%ecx, %esi
	ADDL	%edi, %esi
	MOVL	%esi, %edi
	MOVQ	%r10, %rdi
	MOVL	%esi, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %esi
	JL L8
	JMP L1
L8:
	CMPL	$0, %esi
	JGE L9
	JMP L1
L9:
	MOVL	%esi, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	CMPL	%r9d, %edi
	JL L6
	JMP L7
L6:
	MOVL	%esi, %edi
	ADDL	$1, %edi
	MOVL	%edi, %esi
	JMP L32
L32:
	MOVQ	%r8, %rsi
	MOVQ	%rdi, %rdi
	JMP L15
L7:
	MOVQ	%r10, %rdi
	MOVL	%esi, %edi
	TESTQ	%r10, %r10
	jz	_c0_call_raise
	MOVL	-4(%r10), %edi
	CMPL	%edi, %esi
	JL L12
	JMP L1
L12:
	CMPL	$0, %esi
	JGE L13
	JMP L1
L13:
	MOVL	%esi, %edi
	IMULL	$4, %edi
	ADDQ	%r10, %rdi
	MOVL	0(%rdi), %edi
	CMPL	%r9d, %edi
	JG L10
	JMP L11
L10:
	MOVL	%esi, %edi
	JMP L14
L11:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
L14:
	JMP L33
L33:
	MOVQ	%rsi, %rsi
	MOVQ	%rcx, %rdi
	JMP L15
L15:
	JMP L34
L34:
	MOVQ	%rsi, %r8
	MOVQ	%rdi, %rcx
	JMP L5
L4:
	MOVL	$-1, %eax
	MOVL	$-1, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L17:
	MOVL	$6, %eax
	MOVL	$6, %r15d
	CMPL	$0, %r15d
	JL L16
	JMP L30
L30:
	MOVL	$7, %eax
	MOVL	$7, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	$6, 0(%rax)
	MOVQ	$4, %rsi
	ADDQ	%rax, %rsi
	MOVQ	%rsi, %rax
	MOVQ	%rsi, %rax
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$0, %r15d
	CMPL	%eax, %r15d
	JL L28
	JMP L16
L28:
	MOVL	$0, %r15d
	CMPL	$0, %r15d
	JGE L29
	JMP L16
L29:
	MOVL	$0, %eax
	MOVQ	%rsi, %rax
	ADDQ	$0, %rax
	MOVL	$0, 0(%rax)
	MOVQ	%rsi, %rax
	MOVL	$1, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$1, %r15d
	CMPL	%eax, %r15d
	JL L26
	JMP L16
L26:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L27
	JMP L16
L27:
	MOVL	$4, %eax
	MOVQ	%rsi, %rax
	ADDQ	$4, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rsi, %rax
	MOVL	$2, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$2, %r15d
	CMPL	%eax, %r15d
	JL L24
	JMP L16
L24:
	MOVL	$2, %r15d
	CMPL	$0, %r15d
	JGE L25
	JMP L16
L25:
	MOVL	$8, %eax
	MOVQ	%rsi, %rax
	ADDQ	$8, %rax
	MOVL	$2, 0(%rax)
	MOVQ	%rsi, %rax
	MOVL	$3, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$3, %r15d
	CMPL	%eax, %r15d
	JL L22
	JMP L16
L22:
	MOVL	$3, %r15d
	CMPL	$0, %r15d
	JGE L23
	JMP L16
L23:
	MOVL	$12, %eax
	MOVQ	%rsi, %rax
	ADDQ	$12, %rax
	MOVL	$3, 0(%rax)
	MOVQ	%rsi, %rax
	MOVL	$4, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$4, %r15d
	CMPL	%eax, %r15d
	JL L20
	JMP L16
L20:
	MOVL	$4, %r15d
	CMPL	$0, %r15d
	JGE L21
	JMP L16
L21:
	MOVL	$16, %eax
	MOVQ	%rsi, %rax
	ADDQ	$16, %rax
	MOVL	$4, 0(%rax)
	MOVQ	%rsi, %rax
	MOVL	$5, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	MOVL	$5, %r15d
	CMPL	%eax, %r15d
	JL L18
	JMP L16
L18:
	MOVL	$5, %r15d
	CMPL	$0, %r15d
	JGE L19
	JMP L16
L19:
	MOVL	$20, %eax
	MOVQ	%rsi, %rax
	ADDQ	$20, %rax
	MOVL	$5, 0(%rax)
	MOVL	$3, %edi
	MOVQ	%rsi, %rsi
	MOVL	$6, %edx
	ANDL	$0, %eax
	CALL	_c0_binsearch
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L16:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
