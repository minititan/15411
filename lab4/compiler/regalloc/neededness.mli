(* neededness.mli *)

open Regalloc_util

type define = Liveness_gen.define
type needed = IntSet.t

val intset_of_define : define -> IntSet.t
val intset_of_use : Liveness_gen.use -> IntSet.t
val intset_of_live : Liveness_gen.live -> IntSet.t
val intset_of_nec : Liveness_gen.necessary -> IntSet.t
val intset_of_needed : needed -> IntSet.t

val propagate_neededness : Liveness_gen.predicates list -> define list * needed list

val get_needed_map : Liveness_gen.numbered_commands * define list * needed list -> Ir.instr IntMap.t

val get_new_blocks : Block.block list * Liveness_gen.line IntMap.t Label.Map.t * Ir.instr IntMap.t -> Block.block list
