(* Module to implement liveness generation *)

open Regalloc_util

type live = IntSet.t
type define = IntSet.t
type use = IntSet.t
type successor = IntSet.t
type necessary = IntSet.t
type line = int

type predicates = line * define * use * necessary * successor
type numbered_commands = (line * Ir.instr) list

val intset_of_define : define -> IntSet.t
val intset_of_use : use -> IntSet.t
val intset_of_live : live -> IntSet.t

val label_map_from_instructions : numbered_commands -> int Label.Map.t

val debug : bool

val seed : numbered_commands -> predicates list

val predecessors_of_pred_list : predicates list -> successor IntMap.t

val propagate_liveness : predicates list -> (define list) * (live list) * IntSet.t

val drop_size : Ir.operand -> Ir.simpoper

val align_lists : 'a list * 'b list -> ('a * 'b) list

(* line number from numbered commands maps to the
 * label/line number of the block where it came from
 *)
val instruction_line_list : Block.block list -> numbered_commands * line IntMap.t Label.Map.t

(* end result is temp -> temp set map of interferences *)
val gen_interference : (define list * live list * IntSet.t) -> IntSet.t IntMap.t

module type PRINT = 
  sig
    val pp_livelist : IntSet.t list -> string 
  end

module Print : PRINT
