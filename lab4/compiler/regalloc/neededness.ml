(* neededness.ml *)

open Regalloc_util
module L = Liveness_gen
module IR = Ir
module T = Tree

type define = L.define
type needed = IntSet.t

let (|>) x f = f x
let (@@) f x = f x

(* based on propagate_var_liveness *)
let rec propagate_var_neededness predecessors var pred_map ln =
  let preds = IntMap.find ln predecessors in
  let (d,u,nec,s,needed) = IntMap.find ln pred_map in
  (* var not defined here -> var needed here *)
  (* var defined here -> everything used becomes needed *)
  if IntSet.mem var needed then
    pred_map
  else if (IntSet.mem var d && not (IntSet.mem var u)) then (* done propagating *)
    let needed' = IntSet.union u needed in
    let pred_map' = IntMap.add ln (d,u,nec,s,needed') pred_map in
    let preds = IntMap.find ln predecessors in
    (* let pred_map'' = IntSet.fold (fun new_var pm -> propagate_var_neededness predecessors new_var pm ln) u pred_map' in *)
    (IntSet.fold (fun var pm -> IntSet.fold (fun pred pm' -> propagate_var_neededness predecessors var pm' pred) preds pm) u pred_map')
  else 
    let needed' = IntSet.add var needed in
    let pred_map' = IntMap.add ln (d,u,nec,s,needed') pred_map in
    IntSet.fold (fun ln' pm -> propagate_var_neededness predecessors var pm ln') preds pred_map'

(* based on process_line_liveness *)
let rec process_line_neededness predecessors pred_map ln =
  (* let _ = Printf.printf "Processing line: %d\n" ln in *)
  let (d,u,nec,s,needed) = IntMap.find ln pred_map in
  let new_vars = IntSet.diff nec needed in
  (* let new_need_used = if IntSet.inter d needed <> IntSet.empty then u else IntSet.empty in *)
  (* let new_vars = IntSet.union new_needed new_need_used in *)
  if new_vars = IntSet.empty then
    pred_map
  else
    let pred_map' = IntSet.fold (fun new_var pm -> propagate_var_neededness predecessors new_var pm ln) new_vars pred_map in
    let preds = IntMap.find ln predecessors in
    IntSet.fold (fun pred pm' -> process_line_neededness predecessors pm' pred) preds pred_map'

let debug = false

let intset_of_define x = x
let intset_of_use x = x
let intset_of_live x = x
let intset_of_nec x = x
let intset_of_needed x = x

let propagated_to_list def_list use_list nec_list needed_list =
  let fixup = List.map (fun (i,sl) -> string_of_int i ^ " -> {" ^ (String.concat ", " sl) ^ "}") in
  let def_list_s =
    def_list
    |> (List.map intset_of_define)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let use_list_s =
    use_list
    |> (List.map intset_of_use)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let nec_list_s =
    nec_list
    |> (List.map intset_of_needed)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let needed_list_s =
    needed_list
    |> (List.map intset_of_needed)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  "Def list:\n" ^ def_list_s ^ "\nUse list:\n" ^ use_list_s ^ "\nNec list:\n" ^ nec_list_s ^ "\nNeeded list:\n" ^ needed_list_s

(* Liveness_gen.predicates list -> needed list *)
(* based on propagate_liveness *)
let propagate_neededness (pred_list : L.predicates list) =
  let def_list = List.map (fun (_,d,_,_,_) -> d) pred_list in
  let predecessors = L.predecessors_of_pred_list pred_list in
  let predicates_to_needed_map im (l,d,u,n,s) = IntMap.add l (d,u,n,s,IntSet.empty) im in
  let pred_map_init = List.fold_left predicates_to_needed_map IntMap.empty pred_list in
  let lines = List.rev (0 -- (List.length pred_list - 1)) in
  let neededness_information = List.fold_left (process_line_neededness predecessors) pred_map_init lines in
  (* IntMap.bindings returns in ascending order by key. *)
  let needed_list = List.map (fun (ln,(d,u,nec,s,needed)) -> needed) (IntMap.bindings neededness_information) in
  let () =
    if debug then
      let nec_list = List.map (fun (_,_,_,n,_) -> n) pred_list in
      let use_list = List.map (fun (_,_,u,_,_) -> u) pred_list in
      Printf.printf "%s\n\n" (propagated_to_list def_list use_list nec_list needed_list)
    else () in
  (def_list,needed_list)

exception InvalidGetNeededMapArgs
exception NotMatching

(* Liveness_gen.numbered_commands * needed list *)
let get_needed_map (numbered_ir,def_list,needed_list) =
  let zipped = L.align_lists (def_list, needed_list) in
  let needed_bool_list =
    enumerate @@
    (* this line is needed if anything it defines is needed on the next line *)
    List.map (fun (def,needed) -> (IntSet.inter def needed) <> IntSet.empty) zipped
    @ [true] (* last line always needed *)
  in

  List.fold_left2
  (fun (m : 'a IntMap.t) ((i1 : int),(ir1 : IR.instr)) (i2,(inst_needed : bool)) ->
    if i1 <> i2 then raise NotMatching else
    let has_effect = begin match ir1 with
      | IR.BINOP (T.DIV, _, _, _) | IR.BINOP (T.MOD, _, _, _) -> true
      | IR.BINOP _ -> false
      | IR.MOV (d, s) ->
        let nec_op o = begin match L.drop_size o with
          | IR.DEREF _ | IR.ARGTOP _ | IR.ARGBOTTOM _ -> true
          | IR.IMM _ | IR.REG _ | IR.TEMP _ -> false
        end 
        in  
        begin match (nec_op d, nec_op s) with
          | (true,   true) -> true
          | (true,  false) -> true
          | (false,  true) -> true
          | (false, false) -> false
        end
      | IR.IF _ -> true
      | IR.RET -> true
      | IR.CALL _ -> true (* check if all of these make sense *)
      | IR.DIRECTIVE _ | IR.COMMENT _ -> false
      | IR.GOTO _ -> true
      | IR.CHECKDEREF _ -> true
      | IR.UNOP (_, _, _) -> false
      | IR.LABEL _ -> true (* how else do we jump? *)
      | IR.NOP -> false
    end
    in
    if has_effect || inst_needed then
      IntMap.add i1 ir1 m
    else
      IntMap.add i1 IR.NOP m
  )
  IntMap.empty
  numbered_ir needed_bool_list

let get_new_blocks (bl,line_map,needed_map) =
  let update_ir l ir =
    let ir_map = Label.Map.find l line_map in
    List.mapi (fun i (ir_inst) -> IntMap.find (IntMap.find i ir_map) needed_map) ir
  in
  List.map (fun (l,a,ir) -> (l,a,update_ir l ir)) bl
