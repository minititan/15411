open Ast
module S = Symbol
module T = TypedAst
module SM = Symbol.Map
module SS = Symbol.Set

let rec get_type_str = function
  | T.Bool -> "Bool"
  | T.Int -> "Int"
  | T.Void -> "Void"
  | T.PrId id -> "Type " ^ Symbol.name id
  | T.Struct id -> "Struct " ^ Symbol.name id
  | T.Pointer pt -> "Pointer (" ^ get_type_str pt ^ ")"
  | T.AnyPointer -> "AnyPointer"
  | T.Array pt -> "Array (" ^ get_type_str pt ^ ")"

let rec type_equal t1 t2 = match (t1, t2) with
  | (T.Bool, T.Bool) -> true
  | (T.Int,  T.Int)  -> true
  | (T.Void, T.Void) -> true
  | (T.PrId i1, T.PrId i2) -> i1 = i2
  | (T.Struct i1, T.Struct i2) -> i1 = i2
  | (T.Pointer p1, T.Pointer p2) -> type_equal p1 p2
  | (T.Pointer _, T.AnyPointer) | (T.AnyPointer, T.Pointer _)
  | (T.AnyPointer, T.AnyPointer) -> true
  | (T.Array t1, T.Array t2) -> type_equal t1 t2
  | _ -> false

let rec trans_type = function
  | Bool -> T.Bool
  | Int -> T.Int
  | Void -> T.Void
  | PrId i -> T.PrId i
  | Struct s -> T.Struct s
  | Pointer t -> T.Pointer (trans_type t)
  | Array t -> T.Array (trans_type t)

let called_tracker =
object 
  val mutable called_set = SS.empty

  method add_call fname = 
    called_set <- SS.add fname called_set

  method get_call_set = called_set

end

let raise_error msg = (let _ = ErrorMsg.error None msg in raise ErrorMsg.Error)

(* to keep track of called and defined functions and typedefs. Is mutable. *)
class globalContext =
object(this)
  val def_set = SS.empty
  val dec_set = SS.empty
  val typemap = SM.empty (* mapping from ident to type for typedef *)
  val fmap = SM.empty (* mapping from function to (return type, argument type list) *)
  val structmap = SM.empty (* mapping from struct ident to (field list, name map) *)

  method get_def_set = def_set

  method get_dec_set = dec_set

  method add_def name = 
    {< def_set = SS.add name def_set >}

  method is_typedef id = 
    SM.mem id typemap

  method get_functype name = 
    match SM.find' name fmap with
    | Some p -> p
    | None -> raise_error "function name not in context"

  method get_structinfo name : (T.primtype * Ast.ident) list =
    match SM.find' name structmap with
    | Some l -> l
    | None -> raise_error ("struct " ^ Symbol.name name ^ " used before definition.")

  (* this function scans over the field list every time it is called.
     If we had implemented this with a map, it would be O(log n) instead
     of O(n), but I think O(n) is acceptable in the number of fields for a struct
     as... how many fields can there be? if this is too slow, i can store a second
     map that contains mappings from name to type instead of just a list of (type, name).
   *)
  method get_type_from_struct_field struct_name struct_field =
    let field_list = this#get_structinfo struct_name in
    match List.filter (fun (_, a) -> a = struct_field) field_list with
    | [] -> raise_error ("Name " ^ Symbol.name struct_field ^ " not in struct " ^ Symbol.name struct_name ^ ".")
    | (t,a)::[] -> t
    (* this error should be impossible *)
    | _ -> raise_error ("Name " ^ Symbol.name struct_field ^ " found more than once in " ^ Symbol.name struct_name ^ ".")

  (* makes a set of idents while performing static checks *)
  (* main function is to check duplicate names and already typedef'd names *)
  method check_duplicates ~is_func (name_list : Symbol.symbol list) =
    (* let _ = Printf.printf "name_list: (%s)\n" (String.concat "," (List.map Symbol.name name_list)) in *)
    let combine seen name =
      if SS.mem name seen then
        raise_error "Same name arg"
      else if is_func && (this#is_typedef name) then
        raise_error "Arg name typedefed"
      else
        SS.add name seen
    in
    List.fold_left combine SS.empty name_list

  method add_structdecl struct_name = {< dec_set = SS.add struct_name dec_set >}

  (* add struct definition to context *)
  method add_structdefn struct_name (field_list : field list) =
  (match SM.find' struct_name structmap with
    | None ->
        let get_real_type (t, i) =
          (match this#get_type t with
            | T.Void -> raise_error "struct field void"
            | real_type -> (real_type, i)
          )
        in
        (* get real types and check them in fields *)
        let fl_real_types = List.map get_real_type field_list in
        (* check names in fields *)

        let rec is_struct = function
        | T.Struct _ -> true
        (* | T.Pointer p -> is_struct p *)
        | _ -> false
        in

        let rec get_struct = function
        | T.Struct s -> s
        (* | T.Pointer p -> get_struct p *)
        | _ -> raise_error "Non-struct found in get_struct."
        in

        let (|>) x f = f x in

        let structs_used =
          List.map (fun (t, i) -> t) fl_real_types
          |> (List.filter is_struct)
          |> (List.map get_struct)
        in

        let () =
          if List.mem struct_name structs_used then
            raise_error "Cannot define recursive structures."
          else
            ()
        in

        let names = List.map (fun (_, a) -> a) field_list in
        let _ = this#check_duplicates ~is_func:false names in
        (match SM.find' struct_name structmap with (* note: we don't actually have to add it to the dec_set *)
          | None -> {< structmap = SM.add struct_name fl_real_types structmap; dec_set = SS.add struct_name dec_set >}
          | Some _ -> raise_error ("struct " ^ Symbol.name struct_name ^ " previous defined.")
        )
    | Some _ -> raise_error ("struct " ^ Symbol.name struct_name ^ " previously defined.")
  )

  (* adding a function declaration to scope. If previous declaration exists
    check for type compatibility *)
  method add_fundecl fname ptype ptyplist =

    (* check for typedef names *)
    match SM.find' fname typemap with
    | None -> 
        let rtype = this#get_type ptype in

        let tplist = List.map (fun (t, _) -> match this#get_type t with
                                          | T.Void -> raise_error "Function argument void!"
                                          | x -> x ) ptyplist in
        
        let _ = this#check_duplicates ~is_func:true (List.map (fun (_, a) -> a) ptyplist) in

        let _ = List.iter
          (fun t ->
            if this#is_small_type t then
              ()
            else
              raise_error "Non small type in function parameter.")
          tplist
        in

        (match SM.find' fname fmap with
        | None -> {< fmap = SM.add fname (rtype, tplist) fmap; dec_set = SS.add fname dec_set >}

        | Some((ret_type, argtyp_list)) -> 
            if (ret_type = rtype &&
                (List.for_all2 (fun t1 t2 -> type_equal t1 t2) argtyp_list tplist))
            then {<>}

            else raise_error "function declaration does not match previous declaration"
          )

    | Some _ -> raise_error "function name taken by typedef"


  (* adds a function name to the context and also adds the argument
      identifiers as defined variables *)
  method add_fundefn fname ptype (plist : (Ast.primtype * Ast.ident) list) = 
    if (SS.mem fname def_set) then
      let s = "function " ^ Symbol.name fname ^ " previously defined" in raise_error s
    else
      this#add_fundecl fname ptype plist

  (* Checks if all the arguments of the funciton are correct and also returns the return type
    of the function *)
  method check_funccall fname (typexp_list : T.typexp list) = 
    match SM.find' fname fmap with
    | Some((ret_type, argtyp_list)) ->
        let () = List.iter2 (fun t (t', e) ->
          if not (type_equal t t') then
            (* let () = Printf.printf "type 1: %s\n type 2: %s\n" (get_type_str t) (get_type_str t') in *)
            raise_error ("function argument type mismatch for function " ^ Symbol.name fname)
          else ()) argtyp_list typexp_list in
        ret_type

    | None -> raise_error "Function call being checked not in context"


  (* k is ident and v is primtype *)
  method add_typedef k v = 
    match SM.find' k typemap with
    | Some(v') -> raise_error "ident previously used as typedef"
    | None -> 
        if (SM.mem k fmap) then raise_error "Typedef name already a function name"
      else
        (match v with

          (* if typedefing to another typedef we lookup the default type of that *)
          | T.PrId(id) -> (match SM.find' id typemap with
                          | None -> raise_error "typedefing ident to ident without previous typedef"
                          | Some(t) -> {< typemap = SM.add k t typemap >}
                        )

          | T.Void -> raise_error "Cannot typedef Void"

          | _ -> {< typemap = SM.add k v typemap >}
        )

  method find_idtype id = 
    match SM.find' id typemap with
    | Some t -> t
    | None -> raise_error "Ident not typedefed"

  (* helper method that given a type returns primitive type *)
  method get_type = function
  | PrId id -> this#find_idtype id
  | t -> trans_type t

  (* fake recursion, ugh *)
  method is_small_type' = function
  | T.Bool -> true
  | T.Int -> true
  | T.Void -> true
  | T.PrId id -> raise_error "impossible small type case"
  | T.Struct _ -> false
  | T.Pointer _ -> true
  | T.AnyPointer -> true
  | T.Array pt -> true
  method is_small_type = function
  | T.Bool -> true
  | T.Int -> true
  | T.Void -> true
  | T.PrId id -> this#is_small_type' (this#find_idtype id)
  | T.Struct _ -> false
  | T.Pointer _ -> true
  | T.AnyPointer -> true
  | T.Array pt -> true

end

(* keeps track of the magled function names *)
let mangled_name_holder = 
object
    val mutable names = SM.empty

    method add_mangled_name fname mangled_fname = 
      names <- SM.add fname mangled_fname names

    method get_mangled_name fname = 
      SM.find fname names

    method get_mangled_map = names

end

(* object that holds the context within a particular function *)
class progContext = fun init_set ->
object
  val live_funcs = init_set

  method add_arg (id : Symbol.symbol) = 
    {< live_funcs = SS.remove id live_funcs >}

  method add_func_back (fname : Symbol.symbol) = 
    {< live_funcs = SS.add fname live_funcs >}

  (* checks if a function call is valid under context *)
  method is_live fname = SS.mem fname live_funcs

  method var_declare (global_context : globalContext) id = 
    if global_context#is_typedef id then
      raise_error "Variable name used as typedef"
    else if (SS.mem id live_funcs) then
      (* remove from function context *)
      {< live_funcs = SS.remove id live_funcs >}
    else
      {<>}

  (* checks if variable is valid to use under context *)
  method valid_var_use (global_context : globalContext) id = 
    if (SS.mem id live_funcs || global_context#is_typedef id) then
      raise_error "Variable name used by function or typedef"
    else
      ()

end


(* returns the type of the resulting epressions and the type
    required by each subexp. If subexp type is None then it doesnt
    matter but has to be the same *)
let get_optype = function
  | PLUS -> (T.Int, Some(T.Int))
  | MINUS -> (T.Int, Some(T.Int))
  | TIMES -> (T.Int, Some(T.Int))
  | DIVIDEDBY -> (T.Int, Some(T.Int))
  | MODULO -> (T.Int, Some(T.Int))
  | BITAND -> (T.Int, Some(T.Int))
  | BITOR -> (T.Int, Some(T.Int))
  | SAR -> (T.Int, Some(T.Int))
  | SAL -> (T.Int, Some(T.Int))
  | XOR -> (T.Int, Some(T.Int))
  | LOGICAND -> (T.Bool, Some(T.Bool))
  | LOGICOR -> (T.Bool, Some(T.Bool))
  | EQUALS -> (T.Bool, None)
  | NOTEQUALS -> (T.Bool, None)
  | GREATER -> (T.Bool, Some(T.Int))
  | GREATEREQUAL -> (T.Bool, Some(T.Int))
  | LESS -> (T.Bool, Some(T.Int))
  | LESSEQUAL -> (T.Bool, Some(T.Int))
  | LOGICNOT -> (T.Bool, Some(T.Bool))
  | BITNOT -> (T.Int, Some(T.Int))
  | NEGATIVE -> (T.Int, Some(T.Int))

(* returns a typexp *)
let rec tc_exp globl_context prog_context dec exp =
  match exp with
  | True -> (T.Bool, T.True)
  | False -> (T.Bool, T.False)

  | Var id -> 
      let () = prog_context#valid_var_use globl_context id in
      (match SM.find' id dec with
      | Some t -> (t, T.Var(id))
      | None -> raise_error ("Undeclared variable " ^ S.name id ^ ".")
    )

  | ConstExp i -> (T.Int, T.ConstExp(i))

  | BinopExp (oper, inner_exp1, inner_exp2) ->
    let ((t1, e1), (t2, e2)) = (tc_exp globl_context prog_context dec inner_exp1, 
                                tc_exp globl_context prog_context dec inner_exp2) in
    let (res_type, sub_type) = get_optype oper in

    (match (t1, t2) with
    | (T.Bool, T.Bool) -> 
       (match sub_type with
        | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
        | Some sub_type' -> (match sub_type' with
          | T.Bool -> (res_type, T.BinopExp(oper, (t1, e1), (t2, e2)))
          | T.Int | (T.Struct _) | (T.Pointer _) | (T.Array _) | T.Void | T.PrId _ | T.AnyPointer ->
            raise_error ("Type mismatch: Expected Int got " ^ get_type_str sub_type')
        )
       )
    | (T.Int, T.Int) ->
       (match sub_type with 
        | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
        | Some sub_type' -> (match sub_type' with
          | T.Int -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
          | T.Bool | (T.Struct _) | (T.Pointer _) | (T.Array _) | T.Void | T.PrId _ | T.AnyPointer ->
            raise_error ("Type mismatch: Expected Bool got " ^ get_type_str sub_type')
        )
       )
    | (T.Pointer t1, T.Pointer t2) | (T.Array t1, T.Array t2) ->
      (match sub_type with
       | None ->
        if type_equal t1 t2 then
          (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
        else
          raise_error "Comparison of two incompatible definitely typed pointers."
       | _ -> raise_error "Cannot use pointers in non-equality comparison."
      )
    | (T.Pointer _, T.AnyPointer) | (T.AnyPointer, T.Pointer _) | (T.AnyPointer, T.AnyPointer) ->
      (match sub_type with
       | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
       | _ -> raise_error "Cannot use pointers in non-equality comparison."
      )

    | (T.Void, _) | (_, T.Void) -> raise_error "Voids cannot be used in binop expression."
    | (T.PrId _, _) | (_, T.PrId _) -> raise_error "PrId found in binop expression in tc_exp."
    | (T.Struct _, _) | (_, T.Struct _) -> raise_error "Struct found in binop expression in tc_exp."
    | _ -> raise_error "Type mismatch: Mix of types in binop expression"
    )

  | UnopExp (oper, inner_exp) -> 
    let ((res_type, sub_type), (exp_type, e)) = (get_optype oper, tc_exp globl_context prog_context dec inner_exp) in

    (match (sub_type, exp_type) with
    | (Some T.Bool, T.Bool) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some T.Int, T.Int) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some T.Bool, _) -> raise_error "Type mismatch: Unop expected Bool"
    | (Some T.Int, _) -> raise_error "Type mismatch: Unop expected Int"
    | (Some (T.Void | T.PrId _ | T.Struct _ | T.Pointer _ | T.Array _ | T.AnyPointer), _) ->
        raise_error "Invalid type found in UnopExp."
    | (None, _) -> raise_error "Equality comparison operator is not a Unop"
    )

  | Condition (e1, e2, e3) -> 
    let ((t1, te1), (t2, te2), (t3, te3)) =
      (tc_exp globl_context prog_context dec e1, 
       tc_exp globl_context prog_context dec e2, 
       tc_exp globl_context prog_context dec e3)
    in

    let is_small' = globl_context#is_small_type in

    if not (is_small' t1 && is_small' t2 && is_small' t3) then
      raise_error "Non small type(s) used in condition."
    else
      (match (t1) with
      | T.Bool ->
       (match (t2, t3) with
        | (T.Int, T.Int) -> (T.Int, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.Bool, T.Bool) -> (T.Bool, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.Pointer p1, T.Pointer p2) when type_equal p1 p2 ->
            (T.Pointer p1, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.Array p1, T.Array p2) when type_equal p1 p2 ->
            (T.Array p1, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.Pointer p, T.AnyPointer) -> (T.Pointer p, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.AnyPointer, T.Pointer p) -> (T.Pointer p, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.AnyPointer, T.AnyPointer) -> (T.AnyPointer, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.PrId i1, T.PrId i2) -> raise_error "PrId's found in tc_exp."
        | (T.Struct i1, T.Struct i2) when i1 = i2 -> (T.Struct i1, T.Condition((t1, te1), (t2, te2), (t3, te3)))
        | (T.Void, _) | (_, T.Void) -> raise_error "Ternary condition cannot have void branch"
        | _ -> raise_error "Type mismatch: Condition has multiple type paths"
       )
      | _ -> raise_error "Type mismatch: Condition check exp must have Bool type"
      )

  | Call (id, args) ->
      (* add function to set of called functions *)
      let () = called_tracker#add_call id in

      let tlist = List.map (fun e -> tc_exp globl_context prog_context dec e) args in

      (* arguments cannot be void *)
      let _ = List.iter (fun (t, ex) -> match t with
                                         | T.Void  -> (raise_error "Function parameter has void type")
                                         | _ -> ()) tlist in

      let t = if (prog_context#is_live id) then globl_context#check_funccall id tlist
              else (raise_error ("Function " ^ Symbol.name id ^ " not live")) in

      (t, T.Call(id, tlist))

  | Null -> (T.AnyPointer, T.Null)

  | FieldDeref (e, field_name) ->
    (match tc_exp globl_context prog_context dec e with
      | (T.Struct struct_name, e') ->
          let field_type = globl_context#get_type_from_struct_field struct_name field_name in
          (field_type, T.FieldDeref ((T.Struct struct_name, e'), field_name))
      | _ -> raise_error "Attempting to do field dereference on non-struct."
    )

  | Deref e ->
    (match tc_exp globl_context prog_context dec e with
      | (T.Pointer t, e') -> (t, T.Deref (T.Pointer t, e'))
      | _ -> raise_error "Type checker: cannot dereference a non-pointer."
    )

  | AllocCall ptype ->
    let ptype' = (match trans_type ptype with
      | T.Void -> raise_error "Alloc with type void not allowed!"
      | T.Struct s ->
        let _ = globl_context#get_structinfo s in
        T.Struct s
      | ptype' -> ptype')
    in
    (T.Pointer ptype', T.AllocCall ptype')

  | AllocArrCall (ptype, e) ->
    let ptype' = (match trans_type ptype with
      | T.Struct s ->
        let _ = globl_context#get_structinfo s in
        T.Struct s
      | ptype' -> ptype')
    in

    (match tc_exp globl_context prog_context dec e with
      | (T.Int, e') -> (T.Array ptype', T.AllocArrCall (ptype', (T.Int, e')))
      | _ -> raise_error "alloc_array element quantity must be an int"
    )

  | ArrIndex (array_location, array_offset) ->
    (match (tc_exp globl_context prog_context dec array_location,
            tc_exp globl_context prog_context dec array_offset) with
    (* drop case for now *) (* ((T.Array _, T.AllocArrCall _), _) -> raise_error "alloc_array not an lvalue." *)
     | (T.Array array_type, array_location'), (T.Int, array_offset') ->
        (array_type, T.ArrIndex ((T.Array array_type, array_location'), (T.Int, array_offset')))
     | ((T.Array _, _), _) -> raise_error "Can only index into an array with expression of type Int."
     | (_, (T.Int, _)) -> raise_error "Can only index into an array type."
     | _ -> raise_error "That's not an array and your index is not an Int. What on earth are you doing?"
  )


(* HELPERS *)
let dec_set dec =
  let l = List.map (fun (k,_) -> k) (SM.bindings dec) in
  List.fold_left (fun s e -> SS.add e s) SS.empty l

let check_def def x = 
  if SS.mem x def then () else
  (raise_error ("Variable " ^ S.name x ^ " used before defined in return."))

let rec used_exp : exp -> SS.t = function
  | Var i -> SS.singleton i
  | ConstExp _ -> SS.empty (* def, live *)
  | BinopExp (_, e1, e2) ->
    let live1 = used_exp e1
    and live2 = used_exp e2 in
    SS.union live1 live2
  | UnopExp (_, e) -> used_exp e
  | Condition (e1, e2, e3) ->
    let (live1, live2, live3) = (used_exp e1, used_exp e2, used_exp e3) in
    SS.union (SS.union live1 live2) live3
  | True -> SS.empty
  | False -> SS.empty
  | Call(id, arg_exps) -> List.fold_left (fun s e -> SS.union s (used_exp e)) SS.empty arg_exps
  | Null -> SS.empty
  | FieldDeref (e, _) -> used_exp e
  | Deref e -> used_exp e
  | AllocCall _ -> SS.empty
  | AllocArrCall (_, e) -> used_exp e
  | ArrIndex (e1, e2) -> SS.union (used_exp e1) (used_exp e2)

(* returns the identifier being assigned to and None if no ident *)
let rec is_valid_lvalue l = 
  match l with
  | Var id -> true
  | Deref l' -> is_valid_lvalue l'
  | FieldDeref (l', id) -> is_valid_lvalue l'
  | ArrIndex (l', e) -> is_valid_lvalue l'
  | _ -> false

(* recursively checks the ast. returns a tuple of
  (updated context, typed tree) *)
let rec tc_stm ret_type globl_context prog_context (dec, def, live) = function
  | Declare (ptype, id, s') ->
    let t = (globl_context#get_type ptype) in
    let rec is_void t = (match t with T.Void -> true | T.Pointer tp | T.Array tp -> is_void tp | _ -> false) in
    if is_void t then 
      raise_error "Variable cannot have void type"
    else
      if globl_context#is_small_type t then
        (match SM.find' id dec with
          | Some _ -> raise_error "variable already declared"
          | None -> 
              let new_prog_context = prog_context#var_declare globl_context id in
              let new_dec = SM.add id t dec in
              let (d, def', live', ts) = tc_stm ret_type globl_context new_prog_context (new_dec, def, live) s' in

              if (SS.mem id live') then raise_error "Variable live at declaration"
              else (new_dec, SS.remove id def', SS.remove id live', T.Declare(t, id, ts))
          )
      else
        raise_error "Cannot declare struct as local variable."

  (* isin tells us if a function witht the same name is called inside and hence the id should 
      not shadow the name *)
  | Assign (e1, isin, e2) ->
    if not (is_valid_lvalue e1) then
      raise_error "Not a valid lvalue."
    else
      (match e1 with
        (*  *l, l[e], l.f, or l->f *)
      | Var id ->
        let def' = SS.add id def in
        let live' = used_exp e2  in

        (match SM.find' id dec with
        | None -> raise_error "Variable being assigned to without declaration"
        | Some t1 -> 
            let temp_context = 
                if (isin) then prog_context#add_func_back id else prog_context
            in

            let (t2,_) as texp = tc_exp globl_context temp_context dec e2 in

            if globl_context#is_small_type t1 && globl_context#is_small_type t2 then
              if (type_equal t1 t2) then (dec, def', live', T.Assign((t1, T.Var id), texp))
              else 
                (match (t1, t2) with
                  | (T.Pointer _, T.AnyPointer) -> (dec, def', live', T.Assign((t1, T.Var id), texp))
                  | _ -> (raise_error "Types don't match for assignment.")
                )
                
            else
              raise_error "Non small types used in assign."
        )

      | FieldDeref _ | Deref _ | ArrIndex _ ->
        (* we don't track whether something is defined here, right? *)
        let live' = SS.union (used_exp e1) (used_exp e2) in
        let tc_exp' = tc_exp globl_context prog_context dec in
        (match (tc_exp' e1, tc_exp' e2) with
         ((t1, e1'), (t2, e2')) ->
          if (not (globl_context#is_small_type t1)) then
            raise_error "Cannot assign to struct directly."
          else if type_equal t1 t2 then
            (dec, def, live', T.Assign ((t1,e1'),(t2,e2')))
          else
            (match (t1, t2) with
              | (T.Pointer _, T.AnyPointer) -> (dec, def, live', T.Assign ((t1,e1'),(t2,e2')))
              | _ -> raise_error "Types do not match for assignment to struct field."
            )
            
        )
        (* Thank you cc0 for the lovely message. *)
      | _ -> raise_error "an lvalue l must be a variable or of the form *l, l[e], l.f, or l->f"
    )

  (* same as above case, could refactor this somehow *)
  | AssignOp (e1, isin, e2) ->
    if not (is_valid_lvalue e1) then
      raise_error "Not a valid lvalue."
    else
      (match e1 with
        (*  *l, l[e], l.f, or l->f *)
      | Var id ->
        let def' = SS.add id def in
        let live' = used_exp e2  in

        (match SM.find' id dec with
        | None -> raise_error "Variable being assigned to without declaration"
        | Some t1 -> 
            let temp_context = 
                if (isin) then prog_context#add_func_back id else prog_context
            in

            let (t2,_) as texp = tc_exp globl_context temp_context dec e2 in

            if globl_context#is_small_type t1 && globl_context#is_small_type t2 then
              if (type_equal t1 t2) then (dec, def', live', T.AssignOp((t1, T.Var id), texp))
              else 
                (match (t1, t2) with
                  | (T.Pointer _, T.AnyPointer) -> (dec, def', live', T.AssignOp((t1, T.Var id), texp))
                  | _ -> (raise_error "Types don't match for assignment.")
                )
                
            else
              raise_error "Non small types used in assignOp."
        )

      | FieldDeref _ | Deref _ | ArrIndex _ ->
        (* we don't track whether something is defined here, right? *)
        let live' = SS.union (used_exp e1) (used_exp e2) in
        let tc_exp' = tc_exp globl_context prog_context dec in
        (match (tc_exp' e1, tc_exp' e2) with
         ((t1, e1'), (t2, e2')) ->
          if (not (globl_context#is_small_type t1)) then
            raise_error "Cannot assign to struct directly."
          else if type_equal t1 t2 then
            (dec, def, live', T.AssignOp ((t1,e1'),(t2,e2')))
          else
            (match (t1, t2) with
              | (T.Pointer _, T.AnyPointer) -> (dec, def, live', T.AssignOp ((t1,e1'),(t2,e2')))
              | _ -> raise_error "Types do not match for assignment to struct field."
            )
            
        )
        (* Thank you cc0 for the lovely message. *)
      | _ -> raise_error "an lvalue l must be a variable or of the form *l, l[e], l.f, or l->f"
    )

  | If (exp, s1, s2) ->
      let texp = tc_exp globl_context prog_context dec exp in
      (match (fst texp) with
      | T.Bool -> 
          let (_, def1, live1, ts1) = tc_stm ret_type globl_context prog_context (dec, def, live) s1 in
          let (_, def2, live2, ts2) = tc_stm ret_type globl_context prog_context (dec, def, live) s2 in
          let used = used_exp exp in
          let def' = SS.inter def1 def2 in
          let live' = SS.union used (SS.union live1 live2) in
          (dec, def', live', T.If(texp, ts1, ts2))
      | _ -> raise_error "Expression in If not Bool type"
    )

  | While (exp, s') -> 
    let texp = tc_exp globl_context prog_context dec exp in
    (match (fst texp) with
    | T.Bool ->
        let used = used_exp exp in
        let (_, _, live', ts) = tc_stm ret_type globl_context prog_context (dec, def, live) s' in
        let live'' = SS.union used live' in
        (dec, def, live'', T.While(texp, ts))
    | _ -> raise_error "Expression in While not Bool type"
    )

  | Assert exp ->
       let texp = tc_exp globl_context prog_context dec exp in
        if (fst texp != T.Bool) then (raise_error "Assert statement has non-Bool expression")
      else
        let live' = used_exp exp in
        let _ = SS.iter (check_def def) live' in
        (dec, def, live', T.Assert(texp))

  | Return exp ->
      let texp = tc_exp globl_context prog_context dec exp in
      let exp_type = fst texp in
      (* let _ = Printf.printf "exp_type: %s, ret_type: %s\n" (get_type_str exp_type) (get_type_str ret_type) in *)
      if (not (type_equal exp_type ret_type) || not (globl_context#is_small_type exp_type))
         || (match exp_type with T.Pointer(T.Void) -> true | _ -> false) then
        (raise_error "Return type invalid")
      else

      let live' = used_exp exp in
      let _ = SS.iter (check_def def) live' in
      (* put all declared variables in def *)
      let def' = SS.union def (dec_set dec) in
      (dec, def', live', T.Return(texp))

  | Seq (s1, s2) ->
    let (_, def1, live1, ts1) = tc_stm ret_type globl_context prog_context (dec, def, live) s1 in
    let (_, def2, live2, ts2) = tc_stm ret_type globl_context prog_context (dec, def1, live1) s2 in
    let live' = SS.union live1 (SS.diff live2 def1) in
    (dec, def2, live', T.Seq(ts1, ts2))

  | Expr exp ->
    let (t, e) = tc_exp globl_context prog_context dec exp in
    if globl_context#is_small_type t then
      let live' = used_exp exp in
      (dec, def, live', T.Expr((t, e)))
    else
      raise_error "Non-small type used in expression-as-a-statement."

  | Nop -> (dec, def, live, T.Nop)

  | ReturnVoid ->
    if (ret_type != T.Void) then
      raise_error "Return is not void"
    else
      let def' = SS.union def (dec_set dec) in
      (dec, def', live, T.ReturnVoid)


(* TODO: this should return the context to be used, adding the variables in *)
(* creates the starting definition and declaration sets for a function*)
let get_func_start plist global_context =
  let init_pc = new progContext global_context#get_dec_set in

  let fold_help (dec, def, c) (ptype, id) = 
    let t = global_context#get_type ptype in
    (* function paramater name already exists *)
    if (SS.mem id def) then
      raise_error "multiple params with same name in function"
    else
      (SM.add id t dec, SS.add id def, c#add_arg id)
  in
    List.fold_left fold_help (SM.empty, SS.empty, init_pc) plist

let rec return_check = function
  | Declare(id, t, s') -> return_check s'
  | Assign(id, isin, e) -> false
  | AssignOp(id, isin, e) -> false
  | If(e, s1, s2) -> (return_check s1) && (return_check s2)
  | While(e, s') -> false
  | Return(e) -> true
  | Nop -> false
  | Seq(s1, s2) -> (return_check s1) || (return_check s2)
  | Expr(e) -> false
  | ReturnVoid -> true
  | Assert(e) -> false

(* prepends a _c0_ to name if not already there *)
let mangle_name fname = 
  if (Symbol.name fname = "main") then Symbol.symbol "_c0_main"
  else if (Symbol.name fname = "_c0_main") then Symbol.symbol "_c0__c0_main"

  else if (String.length (Symbol.name fname) > 4)
    && (String.sub (Symbol.name fname) 0 4 = "_c0_") then fname 
  else Symbol.symbol ("_c0_" ^ (Symbol.name fname))

let rec tc_topstm ~is_header gc = function
  | [] -> ([], gc)

  | t::rest -> (match t with

    | Typedef(ptype, id) ->
      let ptype' = trans_type ptype in
      let gc' = gc#add_typedef id ptype' in
      let (rec_list, ret_gc) = tc_topstm is_header gc' rest in
      ((T.Typedef(ptype', id) :: rec_list), ret_gc)

    | Fdecl(ptype, fname, plist) ->

      let ptype' = trans_type ptype in
      (* add mapping from function to new name *)
      let fname_mangled = if is_header then fname else mangle_name fname in
      let () = mangled_name_holder#add_mangled_name fname fname_mangled in

      (* if its a header declaration then add it as defined *)
      let gc' = if is_header then gc#add_def fname else gc in
      let gc'' = gc'#add_fundecl fname ptype plist in
      let (rec_list, ret_gc) = tc_topstm is_header gc'' rest in
      let plist' = List.map (fun (pt, id) -> (trans_type pt, id)) plist in

      ((T.Fdecl(ptype', fname, plist') :: rec_list), ret_gc)

    (* have to add the variables to the context *)
    | Fdefn (ptype, fname, plist, s') ->

      let ptype' = trans_type ptype in
      let plist' = List.map (fun (t, i) -> (trans_type t, i)) plist in
      (* add mapping from function to new name *)
      let fname_mangled = if is_header then fname else mangle_name fname in
      let () = mangled_name_holder#add_mangled_name fname fname_mangled in

      (* add function to set of defined functions *)
      let gc' = gc#add_fundefn fname ptype plist in
      let gc'' = gc'#add_def fname in

      let (decmap, defset, prog_context) = get_func_start plist gc'' in

      let f_ret_type = fst (gc''#get_functype fname) in

      let does_ret = return_check s' in

      let () = (match f_ret_type with
                | T.Void -> () (* What happens if we do return a value in a void function? *)
                | _ when (not does_ret) -> raise_error "Function does not return"
                | _ -> ()
              )
      in

      let (_,_,_,typed_s) = tc_stm f_ret_type gc'' prog_context (decmap, defset, SS.empty) s' in

      let (rec_list, ret_gc) = tc_topstm is_header gc'' rest in

      (* recurse *)
      (T.Fdefn(ptype', fname, plist', typed_s) :: rec_list, ret_gc)

    | Structdecl struct_name ->

        (* if its a header declaration then add it as defined *)
        let gc' = if is_header then gc#add_def struct_name else gc in
        let gc'' = gc'#add_structdecl struct_name in
        let (rec_list, ret_gc) = tc_topstm is_header gc'' rest in

        (((T.StructDecl struct_name) :: rec_list), ret_gc)

    | Structdefn (struct_name, struct_fl) ->
        let struct_fl' = List.map (fun (p,i) -> (trans_type p, i)) struct_fl in

        (* if its a header declaration then add it as defined *)
        let gc' = if is_header then gc#add_def struct_name else gc in
        let gc'' = gc'#add_structdefn struct_name struct_fl in
        let (rec_list, ret_gc) = tc_topstm is_header gc'' rest in

        ((T.StructDefn(struct_name, struct_fl') :: rec_list), ret_gc)
      )

let check (pre_tree, t) = 
  (* by default add in call to main to ensure it is defined *)
  let () = called_tracker#add_call (Symbol.symbol "main") in
  let () =
    let main = Symbol.symbol "main" in
    let main_mangled = mangle_name main in
    mangled_name_holder#add_mangled_name main main_mangled in

  let gc = new globalContext in

  let (pre_typed_prog, gc') = tc_topstm ~is_header:true gc pre_tree in

  let (final_typed_prog, gc'') = tc_topstm ~is_header:false gc' t in

  (* check that every function called was also defined *)
  let called_functions = called_tracker#get_call_set in
  let defined_functions = gc''#get_def_set in
  if (SS.subset called_functions defined_functions) then
    (final_typed_prog, mangled_name_holder#get_mangled_map)
  else
    raise_error "Some function called but not defined"
