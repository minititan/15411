module T = TypedAst

type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * texp * texp
  | Unop of unop * texp
  | Call of Symbol.symbol * (texp list)
  | Deref of Int32.t * texp (* int for offset *)
and texp = 
  | WORD of exp
  | DWORD of exp
  | QWORD of exp

type cmp = 
  | Cmp of cmpop * texp * texp

type command =
  | Mov of texp * texp (* the mov to exp can only be temp or deref of temp *)
  | VoidCall of Symbol.symbol * (texp list)
  | If of cmp * Label.label * Label.label (* if true goto Label.label1 *)
  | CheckDeref of texp
  | Goto of Label.label
  | Label of Label.label
  | Return of texp
  | ReturnVoid

type func = 
  TypedAst.primtype * Symbol.symbol * texp list * command list

(* Start line *)
type irprogram = func list

module type PRINT =
  sig
  	val pp_unop : unop -> string
  	val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_texp : texp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_function : func -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT =
  struct
  	let pp_binop op = 
  		match op with
  		| ADD -> "+"
		  | SUB -> "-"
		  | MUL -> "*"
		  | DIV -> "/"
		  | MOD -> "%"
		  | BAND -> "&"
		  | BOR -> "|"
		  | SAR -> ">>"
		  | SAL -> "<<"
		  | XOR -> "^"


	  let pp_unop op = 
		  match op with
			| NEG -> "-"
		  | BNOT -> "~"

    let pp_cmpop op = 
    match op with
      | EQ -> "=="
      | NOTEQ -> "!="
      | GREATER -> ">"
      | GREATEREQ -> ">="
      | LESS -> "<"
      | LESSEQ -> "<="

  	let rec pp_exp e = 
  		match e with
      | Const(i) -> Int32.to_string i
      | Temp(t) -> Temp.name t 
  	 	| Binop(bop, i1, i2) -> (pp_texp i1) ^ " " ^ (pp_binop bop) ^ " " ^ (pp_texp i2)
  	 	| Unop(uop, i1) -> (pp_unop uop) ^ " " ^ (pp_texp i1)
      | Call(id, explist) -> "Call( " ^ (Symbol.name id) ^ ", [" ^ 
                              (List.fold_left (fun s e -> s ^ pp_texp e ^ ",") "" explist) ^ "])"
      | Deref(i, te) -> Int32.to_string i ^ "(" ^ pp_texp te ^ ")"
    
    and pp_texp te = 
      match te with
      | WORD e -> pp_exp e
      | DWORD e -> pp_exp e
      | QWORD e -> pp_exp e

  	let pp_cmp c = 
  		match c with
  		| Cmp(cop, i1, i2) -> (pp_texp i1) ^ " " ^ (pp_cmpop cop) ^ " " ^ (pp_texp i2)

  	let pp_command c = 
  		match c with
  		| Mov(e1, e2) -> (pp_texp e1) ^ " <- " ^ (pp_texp e2) ^ "\n"
  		| If(c, l1, l2) -> "If (" ^ (pp_cmp c) ^ ") " ^ "Goto " ^ (Label.name l1) ^ " Else Goto " ^ (Label.name l2) ^ "\n"
  		| Goto(l) -> "Goto " ^ (Label.name l) ^ "\n"
      | CheckDeref(e1) -> "CheckDeref(" ^ (pp_texp e1) ^ ")"
  		| Label(l) -> Label.name l ^ ":" ^ "\n"
  		| Return(e) -> "Ret " ^ (pp_texp e) ^ "\n"
      | ReturnVoid -> "RetVoid\n"
      | VoidCall(id, explist) -> "VoidCall( " ^ Symbol.name id ^ ", [" ^ 
                              (List.fold_left (fun s e -> s ^ ", " ^ pp_texp e) "" explist) ^ "])\n"

    let rec pp_type = function
    | T.Int -> "int"
    | T.Bool -> "bool"
    | T.Void -> "void"
    | T.PrId id -> Symbol.name id
    | T.Struct id -> Symbol.name id
    | T.Pointer pt -> "Pointer (" ^ pp_type pt ^ ")"
    | T.AnyPointer -> "AnyPointer"
    | T.Array pt -> pp_type pt ^ "[]"

  	let pp_function = function
      | (t, id, tlist, lst) ->
        let rec pp_funcbody l = 
          (match l with
          | [] -> ""
          | instr::instrs' -> pp_type t ^ " " ^ pp_command instr ^ pp_funcbody instrs'
          )

        in

        let func_str = pp_funcbody lst in

        let arg_help = function
        | WORD(Temp(t)) -> "W." ^ Temp.name t
        | DWORD(Temp(t)) -> "D." ^ Temp.name t
        | QWORD(Temp(t)) -> "Q." ^ Temp.name t
        | _ -> (ErrorMsg.error None "Non-temp in arg_help in pp_function."; raise ErrorMsg.Error)
        
        in
        let arg_temps = "(" ^ List.fold_left (fun s t -> s ^ arg_help t ^ ",") "" tlist ^ ")" in

        Symbol.name id ^ arg_temps ^  " { " ^ func_str ^ "}\n\n"

    let pp_program p = 
      List.fold_left (fun s f -> s ^ pp_function f) "" p

  	end





