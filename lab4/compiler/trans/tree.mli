type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * texp * texp
  | Unop of unop * texp
  | Call of Symbol.symbol * (texp list)
  | Deref of Int32.t * texp
and texp =
  | WORD of exp
  | DWORD of exp
  | QWORD of exp

type cmp = 
  | Cmp of cmpop * texp * texp

type command =
  | Mov of texp * texp (* the mov to exp can only be temp or deref of temp *)
  | VoidCall of Symbol.symbol * (texp list)
  | If of cmp * Label.label * Label.label (* if true goto Label.label *)
  | CheckDeref of texp
  | Goto of Label.label
  | Label of Label.label
  | Return of texp
  | ReturnVoid

type func = 
  TypedAst.primtype * Symbol.symbol * texp list * command list

(* Start line *)
type irprogram = func list

module type PRINT =
  sig
    val pp_unop : unop -> string
    val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_texp : texp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_function : func -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT

