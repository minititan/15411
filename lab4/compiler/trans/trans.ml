open TypedAst
module A = Ast
module T = Tree
module SM = Symbol.Map

let safe_flag = ref true

(* todo: refactor all errormsg's in this file using this *)
let raise_error msg = ErrorMsg.error None msg; raise ErrorMsg.Error

(* Hold information about structs. 
    offmap is a mapping from struct name to a mapping from field names to offsets 
    sizemap is a mapping from struct name to its size in bytes *)
let structInfo = 
object(this)
  val mutable offmap = SM.empty
  val mutable sizemap = SM.empty

  method addStructInfo sname field_list = 
    let (size, field_map) = List.fold_left (fun (s, m) (t, id) -> 
                                            let t_size = this#get_type_size t in
                                            let pad = if (t_size = 0) then 0 else (s mod t_size) in
                                            (s+pad+t_size, SM.add id (s+pad) m)) (0, SM.empty) field_list in

    let () = offmap <- SM.add sname field_map offmap in
    sizemap <- SM.add sname size sizemap

  method get_type_size t = 
    match t with
    | Int -> 4
    | Bool -> 4
    | Void -> 4
    | PrId(id) -> ErrorMsg.error None ("Wtf is a typedef doing here?"); raise ErrorMsg.Error
    | Struct(sname) -> this#getStructSize sname
    | Pointer(_) -> 8
    | Array(t) -> 8
    | AnyPointer -> 8

  method getStructSize sname = 
    SM.find sname sizemap

  method getStructOffset sname field = 
    SM.find field (SM.find sname offmap)

end



(* Hold the mangled function names *)
let nameMap = 
object
  val mutable nmap = SM.empty

  method addMap m = nmap <- m
  method getName fname = SM.find fname nmap
end


(* Helper function that retrieves the temp representing a variable *)
let get_temp_from_var id env = 
  match (SM.find' id env) with 
  | Some t -> T.Temp(t)
  | None -> ErrorMsg.error None ("variable being assigned to not in temp map"); raise ErrorMsg.Error
  
(* each symbol should map to a struct_info,
 * a list of the fields in the structure in order
 * note that field's definition comes from TypedAst *)
type struct_info = Ast.field list

let get_exp e = snd e

let get_res_temp st = 
	match st with
	| None -> T.Temp(Temp.create())
  | Some(stmp) -> T.Temp(stmp)

let op_to_binop op = 
	(match op with
	| A.PLUS -> T.ADD
 	| A.MINUS -> T.SUB
 	| A.TIMES -> T.MUL
 	| A.DIVIDEDBY -> T.DIV
 	| A.MODULO -> T.MOD 
 	| A.BITAND -> T.BAND
  | A.BITOR -> T.BOR
  | A.SAR -> T.SAR
  | A.SAL -> T.SAL
  | A.XOR -> T.XOR
  | _ -> ErrorMsg.error None ("Non binop encountered!"); raise ErrorMsg.Error)

let op_to_unop op = 
	(match op with
	| A.NEGATIVE -> T.NEG
  | A.BITNOT -> T.BNOT
  | _ -> ErrorMsg.error None ("Non unop encountered!"); raise ErrorMsg.Error)

let op_to_cmpop op = 
	(match op with
	| A.EQUALS -> T.EQ 
	| A.NOTEQUALS -> T.NOTEQ 
	| A.GREATER -> T.GREATER 
	| A.GREATEREQUAL -> T.GREATEREQ 
	| A.LESS -> T.LESS 
	| A.LESSEQUAL -> T.LESSEQ
	| _ -> ErrorMsg.error None ("Non cmpop encountered!"); raise ErrorMsg.Error)


let wrapper t e = 
  match t with
  | Int -> T.DWORD e
  | Bool -> T.DWORD e
  | Pointer x -> T.QWORD e
  | Struct id -> T.QWORD e
  | Array id -> T.QWORD e
  | AnyPointer -> T.QWORD e
  | _ -> ErrorMsg.error None ("Impossible case in wrapper."); raise ErrorMsg.Error

(* returns sequence of commands that jumps to l1 if exp is true and l2 if false *)
let rec bool_jmp error_11 error_8 env texp l1 l2 =
  match snd texp with
  | Var(id) -> 
    (match SM.find' id env with
    | Some(t) -> [T.If(T.Cmp(T.NOTEQ, T.DWORD (T.Temp t), T.DWORD (T.Const(Int32.of_int 0))), l1, l2)]
    | None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
    )

  | BinopExp(op, te1, te2) -> 
    (match op with
    | A.LOGICAND ->
      (* optimizing on the values *)
      (match (get_exp te1, get_exp te2) with
      | (_, False) | (False, _) -> [T.Goto(l2)]
      | (True, True) -> [T.Goto(l1)]

      | _ -> 
          let l = Label.create() in
          let (c1, c2) = (bool_jmp error_11 error_8 env te1 l l2, bool_jmp error_11 error_8 env te2 l1 l2) in
          c1 @ (T.Label(l)::c2) 
      )

    | A.LOGICOR -> 
      (* optimizing on values *)
      (match (get_exp te1, get_exp te2) with
        | (_, True) | (True, _) -> [T.Goto(l1)]
        | (False, False) -> [T.Goto(l2)]

        | _ ->
            let l = Label.create() in
            let (c1, c2) = (bool_jmp error_11 error_8 env te1 l1 l, bool_jmp error_11 error_8 env te2 l1 l2) in
            c1 @ (T.Label(l)::c2)
      )

    | A.EQUALS | A.NOTEQUALS | A.GREATER | A.GREATEREQUAL | A.LESS | A.LESSEQUAL ->
      let ((c1, v1), (c2, v2)) = (trans_exp error_11 error_8 te1 env None, trans_exp error_11 error_8 te2 env None) in
      let cop = op_to_cmpop op in
      c1 @ (c2 @ [T.If(T.Cmp(cop, v1, v2), l1, l2)])

    | _ -> ErrorMsg.error None ("Boolean binop not found"); raise ErrorMsg.Error
    )

  | UnopExp(op, te1) -> 
    (match op with
    | A.LOGICNOT -> bool_jmp error_11 error_8 env te1 l2 l1
    | _ -> ErrorMsg.error None ("Boolean unop not found"); raise ErrorMsg.Error
    )

  | True -> [T.Goto(l1)]
  | False -> [T.Goto(l2)]

  | Condition(te1, te2, te3) ->
    let lt = Label.create() in
    let lf = Label.create() in
    let (c1, c2, c3) = (bool_jmp error_11 error_8 env te1 lt lf,
              bool_jmp error_11 error_8 env te2 l1 l2,
              bool_jmp error_11 error_8 env te3 l1 l2) in

    c1 @ (T.Label(lt)::c2) @ (T.Label(lf)::c3)

  | ConstExp _ -> ErrorMsg.error None ("ConstExp in boolean type"); raise ErrorMsg.Error

  | Call(id, texp_list) ->
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp error_11 error_8 ex env None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          let res_t = Temp.create() in
          arg_create_cmds @ [T.Mov(T.DWORD (T.Temp res_t), T.DWORD (T.Call(nameMap#getName id, arg_temp_list))); 
                              T.If(T.Cmp(T.NOTEQ, T.DWORD (T.Temp res_t), T.DWORD (T.Const(Int32.zero))), l1, l2) ]

  | Null -> ErrorMsg.error None ("Null in boolean type"); raise ErrorMsg.Error

  (* going to use trans_exp error_11 error_8 to extract values for these. We cannot infinitely recurse since 
      trans_exp error_11 error_8 cases on these first and immediately evaluates *)
  | ArrIndex(te1, te2) ->
        let (c, t) = trans_exp error_11 error_8 texp env None in
        c @ [T.If(T.Cmp(T.NOTEQ, t, T.DWORD (T.Const(Int32.of_int 0))), l1, l2)]

  | Deref(te) ->
          let (c, t) = trans_exp error_11 error_8 texp env None in
        c @ [T.If(T.Cmp(T.NOTEQ, t, T.DWORD (T.Const(Int32.of_int 0))), l1, l2)]

  | FieldDeref(te, field_id) ->
        let (c, t) = trans_exp error_11 error_8 texp env None in
        c @ [T.If(T.Cmp(T.NOTEQ, t, T.DWORD (T.Const(Int32.of_int 0))), l1, l2)]

  | AllocCall _ -> ErrorMsg.error None ("A call to alloc returns type*, not a bool."); raise ErrorMsg.Error

  | AllocArrCall (_, _) -> ErrorMsg.error None ("A call to alloc_array returns type[], not a bool."); raise ErrorMsg.Error


(* returns a  pair of (command list, imm containing expression value) *)
and trans_exp error_11 error_8 e env store = 
  let (exp_typ, exp) = e in
	 
  (* We first try to use patterns that dont require bool jmp to be invoked *)
  match exp with
      | True ->
      	(match store with
      	| None -> ([], T.DWORD (T.Const(Int32.of_int 1)))
      	| Some(t) -> ([T.Mov(T.DWORD (T.Temp t), T.DWORD (T.Const(Int32.of_int 1)))], T.DWORD (T.Temp t))
      	)

      | False -> 
      		(match store with
      			| Some(tmp) -> ([T.Mov(T.DWORD (T.Temp tmp), T.DWORD (T.Const(Int32.of_int 0)))], T.DWORD (T.Temp tmp))
      			| None -> ([], T.DWORD (T.Const(Int32.of_int 0)))
      		)

      | ConstExp(i) -> 
        (match store with
        | None -> ([], wrapper exp_typ (T.Const(i)))
        | Some(t) -> ([T.Mov(wrapper exp_typ (T.Temp(t)), wrapper exp_typ (T.Const(i)))], wrapper exp_typ (T.Temp(t)))
        )

      | Var(id) -> 
      		(match SM.find' id env with
    			| Some(t) -> 
    				(match store with 
    				| None -> ([], wrapper exp_typ (T.Temp(t)))
    				| Some(tmp) -> ([T.Mov(wrapper exp_typ (T.Temp(tmp)), wrapper exp_typ (T.Temp(t)))], wrapper exp_typ (T.Temp(tmp)))
    				)

    			| None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
    			)

      | Call(id, texp_list) -> 
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp error_11 error_8 ex env None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          let res_t = wrapper exp_typ (get_res_temp store) in
          (arg_create_cmds @ [T.Mov(res_t, wrapper exp_typ (T.Call(nameMap#getName id, arg_temp_list)))], res_t)
      

      | Null -> 
        (match store with
        | None -> ([], T.QWORD (T.Const(Int32.zero)))
        | Some(t) -> 
                ([T.Mov(T.QWORD (T.Temp t), T.QWORD (T.Const(Int32.zero)))], T.QWORD (T.Temp t))
        )

      | Deref(texp) ->
          let res_t = wrapper exp_typ (get_res_temp store) in
          let (c, t) = trans_exp error_11 error_8 texp env None in
          (c @ [T.Mov(res_t, wrapper exp_typ (T.Deref(Int32.zero, t)))], res_t)

      | FieldDeref(texp, field_id) -> 
          let res_t = wrapper exp_typ (get_res_temp store) in 
          let (c, add_t) = get_address error_11 error_8 e env in
          (c @ [T.Mov(res_t, wrapper exp_typ (T.Deref(Int32.zero, add_t)))], res_t)

      (*  Need to check bounds of the array here *)
      | ArrIndex(te1, te2) ->
          let (c, add_t) = get_address error_11 error_8 e env in
          let res_t = wrapper exp_typ (get_res_temp store) in
          (c @ [T.Mov(res_t, wrapper exp_typ (T.Deref(Int32.zero, add_t)))], res_t)


      | AllocCall(t) -> 
          let res_t = T.QWORD (get_res_temp store) in
          let t_size = structInfo#get_type_size t in
          let the_call = T.QWORD (T.Call(Symbol.symbol "calloc", 
                                [T.DWORD (T.Const(Int32.one)); T.DWORD (T.Const(Int32.of_int t_size))])) in

          ([T.Mov(res_t, the_call)], res_t)

      (* Need to be careful to allocate extra space for the length of array *)
      | AllocArrCall(t, te) ->
          let res_t = T.QWORD (get_res_temp store) in
          let (c, num_t) = trans_exp error_11 error_8 te env (Some(Temp.create())) in

          let t_size = structInfo#get_type_size t in
          let size_arg = T.Const(Int32.of_int t_size) in
          let zero = T.DWORD (T.Const(Int32.zero)) in
          let one = T.DWORD (T.Const(Int32.one)) in
          let num_arg_t = T.DWORD (get_res_temp None) in

          if (!safe_flag) then
            let good = Label.create() in

            (c @ 
              [
               T.If(T.Cmp(T.LESS, num_t, zero), error_11, good);

               T.Label(good);
               T.Mov(num_arg_t, T.DWORD (T.Binop(T.ADD, one, num_t)));
               T.Mov(res_t, T.QWORD (T.Call(Symbol.symbol "calloc", [num_arg_t; T.DWORD (size_arg)])));
               T.Mov(wrapper t (T.Deref(Int32.zero, res_t)), num_t);
               T.Mov(res_t, T.QWORD (T.Binop(T.ADD, T.QWORD (size_arg), res_t)));
              ], res_t)

          else
            (c @ 
              [
               T.Mov(num_arg_t, T.DWORD (T.Binop(T.ADD, one, num_t)));
               T.Mov(res_t, T.QWORD (T.Call(Symbol.symbol "calloc", [num_arg_t; T.DWORD (size_arg)])));
               T.Mov(wrapper t (T.Deref(Int32.zero, res_t)), num_t);
               T.Mov(res_t, T.QWORD (T.Binop(T.ADD, T.QWORD (size_arg), res_t)));
              ], res_t)

      (* For these remaining ones we need to use the bool jmp for boolean types *)
      | e' -> 
        
        (match exp_typ with

          | Bool -> 
                (match e' with 
                  | UnopExp(A.LOGICNOT,(Bool,UnopExp(A.LOGICNOT,e'))) ->
                      trans_exp error_11 error_8 e' env None
                  | _ -> 
                    let tmp = get_res_temp store in
                    let l1  = Label.create() in
                    let l2 = Label.create() in
                    let l3 = Label.create() in
                  
                    let cmds = bool_jmp error_11 error_8 env e l1 l2 in

                    (match cmds with
                    | [T.Goto(l)] when (l = l1) -> 
                          ([T.Mov(T.DWORD (tmp), T.DWORD (T.Const(Int32.of_int 1)))], T.DWORD (tmp))

                    | [T.Goto(l)] when (l = l2) -> 
                          ([T.Mov(T.DWORD (tmp), T.DWORD (T.Const(Int32.of_int 0)))], T.DWORD (tmp))

                    | _ ->

                      ((cmds @ [T.Label(l1); 
                                T.Mov(T.DWORD (tmp), T.DWORD (T.Const(Int32.of_int 1))); 
                                T.Goto(l3);
                                
                                T.Label(l2); 
                                T.Mov(T.DWORD (tmp), T.DWORD (T.Const(Int32.of_int 0))); 
                                T.Goto(l3); 

                                T.Label(l3)]), 
                       T.DWORD (tmp))
                    )
                )

          | _ ->
            
            (match e' with
              | BinopExp(op, e1, e2) -> 
                let ((c1, e_res1), (c2, e_res2), con_op) = 

                  (* special case for division where we need to evaluate the divisor in a separate temp *)
                  (match op with
                  | A.DIVIDEDBY | A.MODULO -> 
                    let divtemp = Temp.create() in 
                      (trans_exp error_11 error_8 e1 env None, trans_exp error_11 error_8 e2 env (Some divtemp), op_to_binop op)

                  | A.SAR | A.SAL ->
                    let (cl,e2') = trans_exp error_11 error_8 e2 env None in

                    if (!safe_flag) then
                      let phase2 = Label.create() in
                      let success = Label.create() in
                      let cl_with_check = List.concat
                      [
                        cl;
                        [T.If(T.Cmp(T.GREATEREQ, e2', T.DWORD (T.Const(Int32.zero))), phase2, error_8);
                        T.Label(phase2);
                        T.If(T.Cmp(T.LESS, e2', T.DWORD (T.Const(Int32.of_int 32))), success, error_8)];

                        [T.Label(success)]
                      ]

                      in
                        (trans_exp error_11 error_8 e1 env None, (cl_with_check,e2'), op_to_binop op)
                    
                    else

                      let cl_with_check = List.concat
                      [
                        cl
                      ]

                      in
                        (trans_exp error_11 error_8 e1 env None, (cl_with_check,e2'), op_to_binop op)

                  | _ -> 
                      (trans_exp error_11 error_8 e1 env None, trans_exp error_11 error_8 e2 env None, op_to_binop op)
                  )

                in

                let new_t = wrapper exp_typ (get_res_temp store) in
                let new_comm = [T.Mov(new_t, wrapper exp_typ (T.Binop(con_op, e_res1, e_res2)))] in
                ((c1 @ c2) @ new_comm, new_t)

              | UnopExp(A.NEGATIVE,(Int,UnopExp(A.NEGATIVE,e'))) ->
                trans_exp error_11 error_8 e' env store

              | UnopExp(A.BITNOT,(Int,UnopExp(A.BITNOT,e'))) ->
                trans_exp error_11 error_8 e' env store

              | UnopExp(op, e') -> 
                let (cl, res) = trans_exp error_11 error_8 e' env None in
                let uop = op_to_unop op in
                let res_t = wrapper exp_typ (get_res_temp store) in

                (cl @ [T.Mov(res_t, wrapper exp_typ (T.Unop(uop, res)))], res_t)

              | Condition(e1, e2, e3) ->        
                let l1 = Label.create() in
                let l2  = Label.create() in
                let l3 = Label.create() in

                let t_res = wrapper exp_typ (get_res_temp store) in

                let c1 = bool_jmp error_11 error_8 env e1 l1 l2 in
                let (c2, t2) = trans_exp error_11 error_8 e2 env None in
                let (c3, t3) = trans_exp error_11 error_8 e3 env None in

                (c1 @ ([T.Label(l1)] @ c2 @ [T.Mov(t_res, t2); T.Goto(l3)]) @
                   ([T.Label(l2)] @ c3 @ [T.Mov(t_res, t3); T.Goto(l3)]) @ 
                   [T.Label(l3)], 
                t_res)

              | _ -> raise_error "invalid case in booljmp"
            )
        )

(* returns the address of the thing *)
and get_address error_11 error_8 exp env =
  match snd exp with
  | Deref e -> trans_exp error_11 error_8 e env None

  | FieldDeref(e, field_name) -> 
      let struct_name = (match fst e with
                          | Struct n -> n | _ -> raise_error "non-struct in FieldDeref")
    in

      let (c, add_e) = get_address error_11 error_8 e env in
      let field_off = structInfo#getStructOffset struct_name field_name in
      let new_add_e = T.QWORD (get_res_temp None) in

      if (!safe_flag) then
        (c @ 
          [
          T.CheckDeref(add_e);
          T.Mov(new_add_e, T.QWORD (T.Binop(T.ADD, add_e, T.QWORD (T.Const(Int32.of_int field_off)))))
        ], new_add_e)

      else

        (c @ 
          [
          T.Mov(new_add_e, T.QWORD (T.Binop(T.ADD, add_e, T.QWORD (T.Const(Int32.of_int field_off)))))
        ], new_add_e)


  | ArrIndex(te1, te2) -> 

      let (c1, add_t) = trans_exp error_11 error_8 te1 env (Some(Temp.create())) in
      let (c2, ind_t) = trans_exp error_11 error_8 te2 env (Some(Temp.create())) in

      let array_t = (match fst te1 with
                          Array t -> t | _ -> raise_error "non-array in array_t") in

      let type_size = structInfo#get_type_size array_t in

      let new_add_t = T.QWORD (get_res_temp None) in
      let eff_ind_e = T.DWORD ((T.Binop(T.MUL, ind_t, T.DWORD (T.Const(Int32.of_int type_size))))) in

      if (!safe_flag) then
        let zero = T.DWORD (T.Const(Int32.zero)) in

        (* for holding length *)
        let length_t = T.DWORD (get_res_temp None) in

        let l1 = Label.create() in
        let l3 = Label.create() in

        
        let size_off_t = Int32.of_int (- type_size) in

        (* checking damn bounds *)
        (c1 @ c2 @ 
          [
           T.CheckDeref(add_t);

           T.Mov(length_t, T.DWORD (T.Deref(size_off_t, add_t)));
           T.If(T.Cmp(T.LESS, ind_t, length_t), l1, error_11);
           
           T.Label(l1);
           T.If(T.Cmp(T.GREATEREQ, ind_t, zero), l3, error_11);

           T.Label(l3);
           T.Mov(ind_t, eff_ind_e);
           T.Mov(new_add_t, T.QWORD (T.Binop(T.ADD, add_t, ind_t)))
          ], new_add_t)


      else

        (c1 @ c2 @ 
          [
           T.Mov(ind_t, eff_ind_e);
           T.Mov(new_add_t, T.QWORD (T.Binop(T.ADD, add_t, ind_t)))
          ], new_add_t)

  | _ -> raise_error "Not a valid expression for getting address"

(* Returns an exp that can be assigned to, so a derefed address *)
let get_lvalue error_11 error_8 exp env = 
  match snd exp with
  | Var id -> ([], wrapper (fst exp) (get_temp_from_var id env))

  | Deref e -> 
      let (c, t) = get_address error_11 error_8 exp env in
      (c, wrapper (fst exp) (T.Deref(Int32.zero, t)))

  | FieldDeref(e, f) -> 
      let (c, t) = get_address error_11 error_8 exp env in
      (c, wrapper (fst exp) (T.Deref(Int32.zero, t)))

  | ArrIndex(te1, te2) -> 
      let (c, t) = get_address error_11 error_8 exp env in
      (c, wrapper (fst exp) (T.Deref(Int32.zero, t)))

  | _ -> ErrorMsg.error None ("This type of expression cannot be assigned to"); raise ErrorMsg.Error

(* returns a sequence of commands appended to the accumulated commands (in acc) *)
let rec trans_stmt error_11 error_8 env s acc = 
  match s with 
  | Declare(t, id, s') ->  let tmp = Temp.create() in
              let new_env = (SM.add id tmp env) in
                acc @ (trans_stmt error_11 error_8 new_env s' [])

  | AssignOp (
    (tp1,ArrIndex(te1,te2)),
    (tp2,
      BinopExp(op,_,(tp5,te5)))
    ) ->
      let (c, addr) = get_address error_11 error_8 (tp1,ArrIndex(te1,te2)) env in
      let derefed = wrapper tp1 (T.Deref(Int32.zero, addr)) in
      let tmp = wrapper tp1 (T.Temp (Temp.create())) in
      let (c', t) =
        (
          let (c',t) = trans_exp error_11 error_8 (tp5,te5) env None in
          match op with
          | A.DIVIDEDBY | A.MODULO ->
            let div_tmp = wrapper tp5 (T.Temp (Temp.create())) in
            (c' @ [T.Mov(div_tmp,t)],div_tmp)
          | _ -> (c',t)
        ) in
      let t' =
        (match op with
        | A.SAR | A.SAL -> (match t with
          | T.WORD t' -> T.WORD t' (* trick to make source cl *)
          | T.DWORD t' -> T.WORD t'
          | T.QWORD t' -> T.WORD t'
          )
        | _ -> t
        )
      in
      acc @ c @ [T.Mov(tmp,derefed)] @ c' @ [T.Mov(tmp, wrapper tp1 (T.Binop(op_to_binop op, tmp, t'))); T.Mov(derefed, tmp)]

  (* this can be simplified, but is left in this form for checking with above *)
  | AssignOp ((tp1,Deref(te1)),(tp2,BinopExp(op,_,(tp4,te4)))) ->
      let (c, addr) = get_address error_11 error_8 (tp1,Deref(te1)) env in
      let derefed = wrapper tp1 (T.Deref(Int32.zero, addr)) in
      let tmp = wrapper tp1 (T.Temp (Temp.create())) in
      let (c', t) =
        (
          let (c',t) = trans_exp error_11 error_8 (tp4,te4) env None in
          match op with
          | A.DIVIDEDBY | A.MODULO ->
            let div_tmp = wrapper tp4 (T.Temp (Temp.create())) in
            (c' @ [T.Mov(div_tmp,t)],div_tmp)
          | _ -> (c',t)
        ) in
      let t' =
        (match op with
        | A.SAR | A.SAL -> (match t with
          | T.WORD t' -> T.WORD t' (* trick to make source cl *)
          | T.DWORD t' -> T.WORD t'
          | T.QWORD t' -> T.WORD t'
          )
        | _ -> t
        )
      in
      acc @ c @ c' @ [T.Mov(tmp,derefed)] @ [T.Mov(tmp, wrapper tp1 (T.Binop(op_to_binop op, tmp, t'))); T.Mov(derefed, tmp)]

      (* todo: refactor so assignop contains more specific type for binop case only (don't repeat info) ? *)
  | AssignOp(exp1, exp2) | Assign(exp1, exp2) ->
    let (c, d) = get_lvalue error_11 error_8 exp1 env in
    let (c', t) = trans_exp error_11 error_8 exp2 env None in
      acc @ c @ c' @ [T.Mov(d, t)]

  | Seq(s1, s2) -> 
      (match s1 with
      | Return _ | ReturnVoid -> acc @ (trans_stmt error_11 error_8 env s1 [])
      | _ -> 
          let (t_s1, t_s2) = (trans_stmt error_11 error_8 env s1 [], trans_stmt error_11 error_8 env s2 []) in
            acc @ (t_s1 @ t_s2)
      )

  | Return(exp) -> let (cmds, res) = trans_exp error_11 error_8 exp env None in
            acc @ (cmds @ [T.Return(res)])

  | ReturnVoid -> acc @ [T.ReturnVoid]

  | Nop -> acc

  | Expr(exp) -> 
     (match exp with
      (* void function call *)
      | (Void, Call(id, texp_list)) -> 
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp error_11 error_8 ex env None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          acc @ arg_create_cmds @ [T.VoidCall(nameMap#getName id, arg_temp_list)]

      | _ ->     
          let t = Temp.create() in 
            let (cmds, res) = trans_exp error_11 error_8 exp env (Some t) in
            acc @ cmds
      )

  | If(bool_exp, s1, s2) -> 
            let l1 = Label.create() in
                let l2 = Label.create() in
                let b_cmds = bool_jmp error_11 error_8 env bool_exp l1 l2 in

                (* eliminate unnecessary evaluation of statemnet branch if possible *)
               (match b_cmds with 
                | [T.Goto(l_togo)] when (l_togo = l1) -> 
                            let c1 = trans_stmt error_11 error_8 env s1 [] in
                            acc @ c1
                
                | [T.Goto(l_togo)] when (l_togo = l2) ->
                          let c2 = trans_stmt error_11 error_8 env s2 [] in
                          acc @ c2

                | _ -> 
                    let (c1, c2) = (trans_stmt error_11 error_8 env s1 [], trans_stmt error_11 error_8 env s2 []) in
                    let l3 = Label.create() in

                    (* have to check for ret and in that case not goto l3 *)
                    let (cmds1, need_jump1) = 
                        try 
                            (match List.hd (List.rev c1) with
                            | T.ReturnVoid | T.Return _ -> 
                                (acc @ b_cmds @ (T.Label(l1)::c1), false)
                            | _ -> (acc @ b_cmds @ (T.Label(l1)::c1 @ [T.Goto(l3)]), true)
                            )
                        with Failure "hd" -> (acc @ b_cmds @ (T.Label(l1)::c1 @ [T.Goto(l3)]), true)
                    
                    in
                      let (cmds2, need_jump2) = 
                        try 
                            (match List.hd (List.rev c2) with
                            | T.ReturnVoid | T.Return _ -> 
                                (T.Label(l2)::c2, false)

                            | _ -> (T.Label(l2)::c2 @ [T.Goto(l3)], true)
                            )
                        with Failure "hd" -> (T.Label(l2)::c2 @ [T.Goto(l3)], true)

                    in
                      if (need_jump1 || need_jump2) then 
                        cmds1 @ cmds2 @ [T.Label(l3)]
                      else 
                        cmds1 @ cmds2

                )


  | While(b_exp, s') -> let l1 = Label.create() in
              let l2 = Label.create() in
              let ll = Label.create() in

              let b_cmds = bool_jmp error_11 error_8 env b_exp l1 l2 in

              (match b_cmds with
              | [T.Goto(l_togo)] when l_togo = l2 -> acc
              
              | _ -> 
                    let cmds = trans_stmt error_11 error_8 env s' [] in
                    acc @ ([T.Goto(ll); T.Label(ll)] @ (b_cmds @ ([T.Label(l1)] @ (cmds @ [T.Goto(ll); T.Label(l2)]))))

              )


  | Assert(exp) -> let l1 = Label.create() in
                    let l2 = Label.create() in
                    let cmds = bool_jmp error_11 error_8 env exp l1 l2 in
                    acc @ cmds @ [T.Label(l2); T.VoidCall(Symbol.symbol "abort", []); T.ReturnVoid; T.Label(l1)]


let translate_func id_map s error_11 error_8 = 
  trans_stmt error_11 error_8 id_map s [T.Label(Label.create())] 


(* map each argument id to a temp and return a list of arguments as temps *)
let build_arg_map args =
  List.fold_left (fun (m, lst) (arg_type, id) -> 
                      let t = Temp.create() in
                        (SM.add id t m, lst @ [wrapper arg_type (T.Temp(t))]) ) (SM.empty, []) args

let rec translate_help l = 
  match l with
  | topstm::l' -> 
      (match topstm with
      | Fdefn(t, id, pms, s) ->

        (* label block to raise an error_11 *)
        let error_label_11 = Label.create() in

        let err_t_11 = T.DWORD (get_res_temp None) in
        let error_cmds_11 = [T.Label(error_label_11); 
                          T.Mov(err_t_11,T.DWORD (T.Const(Int32.of_int 11)));
                          T.VoidCall(Symbol.symbol "raise", [err_t_11]);
                          T.ReturnVoid] in

        let error_label_8 = Label.create() in
        let err_t_8 = T.DWORD (get_res_temp None) in
        let error_cmds_8 = [T.Label(error_label_8);
                            T.Mov(err_t_8,T.DWORD (T.Const(Int32.of_int 8)));
                            T.VoidCall(Symbol.symbol "raise", [err_t_8]);
                            T.ReturnVoid] in

        let temp_arg_map, temp_arg_list = build_arg_map pms in

        let cmd_list = translate_func temp_arg_map s error_label_11 error_label_8 in

        (* stuff a ret at the end of a void function if its not there *)
        let cmd_list' = 
          (if (t = Void) then
              (match List.nth cmd_list ((List.length cmd_list) -1) with
                | T.ReturnVoid -> cmd_list
                | T.Return(e) -> cmd_list
                | _ -> cmd_list @ [T.ReturnVoid]
              )
          else cmd_list)
        
        in

        (* clean up cmd list*)
        let clean_cmd_list = 
          let revd = List.rev cmd_list' in
          
            (match revd with
            | T.Label(l)::(x) -> List.rev x
            | _ -> cmd_list'
            )

        in

        let final_cmd_list = 
            if (!safe_flag) then 
                clean_cmd_list @ error_cmds_11 @ error_cmds_8 
            else 
                clean_cmd_list 
        in

        (t, nameMap#getName id, temp_arg_list, final_cmd_list)::(translate_help l')

      | Fdecl(t, id, pms) -> translate_help l'

      | Typedef(t, id) -> translate_help l'

      | StructDecl(id) -> translate_help l'

      | StructDefn(id, flist) -> 
          let () = structInfo#addStructInfo id flist in
          translate_help l'
    )

  | [] -> []


let translate (l, name_map) is_unsafe =
  let () = safe_flag := (not is_unsafe) in
  let () = nameMap#addMap name_map in

  translate_help l




