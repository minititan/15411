type label
type t = label

val reset : unit -> unit              (* resets label numbering *)
val create : unit -> label             (* returns a unique new label *)
val name : label -> string             (* returns the name of a label *)
val compare : label -> label -> int     (* comparison function *)
val get_int : label -> int
val get_label : int -> label

val format : Format.formatter -> label -> unit


(* Sets of labels *)
module type SET = Printable.SET with type elt = t
module Set : SET

(* Maps of labels *)
module type MAP = Printable.MAP with type key = t
module Map : MAP