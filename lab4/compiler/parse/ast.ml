(* L1 Compiler
 * Abstract Syntax Trees
 * Author: Alex Vaynberg
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Forward compatible fragment of C0
 *)

type ident = Symbol.symbol

let (|>) x f = f x

type oper =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDEDBY
  | MODULO
  | NEGATIVE                     (* unary minus *)
  | BITAND
  | BITOR
  | BITNOT
  | SAR
  | SAL
  | XOR
  | LOGICAND
  | LOGICOR
  | LOGICNOT
  | EQUALS
  | NOTEQUALS
  | GREATER
  | GREATEREQUAL
  | LESS
  | LESSEQUAL

type primtype =
  | Int
  | Bool
  | Void
  | PrId of ident
  | Struct of ident
  | Pointer of primtype
  | Array of primtype

type param = primtype * ident

type params = param list

type exp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * exp * exp
  | UnopExp of oper * exp 
  | Condition of exp * exp * exp
  | Call of ident * args
  | True
  | False
  | Null
  | FieldDeref of exp * ident (* for structs *)
  | Deref of exp
  | AllocCall of primtype
  | AllocArrCall of primtype * exp
  | ArrIndex of exp * exp
and stm =
  | Declare of primtype * ident * stm
  | Assign of exp * bool * exp
  | AssignOp of exp * bool * exp
  | If of exp * stm * stm
  | While of exp * stm
  | Assert of exp
  | Return of exp
  | ReturnVoid
  | Seq of stm * stm
  | Expr of exp
  | Nop
and args = exp list

type field = primtype * ident

type gdecl =
  | Fdefn of primtype * ident * params * stm
  | Fdecl of primtype * ident * params
  | Typedef of primtype * ident
  | Structdecl of ident
  | Structdefn of ident * (field list)

type program = gdecl list

module type PRINT =
  sig
    val pp_exp : exp -> string
    val pp_stm : stm -> string
    val pp_program : program -> string
  end

(* some string functions from Batteries Included's src/batString.ml *)
(*******)
let to_list s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) []

let of_list l =
  let res = String.create (List.length l) in
  let rec imp i = function
    | [] -> res
    | c :: l -> res.[i] <- c; imp (i + 1) l in
  imp 0 l

let repeat s n =
  let buf = Buffer.create ( n * (String.length s) ) in
  for i = 1 to n do Buffer.add_string buf s done;
  Buffer.contents buf
(*******)

(* Clean up the output of pp_program by
 * stripping extra newlines, adding spacing after
 * brackets, and removing extra spaces introduced
 * in that step. Pretty hacky, but it works well. *)
let clean s =
  let fix_string f s = of_list (f (to_list s)) in
  let rec remove_newlines = function
    | [] -> []
    | '\n'::'\n'::l' -> remove_newlines ('\n'::l')
    | x::l' -> x::(remove_newlines l')
  in
  let rec add_spacing i = function
    | [] -> []
    | '\n'::l -> '\n'::(to_list (repeat " " i))@(add_spacing i l)
    | '{'::l -> '{'::(add_spacing (i+2) l)
    | '}'::l -> '}'::(add_spacing (i-2) l)
    | x::l -> x::(add_spacing i l)
  in
  let rec rem_extra = function
    | [] -> []
    | ' '::' '::'}'::l -> '}'::(rem_extra l)
    | x::l -> x::(rem_extra l)
  in
  let strip_extra_newlines = fix_string remove_newlines in
  let add_spacing = fix_string (add_spacing 0) in
  let rem_close_spaces = fix_string rem_extra in
  s |> strip_extra_newlines |> add_spacing |> rem_close_spaces

module Print : PRINT =
  struct
    let pp_ident id = Symbol.name id

    let rec pp_type = function
      | Int -> "int"
      | Bool -> "bool"
      | Void -> "void"
      | PrId id -> pp_ident id
      | Pointer t -> pp_type t ^ "*"
      | Struct id -> "struct_" ^ Symbol.name id
      | Array t -> pp_type t ^  "[]"

    let pp_oper = function
      | PLUS -> "+"
      | MINUS -> "-"
      | TIMES -> "*"
      | DIVIDEDBY -> "/"
      | MODULO -> "%"
      | NEGATIVE -> "-"
      | BITAND -> "&"
      | BITOR -> "|"
      | BITNOT -> "~"
      | SAR -> ">>"
      | SAL -> "<<"
      | XOR -> "^"
      | LOGICAND -> "&&"
      | LOGICOR -> "||"
      | LOGICNOT -> "!"
      | EQUALS -> "=="
      | NOTEQUALS -> "!="
      | GREATER -> ">"
      | GREATEREQUAL -> ">="
      | LESS -> "<"
      | LESSEQUAL -> "<="

    let pp_list f l = String.concat ", " (List.map f l)

    let rec pp_exp = function
      | Var id     -> pp_ident id
      | ConstExp c -> Int32.to_string c
      | UnopExp (op, e) -> pp_oper op ^ "(" ^ pp_exp e ^ ")"
      | BinopExp (op, e1, e2) ->
          "(" ^ pp_exp e1 ^ " " ^ pp_oper op ^ " " ^ pp_exp e2 ^ ")"
      | Condition (e1, e2, e3) -> "(" ^ pp_exp e1
          ^ " ? (" ^ pp_exp e2 ^ ") : (" ^ pp_exp e3 ^ "))"
      | True -> "true"
      | False -> "false"
      | Call (i, a) -> pp_ident i ^ "(" ^ pp_list pp_exp a ^ ")"
      | Null -> "NULL"
      | FieldDeref(e, id) -> pp_exp e ^ "." ^ pp_ident id
      | Deref(exp) -> "(*" ^ pp_exp exp ^ ")"
      | AllocCall(ptype) -> "alloc(" ^ pp_type ptype ^ ")"
      | AllocArrCall(ptype, exp) -> "alloc_array(" ^ pp_type ptype ^ "," ^ pp_exp exp ^ ")"
      | ArrIndex(e1, e2) -> pp_exp e1 ^ "[" ^ pp_exp e2 ^ "]"

    let pp_param = function
      | (t, i) -> pp_type t ^ " " ^ pp_ident i

    let rec pp_stm = function
      | If (e, s1, s2) ->
          (match s2 with
            | Nop -> "if (" ^ pp_exp e ^ ")\n{\n" ^ pp_stm s1 ^ "}\n"
            | _ -> "if (" ^ pp_exp e ^ ")\n{\n" ^ pp_stm s1 ^ "}\nelse\n{\n" ^ pp_stm s2 ^ "}\n"
          )
      | While (e, s) -> "while (" ^ pp_exp e ^ ")\n" ^ "{\n" ^ pp_stm s ^ "}\n"
      | Seq (s1, s2) -> pp_stm s1 ^ "\n" ^ pp_stm s2 ^ "\n"
      | Declare (t, i, s) -> pp_type t ^ " " ^ pp_ident i ^ ";\n" ^ pp_stm s ^ "\n"
      | Assign (id, _, e) -> pp_exp id ^ " = " ^ pp_exp e ^ ";\n"
      | AssignOp (a,b,c) -> pp_stm (Assign (a, b, c)) (* could make this the actual asnop *)
      | Expr e -> pp_exp e ^ ";\n"
      | Return e -> "return " ^ pp_exp e ^ ";\n"
      | ReturnVoid -> "return;\n"
      | Nop -> ""
      | Assert e -> "assert (" ^ pp_exp e ^ ");\n"

    let pp_gdecl = function
      | Typedef (t, i) -> "typedef " ^ pp_type t ^ " " ^ pp_ident i ^ ";\n"
      | Fdecl (t, i, ps) -> pp_type t ^ " " ^ pp_ident i ^ " (" ^ pp_list pp_param ps ^ ");\n"
      | Fdefn (t, i, ps, s) -> pp_type t ^ " " ^ pp_ident i ^ " (" ^ pp_list pp_param ps ^ ")\n{\n" ^ pp_stm s ^ "}\n"
      | Structdefn(id, flist) -> "struct " ^ pp_ident id ^ "\n{" ^ 
                                  (List.fold_left (fun s (pt, id) -> s ^ pp_type pt ^ " " ^ pp_ident id ^ "\n") "" flist) ^ 
                                  "}\n"
      | Structdecl(id) -> "struct " ^ pp_ident id ^ "\n"

    let rec pp_gdecllist = function
      | [] -> ""
      | x::l -> (pp_gdecl x) ^ (pp_gdecllist l)

    let pp_program p = pp_gdecllist p |> clean
  end
