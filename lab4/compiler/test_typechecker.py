import glob
import subprocess
import sys

tests = glob.glob("../tests0/*.l4")+glob.glob("../tests1/*.l4")+glob.glob("../tests2/*.l4")

def is_error(fn):
    with open(fn,'r') as f:
        first_line = f.readline()
        return "error" in first_line

errors = [fn for fn in tests if is_error(fn)]
ok = [fn for fn in tests if not is_error(fn)]

#should = []
shouldnt = []

#for fn in errors:
#  #print fn
#    fail = subprocess.call(["bin/l4c","-l","15411.h0",fn])
#    if not fail:
#        should.append(fn)

for fn in ok:
  #print fn
    fail = subprocess.call(["bin/l4c","-l","15411.h0",fn])
    raw_name = ".".join(fn.split(".")[:-1])
    fail2 = subprocess.call(["gcc","-m64",raw_name+".s","15411.c","l4rt.c","-o",raw_name])
    subprocess.call(["rm", "-f", raw_name+".s"])
    subprocess.call(["rm", "-f", raw_name])
    if fail or fail2:
        shouldnt.append(fn)

#print "Should have failed:",should
print "Shouldn't have failed:",shouldnt

test_len = len(tests)
print "{}/{} tests passed.".format(len(tests)-len(shouldnt),len(tests))
