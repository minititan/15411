(* Goes from the transated ast tree to an intermediate representation
  consisting of straight line instructions. This is then converted to a block
  format later for optimizations.
 *)

module T = Tree
module S = Storage
module I = Ir

let raise_error msg = (ErrorMsg.error None msg; raise ErrorMsg.Error)

(*
  let rets = (match size with
              | T.QWORD -> true
              | T.DWORD -> false
            )
  in

  match exp with
  | T.Const(i) -> (I.IMM(i), rets)
  | T.Temp(t) -> (I.TEMP(t), rets)
  | T.Deref(i, t) -> 
      (match t with
      | T.Temp(tmp) -> (I.DEREF(i, tmp), rets)
      | _ -> ErrorMsg.error None ("Dereferencing a non-temp"); raise ErrorMsg.Error
      )
      
  | T.Call(id, elist) -> ErrorMsg.error None ("Cannot have call in operand"); raise ErrorMsg.Error
  | T.Binop _ | T.Unop _ -> ErrorMsg.error None ("Cannot match expression to operand"); raise ErrorMsg.Error
*)

(*

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * texp * texp
  | Unop of unop * texp
  | Call of Symbol.symbol * (texp list)
  | Deref of Int32.t * texp (* int for offset *)
and texp = 
  | DWORD of exp
  | QWORD of exp

*)

let munch_texp = function
  | T.WORD e' -> (I.WORD, e')
  | T.DWORD e' -> (I.DWORD, e')
  | T.QWORD e' -> (I.QWORD, e')

let process_deref e_size = function
  | T.Deref (offset, texp') -> (match munch_texp texp' with
    | (temp_size, T.Temp t) -> (e_size, I.DEREF(offset, (temp_size, I.TEMP(t))))
    | _ -> raise_error "Dereferencing a non-temp."
    )
  | _ -> raise_error "Non-deref in process_deref."

let get_operand texp =
  let (e_size, e_exp) = munch_texp texp in (match e_exp with
  | T.Const i -> (e_size, I.IMM i)
  | T.Temp t -> (e_size, I.TEMP t)
  | T.Deref (_, _) -> process_deref e_size e_exp
  | T.Binop _ | T.Unop _ | T.Call _ -> raise_error "Invalid expression in munch_operand."
  )

(* used when doing mapi over arguments for call *)
let process_arg i e =
  let (size_e,exp_e) = get_operand e in
  [I.MOV((size_e, I.ARGBOTTOM i), (size_e, exp_e))]

let munch_mov src dst =
  let (src_size, src_exp) = munch_texp src in
  match src_exp with
  | T.Const i -> [I.MOV(get_operand dst, (src_size, I.IMM i))]

  | T.Temp t -> [I.MOV(get_operand dst, (src_size, I.TEMP t))]

  (* don't use source size in binop *)
  | T.Binop (op, e1, e2) -> [I.BINOP(op, get_operand dst, get_operand e1, get_operand e2)]

  | T.Unop (op, e1) -> [I.UNOP(op, get_operand dst, get_operand e1)]

  | T.Call (id, elist) ->
    let setup_args = List.flatten (List.mapi process_arg elist) in
    setup_args @ [I.CALL(id); I.MOV(get_operand dst, (src_size, I.REG(S.EAX)))]

  | T.Deref _ -> [I.MOV(get_operand dst, process_deref src_size src_exp)]

(*
type command =
  | Mov of texp * texp (* the mov to exp can only be temp or deref of temp *)
  | VoidCall of Symbol.symbol * (texp list)
  | If of cmp * Label.label * Label.label (* if true goto Label.label *)
  | Goto of Label.label
  | Label of Label.label
  | Return of texp
  | ReturnVoid
*)

let munch_command = function
  | T.Mov (dst, src) -> munch_mov src dst

  | T.If(T.Cmp(cmpop, e1, e2), l1, l2) ->
    [I.IF(cmpop, get_operand e1, get_operand e2, l1, l2)]

  | T.Goto(l) -> [I.GOTO(l)]

  | T.CheckDeref(t) -> [I.CHECKDEREF(get_operand t)]

  | T.Label(l) -> [I.LABEL(l)]

  | T.ReturnVoid -> [I.RET]

  | T.VoidCall(id, elist) ->
    let irlst = List.flatten (List.mapi process_arg elist) in
    irlst @ [I.CALL(id)]

  | T.Return ret_val ->
    let (ret_size, ret_op) = get_operand ret_val in
    [I.MOV((ret_size, I.REG S.EAX), (ret_size, ret_op)); I.RET]

let create_arg_moves temp_arg_list =
  (* should we be using a different sized comparison? *)
  let process_toparg i texp =
    let (typ_size, tmp) = (match texp with
      | T.WORD  (T.Temp t) -> (I.DWORD, t)
      | T.DWORD (T.Temp t) -> (I.DWORD,  t)
      | T.QWORD (T.Temp t) -> (I.QWORD, t)
      | _ -> raise_error "Non-temp found in toparg."
    ) in
    I.MOV((typ_size, I.TEMP tmp), (typ_size, I.ARGTOP i))
  in
  List.mapi process_toparg temp_arg_list

let munch_func (_, id, temp_arg_list, cmdlist) = 
  let arg_moves = create_arg_moves temp_arg_list in
  match cmdlist with
  | cmd::restcmds -> (id, (munch_command cmd) @ arg_moves @ (List.flatten (List.map (munch_command) restcmds)) )
  | [] -> (id, [])

(*
let hack_operand num_args = function
  | I.IMM i -> I.IMM i
  | I.REG r -> I.REG r
  | I.TEMP t ->
    let t_value = Temp.get_int t in
    if t_value > 6 && t_value < num_args then (* temp > 6, arg > 5 *)
      I.ARGTOP (t_value-1) (* hack goes here *)
    else
      I.TEMP t
  | I.ARGTOP i -> I.ARGTOP i
  | I.ARGBOTTOM i -> I.ARGBOTTOM i

let hack_command num_args = function
  | I.BINOP (b, t1, t2, t3) -> I.BINOP (b, hack_operand num_args t1, hack_operand num_args t2, hack_operand num_args t3)
  | I.UNOP (u, t1, t2) -> I.UNOP (u, hack_operand num_args t1, hack_operand num_args t2)
  | I.MOV (t1, t2) -> I.MOV (hack_operand num_args t1, hack_operand num_args t2)
  | I.DIRECTIVE s -> I.DIRECTIVE s
  | I.COMMENT s -> I.COMMENT s
  | I.IF (c, t1, t2, l1, l2) -> I.IF (c, hack_operand num_args t1, hack_operand num_args t2, l1, l2)
  | I.CALL s -> I.CALL s
  | I.GOTO l -> I.GOTO l
  | I.LABEL l -> I.LABEL l
  | I.RET -> I.RET

let hack_commandlist ir num_args =
  List.map (hack_command num_args) ir
  (* for i in [6,num_temps_needed-1], map temp (i+1) to i) *)

let munch_func (t, id, temp_arg_list, cmdlist) =
  let (_, original_ir) = munch_func' (t, id, temp_arg_list, cmdlist) in
  let num_args = List.length temp_arg_list in
  let hacked_ir = hack_commandlist original_ir num_args in
  (id, hacked_ir)
*)

let codegen flist =
  List.map munch_func flist




