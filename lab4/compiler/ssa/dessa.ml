(* Converts multemps to temps *)
module MTM = Multemp.Map
module SIR = Ssair
module IR = Ir

let raise_error s = ErrorMsg.error None s; raise ErrorMsg.Error

let convert_op (s,op) mmap = 
	match op with
	| SIR.IMM i -> ((s, IR.IMM(i)), mmap)
    | SIR.REG r -> ((s, IR.REG(r)), mmap)
    | SIR.MTEMP t ->
    	(match MTM.find' t mmap with
    	| Some(tmp) -> ((s, IR.TEMP(tmp)), mmap)
    	| None -> let tmp = Temp.create() in
    			((s, IR.TEMP(tmp)), MTM.add t tmp mmap)
    	)

    | SIR.DEREF(i, (s', t)) ->
    	(match t with
    	| SIR.MTEMP(t') -> 
	    	(match MTM.find' t' mmap with
	    	| Some(tmp) -> ((s, IR.DEREF(i, (s', IR.TEMP(tmp)))), mmap)
	    	| None -> let tmp = Temp.create() in
	    			((s, IR.DEREF(i, (s', IR.TEMP(tmp)))), MTM.add t' tmp mmap)
	    	)

	    | SIR.IMM(i') -> ((s, IR.DEREF(i, (s', IR.IMM(i')))), mmap)
	    | SIR.REG(r) -> ((s, IR.DEREF(i, (s', IR.REG(r)))), mmap)
	    | SIR.ARGTOP(i') -> ((s, IR.DEREF(i, (s', IR.ARGTOP(i')))), mmap)
	    | SIR.ARGBOTTOM(i') -> ((s, IR.DEREF(i, (s', IR.ARGBOTTOM(i')))), mmap)
	    | SIR.DEREF(_) -> raise_error "Cannot have deref of deref at this stage"
		)

    | SIR.ARGTOP i -> ((s, IR.ARGTOP(i)), mmap)
    | SIR.ARGBOTTOM i -> ((s, IR.ARGBOTTOM(i)), mmap)

let rec convert_ssair block mmap : Ir.instr list * Temp.t Multemp.Map.t = 
	match block with
	| [] -> ([], mmap)
	| ir::rest -> 
		(match ir with
		| SIR.BINOP (op, op1, op2, op3) -> 
			let (nop1, m1) = convert_op op1 mmap in
			let (nop2, m2) = convert_op op2 m1 in
			let (nop3, m3) = convert_op op3 m2 in

			let (rest_con, fmap) = convert_ssair rest m3 in
			(IR.BINOP(op, nop1, nop2, nop3) :: rest_con, fmap)

	    | SIR.UNOP (op, d, op1) -> 
	    	let (nd, m1) = convert_op d mmap in
			let (nop, m2) = convert_op op1 m1 in
			let (rest_con, fmap) = convert_ssair rest m2 in

			(IR.UNOP(op, nd, nop) :: rest_con, fmap)

	    | SIR.IF (op, op1, op2, l1, l2) -> 
	    	let (nop1, m1) = convert_op op1 mmap in
			let (nop2, m2) = convert_op op2 m1 in
			let (rest_con, fmap) = convert_ssair rest m2 in
			(IR.IF(op, nop1, nop2, l1, l2) :: rest_con, fmap)

	    | SIR.MOV (d, s) -> 
	    	let (nd, m1) = convert_op d mmap in
			let (ns, m2) = convert_op s m1 in
			let (rest_con, fmap) = convert_ssair rest m2 in

			(IR.MOV(nd, ns) :: rest_con, fmap)

	    | SIR.CHECKDEREF e -> 
	    	let (ne, m1) = convert_op e mmap in
	    	let (rest_con, fmap) = convert_ssair rest m1 in
	    	(IR.CHECKDEREF(ne) :: rest_con, fmap)

	    | SIR.RET -> let (rest_con, fmap) = convert_ssair rest mmap in
	    			(IR.RET :: rest_con, fmap)

	    | SIR.GOTO l -> 
	    		let (rest_con, fmap) = convert_ssair rest mmap in
	    			(IR.GOTO(l):: rest_con, fmap)
	    | SIR.LABEL l -> 
	    			let (rest_con, fmap) = convert_ssair rest mmap in
	    			(IR.LABEL(l) :: rest_con, fmap)
	    | SIR.CALL(id) -> 
	    			let (rest_con, fmap) = convert_ssair rest mmap in
	    			(IR.CALL(id) :: rest_con, fmap)
		)

let rec convert_blocks bl mtemp_map = 
	match bl with
	| [] -> []
	| block::rest -> 
		let (l, jumpset, ssa_irlist, inc, outg) = block in
		let (new_irlist, new_map) = convert_ssair ssa_irlist mtemp_map in
		(l, jumpset, new_irlist) :: (convert_blocks rest new_map)


let convert bf = 
	let (fname, blocks, jumpmap) = bf in
	let converted_blocks = convert_blocks blocks MTM.empty in
	(fname, converted_blocks, jumpmap)


let dessafier bp = 
	let () = Temp.reset () in
	List.map (convert) bp




