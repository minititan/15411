(* Module to do copy and constant propagation on the multemp type *)
open Ssair
module MTM = Multemp.Map
module T = Tree
module MTS = Multemp.Set

let raise_error s = 
	ErrorMsg.error None s; raise ErrorMsg.Error


type inttemp = 
	| CONST of Int32.t
	| TMP of Multemp.t


class contextHolder = 
object
	val ref_map = MTM.empty
	val copy_map = MTM.empty

	(* adds a reference of t2 to t1, so in future cases t1 can be replaced with t2.
		If existing references exist for t1 then t2 points to those. Preference is given
		to have a reference for a constant *)
	method add_ref (s1, t1 : Ssair.operand) (s2, t2 : Ssair.operand) = 
		match t1, t2 with
		| MTEMP(t), IMM(i) -> {< ref_map = MTM.add t (CONST i) ref_map >}
		| MTEMP(t), MTEMP(t') ->
			let new_copy_map = 
				(match MTM.find' t' copy_map with
				| None -> MTM.add t t' copy_map
				| Some v -> MTM.add t v copy_map
				) 

			in
			
			(match MTM.find' t' ref_map with
			| None -> {< ref_map = MTM.add t (TMP t') ref_map; copy_map = new_copy_map >}
			| Some(v) -> {< ref_map = MTM.add t v ref_map; copy_map = new_copy_map >}
			)

		| MTEMP(t), _ -> {<>}
		| REG(r), _ -> {<>}
		| ARGBOTTOM(i), _ -> {<>}
		| DEREF(_), _ -> {<>}
		| _ -> raise_error ("Cannot have this kind of reference: " ^ 
							Ssair.Print.pp_operand (s1, t1) ^ " <- " ^ Ssair.Print.pp_operand (s2, t2))

	method get_copy_ref t = 
		MTM.find' t copy_map

	(* prefers to retuns the constant binding if possible *)
	method get_value t : (inttemp option) = 
		MTM.find' t ref_map

end;;


(* converts the operand if a temp is present *)
let convert_op (s, op) env = 
	match op with
	  | IMM(_) | REG(_) | ARGTOP(_) | ARGBOTTOM(_) -> (false, (s, op))
	  
	  | MTEMP(t) -> 
	  		(match env#get_value t with
	  		| None -> (false, (s, op))
	  		| Some(x) ->
	  			(match x with
	  			| CONST c -> (true, (s, (IMM(c))))
	  			| TMP tmp -> (true, (s, MTEMP(tmp)))
	  			)
	  		)

	  (* for derefs we do not do constant propagation, only copy *)
	  | DEREF(i, (s', t)) -> 
	  		(match t with
	  		| MTEMP(mt) -> 
		  		(match env#get_copy_ref mt with
	  			| None -> (false, (s, op))
	  			| Some(tmp) -> (true, (s, DEREF(i, (s', MTEMP(tmp)))))
		  		)
		  	| _ -> (false, (s, op))
		  	)

(* applies the operation to the constants if its not effectful *)
let apply_binop op c1 c2 = 
	match op with
	| T.ADD -> Some(Int32.add c1 c2)
  	| T.SUB -> Some(Int32.sub c1 c2)
  	| T.MUL -> Some(Int32.mul c1 c2)
  	| T.DIV -> None
  	| T.MOD -> None
  	| T.BAND -> Some(Int32.logand c1 c2)
  	| T.BOR -> Some(Int32.logor c1 c2)
  	| T.SAR -> None
  	| T.SAL -> None
  	| T.XOR -> Some(Int32.logxor c1 c2)


(* if non-effect binop between 2 constants then fold and transform to a move instruction *)
let reduce_binop ir = 
	match ir with
	| BINOP(op, d, op1, op2) ->
		
		(match d, op1, op2 with

		(* can only reduce if both the operands are integers *)
		| (sd, dest), (s1, IMM(i1)), (s2, IMM(i2)) -> 
			let res = apply_binop op i1 i2 in
			
			(match res with
			| None -> None
			| Some(res_i) -> Some (MOV(d, (sd, IMM(res_i))))
			)

		| _ -> None
		)

	| _ -> raise_error "Not a binop to reduce"

let apply_unop op c =
	match op with
	| T.NEG -> Int32.neg c 
	| T.BNOT -> Int32.lognot c

let reduce_unop ir = 
	match ir with
	| UNOP(op, d, s) -> 
		(match d, s with
		| (sd, dest), (ss, IMM(si)) ->
			let res = apply_unop op si in
			Some (MOV(d, (sd, IMM(res))))

		| _ -> None 

		)

	| _ -> raise_error "Not a unop to reduce"


let is_restricted d restricted = 
	match d with
	| (s, MTEMP(t)) -> MTS.mem t restricted
	| (s, _) -> false

(* The main thing to be careful about is that we cannot copy propagate when the temp being moved
	to is a parameter 
	TODO: WORRY ABOUT THE SIZES THAT YOU ARE PROPAGATING!!!! ALSO PAY ATTENTION IF 
		OPERAND IS A PARAMETER 
*)
let propagate block_list init_env = 
	
	let helper (env, change, blocks_sofar) (l, jumps, irlist, inc, out) = 
		
		(* looks at a single instruction and propagates the environment as well *)
		let my_helper (myenv, change_made, ir_sofar) ir = 
			match ir with
			| BINOP (op, op1, op2, op3) -> 
				let (ch1, cop2), (ch2, cop3)  = (convert_op op2 myenv, convert_op op3 myenv) in
				let nir = BINOP(op, op1, cop2, cop3) in

				(match reduce_binop nir with
					| None -> 
						
						(* cannot have immediate for div and mod *)
						(match op with
						| T.DIV | T.MOD -> 
							(match cop3 with
							| (s, IMM(c)) -> (myenv, change_made || ch1, ir_sofar @ [BINOP(op, op1, cop2, op3)])
							| _ -> (myenv, change_made || ch1 || ch2, ir_sofar @ [nir])
							)

						| _ -> (myenv, change_made || ch1 || ch2, ir_sofar @ [nir])
						)

					| Some(reduce_ir) -> 
						(match reduce_ir with
						| MOV(d, s) -> 
						 	let new_env = myenv#add_ref d s in
							(new_env, true, ir_sofar @ [reduce_ir])
						)

				)

		    | UNOP (op, d, op1) -> 
		    	let (ch, cop1) = (convert_op op1 myenv) in
		    	let nir = UNOP(op, d, cop1) in

		    	(match reduce_unop nir with
		    	| None -> (myenv, change_made || ch, ir_sofar @ [nir])
		    	| Some(new_ir) -> 
		    		match new_ir with
		    		| MOV(d, s) -> 
						let new_env = myenv#add_ref d s in
						(new_env, true, ir_sofar @ [new_ir])
		    	)


		    | IF (op, op1, op2, l1, l2) -> 
		    	let (ch1, cop1), (ch2, cop2)  = (convert_op op1 myenv, convert_op op2 myenv) in
		    	let nir = IF(op, cop1, cop2, l1, l2) in
		    	(myenv, change_made || ch1 || ch2, ir_sofar @ [nir])


		    | MOV (d, s) -> 
		    	let (ch, cs) = convert_op s myenv in
		    	let new_ir = MOV(d, cs) in
		    	let new_env = myenv#add_ref d s in
		    	(new_env, change_made || ch, ir_sofar @ [new_ir])


		    | CHECKDEREF e -> 
	    		(match e with
	    		| (s, MTEMP(mt)) -> 
	    			(match myenv#get_copy_ref mt with
	    			| None -> (myenv, change_made, ir_sofar @ [CHECKDEREF(e)])
	    			| Some new_mt -> (myenv, true, ir_sofar @ [CHECKDEREF((s, MTEMP(new_mt)))])
	    			)
	    		| (s, _) -> raise_error "Checkderef on a non-temp"
	    		)
	    

		    | RET | GOTO _ | LABEL _ | CALL _ ->
		    	(myenv, change_made, ir_sofar @ [ir]) 


		in
			let (final_env, changes, new_irlist) = List.fold_left my_helper (env, change, []) irlist in
			(final_env, change || changes, blocks_sofar @ [(l, jumps, new_irlist, inc, out)])

	in
		let (final_env, final_change, new_blocks) =  List.fold_left helper (init_env, false, []) block_list in
		(final_change, new_blocks, final_env)


(* recursively calls itself till no changes made *)
let rec apply_propagations change_made blocks env : Ssair.ssablock list = 
	if (change_made) then
		let (change, new_blocks, new_env) = propagate blocks env in
		apply_propagations change new_blocks new_env
	else
		blocks

(* optimizes a ssair blcok program *)
let optimize blocks = 
	let env = new contextHolder in
	apply_propagations true blocks env







