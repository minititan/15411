module SI = Ssair
module I = Ir
module TM = Temp.Map
module TS = Temp.Set
module LM = Label.Map
module LS = Label.Set
module MT = Multemp
module MTS = Multemp.Set
module MTM = Multemp.Map
module RS = Regalloc_util.IntSet

let raise_error s = 
	ErrorMsg.error None s; raise ErrorMsg.Error

exception ShouldExist

(* blockinfo contains a mapping from block label to (blocks_jump_set, start_multemps, ending_multemps) *)
class holderBlockInfo =
object(this)
	val blockstart = LM.empty
	val blockend = LM.empty
	val jumpmap = LM.empty

	method printJumpMap = 
		print_endline (LM.fold (fun k (i, o) st -> 
								st ^ Label.name k ^ " -> " ^ 
									"\n\tIN: " ^ (LS.fold (fun e s -> s ^ Label.name e ^ ",") i "") ^ 
									"\n\tOUT: " ^ (LS.fold (fun e s -> s ^ Label.name e ^ ",") o "") ^ "\n") jumpmap "")

	method set_jumpmap jmap = 
		{< jumpmap = jmap >}

	method getParamTemps bl = 
		LM.find bl blockstart

	method getJumpTemps bl = 
		LM.find bl blockend

	method rerouteJump b1 b2 newb = 
		let (b1_in, b1_out) = LM.find b1 jumpmap in
		let (b2_in, b2_out) = LM.find b2 jumpmap in
		let n1 = LM.add b1 (b1_in, LS.add newb (LS.remove b2 b1_out)) jumpmap in
		let n2 = LM.add b2 (LS.add newb (LS.remove b1 b2_in), b2_out) n1 in
		{< jumpmap = LM.add newb (LS.singleton b1, LS.singleton b2) n2 >}

	method addBlockLabel bl = 
		{< jumpmap = LM.add bl (LS.empty, LS.empty) jumpmap >}

	method addJump b1 b2 m = 
		let (b1_in, b1_out) = LM.find b1 m in
		let (b2_in, b2_out) = LM.find b2 m in
		let n1 = LM.add b1 (b1_in, LS.add b2 b1_out) m in
		LM.add b2 (LS.add b1 b2_in, b2_out) n1

	method addJumps block jump_set =
		let new_jmap = LS.fold (fun b m -> this#addJump block b m) jump_set jumpmap in
		{< jumpmap = new_jmap >}

	method addBlockStart block ms =
		{< blockstart = LM.add block ms blockstart >}

	method addBlockEnd block ms = 
		{< blockend = LM.add block ms blockend >}

	(* replace the params with the replacements *)
	method updateAllParams rmap = 
		let new_blockend = 
			LM.mapi (fun bl out_params ->
				let tbl = Hashtbl.create (MTS.cardinal out_params) in
				(* insert self map for everything in out params *)
				(* fold over rmap -> any key in hash table gets updated, otherwise ignored *)
				let () = MTS.iter (fun e -> Hashtbl.add tbl e e) out_params in
				let replace init updated =
					if Hashtbl.mem tbl init then
						Hashtbl.replace tbl init updated
					else
						()
				in
				let () = MTM.iter replace rmap in
				Hashtbl.fold (fun _ v s -> MTS.add v s) tbl MTS.empty
												
				) blockend in
		{< blockend = new_blockend >}

		

	(* looks at the parameters of the block and looks at all the blocks jumping to it.
		If we have just one block jumping to it then the parameter is replaced by that one.
		If we have multiple then if there are only two subtemps, that of the same as block
		and of different then it is replaced by the different one. A mapping is returned 
		for changed parameters which can be used to replace in the ir list 

		Also updates the maps internally 
	*)
	method get_multemp_replacements block =
		let (inc, out) = LM.find block jumpmap in
		
		(* returns an updated map containg what the subtemp can be replaces with. If None then it means it can 
			be removed as a param. If its Some(t) then it can be removed as a param and must be replaced by t *)
		let helper (p, c) mapping = 

			(* collect all the incoming params of same family *)
			let in_params_same_type = 
					LS.fold (fun in_block mult_set -> 
							let in_params = LM.find in_block blockend in
							let same_type = MTS.filter (fun (p1, c1) -> p = p1) in_params in
							MTS.union mult_set same_type) inc MTS.empty in

			(* remove the type of the current block param *)
			let in_params_filtered = MTS.filter (fun (p1, c1) -> c1 <> c) in_params_same_type in

			(* this means we dont need the label at all *)
			if (MTS.cardinal in_params_filtered = 0) then
				MTM.add (p,c) (p,c) mapping
			else if (MTS.cardinal in_params_filtered = 1) then 
				let to_add = MTS.choose in_params_filtered in
				MTM.add (p,c) to_add mapping
			else 
				mapping

		in

		let params = LM.find block blockstart in

		(* if only one block entering then parameters are exactly those *)
		if (LS.cardinal inc = 1) then
			let incoming_block = LS.choose inc in
			let incoming_params = LM.find incoming_block blockend in
			MTS.fold (fun (p,c) mp -> 
						let same_type_incoming = MTS.filter (fun (p1, c1) -> p1 = p) incoming_params in
						if (MTS.cardinal same_type_incoming = 1) then
							MTM.add (p,c) (MTS.choose same_type_incoming) mp
						else
							MTM.add (p,c) (p,c) mp ) params MTM.empty
		
		(* otherwise we need to check if different type is only one *)
		else
			MTS.fold helper params MTM.empty


	(* takes a replacement map and fixes the parameters 
		if a parameter is present in the map then it is removed from the 
		incoming params and the outgoing params are
		updated appropriately *)
	method updateParameters bl rmap =
		let in_params = LM.find bl blockstart in
		let out_params = LM.find bl blockend in

		let (new_in, new_out) = 
			MTM.fold (fun p p' (inp, outp) -> 
							let ninp = MTS.remove p inp in
							let noutp = 
								if (MTS.mem p outp) then 
									MTS.add p' (MTS.remove p outp)
								else
									outp
							in
								(ninp, noutp)

					) rmap (in_params, out_params) in

		{< blockstart = LM.add bl new_in blockstart; blockend = LM.add bl new_out blockend >}

	method get_jumpmap = jumpmap

end;;


let rec convert_dest (s, op) = 
	match op with
	| I.TEMP(t) -> (s, SI.MTEMP(MT.convert_temp t))
	| I.IMM(i) -> (s, SI.IMM(i))
	| I.REG(r) -> (s, SI.REG(r))
	| I.DEREF(i, (s', t)) -> 
		let st = (match t with
				 | I.TEMP(t') ->  
					(match MT.get_subtemp t' with
					 | None -> raise_error "Deref of temp does not have sub temp"
					 | Some x -> SI.MTEMP(x)
					)
				 | I.IMM(i') -> SI.IMM(i')
				 | I.REG(r) -> SI.REG(r)
				 | I.ARGTOP(i') -> SI.ARGTOP(i')
				 | I.ARGBOTTOM(i') -> SI.ARGBOTTOM(i')
				 | I.DEREF(_) -> raise_error "Cannot have deref of deref here"
				)

		in 

		(s, SI.DEREF(i, (s', st)))

	| I.ARGTOP(i) -> (s, SI.ARGTOP(i))
	| I.ARGBOTTOM(i) -> (s, SI.ARGBOTTOM(i))

let convert_src (s, op) = 
	match op with
	| I.TEMP(t) -> 
		(match MT.get_subtemp t with
		| None -> (s, SI.MTEMP(MT.convert_temp t))
		| Some st -> (s, SI.MTEMP(st))
		)

	| I.IMM(i) -> (s, SI.IMM(i))
	| I.REG(r) -> (s, SI.REG(r))
	| I.DEREF(i, (s', t)) -> 
		let st = (match t with
					| I.TEMP(t') -> 
						(match MT.get_subtemp t' with
						 | None -> ErrorMsg.error None ("deref source should be a subtemp"); raise ErrorMsg.Error
						 | Some x -> SI.MTEMP(x)
						)
					| I.IMM(i') -> SI.IMM(i')
					| I.REG(r) -> SI.REG(r)
					| I.ARGTOP(i') -> SI.ARGTOP(i')
					| I.ARGBOTTOM(i') -> SI.ARGBOTTOM(i')
					| I.DEREF(_) -> raise_error "Cannot have deref of deref here"
				)
		in 

		(s, SI.DEREF(i, (s', st)))

	| I.ARGTOP(i) -> (s, SI.ARGTOP(i))
	| I.ARGBOTTOM(i) -> (s, SI.ARGBOTTOM(i))

(* does an initial sweep converting temps to multemps and adding in all the jumps and 
	block information *)
let rec sweep_block irlist =
	match irlist with
	| [] -> []
	| ir::rest -> 
		let convert_ir =
			(match ir with
			| I.BINOP(oper, d_op, op1, op2) -> SI.BINOP(oper, convert_dest d_op, convert_src op1, convert_src op2)

			| I.UNOP(oper, d_op, op) -> SI.UNOP(oper, convert_dest d_op, convert_src op)

			| I.MOV(dst, src) -> SI.MOV(convert_dest dst, convert_src src)

			| I.GOTO(l) -> SI.GOTO(l)

			| I.IF(cop, op1, op2, l1, l2) -> SI.IF(cop, convert_src op1, convert_src op2, l1, l2)

			| I.CALL fn -> SI.CALL(fn)
			| I.CHECKDEREF op -> SI.CHECKDEREF(convert_src op)

			| I.LABEL l -> SI.LABEL(l)
			| I.RET -> SI.RET
			)
		
		in
		
		convert_ir::(sweep_block rest)


(* initial conversion from normal program to ssa program.
	This is not minimized *)
let funcssa bp blockInfo blockLive =

	let rec helper bp blockinfo = 
		match bp with 
		| [] -> ([], blockinfo)

		| (l, jumpset, irlist)::rest ->
			
			let block_inlive_temps = 
				RS.fold (fun i s -> 
							if (i < 0) then s else TS.add (Temp.get_temp i) s) (LM.find l blockLive) TS.empty in

			let block_start_multemps = 
				TS.fold (fun t s  -> let new_mt = MT.convert_temp t in MTS.add new_mt s) block_inlive_temps MTS.empty in

			(* sweep across block updating temps as well *)
			let new_irlist = sweep_block irlist in
			
			(* get the temps that are live in all the jump targets since we need to exit the block  with these *)
			let outgoing_temps = LS.fold (fun bl ts -> 
											RS.fold (fun i s -> 
														if (i < 0) then s else
														TS.add (Temp.get_temp i) s) 
											(LM.find bl blockLive) ts) 
										jumpset TS.empty 
			
			in

			let block_end_multemps = 
				TS.fold (fun t s -> match MT.get_subtemp t with
									| None -> raise_error ("temp at end of block does not have subtemp: " ^ 
															Temp.name t ^ " " ^ Label.name l)
									| Some(mt) -> MTS.add mt s) outgoing_temps MTS.empty in


			(* If the multemps are same across block then no need to increase count *)
			
			(* add block start param temps *)
			let newBlockinfo1 = blockinfo#addBlockStart l block_start_multemps in

			(* add in temps that exit this block *)
			let newBlockinfo2 = newBlockinfo1#addBlockEnd l block_end_multemps in

			let (rest_done, finalblockinfo) = helper rest newBlockinfo2 in
			((l, jumpset, new_irlist, block_start_multemps, block_end_multemps)::rest_done, finalblockinfo)

	in
		helper bp blockInfo


let convert_op (s, op) rmap =
	let apply tmp = 
		match MTM.find' tmp rmap with
		| None -> tmp
		| Some(x) -> x
	in

	match op with
	| SI.MTEMP(t) -> (s, SI.MTEMP (apply t))
	| SI.IMM(i) -> (s, SI.IMM(i))
	| SI.REG(r) -> (s, SI.REG(r))
	| SI.DEREF(i, (s', t)) -> 
		(match t with
		| SI.MTEMP(mt) -> (s, SI.DEREF(i, (s', SI.MTEMP(apply mt))))
		| _ -> (s, op)
		)

	| SI.ARGTOP(i) -> (s, SI.ARGTOP(i))
	| SI.ARGBOTTOM(i) -> (s, SI.ARGBOTTOM(i))


(* applies the replacements to temps in the block *)
let rec apply_replacements irlist replace_map =
	match irlist with
	| [] -> []
	| ir::rest ->
		let new_ir = 
		(match ir with
		| SI.BINOP(oper, d, s1, s2) -> SI.BINOP(oper, convert_op d replace_map, 
													  convert_op s1 replace_map,
													  convert_op s2 replace_map)

		| SI.UNOP(oper, d, s) -> SI.UNOP(oper, convert_op d replace_map, 
											   convert_op s replace_map)

		| SI.MOV(d, s) -> SI.MOV(convert_op d replace_map, convert_op s replace_map)

		| SI.DIRECTIVE(s) -> SI.DIRECTIVE(s)
		| SI.COMMENT(s) -> SI.COMMENT(s)

		| SI.IF(cm, op1, op2, l1, l2) -> SI.IF(cm, convert_op op1 replace_map, 
												   convert_op op2 replace_map, l1, l2)
		| SI.CALL(f) -> SI.CALL(f)
		| SI.GOTO(l) -> SI.GOTO(l)

		| SI.CHECKDEREF(t) -> SI.CHECKDEREF(convert_op t replace_map)

		| SI.LABEL(l) -> SI.LABEL(l)
		| SI.RET -> SI.RET
		)

		in
			new_ir::(apply_replacements rest replace_map)


(* minimizes the current block and updates blockinfo *)
let rec minimize_ssa_helper blockinfo blist change = 
	match blist with
	| [] -> ([], change, blockinfo)
	| block::rest -> 
		let (l, jset, irlist, inc, outg) = block in
		let replacement_map = blockinfo#get_multemp_replacements l in

		if (MTM.cardinal replacement_map = 0) then 
			let (rest_min, ch, finalblockinfo) = minimize_ssa_helper blockinfo rest change in
			(block::rest_min, ch, finalblockinfo)

		else
			(*let () = print_endline ((Label.name l) ^ ": " ^  (MTM.fold (fun k v s -> MT.name k ^ " -> " ^ MT.name v ^ " ; " ^ s) replacement_map "")) in*)
			let new_irlist = apply_replacements irlist replacement_map in
			
			let b1 = blockinfo#updateParameters l replacement_map in
			let b2 = b1#updateAllParams replacement_map in

			let new_rest = List.map (fun (la, jmp, irl, i, o) -> 
									let new_irl = apply_replacements irl replacement_map in
									(la, jmp, new_irl, b2#getParamTemps la, b2#getJumpTemps la)) rest in

			(* fix the parameters for start and end of block *)

			let (rest_min, ch, finalblockinfo) = minimize_ssa_helper b2 new_rest true in
			((l, jset, new_irlist, b2#getParamTemps l, b2#getJumpTemps l)::rest_min, true, finalblockinfo)


let rec minimizer ssa_blocks blockinfo change_made = 
	if (change_made) then
	(*let () = print_endline "minimizing..." in*)
	let (new_blocks, change, newBlockinfo) = minimize_ssa_helper blockinfo ssa_blocks false in
	minimizer new_blocks newBlockinfo change

	else (ssa_blocks, blockinfo)


(* returns a block that moves from same subtype temps in order to jump from b1 to b2 *)
let get_jump_block b1 b2 blockInfo =
	let b2start = blockInfo#getParamTemps b2 in
	let b1end = blockInfo#getJumpTemps b1 in
	let irlist = MTS.fold (fun (p,c) block -> 
					let same_type_s = MTS.filter (fun (p', c') -> p = p') b1end in
					match MTS.cardinal same_type_s with
					| 0 -> block
					| 1 -> let same_type_temp = MTS.choose same_type_s in
							(SI.MOV((I.QWORD, SI.MTEMP(p,c)), (I.QWORD, SI.MTEMP(same_type_temp))))::block
					| _ -> raise_error "Have multiple type temps in jump") b2start [] in

	if (List.length irlist = 0) then (None, blockInfo)
	else
	
	let new_bl = Label.create() in
	let newBlockinfo = blockInfo#rerouteJump b1 b2 new_bl in
	(Some((new_bl, LS.singleton b2, SI.LABEL(new_bl)::(irlist @ [SI.GOTO(b2)]), MTS.empty, MTS.empty)), newBlockinfo)

exception Unexpected

(* a block can have either 0, 1 or 2 jumps *)
let get_add_blocks block blockInfo = 
	let (bl, jump_set, irlist, in_tmps, out_tmps) = block in
	match (LS.cardinal jump_set) with
	| 0 -> ([block], blockInfo)

	(* ends in a goto *)
	| 1 -> (match get_jump_block bl (LS.choose jump_set) blockInfo with
			| (None, newBlockinfo) -> ([block], newBlockinfo)
			| (Some(jmp_block), newBlockinfo) -> 
				let (jl,_,_,_,_) = jmp_block in
				let rev_ir = List.rev irlist in
				let old_jmp = (match List.hd rev_ir with
								| SI.GOTO(l) -> l | _ -> raise Unexpected) in

				let new_irlist = List.rev (SI.GOTO(jl)::(List.tl rev_ir)) in
				([(bl, LS.remove old_jmp (LS.add jl jump_set), new_irlist, in_tmps, out_tmps); jmp_block], newBlockinfo)
			)

	(* ends in a comparison *)
	| 2 -> 
		let (new_blocks, new_labs, finalblockinfo) = 
			LS.fold (fun jbl (sofar, lmap, binfo) -> 
					match get_jump_block bl jbl binfo with
					| (None, bi) -> (sofar, LM.add jbl jbl lmap, bi)
					| (Some(jmp_block), bi) -> 
						let (jl,_,_,_,_) = jmp_block in
						(jmp_block::sofar, LM.add jbl jl lmap, bi)) jump_set ([], LM.empty, blockInfo) in

		let (old_jmps, new_jmps) = LM.fold (fun k v (old, news) -> 
												(LS.add k old, LS.add v news)) new_labs (LS.empty, LS.empty) in
		let rev_ir = List.rev irlist in
		let last_ir = List.hd rev_ir in
		let new_irlist = (match last_ir with
							| SI.IF(cop, op1, op2, l1, l2) -> 
								List.rev (SI.IF(cop, op1, op2, LM.find l1 new_labs, LM.find l2 new_labs) :: 
									(List.tl rev_ir))
							| _ -> raise Unexpected
						 ) in
		
		((bl, LS.union new_jmps (LS.diff jump_set old_jmps), new_irlist, in_tmps, out_tmps)::new_blocks, finalblockinfo)

	| _ -> raise_error "Cannot have more than 2 jumps in a block!"

(* adds in the blocks that are required to jump b/w ssa blocks
	(adds in instructions of the form t1,0 <- t1,1) *)
let rec add_in_blocks ssa_blocks blockInfo = 
	match ssa_blocks with
	| block::rest -> 
		let (new_blocks, newBlockinfo) = get_add_blocks block blockInfo in
		let (rem, finalblockinfo) = add_in_blocks rest newBlockinfo in
		(new_blocks @ rem, finalblockinfo)

	| [] -> ([], blockInfo)	


let ssa_convert bl fname blockInfo blockLive = 

	(* do an initial sweep to get the multemps and block info *)
	let (init_ssa_blist, newblockinfo) = funcssa bl blockInfo blockLive in	

	(*let () = blockInfo#printJumpMap in*)
	let (min_ssa_blist, almostblockInfo) = minimizer init_ssa_blist newblockinfo true in

	let optimized_min_ssa_blist = Propagation.optimize min_ssa_blist in

	let (final_ssa_blist, finalblockinfo) = add_in_blocks optimized_min_ssa_blist almostblockInfo in

	(fname, final_ssa_blist, finalblockinfo#get_jumpmap)

let genssa bp = 
	let live_at_blocks = Allocator.get_block_live bp in
	List.map (fun (fname, bl, jmap) -> 
				let binfo = new holderBlockInfo in
				let binfo' = binfo#set_jumpmap jmap in
				ssa_convert bl fname binfo' live_at_blocks) bp




