module AS = Assem

module S = Storage

let rec optimize_function = function
  | [] -> []
  | inst::insts ->
    match inst with
      | AS.MOV (AS.REG (_, r1), AS.REG (_, r2)) when r1 = r2 -> optimize_function insts
      | _ -> inst::(optimize_function insts)

let optimize = optimize_function
