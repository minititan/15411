(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 *)

type size =
  | WORD
  | DWORD
  | QWORD

type simpop =
  | IMM of Int32.t
  | REG of Storage.reg 
  | ARGTOP of int
  | ARGBOTTOM of int
  | STACK of int
  | DEREF of Int32.t * (size * simpop)
type operand =
  size * simpop

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label (* if true goto Label.label *)
  | CALL of Symbol.symbol
  | GOTO of Label.label
  | CHECKDEREF of operand
  | LABEL of Label.label
  | CLTD
  | RET

type func = Symbol.symbol * (instr list)

type program = func list

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT
