type reg =
  | EAX (* 32 bit *)
  | AL
  | EBX
  | ECX
  | CL (* 8 bit version of ECX *)
  | EDX
  | EBP
  | ESP
  | ESI
  | EDI
  | R8D
  | R9D
  | R10D
  | R11D
  | R12D
  | R13D
  | R14D
  | R15D
  | RAX (* 64 bit *)
  | RBX
  | RCX
  | RDX
  | RBP
  | RSP
  | RSI
  | RDI
  | R8
  | R9
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15

type offset = int

type str = REG of reg | STACK of offset

val reg_to_16 : reg -> reg

val reg_to_32 : reg -> reg

val reg_to_64 : reg -> reg

val format_reg: reg -> string

val format: str -> string
