(* Module that creates the headers for assembly *)

let arch = "linux" (*" linux" or "mac" *)

exception UnknownArch

let symbol_name =
  if arch = "linux" then "_c0_main"
  else if arch = "mac" then "__c0_main"
  else raise UnknownArch

let header_list = 
  ["\t.globl\t" ^ symbol_name ^ "\n";
   "\t.align\t4, 0x90\n";
   symbol_name ^ ":\n"]

let get_headers_string = 
  String.concat "" header_list
