module AS = Assem
module LL = Llir
module Alloc = Allocator
module S = Storage
module SM = Symbol.Map
module IM = Regalloc_util.IntMap
module IS = Regalloc_util.IntSet
open Regalloc_util

 let munch_binop = function
  | LL.ADD -> AS.ADD
  | LL.SUB -> AS.SUB
  | LL.MUL -> AS.MUL
  | LL.BAND -> AS.ANDOP
  | LL.BOR -> AS.OR
  | LL.SAR -> AS.SAR
  | LL.SAL -> AS.SAL
  | LL.XOR -> AS.XOR

let munch_unop = function
  | LL.IDIV -> AS.IDIV
  | LL.NEG -> AS.NEG
  | LL.BNOT -> AS.BNOT
  | LL.INC -> AS.INC
  | LL.DEC -> AS.DEC

let munch_cmpop = function
  | LL.EQ -> AS.EQ
  | LL.NOTEQ -> AS.NOTEQ
  | LL.GREATER -> AS.GREATER
  | LL.GREATEREQ -> AS.GREATEREQ
  | LL.LESS -> AS.LESS
  | LL.LESSEQ -> AS.LESSEQ

let is_shift = function
  | AS.SAR | AS.SAL -> true
  | AS.ADD | AS.SUB | AS.ANDOP | AS.XOR | AS.OR | AS.MUL -> false

let is_memsafe = function
  | AS.ADD | AS.SUB | AS.ANDOP | AS.XOR | AS.OR -> true
  | AS.SAR | AS.SAL | AS.MUL  -> false

let color_to_storage c =
  if c < stack_min_temp then
    S.STACK(stack_min_temp - c)
  else
    S.REG(temp_to_reg c)

let get_arg ~is_argbottom wholestack (size, arg_num) =
  (match arg_num with
  | 0 -> AS.REG (size, S.EDI) (* still ints *)
  | 1 -> AS.REG (size, S.ESI)
  | 2 -> AS.REG (size, S.EDX)
  | 3 -> AS.REG (size, S.ECX)
  | 4 -> AS.REG (size, S.R8D)
  | 5 -> AS.REG (size, S.R9D)
  | a -> 
      if (is_argbottom) then AS.STACK (size, (word_size * (a - 6)))
    else AS.STACK (size, (word_size * (wholestack + 1 + a - 6)))
  )

let trans_size = function
  | LL.WORD -> AS.WORD (* note that until now the sizes were inaccurate *)
  | LL.DWORD -> AS.DWORD
  | LL.QWORD -> AS.QWORD

type memory_access =
  | REGISTER
  | MEMORY (* stack/deref *)
  | IMMEDIATE
  | DEREFSTACK

let get_size = Assem.get_size

let stack_reg op = AS.REG (get_size op, S.R15)
let stack_reg2 op = AS.REG (get_size op, S.R14)

exception DerefStackUnexpected
exception InvalidMatch

(* returns a list of assem instuctions and stack space used *)
let llir_to_assem_list inst arg_stack var_stack reg_stack extra_stack epilogue = 
  let wholestack = arg_stack + var_stack + reg_stack + extra_stack in
  let rec unwrap_temp (size, llop) =
    (match llop with
    | LL.REG r -> AS.REG (trans_size size, r)
    | LL.IMM i -> AS.IMM (trans_size size, i)
    | LL.STACK i -> AS.STACK (trans_size size, (word_size * (arg_stack + i - 1)))
    | LL.ARGTOP a -> get_arg ~is_argbottom:false wholestack (trans_size size, a)
    | LL.ARGBOTTOM a -> get_arg ~is_argbottom:true wholestack (trans_size size, a)
    | LL.DEREF (offset, op) -> AS.DEREF(trans_size size, Int32.to_int offset, unwrap_temp op)
    )
  in

  let unwrap_temps (t1,t2) = (unwrap_temp t1, unwrap_temp t2) in

  let rec get_access_type = function
  | AS.REG _ -> REGISTER
  | AS.IMM _ -> IMMEDIATE
  | AS.STACK _ -> MEMORY
  | AS.DEREF(_, _, o) ->
    match get_access_type o with
    | REGISTER -> MEMORY
    | IMMEDIATE -> MEMORY (* is dereferencing an immediate possible? *)
    | MEMORY -> DEREFSTACK
    | DEREFSTACK -> raise DerefStackUnexpected
  in

  let get_access_types (x,y) = (get_access_type x, get_access_type y) in

  match inst with
    | LL.BINOP (oper, d, s) ->
      let oper = munch_binop oper in
      let (d', s') = unwrap_temps (d, s) in
      let (d'', s'') = get_access_types (d', s') in
      if is_memsafe oper then
        (match (d'', s'') with
          | (REGISTER, REGISTER)
          | (REGISTER, IMMEDIATE)
          | (MEMORY, REGISTER) -> [AS.BINOP(oper, d', s')]

          | (REGISTER, MEMORY)
          | (MEMORY, MEMORY) ->
            [AS.MOV(stack_reg s', s'); AS.BINOP(oper, d', stack_reg d')]

          | (DEREFSTACK, DEREFSTACK) ->
            (match (d',s') with
              | (AS.DEREF(dst_drf_size, dst_drf_offset, dst_stk_operand), AS.DEREF(src_drf_size, src_drf_offset, src_stk_operand)) ->
                let stack_reg' = stack_reg src_stk_operand in
                let stack_reg2' = stack_reg2 dst_stk_operand in
                [AS.MOV(stack_reg', src_stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(src_drf_size, src_drf_offset, stack_reg'));
                 AS.MOV(stack_reg2', dst_stk_operand);
                 AS.MOV(stack_reg2', AS.DEREF(dst_drf_size, dst_drf_offset, stack_reg2'));
                 AS.BINOP(oper, stack_reg2', stack_reg'); (* stack_reg2 holds result, now use stack_reg for dest *)
                 AS.MOV(stack_reg dst_stk_operand, dst_stk_operand);
                 AS.MOV(AS.DEREF(dst_drf_size, dst_drf_offset, stack_reg dst_stk_operand), stack_reg2')
                ]
              | _ -> raise InvalidMatch
            )

          | (MEMORY, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, d', stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (DEREFSTACK, REGISTER) | (DEREFSTACK, IMMEDIATE) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                let stack_reg2' = stack_reg2 stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, stack_reg', s');
                 AS.MOV(stack_reg2', stk_operand);
                 AS.MOV(AS.DEREF(sz, offset, stack_reg2'), stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (DEREFSTACK, MEMORY) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                let stack_reg2' = stack_reg2 stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, stack_reg', s');
                 AS.MOV(stack_reg2', stk_operand);
                 AS.MOV(AS.DEREF(sz, offset, stack_reg2'), stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (REGISTER, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, d', stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (MEMORY, IMMEDIATE) ->
            if is_shift oper then
              [AS.BINOP(oper, d', s')]
            else
              let stack_reg' = stack_reg d' in
              [AS.MOV(stack_reg', d');
               AS.BINOP(oper, stack_reg', s');
               AS.MOV(d', stack_reg')]

          | (IMMEDIATE, _) -> raise (Failure "cannot store into IMM")
        )
      else
        (match (d'',s'') with (* non shift case *)

          | (REGISTER, MEMORY) -> [AS.MOV(stack_reg s', s');
                                      AS.BINOP(oper, d', stack_reg s')]
          | (MEMORY, REGISTER)
          | (MEMORY, IMMEDIATE)
          | (MEMORY, MEMORY) -> [AS.MOV(stack_reg d', d');
                                      AS.BINOP(oper, stack_reg d', s');
                                      AS.MOV(d', stack_reg d')]


          | (REGISTER, REGISTER)
          | (REGISTER, IMMEDIATE) -> [AS.BINOP(oper, d', s')]
          | (IMMEDIATE, _) -> raise (Failure "cannot store into IMM")

          | (DEREFSTACK, DEREFSTACK) ->
          (* note that sizes aren't used here as we assume the size is always QWORD - check this assumption *)
            (match (d',s') with
              | (AS.DEREF(dst_drf_size, dst_drf_offset, dst_stk_operand), AS.DEREF(src_drf_size, src_drf_offset, src_stk_operand)) ->
                let stack_reg' = stack_reg src_stk_operand in
                let stack_reg2' = stack_reg2 dst_stk_operand in
                [AS.MOV(stack_reg', src_stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(src_drf_size, src_drf_offset, stack_reg'));
                 AS.MOV(stack_reg2', dst_stk_operand);
                 AS.MOV(stack_reg2', AS.DEREF(dst_drf_size, dst_drf_offset, stack_reg2'));
                 AS.BINOP(oper, stack_reg2', stack_reg'); (* stack_reg2 holds result, now use stack_reg for dest *)
                 AS.MOV(stack_reg dst_stk_operand, dst_stk_operand);
                 AS.MOV(AS.DEREF(dst_drf_size, dst_drf_offset, stack_reg dst_stk_operand), stack_reg2')
                ]
              | _ -> raise InvalidMatch
            )

          | (MEMORY, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                let stack_reg2' = stack_reg2 d' in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.MOV(stack_reg2', d');
                 AS.BINOP(oper, stack_reg2', stack_reg');
                 AS.MOV(d', stack_reg2')
                ]
              | _ -> raise InvalidMatch
            )


          | (DEREFSTACK, REGISTER) | (DEREFSTACK, IMMEDIATE) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                let stack_reg2' = stack_reg2 stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, stack_reg', s');
                 AS.MOV(stack_reg2', stk_operand);
                 AS.MOV(AS.DEREF(sz, offset, stack_reg2'), stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (DEREFSTACK, MEMORY) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.MOV(stack_reg2 s', s');
                 AS.BINOP(oper, stack_reg', stack_reg2 s');
                 AS.MOV(stack_reg2 stk_operand, stk_operand);
                 AS.MOV(AS.DEREF(sz, offset, stack_reg2 stk_operand), stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (REGISTER, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.BINOP(oper, d', stack_reg')
                ]
              | _ -> raise InvalidMatch
            )
          )

  | LL.CHECKDEREF e ->
    let e' = unwrap_temp e in
    let e'' = get_access_type e' in
    (match e'' with
      | REGISTER | IMMEDIATE -> [AS.TEST(e',e'); AS.JZFAIL]
      | MEMORY ->
        let stack_reg' = stack_reg e' in
        [AS.MOV(stack_reg', e'); AS.TEST(stack_reg', stack_reg'); AS.JZFAIL]
      | DEREFSTACK ->
        (match e' with
          | AS.DEREF(sz, offset, stk_operand) ->
            let stack_reg' = stack_reg stk_operand in
            [AS.MOV(stack_reg', stk_operand);
             AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
             AS.TEST(stack_reg', stack_reg')
             ]
          | _ -> raise InvalidMatch
        )
      )

  | LL.MOV(d, s) ->
    let (d', s') = unwrap_temps (d, s) in
    let (d'', s'') = get_access_types (d', s') in
    (match (d'',s'') with
        | (REGISTER, REGISTER)
        | (REGISTER, MEMORY)
        | (MEMORY, REGISTER)
        | (MEMORY, IMMEDIATE)
        | (REGISTER, IMMEDIATE) -> [AS.MOV(d', s')]
        | (MEMORY, MEMORY) ->
            [AS.MOV(stack_reg s', s'); AS.MOV(d', stack_reg s')]
        | (IMMEDIATE, _) -> raise (Failure "cannot store into IMM for MOV")
          | (DEREFSTACK, DEREFSTACK) ->
          (* note that sizes aren't used here as we assume the size is always QWORD - check this assumption *)
            (match (d',s') with
              | (AS.DEREF(dst_drf_size, dst_drf_offset, dst_stk_operand), AS.DEREF(src_drf_size, src_drf_offset, src_stk_operand)) ->
                let stack_reg' = stack_reg src_stk_operand in
                let stack_reg2' = stack_reg2 dst_stk_operand in
                [AS.MOV(stack_reg', src_stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(src_drf_size, src_drf_offset, stack_reg'));
                 AS.MOV(stack_reg2', dst_stk_operand);
                 AS.MOV(AS.DEREF(dst_drf_size, dst_drf_offset, stack_reg2'), stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (MEMORY, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
                 AS.MOV(d', stack_reg')
                ]
              | _ -> raise InvalidMatch
            )

          | (DEREFSTACK, REGISTER) | (DEREFSTACK, IMMEDIATE) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(AS.DEREF(sz, offset, stack_reg'), s')
                ]
              | _ -> raise InvalidMatch
            )

          | (DEREFSTACK, MEMORY) ->
            (match d' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                let stack_reg2' = stack_reg2 s' in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(stack_reg2', s');
                 AS.MOV(AS.DEREF(sz, offset, stack_reg'), stack_reg2')
                ]
              | _ -> raise InvalidMatch
            )


          | (REGISTER, DEREFSTACK) ->
            (match s' with
              | AS.DEREF(sz, offset, stk_operand) ->
                let stack_reg' = stack_reg stk_operand in
                [AS.MOV(stack_reg', stk_operand);
                 AS.MOV(d', AS.DEREF(sz, offset, stack_reg'))
                ]
              | _ -> raise InvalidMatch
            )
        )

  | LL.UNOP (oper, d) ->
    let oper' = munch_unop oper in
    let d' = unwrap_temp d in
    let d'' = get_access_type d' in
    (match d'' with
      | DEREFSTACK ->
        (match d' with
          | AS.DEREF(sz, offset, stk_operand) ->
            let stack_reg' = stack_reg stk_operand in
            let stack_reg2' = stack_reg stk_operand in
            [AS.MOV(stack_reg', stk_operand);
             AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
             AS.UNOP(oper', stack_reg');
             AS.MOV(stack_reg2', stk_operand);
             AS.MOV(AS.DEREF(sz, offset, stack_reg2'), stack_reg')]
          | _ -> raise InvalidMatch
        )

      | REGISTER | MEMORY -> [AS.UNOP(oper', d')]
      | IMMEDIATE -> raise (Failure ("cannot store into IMM for UNOP: " ^
                                    Llir.Print.pp_unop oper ^ " " ^ 
                                    Llir.Print.pp_operand d ))
    )

  | LL.IF (cop, o1, o2, l) -> (* CMPL is happening twice, swapped MOV *)
    let jump_inst = AS.JUMP (munch_cmpop cop, l) in
    let (o1', o2') = unwrap_temps (o1, o2) in
    let (o1'', o2'') = get_access_types (o1', o2') in
    (match (o1'', o2'') with
      | (REGISTER, REGISTER) | (REGISTER, MEMORY)
      | (MEMORY, REGISTER) | (REGISTER, IMMEDIATE) -> [AS.CMP(o1', o2'); jump_inst]
      | (MEMORY, MEMORY) ->
          [AS.MOV(stack_reg o2', o2'); AS.CMP(o1', stack_reg o2'); jump_inst]
      | (MEMORY, IMMEDIATE) | (IMMEDIATE, MEMORY) (* these cases look kinda off (from previous lab); revisit this later *)
      | (IMMEDIATE, REGISTER) | (IMMEDIATE, IMMEDIATE) ->
          [AS.MOV(stack_reg o1', o1'); AS.CMP(stack_reg o1', o2'); jump_inst]
      | (DEREFSTACK, DEREFSTACK) ->
        (match (o1',o2') with
          | (AS.DEREF(o1_drf_size, o1_drf_offset, o1_stk_operand), AS.DEREF(o2_drf_size, o2_drf_offset, o2_stk_operand)) ->
            let stack_reg' = stack_reg o1_stk_operand in
            let stack_reg2' = stack_reg2 o2_stk_operand in
            [AS.MOV(stack_reg', o1_stk_operand);
             AS.MOV(stack_reg', AS.DEREF(o1_drf_size, o1_drf_offset, stack_reg'));
             AS.MOV(stack_reg2', o2_stk_operand);
             AS.MOV(stack_reg2', AS.DEREF(o2_drf_size, o2_drf_offset, stack_reg2'));
             AS.CMP(stack_reg', stack_reg2');
             jump_inst]
          | _ -> raise InvalidMatch
        )
      | (DEREFSTACK, REGISTER) | (DEREFSTACK, MEMORY) | (DEREFSTACK, IMMEDIATE)->
        (match o1' with
          | AS.DEREF(sz, offset, stk_operand) ->
            let stack_reg' = stack_reg stk_operand in
            [AS.MOV(stack_reg', stk_operand);
             AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
             AS.CMP(stack_reg', o2')
             ]
          | _ -> raise InvalidMatch
        )

      | (REGISTER, DEREFSTACK) | (MEMORY, DEREFSTACK) | (IMMEDIATE, DEREFSTACK) ->
        (match o2' with
          | AS.DEREF(sz, offset, stk_operand) ->
            let stack_reg' = stack_reg stk_operand in
            [AS.MOV(stack_reg', stk_operand);
             AS.MOV(stack_reg', AS.DEREF(sz, offset, stack_reg'));
             AS.CMP(o1', stack_reg')
             ]
          | _ -> raise InvalidMatch
        )
    )
  | LL.LABEL l -> [AS.LABEL l]
  | LL.GOTO l -> [AS.GOTO l]
  | LL.CALL f -> [AS.AND((AS.REG (AS.WORD, S.AL)), (AS.IMM (AS.WORD, Int32.zero))); AS.CALL f]
  | LL.CLTD -> [AS.CLTD]
  | LL.RET -> epilogue @ [AS.RET]
  | LL.DIRECTIVE str -> [AS.DIRECTIVE str]
  | LL.COMMENT str -> [AS.COMMENT str] 

(*
let rec llir_to_assem max_arg_stack_used llir_list storage_map stack_set sofar = 
  match llir_list with
  | inst::rest ->
      let (stack_set', assemlist) = get_assem_inst_info inst storage_map max_arg_stack_used in
      llir_to_assem max_arg_regs_used rest storage_map
          (IM.union stack_set stack_set') sofar @ assemlist
  | [] -> (stack_set, sofar)
*)

let callee_saved_registers_as_temps =
  let intlist_to_intset int_list =
    List.fold_left (fun s x -> IS.add x s) IS.empty int_list
  in
  let callee_saved_temps =
    List.map reg_to_temp
      [S.RBX; S.RBP; S.R12; S.R13; S.R14; S.R15]
  in
  intlist_to_intset callee_saved_temps

(* returns the amount of stack space required by the largest function
 * call made in the function
 *)
let get_max_arg_stack_used llir_list =
  let rec get_max_args_used llir_list' maxsofar =
    match llir_list' with
    | ins::rest -> 
      (match ins with
      | LL.MOV ((_, LL.ARGBOTTOM i), _) when i > maxsofar -> get_max_args_used rest i
      | _ -> get_max_args_used rest maxsofar
      )
    | [] -> maxsofar
  in
  let max_args_used = get_max_args_used llir_list 0 in
  let num_argument_registers = 5 in
  (max (max_args_used - num_argument_registers) 0)

let storage_map_to_temp_set storage_map =
  IntMap.fold
  (fun _ temp_used temp_set -> IS.add temp_used temp_set)
  storage_map
  IS.empty

let get_max_stack_spot storage_map =
  let temp_set = storage_map_to_temp_set storage_map in
  let minimum_temp =
    IS.fold
    (fun i1 i2 -> if i1 < i2 then i1 else i2)
    temp_set
    0
  in

  if (minimum_temp = 0) then 0 else
  match color_to_storage minimum_temp with
  | S.REG _ -> 0
  | S.STACK i -> i

(* Applies the register mapping to the llir and generates the relevant
 * assembly as well. Does not take care of stack manipulation assembly instructions 
 *)
let translate_llir llir_list arg_stack var_stack callee_stack align_stack epilogue = 
  let inst_llist =
    List.map
    (fun llir -> llir_to_assem_list llir arg_stack var_stack callee_stack align_stack epilogue)
    llir_list
  in
  List.concat inst_llist

let callee_registers_to_pro_ep callee_set =
  let callee_list = IS.elements callee_set in
  let registers = List.map temp_to_reg callee_list in
  let registers_64 = List.map S.reg_to_64 registers in
  let push_instructions = List.map (fun reg -> AS.PUSH (AS.REG (AS.QWORD, reg))) registers_64 in
  let pop_instructions = List.rev (List.map (fun reg -> AS.POP (AS.REG (AS.QWORD, reg))) registers_64) in
  (push_instructions, pop_instructions)


(* returns the number of spots needed after aligning rsp to 16 bytes *)
let get_extra_space_needed var_and_arg_spots callee_spots = 
    if ((var_and_arg_spots + callee_spots) mod 2 = 0) then 1
    else 0

let func_to_assem llir_list storage_map =
  let storage_temps_used = storage_map_to_temp_set storage_map in
  let callee_registers_used =
    IS.inter callee_saved_registers_as_temps storage_temps_used
  in

  let num_callee_spots = IS.cardinal callee_registers_used in

  let arg_stack_spots = get_max_arg_stack_used llir_list in

  let local_var_stack_spots_used = get_max_stack_spot storage_map in

  let extra_spot = get_extra_space_needed (arg_stack_spots + local_var_stack_spots_used) num_callee_spots in

  let (alignpush, alignpop) = if (extra_spot = 0) then ([], []) else 
                                  ([AS.BINOP(AS.SUB, AS.REG (AS.QWORD, S.RSP), AS.IMM
                                  (AS.QWORD,(Int32.of_int (word_size * 1))))], 
                                      [AS.BINOP(AS.ADD, AS.REG (AS.QWORD,S.RSP), AS.IMM
                                      (AS.QWORD,(Int32.of_int (word_size * 1))))])

  in

  let space_needed = local_var_stack_spots_used + arg_stack_spots in

  let (alloc_instr,dealloc_instr) =
    if space_needed = 0 then
      ([],[])
    else
      let stack_shift = AS.IMM (AS.QWORD,Int32.of_int(space_needed * word_size)) in
      ([AS.BINOP(AS.SUB, AS.REG (AS.QWORD,S.RSP), stack_shift)],
       [AS.BINOP(AS.ADD, AS.REG (AS.QWORD,S.RSP), stack_shift)])
  in
  let (callee_pushes, callee_pops) = callee_registers_to_pro_ep callee_registers_used in
  let epilogue = dealloc_instr @ callee_pops @ alignpop in
  let assem_list = translate_llir llir_list 
                      arg_stack_spots local_var_stack_spots_used num_callee_spots extra_spot epilogue in

  alignpush @ callee_pushes @ alloc_instr @ assem_list

(* plan from Sharman:
 * first get the maximum argument function and based on that calculate
 * initial stack space (i think i just passed the max number of args as
 * initial stack space so subtract the number of arg regs from that)
 *
 * next recurse through the llir list and accumulate the callee-save
 * regs and stack spots along the way
 *
 * using callee-save reg info we have to append push and pop of those
 * regs before and after the assem list
 *
 * we also use stack spots to calculate total stack space as such:
 * (max_args - (number of arg regs) + stack spots used
 * by temps + no. of callee-save regs used)
 *)

(*
let rec get_size = function
  | IMM (s, _) -> s
  | REG (s, _) -> s
  | STACK (s, _) -> s
  | DEREF (_, o) -> get_size o

let biggest_size s1 s2 =
  match (get_size s1, get_size s2) with
  | (QWORD, _) | (_, QWORD) -> QWORD
  | _ -> DWORD

let normalize_size : AS.instr -> AS.instr = function
  | AS.BINOP (oper, d, s) ->
    let (prefix) = (match (sz1, sz2) with
      | (QWORD, QWORD) -> []
      | (DWORD, DWORD) -> []
      | (WORD, WORD)   -> []
      | (QWORD, DWORD) -> [MOVQ]

    )
    in
    let new_size = biggest_size d s in


  | AS.UNOP (oper, d) ->
      "\t" ^ format_unop oper
      ^ "\t" ^ format_operand d ^ "\n"
  | AS.MOV ((sz1, d), (sz2, s)) ->
      let ins = (match (sz1, sz2) with
        | (QWORD, QWORD) -> "MOVQ"
        | (DWORD, DWORD) -> "MOVL"
        | _ -> "mismatched or invalid types"
      )
      in
      "\t" ^ ins
      ^ "\t" ^ format_operand s
      ^ ", " ^ format_operand d ^ "\n"

  | PUSH op -> 
      "\t" ^ "PUSHQ" 
      ^ "\t" ^ (format_operand op) ^ "\n"

  | POP op ->
      "\t" ^ "POPQ" 
      ^ "\t" ^ (format_operand op) ^ "\n"

  | CALL func ->
      "\tCALL\t" ^ Symbol.name func ^ "\n"

  | DIRECTIVE str ->
      "\t" ^ str ^ "\n"
  | COMMENT str ->
      "\t" ^ "/* " ^ str ^ "*/\n"
  | FUNCHEADER name ->
      let name' = name in
      "\t.global\t" ^ name' ^ "\n"
      ^ name' ^ ":\n"
  | CLTD ->
      "\t" ^ "CLTD" ^ "\n"
  | RET ->
      "\t" ^ "RET" ^ "\n"
  | GOTO l ->
      "\t" ^ "JMP " ^ format_label l ^ "\n"
  | LABEL l ->
      format_label l ^ ":" ^ "\n"
  | CMP (o1, o2) ->
      "\t" ^ "CMPL" ^
      "\t" ^ format_operand o2 ^ "," ^ format_operand o1 ^ "\n"
  | JUMP (cop, l) ->
    let jinst = (match cop with
      | EQ -> "JE"
      | NOTEQ -> "JNE"
      | GREATER -> "JG"
      | GREATEREQ -> "JGE"
      | LESS -> "JL"
      | LESSEQ -> "JLE") in
    "\t" ^ jinst ^ " " ^ format_label l ^ "\n"

  | AND(op1, op2) -> "\t" ^ "AND" ^ "\t" ^ format_operand op2 ^ "," ^ format_operand op1 ^ "\n"

let function_normalize_sizes = List.map normalize_size |> List.concat
*)

let assembly_gen prog storage_map =
  let process_function (f_name,llir_list) =
    let func_storage_map = SM.find f_name storage_map in
    let func_assem = func_to_assem llir_list func_storage_map in
    (Symbol.name f_name, func_assem)
  in
  let functions_with_assem = List.map process_function prog in
  let assem_with_names =
    List.map (fun (name,ins) -> (AS.FUNCHEADER name)::ins) functions_with_assem
  in
  List.concat assem_with_names




