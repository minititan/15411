(* L1 Compiler
 * Top Level Environment
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

let (|>) x f = f x

let say = print_endline

let flag_verbose = Flag.flag "verbose"
let flag_ast = Flag.flag "ast"
let flag_ir1 = Flag.flag "ir1"
let flag_ir2 = Flag.flag "ir2"
let flag_assem = Flag.flag "assem"
let flag_parse = Flag.flag "parse"
let flag_irinst = Flag.flag "irinst"
let flag_llir = Flag.flag "llir"
let flag_coloring = Flag.flag "coloring"
let flag_optimize = Flag.flag "optimize"
let flag_livelist = Flag.flag "livelist"
let flag_unsafe = Flag.flag "unsafe"

let library_file = ref ""

let reset_flags () =
  List.iter Flag.unset [flag_verbose; flag_ast; flag_ir1; flag_ir2; flag_assem;
  flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist;
  flag_unsafe]

let set flag = Arg.Unit (fun () -> Flag.set flag)

let set_all () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir1; flag_ir2;
    flag_assem; flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize;
    flag_livelist])

let set_code () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir1; flag_ir2;
    flag_assem; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist])

let options =
  [("--verbose", set flag_verbose, "verbose message");
   ("-v", set flag_verbose, "verbose message");
   ("-l", Arg.String (fun s -> library_file := s), "library file (.h0)");
   ("--unsafe", set flag_unsafe, "don't check null pointers");
   ("--debug-parse", set flag_parse, "debug the parser");
   ("--dump-ast", set flag_ast, "pretty print the AST");
   ("--dump-ir1", set flag_ir1, "pretty print IR1");
   ("--dump-ir2", set flag_ir2, "pretty print IR2");
   ("--dump-irinst", set flag_irinst, "pretty print the IR instructions");
   ("--dump-llir", set flag_llir, "pretty print the LLIR instructions");
   ("--dump-coloring", set flag_coloring, "pretty print the coloring map");
   ("--dump-assem", set flag_assem, "pretty print the assembly");
   ("--dump-opt", set flag_optimize, "pretty print the optimized assembly");
   ("--dump-livelist", set flag_livelist, "pretty print the live list");
   ("--all", set_all() , "set all print flags (super verbose mode)");
   ("--code", set_code(), "set all flags related to code generation")
 ]

exception EXIT

let stem s =
  try
    let dot = String.rindex s '.' in
    String.sub s 0 dot
  with Not_found -> s

let main args =
  try
    let header = "Usage: compile [OPTION...] SOURCEFILE\nwhere OPTION is" in
    let usageinfo () = Arg.usage options header in
    let errfn msg = say (msg ^ "\n"); usageinfo (); raise EXIT in

    let _ = Temp.reset () in
    let _ = reset_flags () in

    let _ = if Array.length args < 2 then (usageinfo (); raise EXIT) in

    (* Parse the arguments.  Non-keyword arguments accumulate in fref,
       and then is assigned to files. *)
    let files =
      let fref = ref []
      and current = ref 0 in
      let () = Arg.parse_argv ~current args options
          (fun s -> fref := s::!fref) header in
      List.rev !fref in

    let source = match files with
    | [] -> errfn "Error: no input file"
    | [filename] -> filename
    | _ -> errfn "Error: more than one input file" in

    (* Parse Header *)
    let library_ast = if !library_file = "" then
      []
    else
      let _ = Flag.guard flag_verbose say ("Parsing header... " ^ !library_file) in
      let _ = Flag.guard flag_parse
          (fun _ -> ignore (Parsing.set_trace true)) () in
      let library_ast = !library_file
          |> Parse.parse
          |> (Elaboration.trans_program false)
      in
      library_ast
    in

    (* Parse *)
    let _ = Flag.guard flag_verbose say ("Parsing... " ^ source) in
    let _ = Flag.guard flag_parse
        (fun _ -> ignore (Parsing.set_trace true)) () in
    let parsetree = Parse.parse source in
    let ast = (Elaboration.trans_program true) parsetree in
    let _ = Flag.guard flag_ast
      (fun () -> say (Ast.Print.pp_program ast)) () in

    (* Typecheck *)
    let _ = Flag.guard flag_verbose say "Checking..." in
    let typed_ast = StaticChecker.check (library_ast,ast) in
    let _ = Flag.guard flag_verbose say "Passed Checks.\n" in

    (* Expression tree *)
    let _ = Flag.guard flag_verbose say "Typed AST -> Command List...\n" in
    let base_tree = Trans.translate typed_ast (Flag.isset flag_unsafe) in
    let _ = Flag.guard flag_ir1 (fun () -> say (Tree.Print.pp_program base_tree)) () in

    (* IR *)
    let _ = Flag.guard flag_verbose say "Command List -> IR...\n" in
    let base_ir = Codegen.codegen base_tree in
    let _ = Flag.guard flag_ir1 (fun () -> say (Ir.Print.pp_program base_ir)) () in

    (* Basic Blocks *)
    let _ = Flag.guard flag_verbose say "IR -> Basic Blocks...\n" in
    let basic_blocks = Blockgen.block_convert base_ir in
    let _ = Flag.guard flag_ir2 (fun () ->
      say (Block.Print.pp_blockprogram basic_blocks)) () in

    (* SSA conversion *)
    let _ = Flag.guard flag_verbose say "BB-> SSA...\n" in
    let ssa_blocks = Ssagen.genssa basic_blocks in
    let _ = Flag.guard flag_ir2 (fun () ->
      say (Ssair.Print.pp_ssaprogram ssa_blocks)) () in   
    
    (* DESSA (still in ssa form but now has temps) *)
    let _ = Flag.guard flag_verbose say "SSA -> De-SSA...\n" in
    let dessa_blocks = Dessa.dessafier ssa_blocks in
    let _ = Flag.guard flag_ir2 (fun () ->
      say (Block.Print.pp_blockprogram dessa_blocks)) () in

    (* Neededness Analysis *)
    let _ = Flag.guard flag_verbose say "Neededness...\n" in
    let needed_blocks = Allocator.get_needed_of_blockprogram dessa_blocks in
    let _ = Flag.guard flag_ir2 (fun () -> say (Block.Print.pp_blockprogram needed_blocks)) () in

    (* Liveness Analysis *)
    let _ = Flag.guard flag_verbose say "Performing Regalloc...\n" in
    let storage_map = Allocator.storage_maps_of_blockprogram needed_blocks in
    let _ = Flag.guard flag_coloring (fun () -> say (Allocator.Print.pp_storagemap storage_map)) () in

    (* LLIR *)
    let _ = Flag.guard flag_verbose say "Basic Blocks -> LLIR...\n" in
    let llir = Llirgen.llirgen needed_blocks storage_map in
    let _ = Flag.guard flag_llir (fun () -> say (Llir.Print.pp_program llir)) () in

    let _ = Flag.guard flag_verbose say "LLIR -> Assembly..." in
    let assem = Assemgen.assembly_gen llir storage_map in
    let _ = Flag.guard flag_assem
        (fun () -> say (Assem.Print.pp_program assem)) () in

    let _ = Flag.guard flag_verbose say "Optimizing.." in
    let assem_opt = Optimize.optimize assem in
    let _ = Flag.guard flag_verbose
        (fun () -> say (Assem.Print.pp_program assem_opt)) () in

    (* Add assembly header and footer *)
    let assem =
      Assem.DIRECTIVE(".file\t\"" ^ source ^ "\"")::[Assem.DIRECTIVE(".text")]
      @ assem
      @ [Assem.DIRECTIVE ".ident\t\"15-411 L3 compiler\""] in

    let c0_raise = "\t.global\t_c0_call_raise\n_c0_call_raise:\n\tmov $11,%rdi\n\tcall raise\n" in
    let code = String.concat "" (List.map Assem.format assem) ^ c0_raise in

    (* Output assembly *)
    let afname = stem source ^ ".s" in
    let _ = Flag.guard flag_verbose
        say ("Writing assembly to " ^ afname ^ " ...") in
    let _ = SafeIO.withOpenOut afname
        (fun afstream -> output_string afstream code) in
    (* Return success status *)
    0

  with
    ErrorMsg.Error -> say "Compilation failed"; 1
  | EXIT -> 1
  | Arg.Help x -> prerr_string x; 1
  | e -> prerr_string (Printexc.to_string e); 1


let test s =
  main (Array.of_list (""::(Str.split (Str.regexp "[ \t\n]+") s)))
