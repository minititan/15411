	.file	"../tests0/vacuous.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
