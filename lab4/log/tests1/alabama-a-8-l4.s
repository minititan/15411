	.file	"../tests1/alabama-a-8.l4"
	.text
	.global	_c0_push
_c0_push:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVQ	%rdi, %rbp
	MOVL	%esi, %r13d
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %r12
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$0, %rbx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVQ	%rdi, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$0, %rdi
	MOVL	%r13d, 0(%rdi)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$8, %rdi
	MOVQ	%r12, 0(%rdi)
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.global	_c0_pop
_c0_pop:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L3
	JMP L4
L3:
	JMP L6
L6:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$8, %rsp
	RET
L5:
	JMP L65
L65:
	MOVQ	%rax, %rdx
L4:
	JMP L66
L66:
	MOVQ	%rdi, %rdx
L7:
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rsi
	ADDQ	$0, %rsi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, 0(%rsi)
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_stack_new
_c0_stack_new:
	SUBQ	$8, %rsp
L8:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	ADDQ	$8, %rsp
	RET
	.global	_c0_queue_new
_c0_queue_new:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L9:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$0, %rbx
	ANDL	$0, %eax
	CALL	_c0_stack_new
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$8, %rbx
	ANDL	$0, %eax
	CALL	_c0_stack_new
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_enq
_c0_enq:
	SUBQ	$8, %rsp
L10:
	MOVQ	%rdi, %rax
	MOVL	%esi, %esi
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	MOVL	%esi, %esi
	ANDL	$0, %eax
	CALL	_c0_push
	ADDQ	$8, %rsp
	RET
	.global	_c0_deq
_c0_deq:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L11:
	MOVQ	%rdi, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L12
	JMP L13
L12:
	JMP L21
L21:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JNE L19
	JMP L20
L19:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rbx
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_pop
	MOVL	%eax, %eax
	MOVQ	%rbx, %rdi
	MOVL	%eax, %esi
	ANDL	$0, %eax
	CALL	_c0_push
	JMP L21
L20:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L14
	JMP L15
L14:
	JMP L17
L17:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L16:
	JMP L67
L67:
	MOVQ	%rax, %rax
L15:
	JMP L68
L68:
	MOVQ	%r12, %rax
L18:
	JMP L69
L69:
	MOVQ	%rax, %rax
L13:
	JMP L70
L70:
	MOVQ	%rbp, %rax
L22:
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_pop
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_queue_empty
_c0_queue_empty:
	SUBQ	$8, %rsp
L23:
	MOVQ	%rdi, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L27
	JMP L28
L27:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L24
	JMP L25
L28:
	JMP L25
L24:
	MOVL	$1, %eax
	JMP L71
L71:
	MOVQ	%rax, %rax
L25:
	MOVL	$0, %eax
	JMP L72
L72:
	MOVQ	%rax, %rax
L26:
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L29:
	MOVL	$10, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L64
	JMP L63
L64:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L63:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %r13
	MOVL	$0, %eax
	JMP L73
L73:
	MOVQ	%rax, %rbp
L50:
	CMPL	$10, %ebp
	JL L48
	JMP L49
L48:
	MOVQ	%r13, %rsi
	MOVL	%ebp, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L60
	JMP L61
L60:
	CMPL	$0, %eax
	JGE L62
	JMP L61
L61:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L62:
	IMULL	$8, %eax
	MOVQ	%rsi, %rbx
	ADDQ	%rax, %rbx
	ANDL	$0, %eax
	CALL	_c0_queue_new
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%r13, %rdi
	MOVL	%ebp, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L57
	JMP L58
L57:
	CMPL	$0, %eax
	JGE L59
	JMP L58
L58:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L59:
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	MOVL	%ebp, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	CMPL	$0, %ebp
	JG L51
	JMP L52
L51:
	MOVQ	%r13, %rsi
	MOVL	%ebp, %edi
	SUBL	$1, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L53
	JMP L54
L53:
	CMPL	$0, %edi
	JGE L55
	JMP L54
L54:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L55:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	MOVL	%ebp, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	JMP L56
L52:
	JMP L56
L56:
	MOVL	%ebp, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L74
L74:
	MOVQ	%rax, %rbp
L49:
	MOVL	$9, %eax
	JMP L75
L75:
	MOVQ	%rax, %rbp
L41:
	CMPL	$0, %ebp
	JGE L39
	JMP L40
L39:
	MOVQ	%r12, %rdi
	MOVL	$0, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L42
	JMP L43
L42:
	CMPL	$0, %eax
	JGE L44
	JMP L43
L43:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L44:
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rbx
	MOVQ	%r12, %rsi
	MOVL	%ebp, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L45
	JMP L46
L45:
	CMPL	$0, %edi
	JGE L47
	JMP L46
L46:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L47:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	MOVQ	%rbx, %rdi
	MOVL	%eax, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVL	%ebp, %eax
	SUBL	$1, %eax
	MOVL	%eax, %eax
	JMP L76
L76:
	MOVQ	%rax, %rbp
L40:
	MOVL	$0, %eax
	JMP L77
L77:
	MOVQ	%rax, %rbx
L32:
	MOVQ	%r12, %rsi
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L33
	JMP L34
L33:
	CMPL	$0, %eax
	JGE L35
	JMP L34
L34:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L35:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_queue_empty
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L31
	JMP L30
L30:
	MOVQ	%r12, %rsi
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L36
	JMP L37
L36:
	CMPL	$0, %eax
	JGE L38
	JMP L37
L37:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L38:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %eax
	JMP L78
L78:
	MOVQ	%rax, %rbx
L31:
	MOVL	%ebx, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
