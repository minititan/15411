	.file	"../tests1/alabama-a-6.l4"
	.text
	.global	_c0_push
_c0_push:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVQ	%rdi, %rbp
	MOVL	%esi, %r13d
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %r12
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$0, %rbx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVQ	%rdi, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$0, %rdi
	MOVL	%r13d, 0(%rdi)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rdi
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$8, %rdi
	MOVQ	%r12, 0(%rdi)
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.global	_c0_pop
_c0_pop:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L3
	JMP L4
L3:
	JMP L6
L6:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$8, %rsp
	RET
L5:
	JMP L44
L44:
	MOVQ	%rax, %rdx
L4:
	JMP L45
L45:
	MOVQ	%rdi, %rdx
L7:
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rsi
	ADDQ	$0, %rsi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, 0(%rsi)
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_stack_new
_c0_stack_new:
	SUBQ	$8, %rsp
L8:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	ADDQ	$8, %rsp
	RET
	.global	_c0_queue_new
_c0_queue_new:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L9:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$0, %rbx
	ANDL	$0, %eax
	CALL	_c0_stack_new
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$8, %rbx
	ANDL	$0, %eax
	CALL	_c0_stack_new
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_enq
_c0_enq:
	SUBQ	$8, %rsp
L10:
	MOVQ	%rdi, %rax
	MOVL	%esi, %esi
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	MOVL	%esi, %esi
	ANDL	$0, %eax
	CALL	_c0_push
	ADDQ	$8, %rsp
	RET
	.global	_c0_deq
_c0_deq:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L11:
	MOVQ	%rdi, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L12
	JMP L13
L12:
	JMP L21
L21:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JNE L19
	JMP L20
L19:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rbx
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_pop
	MOVL	%eax, %eax
	MOVQ	%rbx, %rdi
	MOVL	%eax, %esi
	ANDL	$0, %eax
	CALL	_c0_push
	JMP L21
L20:
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L14
	JMP L15
L14:
	JMP L17
L17:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L16:
	JMP L46
L46:
	MOVQ	%rax, %rax
L15:
	JMP L47
L47:
	MOVQ	%r12, %rax
L18:
	JMP L48
L48:
	MOVQ	%rax, %rax
L13:
	JMP L49
L49:
	MOVQ	%rbp, %rax
L22:
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_pop
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L23:
	ANDL	$0, %eax
	CALL	_c0_queue_new
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	MOVQ	%rbx, %rdi
	MOVL	$1, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$2, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$3, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$5, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$6, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$7, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$9, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	MOVL	$10, %esi
	ANDL	$0, %eax
	CALL	_c0_enq
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$1, %eax
	JE L42
	JMP L43
L43:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L42:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$2, %eax
	JE L40
	JMP L41
L41:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L40:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$3, %eax
	JE L38
	JMP L39
L39:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L38:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$4, %eax
	JE L36
	JMP L37
L37:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L36:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$5, %eax
	JE L34
	JMP L35
L35:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L34:
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rsi
	ADDQ	$8, %rsi
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, 0(%rsi)
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVQ	%rdi, 0(%rax)
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$10, %eax
	JE L32
	JMP L33
L33:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L32:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$9, %eax
	JE L30
	JMP L31
L31:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L30:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$8, %eax
	JE L28
	JMP L29
L29:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L28:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$7, %eax
	JE L26
	JMP L27
L27:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L26:
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_deq
	MOVL	%eax, %eax
	CMPL	$6, %eax
	JE L24
	JMP L25
L25:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	RET
L24:
	MOVL	$0, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
