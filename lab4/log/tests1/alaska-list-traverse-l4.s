	.file	"../tests1/alaska-list-traverse.l4"
	.text
	.global	_c0_make_list
_c0_make_list:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	%edi, %eax
	MOVQ	$0, %rdi
	MOVL	%eax, %eax
	JMP L9
L9:
	MOVQ	%rax, %rbp
	MOVQ	%rdi, %rbx
L4:
	CMPL	$0, %ebp
	JG L2
	JMP L3
L2:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	%ebp, 0(%rax)
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	%rbx, 0(%rax)
	MOVQ	%rdi, %rdi
	MOVL	%ebp, %eax
	SUBL	$1, %eax
	MOVL	%eax, %eax
	JMP L10
L10:
	MOVQ	%rax, %rbp
	MOVQ	%rdi, %rbx
L3:
	MOVQ	%rbx, %rax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$100000, %edi
	ANDL	$0, %eax
	CALL	_c0_make_list
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	JMP L11
L11:
	MOVQ	%rax, %rdi
L8:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JNE L6
	JMP L7
L6:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rax
	JMP L12
L12:
	MOVQ	%rax, %rdi
L7:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
