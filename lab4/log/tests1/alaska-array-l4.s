	.file	"../tests1/alaska-array.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$1000, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L15
	JMP L14
L15:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L14:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rdi
	MOVL	$0, %eax
	JMP L16
L16:
	MOVQ	%rax, %rdx
L10:
	CMPL	$1000, %edx
	JL L8
	JMP L9
L8:
	MOVQ	%rdi, %rdi
	MOVL	%edx, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L11
	JMP L12
L11:
	CMPL	$0, %esi
	JGE L13
	JMP L12
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L13:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rax, %rdi
	MOVL	%edx, %eax
	IMULL	%edx, %eax
	IMULL	%edx, %eax
	MOVL	%eax, 0(%rdi)
	MOVL	%edx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L17
L17:
	MOVQ	%rax, %rdx
L9:
	MOVL	$0, %edi
	MOVL	$0, %eax
	JMP L18
L18:
	MOVQ	%rax, %rcx
	MOVQ	%rdi, %rdx
L4:
	CMPL	$1000, %ecx
	JL L2
	JMP L3
L2:
	MOVQ	%rbp, %rsi
	MOVL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L5
	JMP L6
L5:
	CMPL	$0, %edi
	JGE L7
	JMP L6
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L7:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ADDL	%edx, %eax
	MOVL	%eax, %edi
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L19
L19:
	MOVQ	%rax, %rcx
	MOVQ	%rdi, %rdx
L3:
	MOVL	%edx, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
