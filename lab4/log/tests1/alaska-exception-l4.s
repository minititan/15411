	.file	"../tests1/alaska-exception.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L1:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$0, %rax
	MOVL	$0, 0(%rax)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVQ	%rdi, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rsi
	ADDQ	$0, %rsi
	MOVL	$10, 0(%rsi)
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$0, %rdi
	MOVL	0(%rdi), %esi
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rdi
	ADDQ	$0, %rdi
	MOVL	0(%rdi), %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%edi, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
