	.file	"../tests1/alabama-a-10.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %r12
	MOVL	$4, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L77
	JMP L76
L77:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L76:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %rbp
	MOVL	$4, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L75
	JMP L74
L75:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L74:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%r12)
	MOVQ	0(%r12), %rsi
	MOVL	$0, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L71
	JMP L72
L71:
	CMPL	$0, %edi
	JGE L73
	JMP L72
L72:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L73:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	$0, 0(%rax)
	MOVQ	0(%r12), %rdi
	MOVL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L68
	JMP L69
L68:
	CMPL	$0, %eax
	JGE L70
	JMP L69
L69:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L70:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	0(%r12), %rdi
	MOVL	$2, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L65
	JMP L66
L65:
	CMPL	$0, %esi
	JGE L67
	JMP L66
L66:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L67:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	$2, 0(%rax)
	MOVQ	0(%r12), %rdi
	MOVL	$3, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L62
	JMP L63
L62:
	CMPL	$0, %esi
	JGE L64
	JMP L63
L63:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L64:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	$3, 0(%rax)
	MOVQ	%rbp, %rdi
	MOVL	$0, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L59
	JMP L60
L59:
	CMPL	$0, %eax
	JGE L61
	JMP L60
L60:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L61:
	IMULL	$8, %eax
	MOVQ	%rdi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rsi
	MOVL	$1, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L56
	JMP L57
L56:
	CMPL	$0, %edi
	JGE L58
	JMP L57
L57:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L58:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	MOVQ	%rsi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rsi
	MOVL	$2, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L53
	JMP L54
L53:
	CMPL	$0, %edi
	JGE L55
	JMP L54
L54:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L55:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	MOVQ	%rsi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rsi
	MOVL	$3, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L50
	JMP L51
L50:
	CMPL	$0, %edi
	JGE L52
	JMP L51
L51:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L52:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	MOVQ	%rsi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%rbp, %rdi
	MOVL	$0, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L44
	JMP L45
L44:
	CMPL	$0, %eax
	JGE L46
	JMP L45
L45:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L46:
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rsi
	MOVQ	0(%r12), %rdx
	MOVL	$3, %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %eax
	CMPL	%eax, %edi
	JL L47
	JMP L48
L47:
	CMPL	$0, %edi
	JGE L49
	JMP L48
L48:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L49:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rsi)
	MOVQ	%rbp, %rsi
	MOVL	$1, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L38
	JMP L39
L38:
	CMPL	$0, %edi
	JGE L40
	JMP L39
L39:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L40:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rdx
	MOVQ	0(%r12), %rdi
	MOVL	$2, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L41
	JMP L42
L41:
	CMPL	$0, %eax
	JGE L43
	JMP L42
L42:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L43:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdx)
	MOVQ	%rbp, %rdi
	MOVL	$2, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	CMPL	%eax, %esi
	JL L32
	JMP L33
L32:
	CMPL	$0, %esi
	JGE L34
	JMP L33
L33:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L34:
	MOVL	%esi, %eax
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rdx
	MOVQ	0(%r12), %rsi
	MOVL	$2, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L35
	JMP L36
L35:
	CMPL	$0, %eax
	JGE L37
	JMP L36
L36:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L37:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdx)
	MOVQ	%rbp, %rsi
	MOVL	$3, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L26
	JMP L27
L26:
	CMPL	$0, %eax
	JGE L28
	JMP L27
L27:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L28:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rdx
	MOVQ	0(%r12), %rdi
	MOVL	$3, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L29
	JMP L30
L29:
	CMPL	$0, %esi
	JGE L31
	JMP L30
L30:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L31:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdx)
	MOVQ	%rbp, %rsi
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L23
	JMP L24
L23:
	CMPL	$0, %eax
	JGE L25
	JMP L24
L24:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L25:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %edx
	MOVQ	0(%r12), %rsi
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L20
	JMP L21
L20:
	CMPL	$0, %eax
	JGE L22
	JMP L21
L21:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L22:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ADDL	%eax, %edx
	MOVQ	%rbp, %rsi
	MOVL	$1, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L17
	JMP L18
L17:
	CMPL	$0, %eax
	JGE L19
	JMP L18
L18:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L19:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	MOVL	%edx, %edi
	ADDL	%eax, %edi
	MOVQ	0(%r12), %rdx
	MOVL	$1, %eax
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %esi
	CMPL	%esi, %eax
	JL L14
	JMP L15
L14:
	CMPL	$0, %eax
	JGE L16
	JMP L15
L15:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L16:
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	0(%rax), %eax
	MOVL	%edi, %edx
	ADDL	%eax, %edx
	MOVQ	%rbp, %rdi
	MOVL	$2, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	CMPL	%eax, %esi
	JL L11
	JMP L12
L11:
	CMPL	$0, %esi
	JGE L13
	JMP L12
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L13:
	MOVL	%esi, %eax
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	ADDL	%eax, %edx
	MOVQ	0(%r12), %rsi
	MOVL	$2, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L8
	JMP L9
L8:
	CMPL	$0, %edi
	JGE L10
	JMP L9
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L10:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ADDL	%eax, %edx
	MOVQ	%rbp, %rsi
	MOVL	$3, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L5
	JMP L6
L5:
	CMPL	$0, %edi
	JGE L7
	JMP L6
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L7:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	ADDL	%eax, %edx
	MOVQ	0(%r12), %rdi
	MOVL	$3, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L2
	JMP L3
L2:
	CMPL	$0, %eax
	JGE L4
	JMP L3
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L4:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	ADDL	%edx, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
