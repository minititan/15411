	.file	"../tests1/alaska-mdu-matrix-mult.l4"
	.text
	.global	_c0_mult
_c0_mult:
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$24, %rsp
L1:
	MOVQ	%rdi, %r13
	MOVQ	%rsi, %rbp
	MOVL	$3, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L35
	JMP L34
L35:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L34:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %rdi
	MOVL	$0, %eax
	JMP L75
L75:
	MOVQ	%rax, 8(%rsp)
	MOVQ	%rdi, 0(%rsp)
	MOVQ	%rbp, 16(%rsp)
	MOVQ	%r13, %rbp
L4:
	MOVL	8(%rsp), %r15d
	CMPL	$3, %r15d
	JL L2
	JMP L3
L2:
	MOVQ	0(%rsp), %rdi
	MOVL	8(%rsp), %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L29
	JMP L30
L29:
	CMPL	$0, %eax
	JGE L31
	JMP L30
L30:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L31:
	IMULL	$8, %eax
	MOVQ	%rdi, %r13
	ADDQ	%rax, %r13
	MOVL	$3, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L33
	JMP L32
L33:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L32:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%r13)
	MOVL	$0, %eax
	JMP L76
L76:
	MOVQ	%rax, %rbx
	MOVQ	8(%rsp), %r11
	MOVQ	0(%rsp), %rdi
	MOVQ	16(%rsp), %r10
	MOVQ	%rbp, %rdx
L7:
	CMPL	$3, %ebx
	JL L5
	JMP L6
L5:
	MOVL	$0, %eax
	JMP L77
L77:
	MOVQ	%rax, %r9
L10:
	CMPL	$3, %r9d
	JL L8
	JMP L9
L8:
	MOVQ	%rdi, %rsi
	MOVL	%r11d, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L11
	JMP L12
L11:
	CMPL	$0, %eax
	JGE L13
	JMP L12
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L13:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rdi
	MOVL	%ebx, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L14
	JMP L15
L14:
	CMPL	$0, %eax
	JGE L16
	JMP L15
L15:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L16:
	IMULL	$4, %eax
	MOVQ	%rdi, %r8
	ADDQ	%rax, %r8
	MOVL	0(%r8), %ecx
	MOVQ	%rdx, %rsi
	MOVL	%r11d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L23
	JMP L24
L23:
	CMPL	$0, %edi
	JGE L25
	JMP L24
L24:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L25:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rsi
	MOVL	%r9d, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L26
	JMP L27
L26:
	CMPL	$0, %edi
	JGE L28
	JMP L27
L27:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L28:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %edx
	MOVQ	%r10, %rsi
	MOVL	%r9d, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L17
	JMP L18
L17:
	CMPL	$0, %eax
	JGE L19
	JMP L18
L18:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L19:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rsi
	MOVL	%ebx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L20
	JMP L21
L20:
	CMPL	$0, %edi
	JGE L22
	JMP L21
L21:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L22:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ADDL	%edx, %eax
	ADDL	%ecx, %eax
	MOVL	%eax, 0(%r8)
	MOVL	%r9d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L78
L78:
	MOVQ	%rax, %r9
L9:
	MOVL	%ebx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L79
L79:
	MOVQ	%rax, %rbx
	MOVQ	%r12, %r11
	MOVQ	%r12, %rdi
	MOVQ	%r12, %r10
	MOVQ	%r12, %rdx
L6:
	MOVL	%r11d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L80
L80:
	MOVQ	%rax, 8(%rsp)
	MOVQ	%rdi, 0(%rsp)
	MOVQ	%r10, 16(%rsp)
	MOVQ	%rdx, %rbp
L3:
	MOVQ	0(%rsp), %rax
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$16, %rsp
L36:
	MOVL	$3, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L74
	JMP L73
L74:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L73:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %rbp
	MOVL	$3, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L72
	JMP L71
L72:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L71:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %rdi
	MOVL	$0, %eax
	JMP L81
L81:
	MOVQ	%rax, 0(%rsp)
	MOVQ	%rdi, %r13
	MOVQ	%rbp, %r12
L45:
	MOVL	0(%rsp), %r15d
	CMPL	$3, %r15d
	JL L43
	JMP L44
L43:
	MOVQ	%r12, %rdi
	MOVL	0(%rsp), %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %eax
	CMPL	%eax, %esi
	JL L66
	JMP L67
L66:
	CMPL	$0, %esi
	JGE L68
	JMP L67
L67:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L68:
	MOVL	%esi, %eax
	IMULL	$8, %eax
	MOVQ	%rdi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$3, %ebp
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	CMPL	$0, %ebp
	JL L70
	JMP L69
L70:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L69:
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebp, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	%r13, %rsi
	MOVL	0(%rsp), %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L61
	JMP L62
L61:
	CMPL	$0, %edi
	JGE L63
	JMP L62
L62:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L63:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	MOVQ	%rsi, %rbx
	ADDQ	%rax, %rbx
	MOVL	$3, %ebp
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	CMPL	$0, %ebp
	JL L65
	JMP L64
L65:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L64:
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebp, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%rbx)
	MOVL	$0, %eax
	JMP L82
L82:
	MOVQ	%rax, %rdx
L48:
	CMPL	$3, %edx
	JL L46
	JMP L47
L46:
	MOVQ	%r12, %rdi
	MOVL	0(%rsp), %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-8(%rdi), %esi
	CMPL	%esi, %eax
	JL L55
	JMP L56
L55:
	CMPL	$0, %eax
	JGE L57
	JMP L56
L56:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L57:
	IMULL	$8, %eax
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rsi
	MOVL	%edx, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L58
	JMP L59
L58:
	CMPL	$0, %eax
	JGE L60
	JMP L59
L59:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L60:
	IMULL	$4, %eax
	ADDQ	%rax, %rsi
	MOVL	$91, %edi
	MOVL	0(%rsp), %r15d
	IMULL	%r15d, %edi
	MOVL	$37, %eax
	IMULL	%edx, %eax
	ADDL	%edi, %eax
	MOVL	%eax, 0(%rsi)
	MOVQ	%r13, %rsi
	MOVL	0(%rsp), %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %edi
	CMPL	%edi, %eax
	JL L49
	JMP L50
L49:
	CMPL	$0, %eax
	JGE L51
	JMP L50
L50:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L51:
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rdi
	MOVL	%edx, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L52
	JMP L53
L52:
	CMPL	$0, %eax
	JGE L54
	JMP L53
L53:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L54:
	IMULL	$4, %eax
	ADDQ	%rax, %rdi
	MOVL	$17, %eax
	MOVL	0(%rsp), %r15d
	IMULL	%r15d, %eax
	MOVL	$5, %esi
	IMULL	%edx, %esi
	ADDL	%esi, %eax
	MOVL	%eax, 0(%rdi)
	MOVL	%edx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L83
L83:
	MOVQ	%rax, %rdx
L47:
	MOVL	0(%rsp), %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L84
L84:
	MOVQ	%rax, 0(%rsp)
	MOVQ	8(%rsp), %r13
	MOVQ	8(%rsp), %r12
L44:
	MOVQ	%r12, %rdi
	MOVQ	%r13, %rsi
	ANDL	$0, %eax
	CALL	_c0_mult
	MOVQ	%rax, %rsi
	MOVL	$1, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-8(%rsi), %eax
	CMPL	%eax, %edi
	JL L37
	JMP L38
L37:
	CMPL	$0, %edi
	JGE L39
	JMP L38
L38:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L39:
	MOVL	%edi, %eax
	IMULL	$8, %eax
	ADDQ	%rsi, %rax
	MOVQ	0(%rax), %rdi
	MOVL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L40
	JMP L41
L40:
	CMPL	$0, %eax
	JGE L42
	JMP L41
L41:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L42:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
