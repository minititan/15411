	.file	"../tests1/alabama-a-1.l4"
	.text
	.global	_c0_remove_red
_c0_remove_red:
	SUBQ	$8, %rsp
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %r13
	MOVL	%esi, %r12d
	MOVL	%edx, %ebp
	MOVL	%r12d, %ebx
	IMULL	%ebp, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L15
	JMP L14
L15:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
L14:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rax
	MOVL	$0, %edi
	JMP L50
L50:
	MOVQ	%rax, %rsi
	MOVQ	%rdi, %r10
	MOVQ	%rbp, %rax
	MOVQ	%r12, %r8
	MOVQ	%r13, %r9
L4:
	CMPL	%eax, %r10d
	JL L2
	JMP L3
L2:
	MOVL	$0, %eax
	JMP L51
L51:
	MOVQ	%rax, %rcx
L7:
	CMPL	%r8d, %ecx
	JL L5
	JMP L6
L5:
	MOVQ	%rsi, %rsi
	MOVL	%r10d, %eax
	IMULL	%r8d, %eax
	MOVL	%eax, %edi
	ADDL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L8
	JMP L9
L8:
	CMPL	$0, %edi
	JGE L10
	JMP L9
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
L10:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	MOVQ	%rsi, %rdx
	ADDQ	%rax, %rdx
	MOVQ	%r9, %rsi
	MOVL	%r10d, %eax
	IMULL	%r8d, %eax
	ADDL	%ecx, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L11
	JMP L12
L11:
	CMPL	$0, %eax
	JGE L13
	JMP L12
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
L13:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ANDL	$-16711681, %eax
	MOVL	%eax, 0(%rdx)
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L52
L52:
	MOVQ	%rax, %rcx
L6:
	MOVL	%r10d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L53
L53:
	MOVQ	0(%rsp), %rsi
	MOVQ	%rax, %r10
	MOVQ	0(%rsp), %rax
	MOVQ	0(%rsp), %r8
	MOVQ	0(%rsp), %r9
L3:
	MOVQ	%rsi, %rax
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$16, %rsp
L16:
	MOVL	$-1071772, 8(%rsp)
	MOVL	$305419896, %r13d
	MOVL	$10, %ebp
	MOVL	$10, 0(%rsp)
	MOVL	%ebp, %r12d
	MOVL	0(%rsp), %r15d
	IMULL	%r15d, %r12d
	MOVL	$1, %eax
	ADDL	%r12d, %eax
	CMPL	$0, %r12d
	JL L49
	JMP L48
L49:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L48:
	MOVL	$1, %eax
	ADDL	%r12d, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVL	%r12d, 0(%rdi)
	ADDQ	$4, %rdi
	MOVQ	%rdi, %rdi
	MOVL	$0, %esi
	JMP L54
L54:
	MOVQ	%rdi, %r11
	MOVQ	%rsi, %r10
	MOVQ	0(%rsp), %r12
	MOVQ	%rbp, %rbp
	MOVQ	%r13, %r9
	MOVQ	8(%rsp), %r8
L35:
	CMPL	%r12d, %r10d
	JL L33
	JMP L34
L33:
	MOVL	$0, %edi
	JMP L55
L55:
	MOVQ	%rdi, %rcx
L38:
	CMPL	%ebp, %ecx
	JL L36
	JMP L37
L36:
	MOVL	%r10d, %edi
	IMULL	%ecx, %edi
	MOVL	$2, %esi
	MOVL	%edi, %eax
	CLTD
	IDIVL	%esi
	MOVL	%edx, %edi
	CMPL	$0, %edi
	JE L39
	JMP L40
L39:
	MOVQ	%r11, %rsi
	MOVL	%r10d, %eax
	IMULL	%r12d, %eax
	MOVL	%eax, %edi
	ADDL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L44
	JMP L45
L44:
	CMPL	$0, %edi
	JGE L46
	JMP L45
L45:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L46:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	%r8d, 0(%rax)
	JMP L47
L40:
	MOVQ	%r11, %rsi
	MOVL	%r10d, %eax
	IMULL	%r12d, %eax
	MOVL	%eax, %edi
	ADDL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L41
	JMP L42
L41:
	CMPL	$0, %edi
	JGE L43
	JMP L42
L42:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L43:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	%r9d, 0(%rax)
	JMP L47
L47:
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L56
L56:
	MOVQ	%rax, %rcx
L37:
	MOVL	%r10d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L57
L57:
	MOVQ	%rbx, %r11
	MOVQ	%rax, %r10
	MOVQ	%rbx, %r12
	MOVQ	%rbx, %rbp
	MOVQ	%rbx, %r9
	MOVQ	%rbx, %r8
L34:
	MOVQ	%r11, %rdi
	MOVL	%ebp, %esi
	MOVL	%r12d, %edx
	ANDL	$0, %eax
	CALL	_c0_remove_red
	MOVQ	%rax, %rdi
	MOVQ	%rdi, %rdi
	MOVL	$0, %esi
	JMP L58
L58:
	MOVQ	%rdi, %r10
	MOVQ	%rsi, %r9
	MOVQ	%r12, %r8
	MOVQ	%rbp, %rsi
L19:
	CMPL	%r8d, %r9d
	JL L17
	JMP L18
L17:
	MOVL	$0, %edi
	JMP L59
L59:
	MOVQ	%rdi, %rcx
L22:
	CMPL	%esi, %ecx
	JL L20
	JMP L21
L20:
	MOVL	%r9d, %edi
	IMULL	%ecx, %edi
	MOVL	$2, %esi
	MOVL	%edi, %eax
	CLTD
	IDIVL	%esi
	MOVL	%edx, %edi
	CMPL	$0, %edi
	JE L25
	JMP L26
L25:
	MOVQ	%r10, %rsi
	MOVL	%r9d, %eax
	IMULL	%r8d, %eax
	MOVL	%eax, %edi
	ADDL	%ecx, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L30
	JMP L31
L30:
	CMPL	$0, %edi
	JGE L32
	JMP L31
L31:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L32:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	CMPL	$-16734876, %eax
	JE L23
	JMP L24
L26:
	MOVQ	%r10, %rsi
	MOVL	%r9d, %eax
	IMULL	%r8d, %eax
	ADDL	%ecx, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L27
	JMP L28
L27:
	CMPL	$0, %eax
	JGE L29
	JMP L28
L28:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L29:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	CMPL	$302012024, %eax
	JE L23
	JMP L24
L24:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L23:
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L60
L60:
	MOVQ	%rax, %rcx
L21:
	MOVL	%r9d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L61
L61:
	MOVQ	%rbx, %r10
	MOVQ	%rax, %r9
	MOVQ	%rbx, %r8
	MOVQ	%rbx, %rsi
L18:
	MOVL	$0, %eax
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
