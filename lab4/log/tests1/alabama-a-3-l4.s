	.file	"../tests1/alabama-a-3.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %r13
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%r13)
	MOVQ	0(%r13), %rbx
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	0(%r13), %rax
	MOVQ	0(%rax), %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	MOVQ	0(%r13), %rax
	MOVQ	%rax, %r12
	MOVQ	0(%r12), %rax
	MOVQ	%rax, %rbp
	MOVQ	0(%rbp), %rax
	MOVQ	%rax, %rdi
	MOVL	$5, %eax
	MOVL	%eax, 0(%rdi)
	MOVQ	0(%rbp), %rax
	MOVL	0(%rax), %eax
	CMPL	$5, %eax
	JE L20
	JMP L21
L21:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L20:
	MOVQ	0(%r12), %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	CMPL	$5, %eax
	JE L18
	JMP L19
L19:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L18:
	MOVQ	0(%r13), %rax
	MOVQ	0(%rax), %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	CMPL	$5, %eax
	JE L16
	JMP L17
L17:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L16:
	MOVQ	0(%r13), %rax
	MOVQ	0(%rax), %rax
	MOVQ	0(%rax), %rax
	MOVL	$10, 0(%rax)
	MOVL	0(%rdi), %eax
	CMPL	$10, %eax
	JE L14
	JMP L15
L15:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L14:
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	MOVL	$5, 0(%rbx)
	MOVL	0(%rbx), %eax
	CMPL	$5, %eax
	JE L12
	JMP L13
L13:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L12:
	MOVQ	0(%rbp), %rax
	MOVL	0(%rax), %eax
	CMPL	$10, %eax
	JE L10
	JMP L11
L11:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L10:
	MOVQ	0(%r12), %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	CMPL	$10, %eax
	JE L8
	JMP L9
L9:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L8:
	MOVQ	0(%r13), %rax
	MOVQ	0(%rax), %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	CMPL	$10, %eax
	JE L6
	JMP L7
L7:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L6:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%r12)
	MOVQ	0(%r12), %rax
	MOVQ	%rbx, 0(%rax)
	MOVQ	0(%r12), %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	CMPL	$5, %eax
	JE L4
	JMP L5
L5:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L4:
	MOVQ	0(%rbp), %rax
	MOVL	0(%rax), %eax
	CMPL	$10, %eax
	JE L2
	JMP L3
L3:
	ANDL	$0, %eax
	CALL	abort
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L2:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
