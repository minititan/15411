	.file	"../tests1/alabama-g-6.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L1:
	MOVL	$5, %ebx
	NEGL	%ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L9
	JMP L8
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L8:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rsi
	MOVQ	%rsi, %rdx
	MOVL	$0, %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %eax
	CMPL	%eax, %edi
	JL L5
	JMP L6
L5:
	CMPL	$0, %edi
	JGE L7
	JMP L6
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L7:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	$5, 0(%rax)
	MOVQ	%rsi, %rdi
	MOVL	$0, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L2
	JMP L3
L2:
	CMPL	$0, %esi
	JGE L4
	JMP L3
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L4:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
