	.file	"../tests1/alabama-a-4.l4"
	.text
	.global	_c0_clear
_c0_clear:
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rsi
	ADDQ	$0, %rsi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$8, %rdi
	MOVQ	0(%rdi), %rdi
	MOVQ	0(%rdi), %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	ADDQ	$0, %rdi
	MOVQ	0(%rdi), %rdi
	MOVQ	%rdi, 0(%rsi)
	MOVQ	$0, %rdi
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	%rbp, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$8, %rbx
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	$0, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	$0, 0(%rax)
	MOVQ	%rbp, %rdi
	ANDL	$0, %eax
	CALL	_c0_clear
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	$0, 0(%rax)
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
