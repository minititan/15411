	.file	"../tests1/alabama-a-7.l4"
	.text
	.global	_c0_is_acyclic
_c0_is_acyclic:
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %rdi
	MOVQ	%rsi, %rdx
	CMPQ	$0, %rdi
	JE L15
	JMP L16
L15:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L16:
	JMP L17
L17:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rsi
	MOVQ	%rdi, %rax
	JMP L23
L23:
	MOVQ	%rax, %rcx
	MOVQ	%rsi, %rdi
L4:
	CMPQ	%rcx, %rdi
	JNE L2
	JMP L3
L2:
	CMPQ	$0, %rdi
	JE L12
	JMP L13
L12:
	JMP L10
L13:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	CMPQ	$0, %rax
	JE L10
	JMP L11
L10:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L11:
	JMP L14
L14:
	CMPQ	%rdx, %rdi
	JE L7
	JMP L8
L7:
	JMP L5
L8:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	CMPQ	%rdx, %rax
	JE L5
	JMP L6
L5:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L6:
	JMP L9
L9:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVQ	%rcx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rax
	JMP L24
L24:
	MOVQ	%rax, %rcx
	MOVQ	%rdi, %rdi
L3:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$48, %rsp
L18:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 24(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 32(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %r12
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 16(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 40(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 8(%rsp)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %r13
	MOVQ	24(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	24(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	%rbx, 0(%rax)
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	%rbp, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	32(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	32(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	32(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	0(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	0(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	0(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	%r12, 0(%rax)
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$8, %rax
	MOVQ	16(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	16(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	16(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	40(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	40(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	40(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	0(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	8(%rsp), %r15
	TESTQ	%r15, %r15
	jz	_c0_call_raise
	MOVQ	8(%rsp), %rax
	ADDQ	$8, %rax
	MOVQ	%r13, 0(%rax)
	TESTQ	%r13, %r13
	jz	_c0_call_raise
	MOVQ	%r13, %rax
	ADDQ	$8, %rax
	MOVQ	24(%rsp), %r15
	MOVQ	%r15, 0(%rax)
	MOVQ	24(%rsp), %rdi
	MOVQ	0(%rsp), %rsi
	ANDL	$0, %eax
	CALL	_c0_is_acyclic
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L21
	JMP L22
L22:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$48, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L21:
	MOVQ	24(%rsp), %rdi
	MOVQ	%r13, %rsi
	ANDL	$0, %eax
	CALL	_c0_is_acyclic
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L20
	JMP L19
L20:
	ANDL	$0, %eax
	CALL	abort
	ADDQ	$48, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
L19:
	MOVL	$0, %eax
	ADDQ	$48, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
