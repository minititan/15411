	.file	"../tests1/alabama-g-9.l4"
	.text
	.global	_c0_is_palindrome
_c0_is_palindrome:
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %rcx
	MOVL	%esi, %r8d
	MOVL	$0, %edi
	JMP L29
L29:
	MOVQ	%rdi, %r9
L4:
	MOVL	$2, %edi
	MOVL	%r8d, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	CMPL	%edi, %r9d
	JLE L2
	JMP L3
L2:
	MOVQ	%rcx, %rdi
	MOVL	%r9d, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L10
	JMP L11
L10:
	CMPL	$0, %esi
	JGE L12
	JMP L11
L11:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L12:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %edx
	MOVQ	%rcx, %rsi
	MOVL	%r8d, %eax
	SUBL	%r9d, %eax
	MOVL	%eax, %edi
	SUBL	$1, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L7
	JMP L8
L7:
	CMPL	$0, %edi
	JGE L9
	JMP L8
L8:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L9:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	CMPL	%eax, %edx
	JNE L5
	JMP L6
L5:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L6:
	JMP L13
L13:
	MOVL	%r9d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L30
L30:
	MOVQ	%rax, %r9
L3:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L14:
	MOVL	$25, %ebx
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	CMPL	$0, %ebx
	JL L28
	JMP L27
L28:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L27:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rcx
	MOVL	$0, %eax
	JMP L31
L31:
	MOVQ	%rax, %rdx
L20:
	CMPL	$25, %edx
	JL L18
	JMP L19
L18:
	MOVQ	%rcx, %rdi
	MOVL	%edx, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L21
	JMP L22
L21:
	CMPL	$0, %eax
	JGE L23
	JMP L22
L22:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L23:
	IMULL	$4, %eax
	MOVQ	%rdi, %rsi
	ADDQ	%rax, %rsi
	MOVL	$12, %edi
	SUBL	%edx, %edi
	MOVL	%edx, %eax
	SUBL	$12, %eax
	CMPL	%eax, %edi
	JL L24
	JMP L25
L24:
	MOVL	%edx, %eax
	SUBL	$12, %eax
	MOVL	%eax, %eax
	JMP L32
L32:
	MOVQ	%rax, %rax
L25:
	MOVL	$12, %eax
	SUBL	%edx, %eax
	MOVL	%eax, %eax
	JMP L33
L33:
	MOVQ	%rax, %rax
L26:
	MOVL	%eax, 0(%rsi)
	MOVL	%edx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L34
L34:
	MOVQ	%rax, %rdx
L19:
	MOVQ	%rcx, %rdi
	MOVL	$25, %esi
	ANDL	$0, %eax
	CALL	_c0_is_palindrome
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L15
	JMP L16
L15:
	MOVL	$0, %eax
	POPQ	%rbx
	RET
L16:
	JMP L17
L17:
	MOVL	$1, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
