	.file	"../tests1/arkansas-return01.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L1:
	MOVL	$10, %ebx
	CMPL	$0, %ebx
	JL L17
	JMP L15
L15:
	CMPL	$268435454, %ebx
	JG L17
	JMP L16
L17:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L16:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rax
	MOVL	$0, %edi
	MOVL	$0, %esi
L4:
	CMPL	$10, %edi
	JL L2
	JMP L3
L2:
	MOVQ	%rax, %rdx
	MOVL	%edi, %ecx
	MOVQ	$0, %r15
	CMPQ	%rdx, %r15
	JE L10
	JMP L11
L10:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L11:
	MOVL	-4(%rdx), %r8d
	CMPL	%r8d, %ecx
	JL L12
	JMP L13
L12:
	CMPL	$0, %ecx
	JGE L14
	JMP L13
L13:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L14:
	IMULL	$4, %ecx
	ADDQ	%rcx, %rdx
	MOVL	%edi, 0(%rdx)
	MOVQ	%rax, %rdx
	MOVL	%edi, %ecx
	MOVQ	$0, %r15
	CMPQ	%rdx, %r15
	JE L5
	JMP L6
L5:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L6:
	MOVL	-4(%rdx), %r8d
	CMPL	%r8d, %ecx
	JL L7
	JMP L8
L7:
	CMPL	$0, %ecx
	JGE L9
	JMP L8
L8:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
L9:
	IMULL	$4, %ecx
	ADDQ	%rcx, %rdx
	MOVL	0(%rdx), %edx
	ADDL	%edx, %esi
	MOVL	%esi, %esi
	INCL	%edi
	MOVL	%edi, %edi
	JMP L4
L3:
	MOVL	%esi, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
