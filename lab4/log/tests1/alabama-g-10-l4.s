	.file	"../tests1/alabama-g-10.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	%rax, %rbx
	ADDQ	$0, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVL	$2, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVQ	0(%rax), %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
