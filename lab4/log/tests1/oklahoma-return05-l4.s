	.file	"../tests1/oklahoma-return05.l4"
	.text
	.global	_c0_sum
_c0_sum:
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %rax
	MOVL	%esi, %edi
	MOVL	%edx, %esi
	MOVL	$0, %edx
	MOVL	$0, %ecx
L4:
	CMPL	%edi, %ecx
	JL L2
	JMP L3
L2:
	MOVL	$0, %r8d
L7:
	CMPL	%esi, %r8d
	JL L5
	JMP L6
L5:
	MOVQ	%rax, %r9
	MOVL	%ecx, %r10d
	MOVQ	$0, %r15
	CMPQ	%r9, %r15
	JE L8
	JMP L9
L8:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L9:
	MOVL	-8(%r9), %r11d
	CMPL	%r11d, %r10d
	JL L10
	JMP L11
L10:
	CMPL	$0, %r10d
	JGE L12
	JMP L11
L11:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L12:
	IMULL	$8, %r10d
	ADDQ	%r10, %r9
	MOVQ	0(%r9), %r9
	MOVL	%r8d, %r10d
	MOVQ	$0, %r15
	CMPQ	%r9, %r15
	JE L13
	JMP L14
L13:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L14:
	MOVL	-4(%r9), %r11d
	CMPL	%r11d, %r10d
	JL L15
	JMP L16
L15:
	CMPL	$0, %r10d
	JGE L17
	JMP L16
L16:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L17:
	IMULL	$4, %r10d
	ADDQ	%r10, %r9
	MOVL	0(%r9), %r9d
	ADDL	%r9d, %edx
	MOVL	%edx, %edx
	INCL	%r8d
	MOVL	%r8d, %r8d
	JMP L7
L6:
	INCL	%ecx
	MOVL	%ecx, %ecx
	JMP L4
L3:
	MOVL	%edx, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_iter
_c0_iter:
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$24, %rsp
L18:
	MOVQ	%rdi, %rbx
	MOVL	%esi, %ebp
	MOVL	%edx, %r12d
	MOVL	$10, %r13d
	CMPL	$0, %r13d
	JL L237
	JMP L236
L237:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L236:
	MOVL	$1, %eax
	ADDL	%r13d, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVQ	%r13, 0(%rdi)
	ADDQ	$8, %rdi
	MOVQ	%rdi, %r13
	MOVL	$0, 0(%rsp)
L228:
	MOVL	0(%rsp), %r15d
	CMPL	$10, %r15d
	JL L226
	JMP L227
L226:
	MOVQ	%r13, %rax
	MOVL	0(%rsp), %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L229
	JMP L230
L229:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L230:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L231
	JMP L232
L231:
	CMPL	$0, %edi
	JGE L233
	JMP L232
L232:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L233:
	IMULL	$8, %edi
	MOVQ	%rax, 8(%rsp)
	ADDQ	%rdi, 8(%rsp)
	MOVL	$10, 16(%rsp)
	MOVL	16(%rsp), %r15d
	CMPL	$0, %r15d
	JL L235
	JMP L234
L235:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L234:
	MOVL	$1, %eax
	ADDL	16(%rsp), %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rdi
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 0(%rdi)
	ADDQ	$4, %rdi
	MOVQ	8(%rsp), %r15
	MOVQ	%rdi, 0(%r15)
	MOVL	0(%rsp), %edi
	ADDL	$1, %edi
	MOVL	%edi, 0(%rsp)
	JMP L228
L227:
	MOVL	$0, %edi
L47:
	CMPL	%ebp, %edi
	JL L45
	JMP L46
L45:
	MOVL	$0, %esi
L50:
	CMPL	%r12d, %esi
	JL L48
	JMP L49
L48:
	MOVL	%edi, %ecx
	ADDL	%ebp, %ecx
	MOVL	%ecx, %r8d
	SUBL	$1, %r8d
	MOVL	%ebp, %ecx
	MOVL	%r8d, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %ecx
	MOVL	%ecx, %ecx
	MOVL	%edi, %r9d
	ADDL	$1, %r9d
	MOVL	%ebp, %r8d
	MOVL	%r9d, %eax
	CLTD
	IDIVL	%r8d
	MOVL	%edx, %r8d
	MOVL	%r8d, %r8d
	MOVL	%esi, %r9d
	ADDL	%r12d, %r9d
	MOVL	%r9d, %r10d
	SUBL	$1, %r10d
	MOVL	%r12d, %r9d
	MOVL	%r10d, %eax
	CLTD
	IDIVL	%r9d
	MOVL	%edx, %r9d
	MOVL	%r9d, %r9d
	MOVL	%esi, %r11d
	ADDL	$1, %r11d
	MOVL	%r12d, %r10d
	MOVL	%r11d, %eax
	CLTD
	IDIVL	%r10d
	MOVL	%edx, %r10d
	MOVL	%r10d, %r10d
	MOVQ	%rbx, %r11
	MOVL	%ecx, 0(%rsp)
	MOVQ	$0, %r15
	CMPQ	%r11, %r15
	JE L216
	JMP L217
L216:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L217:
	MOVL	-8(%r11), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	CMPL	%r15d, 0(%rsp)
	JL L218
	JMP L219
L218:
	MOVL	0(%rsp), %r15d
	CMPL	$0, %r15d
	JGE L220
	JMP L219
L219:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L220:
	MOVL	0(%rsp), %r15d
	IMULL	$8, %r15d
	MOVL	%r15d, 0(%rsp)
	ADDQ	0(%rsp), %r11
	MOVQ	0(%r11), %r11
	MOVL	%r9d, 0(%rsp)
	MOVQ	$0, %r15
	CMPQ	%r11, %r15
	JE L221
	JMP L222
L221:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L222:
	MOVL	-4(%r11), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	CMPL	%r15d, 0(%rsp)
	JL L223
	JMP L224
L223:
	MOVL	0(%rsp), %r15d
	CMPL	$0, %r15d
	JGE L225
	JMP L224
L224:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L225:
	MOVL	0(%rsp), %r15d
	IMULL	$4, %r15d
	MOVL	%r15d, 0(%rsp)
	ADDQ	0(%rsp), %r11
	MOVL	0(%r11), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVQ	%rbx, %r11
	MOVL	%ecx, 0(%rsp)
	MOVQ	$0, %r15
	CMPQ	%r11, %r15
	JE L206
	JMP L207
L206:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L207:
	MOVL	-8(%r11), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	CMPL	%r15d, 0(%rsp)
	JL L208
	JMP L209
L208:
	MOVL	0(%rsp), %r15d
	CMPL	$0, %r15d
	JGE L210
	JMP L209
L209:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L210:
	MOVL	0(%rsp), %r15d
	IMULL	$8, %r15d
	MOVL	%r15d, 0(%rsp)
	ADDQ	0(%rsp), %r11
	MOVQ	0(%r11), %r11
	MOVL	%esi, 0(%rsp)
	MOVQ	$0, %r15
	CMPQ	%r11, %r15
	JE L211
	JMP L212
L211:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L212:
	MOVL	-4(%r11), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	CMPL	%r15d, 0(%rsp)
	JL L213
	JMP L214
L213:
	MOVL	0(%rsp), %r15d
	CMPL	$0, %r15d
	JGE L215
	JMP L214
L214:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L215:
	MOVL	0(%rsp), %r15d
	IMULL	$4, %r15d
	MOVL	%r15d, 0(%rsp)
	ADDQ	0(%rsp), %r11
	MOVL	0(%r11), %r11d
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	ADDL	%r11d, 8(%rsp)
	MOVQ	%rbx, %r11
	MOVL	%ecx, %ecx
	MOVQ	$0, %r15
	CMPQ	%r11, %r15
	JE L196
	JMP L197
L196:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L197:
	MOVL	-8(%r11), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %ecx
	JL L198
	JMP L199
L198:
	CMPL	$0, %ecx
	JGE L200
	JMP L199
L199:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L200:
	IMULL	$8, %ecx
	ADDQ	%r11, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%r10d, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L201
	JMP L202
L201:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L202:
	MOVL	-4(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L203
	JMP L204
L203:
	CMPL	$0, %r11d
	JGE L205
	JMP L204
L204:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L205:
	IMULL	$4, %r11d
	ADDQ	%r11, %rcx
	MOVL	0(%rcx), %ecx
	ADDL	%ecx, 8(%rsp)
	MOVQ	%rbx, %rcx
	MOVL	%edi, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L186
	JMP L187
L186:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L187:
	MOVL	-8(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L188
	JMP L189
L188:
	CMPL	$0, %r11d
	JGE L190
	JMP L189
L189:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L190:
	IMULL	$8, %r11d
	ADDQ	%r11, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%r9d, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L191
	JMP L192
L191:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L192:
	MOVL	-4(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L193
	JMP L194
L193:
	CMPL	$0, %r11d
	JGE L195
	JMP L194
L194:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L195:
	IMULL	$4, %r11d
	ADDQ	%r11, %rcx
	MOVL	0(%rcx), %ecx
	ADDL	%ecx, 8(%rsp)
	MOVQ	%rbx, %rcx
	MOVL	%edi, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L176
	JMP L177
L176:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L177:
	MOVL	-8(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L178
	JMP L179
L178:
	CMPL	$0, %r11d
	JGE L180
	JMP L179
L179:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L180:
	IMULL	$8, %r11d
	ADDQ	%r11, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%r10d, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L181
	JMP L182
L181:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L182:
	MOVL	-4(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L183
	JMP L184
L183:
	CMPL	$0, %r11d
	JGE L185
	JMP L184
L184:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L185:
	IMULL	$4, %r11d
	ADDQ	%r11, %rcx
	MOVL	0(%rcx), %ecx
	ADDL	%ecx, 8(%rsp)
	MOVQ	%rbx, %rcx
	MOVL	%r8d, %r11d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L166
	JMP L167
L166:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L167:
	MOVL	-8(%rcx), %r15d
	MOVL	%r15d, 0(%rsp)
	CMPL	0(%rsp), %r11d
	JL L168
	JMP L169
L168:
	CMPL	$0, %r11d
	JGE L170
	JMP L169
L169:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L170:
	IMULL	$8, %r11d
	ADDQ	%r11, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%r9d, %r9d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L171
	JMP L172
L171:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L172:
	MOVL	-4(%rcx), %r11d
	CMPL	%r11d, %r9d
	JL L173
	JMP L174
L173:
	CMPL	$0, %r9d
	JGE L175
	JMP L174
L174:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L175:
	IMULL	$4, %r9d
	ADDQ	%r9, %rcx
	MOVL	0(%rcx), %ecx
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 0(%rsp)
	ADDL	%ecx, 0(%rsp)
	MOVQ	%rbx, %rcx
	MOVL	%r8d, %r9d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L156
	JMP L157
L156:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L157:
	MOVL	-8(%rcx), %r11d
	CMPL	%r11d, %r9d
	JL L158
	JMP L159
L158:
	CMPL	$0, %r9d
	JGE L160
	JMP L159
L159:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L160:
	IMULL	$8, %r9d
	ADDQ	%r9, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r9d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L161
	JMP L162
L161:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L162:
	MOVL	-4(%rcx), %r11d
	CMPL	%r11d, %r9d
	JL L163
	JMP L164
L163:
	CMPL	$0, %r9d
	JGE L165
	JMP L164
L164:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L165:
	IMULL	$4, %r9d
	ADDQ	%r9, %rcx
	MOVL	0(%rcx), %ecx
	MOVL	0(%rsp), %r11d
	ADDL	%ecx, %r11d
	MOVQ	%rbx, %rcx
	MOVL	%r8d, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L146
	JMP L147
L146:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L147:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L148
	JMP L149
L148:
	CMPL	$0, %r8d
	JGE L150
	JMP L149
L149:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L150:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%r10d, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L151
	JMP L152
L151:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L152:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L153
	JMP L154
L153:
	CMPL	$0, %r8d
	JGE L155
	JMP L154
L154:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L155:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVL	0(%rcx), %ecx
	ADDL	%r11d, %ecx
	MOVL	%ecx, %ecx
	MOVQ	%rbx, %r8
	MOVL	%edi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L55
	JMP L56
L55:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L56:
	MOVL	-8(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L57
	JMP L58
L57:
	CMPL	$0, %r9d
	JGE L59
	JMP L58
L58:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L59:
	IMULL	$8, %r9d
	ADDQ	%r9, %r8
	MOVQ	0(%r8), %r8
	MOVL	%esi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L60
	JMP L61
L60:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L61:
	MOVL	-4(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L62
	JMP L63
L62:
	CMPL	$0, %r9d
	JGE L64
	JMP L63
L63:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L64:
	IMULL	$4, %r9d
	ADDQ	%r9, %r8
	MOVL	0(%r8), %r8d
	CMPL	$0, %r8d
	JE L53
	JMP L54
L53:
	CMPL	$3, %ecx
	JE L51
	JMP L52
L54:
	JMP L52
L51:
	MOVQ	%r13, %rcx
	MOVL	%edi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L135
	JMP L136
L135:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L136:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L137
	JMP L138
L137:
	CMPL	$0, %r8d
	JGE L139
	JMP L138
L138:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L139:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L140
	JMP L141
L140:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L141:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L142
	JMP L143
L142:
	CMPL	$0, %r8d
	JGE L144
	JMP L143
L143:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L144:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVL	$1, 0(%rcx)
	JMP L145
L52:
	MOVQ	%rbx, %r8
	MOVL	%edi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L69
	JMP L70
L69:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L70:
	MOVL	-8(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L71
	JMP L72
L71:
	CMPL	$0, %r9d
	JGE L73
	JMP L72
L72:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L73:
	IMULL	$8, %r9d
	ADDQ	%r9, %r8
	MOVQ	0(%r8), %r8
	MOVL	%esi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L74
	JMP L75
L74:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L75:
	MOVL	-4(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L76
	JMP L77
L76:
	CMPL	$0, %r9d
	JGE L78
	JMP L77
L77:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L78:
	IMULL	$4, %r9d
	ADDQ	%r9, %r8
	MOVL	0(%r8), %r8d
	CMPL	$1, %r8d
	JE L67
	JMP L68
L67:
	CMPL	$2, %ecx
	JL L65
	JMP L66
L68:
	JMP L66
L65:
	MOVQ	%r13, %rcx
	MOVL	%edi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L124
	JMP L125
L124:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L125:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L126
	JMP L127
L126:
	CMPL	$0, %r8d
	JGE L128
	JMP L127
L127:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L128:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L129
	JMP L130
L129:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L130:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L131
	JMP L132
L131:
	CMPL	$0, %r8d
	JGE L133
	JMP L132
L132:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L133:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVL	$0, 0(%rcx)
	JMP L134
L66:
	MOVQ	%rbx, %r8
	MOVL	%edi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L83
	JMP L84
L83:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L84:
	MOVL	-8(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L85
	JMP L86
L85:
	CMPL	$0, %r9d
	JGE L87
	JMP L86
L86:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L87:
	IMULL	$8, %r9d
	ADDQ	%r9, %r8
	MOVQ	0(%r8), %r8
	MOVL	%esi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L88
	JMP L89
L88:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L89:
	MOVL	-4(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L90
	JMP L91
L90:
	CMPL	$0, %r9d
	JGE L92
	JMP L91
L91:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L92:
	IMULL	$4, %r9d
	ADDQ	%r9, %r8
	MOVL	0(%r8), %r8d
	CMPL	$1, %r8d
	JE L81
	JMP L82
L81:
	CMPL	$3, %ecx
	JG L79
	JMP L80
L82:
	JMP L80
L79:
	MOVQ	%r13, %rcx
	MOVL	%edi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L113
	JMP L114
L113:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L114:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L115
	JMP L116
L115:
	CMPL	$0, %r8d
	JGE L117
	JMP L116
L116:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L117:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L118
	JMP L119
L118:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L119:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L120
	JMP L121
L120:
	CMPL	$0, %r8d
	JGE L122
	JMP L121
L121:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L122:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVL	$0, 0(%rcx)
	JMP L123
L80:
	MOVQ	%r13, %rcx
	MOVL	%edi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L93
	JMP L94
L93:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L94:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L95
	JMP L96
L95:
	CMPL	$0, %r8d
	JGE L97
	JMP L96
L96:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L97:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L98
	JMP L99
L98:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L99:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L100
	JMP L101
L100:
	CMPL	$0, %r8d
	JGE L102
	JMP L101
L101:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L102:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVQ	%rbx, %r8
	MOVL	%edi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L103
	JMP L104
L103:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L104:
	MOVL	-8(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L105
	JMP L106
L105:
	CMPL	$0, %r9d
	JGE L107
	JMP L106
L106:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L107:
	IMULL	$8, %r9d
	ADDQ	%r9, %r8
	MOVQ	0(%r8), %r8
	MOVL	%esi, %r9d
	MOVQ	$0, %r15
	CMPQ	%r8, %r15
	JE L108
	JMP L109
L108:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L109:
	MOVL	-4(%r8), %r10d
	CMPL	%r10d, %r9d
	JL L110
	JMP L111
L110:
	CMPL	$0, %r9d
	JGE L112
	JMP L111
L111:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L112:
	IMULL	$4, %r9d
	ADDQ	%r9, %r8
	MOVL	0(%r8), %r8d
	MOVL	%r8d, 0(%rcx)
	JMP L123
L123:
	JMP L134
L134:
	JMP L145
L145:
	INCL	%esi
	MOVL	%esi, %esi
	JMP L50
L49:
	INCL	%edi
	MOVL	%edi, %edi
	JMP L47
L46:
	MOVL	$0, %edi
L21:
	CMPL	%ebp, %edi
	JL L19
	JMP L20
L19:
	MOVL	$0, %esi
L24:
	CMPL	%r12d, %esi
	JL L22
	JMP L23
L22:
	MOVQ	%rbx, %rdx
	MOVL	%edi, %ecx
	MOVQ	$0, %r15
	CMPQ	%rdx, %r15
	JE L25
	JMP L26
L25:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L26:
	MOVL	-8(%rdx), %r8d
	CMPL	%r8d, %ecx
	JL L27
	JMP L28
L27:
	CMPL	$0, %ecx
	JGE L29
	JMP L28
L28:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L29:
	IMULL	$8, %ecx
	ADDQ	%rcx, %rdx
	MOVQ	0(%rdx), %rdx
	MOVL	%esi, %ecx
	MOVQ	$0, %r15
	CMPQ	%rdx, %r15
	JE L30
	JMP L31
L30:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L31:
	MOVL	-4(%rdx), %r8d
	CMPL	%r8d, %ecx
	JL L32
	JMP L33
L32:
	CMPL	$0, %ecx
	JGE L34
	JMP L33
L33:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L34:
	IMULL	$4, %ecx
	ADDQ	%rcx, %rdx
	MOVQ	%r13, %rcx
	MOVL	%edi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L35
	JMP L36
L35:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L36:
	MOVL	-8(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L37
	JMP L38
L37:
	CMPL	$0, %r8d
	JGE L39
	JMP L38
L38:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L39:
	IMULL	$8, %r8d
	ADDQ	%r8, %rcx
	MOVQ	0(%rcx), %rcx
	MOVL	%esi, %r8d
	MOVQ	$0, %r15
	CMPQ	%rcx, %r15
	JE L40
	JMP L41
L40:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L41:
	MOVL	-4(%rcx), %r9d
	CMPL	%r9d, %r8d
	JL L42
	JMP L43
L42:
	CMPL	$0, %r8d
	JGE L44
	JMP L43
L43:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
L44:
	IMULL	$4, %r8d
	ADDQ	%r8, %rcx
	MOVL	0(%rcx), %ecx
	MOVL	%ecx, 0(%rdx)
	INCL	%esi
	MOVL	%esi, %esi
	JMP L24
L23:
	INCL	%edi
	MOVL	%edi, %edi
	JMP L21
L20:
	ADDQ	$24, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L238:
	MOVL	$10, %ebx
	CMPL	$0, %ebx
	JL L303
	JMP L302
L303:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L302:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	ADDQ	$8, %rax
	MOVQ	%rax, %rbx
	MOVL	$0, %ebp
L294:
	CMPL	$10, %ebp
	JL L292
	JMP L293
L292:
	MOVQ	%rbx, %rax
	MOVL	%ebp, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L295
	JMP L296
L295:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L296:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L297
	JMP L298
L297:
	CMPL	$0, %edi
	JGE L299
	JMP L298
L298:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L299:
	IMULL	$8, %edi
	MOVQ	%rax, %r12
	ADDQ	%rdi, %r12
	MOVL	$10, %r13d
	CMPL	$0, %r13d
	JL L301
	JMP L300
L301:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L300:
	MOVL	$1, %eax
	ADDL	%r13d, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%r13d, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, 0(%r12)
	MOVL	%ebp, %eax
	ADDL	$1, %eax
	MOVL	%eax, %ebp
	JMP L294
L293:
	MOVQ	%rbx, %rax
	MOVL	$0, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L282
	JMP L283
L282:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L283:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L284
	JMP L285
L284:
	CMPL	$0, %edi
	JGE L286
	JMP L285
L285:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L286:
	IMULL	$8, %edi
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	$1, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L287
	JMP L288
L287:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L288:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L289
	JMP L290
L289:
	CMPL	$0, %edi
	JGE L291
	JMP L290
L290:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L291:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$1, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L272
	JMP L273
L272:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L273:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L274
	JMP L275
L274:
	CMPL	$0, %edi
	JGE L276
	JMP L275
L275:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L276:
	IMULL	$8, %edi
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	$2, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L277
	JMP L278
L277:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L278:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L279
	JMP L280
L279:
	CMPL	$0, %edi
	JGE L281
	JMP L280
L280:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L281:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$2, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L262
	JMP L263
L262:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L263:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L264
	JMP L265
L264:
	CMPL	$0, %edi
	JGE L266
	JMP L265
L265:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L266:
	IMULL	$8, %edi
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	$0, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L267
	JMP L268
L267:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L268:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L269
	JMP L270
L269:
	CMPL	$0, %edi
	JGE L271
	JMP L270
L270:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L271:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$2, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L252
	JMP L253
L252:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L253:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L254
	JMP L255
L254:
	CMPL	$0, %edi
	JGE L256
	JMP L255
L255:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L256:
	IMULL	$8, %edi
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	$1, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L257
	JMP L258
L257:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L258:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L259
	JMP L260
L259:
	CMPL	$0, %edi
	JGE L261
	JMP L260
L260:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L261:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%rbx, %rax
	MOVL	$2, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L242
	JMP L243
L242:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L243:
	MOVL	-8(%rax), %esi
	CMPL	%esi, %edi
	JL L244
	JMP L245
L244:
	CMPL	$0, %edi
	JGE L246
	JMP L245
L245:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L246:
	IMULL	$8, %edi
	ADDQ	%rdi, %rax
	MOVQ	0(%rax), %rax
	MOVL	$2, %edi
	MOVQ	$0, %r15
	CMPQ	%rax, %r15
	JE L247
	JMP L248
L247:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L248:
	MOVL	-4(%rax), %esi
	CMPL	%esi, %edi
	JL L249
	JMP L250
L249:
	CMPL	$0, %edi
	JGE L251
	JMP L250
L250:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L251:
	IMULL	$4, %edi
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVL	$0, %ebp
L241:
	CMPL	$100, %ebp
	JL L239
	JMP L240
L239:
	MOVQ	%rbx, %rdi
	MOVL	$10, %esi
	MOVL	$10, %edx
	ANDL	$0, %eax
	CALL	_c0_iter
	MOVL	%ebp, %eax
	ADDL	$1, %eax
	MOVL	%eax, %ebp
	JMP L241
L240:
	MOVQ	%rbx, %rdi
	MOVL	$10, %esi
	MOVL	$10, %edx
	ANDL	$0, %eax
	CALL	_c0_sum
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
