	.file	"../tests1/alabama-g-5.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$5, %ebx
	MOVL	$5, %ebp
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	CMPL	$0, %ebp
	JL L12
	JMP L11
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L11:
	MOVL	$1, %eax
	ADDL	%ebp, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebp, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rdi
	MOVL	$0, %eax
	JMP L13
L13:
	MOVQ	%rax, %rsi
L7:
	CMPL	$5, %esi
	JL L5
	JMP L6
L5:
	MOVQ	%rdi, %rdx
	MOVL	%esi, %eax
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %ecx
	CMPL	%ecx, %eax
	JL L8
	JMP L9
L8:
	CMPL	$0, %eax
	JGE L10
	JMP L9
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L10:
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	%ebx, 0(%rax)
	MOVL	%esi, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L14
L14:
	MOVQ	%rax, %rsi
L6:
	MOVQ	%rdi, %rdi
	MOVL	$0, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L2
	JMP L3
L2:
	CMPL	$0, %esi
	JGE L4
	JMP L3
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L4:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
