	.file	"../tests1/alabama-g-7.l4"
	.text
	.global	_c0_foo
_c0_foo:
	SUBQ	$8, %rsp
L1:
	MOVQ	%rdi, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$56, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVL	$1, %edi
	MOVL	$60, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	%rax, %rdi
	ADDQ	$56, %rdi
	MOVL	$0, 0(%rdi)
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_foo
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
