	.file	"../tests0/fibarray.l4"
	.text
	.global	_c0_fib
_c0_fib:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVL	%edi, %ebp
	MOVL	%ebp, %ebx
	ADDL	$1, %ebx
	CMPL	$0, %ebx
	JL L1
	JMP L18
L18:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %r9
	MOVQ	%r9, %rsi
	MOVL	$0, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L16
	JMP L1
L16:
	CMPL	$0, %eax
	JGE L17
	JMP L1
L17:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	$1, 0(%rax)
	MOVQ	%r9, %rdi
	MOVL	$1, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L14
	JMP L1
L14:
	CMPL	$0, %esi
	JGE L15
	JMP L1
L15:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	$1, 0(%rax)
	MOVL	$2, %eax
	JMP L21
L21:
	MOVQ	%rax, %r8
	JMP L7
L7:
	CMPL	%ebp, %r8d
	JLE L5
	JMP L6
L5:
	MOVQ	%r9, %rdi
	MOVL	%r8d, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L8
	JMP L1
L8:
	CMPL	$0, %esi
	JGE L9
	JMP L1
L9:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	MOVQ	%rdi, %rdx
	ADDQ	%rax, %rdx
	MOVQ	%r9, %rdi
	MOVL	%r8d, %eax
	SUBL	$1, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L12
	JMP L1
L12:
	CMPL	$0, %eax
	JGE L13
	JMP L1
L13:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %ecx
	MOVQ	%r9, %rsi
	MOVL	%r8d, %edi
	SUBL	$2, %edi
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %eax
	CMPL	%eax, %edi
	JL L10
	JMP L1
L10:
	CMPL	$0, %edi
	JGE L11
	JMP L1
L11:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	ADDL	%ecx, %eax
	MOVL	%eax, 0(%rdx)
	MOVL	%r8d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L22
L22:
	MOVQ	%rax, %r8
	JMP L7
L6:
	MOVQ	%r9, %rsi
	MOVL	%ebp, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L3
	JMP L1
L3:
	CMPL	$0, %eax
	JGE L4
	JMP L1
L4:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L20:
	MOVL	$10, %edi
	ANDL	$0, %eax
	CALL	_c0_fib
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L19:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
