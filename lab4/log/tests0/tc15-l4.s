	.file	"../tests0/tc15.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVQ	$0, %r15
	CMPQ	$0, %r15
	JE L3
	JMP L4
L3:
	MOVL	$0, %eax
	JMP L6
L6:
	MOVQ	%rax, %rax
	JMP L5
L4:
	MOVL	$1, %eax
	JMP L7
L7:
	MOVQ	%rax, %rax
	JMP L5
L5:
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
