	.file	"../tests0/bool.l4"
	.text
	.global	_c0_xor
_c0_xor:
	SUBQ	$8, %rsp
L2:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	CMPL	$0, %eax
	JNE L10
	JMP L11
L10:
	CMPL	$0, %edi
	JNE L7
	JMP L6
L11:
	JMP L7
L6:
	JMP L3
L7:
	CMPL	$0, %eax
	JNE L9
	JMP L8
L8:
	CMPL	$0, %edi
	JNE L3
	JMP L4
L9:
	JMP L4
L3:
	MOVL	$1, %eax
	JMP L23
L23:
	MOVQ	%rax, %rax
	JMP L5
L4:
	MOVL	$0, %eax
	JMP L24
L24:
	MOVQ	%rax, %rax
	JMP L5
L5:
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L13:
	MOVL	$1, %ebx
	MOVL	$0, %ebp
	MOVL	%ebx, %edi
	MOVL	%ebx, %esi
	ANDL	$0, %eax
	CALL	_c0_xor
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L21
	JMP L20
L20:
	MOVL	%ebx, %edi
	MOVL	%ebp, %esi
	ANDL	$0, %eax
	CALL	_c0_xor
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L18
	JMP L19
L21:
	JMP L19
L18:
	MOVL	%ebp, %edi
	MOVL	%ebx, %esi
	ANDL	$0, %eax
	CALL	_c0_xor
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L16
	JMP L17
L19:
	JMP L17
L16:
	MOVL	%ebp, %edi
	MOVL	%ebp, %esi
	ANDL	$0, %eax
	CALL	_c0_xor
	MOVL	%eax, %eax
	CMPL	$0, %eax
	JNE L15
	JMP L14
L17:
	JMP L15
L14:
	MOVL	$1, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L15:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L22:
L12:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
