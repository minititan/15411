	.file	"../tests0/name-val-alias.l4"
	.text
	.global	_c0_alias
_c0_alias:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdx
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	%rax, %rdi
	ADDQ	$0, %rdi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %esi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	ADDL	%esi, %eax
	MOVL	%eax, 0(%rdi)
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_your
_c0_your:
	SUBQ	$8, %rsp
L4:
	MOVQ	%rdi, %rdx
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	%rax, %rsi
	ADDQ	$0, %rsi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	ADDL	%edi, %eax
	MOVL	%eax, 0(%rsi)
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVQ	%rdx, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_pointer
_c0_pointer:
	SUBQ	$8, %rsp
L6:
	MOVQ	%rdi, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L5:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_lolias
_c0_lolias:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L8:
	MOVQ	%rdi, %rdi
	MOVQ	%rsi, %rbp
	MOVQ	%rdx, %r12
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %esi
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%esi, %ebx
	ADDL	%eax, %ebx
	MOVQ	%rdi, %rdi
	ANDL	$0, %eax
	CALL	_c0_alias
	MOVL	%eax, %eax
	ADDL	%eax, %ebx
	MOVQ	%rbp, %rdi
	ANDL	$0, %eax
	CALL	_c0_your
	MOVL	%eax, %eax
	ADDL	%eax, %ebx
	MOVQ	%r12, %rdi
	ANDL	$0, %eax
	CALL	_c0_pointer
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L7:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L10:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rbx
	ADDQ	$8, %rbx
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	MOVQ	%rax, %rbx
	ADDQ	$8, %rbx
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbx)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVL	$1, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	$10, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	$100, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rsi
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rbp, %rdi
	MOVQ	%rsi, %rsi
	MOVQ	%rax, %rdx
	ANDL	$0, %eax
	CALL	_c0_lolias
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L9:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
