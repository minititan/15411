	.file	"../tests0/mmult.l4"
	.text
	.global	_c0_mmult
_c0_mmult:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVQ	%rdi, %r10
	MOVQ	%rsi, %rbp
	MOVQ	%rdx, %r11
	MOVL	%ecx, %edx
	MOVL	$0, %eax
	JMP L32
L32:
	MOVQ	%rax, %rcx
	JMP L5
L5:
	CMPL	%edx, %ecx
	JL L3
	JMP L4
L3:
	MOVL	$0, %eax
	JMP L33
L33:
	MOVQ	%rax, %r9
	JMP L8
L8:
	CMPL	%edx, %r9d
	JL L6
	JMP L7
L6:
	MOVL	$0, %eax
	MOVL	$0, %edi
	JMP L34
L34:
	MOVQ	%rax, %rbx
	MOVQ	%rdi, %r8
	JMP L13
L13:
	CMPL	%edx, %r8d
	JL L11
	JMP L12
L11:
	MOVQ	%r10, %rsi
	MOVL	%r9d, %eax
	IMULL	%edx, %eax
	ADDL	%r8d, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L16
	JMP L1
L16:
	CMPL	$0, %eax
	JGE L17
	JMP L1
L17:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %esi
	MOVQ	%rbp, %r12
	MOVL	%r8d, %eax
	IMULL	%edx, %eax
	ADDL	%ecx, %eax
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVL	-4(%r12), %edi
	CMPL	%edi, %eax
	JL L14
	JMP L1
L14:
	CMPL	$0, %eax
	JGE L15
	JMP L1
L15:
	IMULL	$4, %eax
	ADDQ	%r12, %rax
	MOVL	0(%rax), %eax
	IMULL	%esi, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	%r8d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L35
L35:
	MOVQ	%rdi, %rbx
	MOVQ	%rax, %r8
	JMP L13
L12:
	MOVQ	%r11, %rdi
	MOVL	%r9d, %eax
	IMULL	%edx, %eax
	ADDL	%ecx, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L9
	JMP L1
L9:
	CMPL	$0, %eax
	JGE L10
	JMP L1
L10:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	%ebx, 0(%rax)
	MOVL	%r9d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L36
L36:
	MOVQ	%rax, %r9
	JMP L8
L7:
	MOVL	%ecx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L37
L37:
	MOVQ	%rax, %rcx
	JMP L5
L4:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_init
_c0_init:
	SUBQ	$8, %rsp
L19:
	MOVQ	%rdi, %r9
	MOVL	%esi, %edi
	MOVL	%edx, %esi
	MOVL	$0, %eax
	JMP L38
L38:
	MOVQ	%rax, %rdx
	MOVQ	%rdi, %r8
	JMP L22
L22:
	MOVL	%esi, %eax
	IMULL	%esi, %eax
	CMPL	%eax, %edx
	JL L20
	JMP L21
L20:
	MOVQ	%r9, %rdi
	MOVL	%edx, %ecx
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %ecx
	JL L23
	JMP L18
L23:
	CMPL	$0, %ecx
	JGE L24
	JMP L18
L24:
	MOVL	%ecx, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	%r8d, 0(%rax)
	MOVL	%r8d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %edi
	MOVL	%edx, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L39
L39:
	MOVQ	%rax, %rdx
	MOVQ	%rdi, %r8
	JMP L22
L21:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L18:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L26:
	MOVL	$3, %r13d
	MOVL	%r13d, %ebx
	IMULL	%r13d, %ebx
	CMPL	$0, %ebx
	JL L25
	JMP L31
L31:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %r12
	MOVQ	%r12, %rdi
	MOVL	$1, %esi
	MOVL	%r13d, %edx
	ANDL	$0, %eax
	CALL	_c0_init
	MOVL	%r13d, %ebx
	IMULL	%r13d, %ebx
	CMPL	$0, %ebx
	JL L25
	JMP L30
L30:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rbp
	MOVL	%r13d, %eax
	IMULL	%r13d, %eax
	INCL	%eax
	MOVQ	%rbp, %rdi
	MOVL	%eax, %esi
	MOVL	%r13d, %edx
	ANDL	$0, %eax
	CALL	_c0_init
	MOVL	%r13d, %ebx
	IMULL	%r13d, %ebx
	CMPL	$0, %ebx
	JL L25
	JMP L29
L29:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rbx
	MOVQ	%r12, %rdi
	MOVQ	%rbp, %rsi
	MOVQ	%rbx, %rdx
	MOVL	%r13d, %ecx
	ANDL	$0, %eax
	CALL	_c0_mmult
	MOVQ	%rbx, %rsi
	MOVL	%r13d, %eax
	IMULL	%r13d, %eax
	DECL	%eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L27
	JMP L25
L27:
	CMPL	$0, %eax
	JGE L28
	JMP L25
L28:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
L25:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
