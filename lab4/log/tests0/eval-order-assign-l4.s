	.file	"../tests0/eval-order-assign.l4"
	.text
	.global	_c0_foo
_c0_foo:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdi
	MOVL	0(%rdi), %eax
	INCL	%eax
	MOVL	%eax, 0(%rdi)
	MOVQ	%rdi, %rax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L4:
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	MOVL	0(%rbx), %ebp
	MOVQ	%rbx, %rdi
	ANDL	$0, %eax
	CALL	_c0_foo
	MOVQ	%rax, %rax
	MOVL	0(%rax), %eax
	ADDL	%ebp, %eax
	MOVL	%eax, 0(%rbx)
	MOVL	0(%rbx), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
