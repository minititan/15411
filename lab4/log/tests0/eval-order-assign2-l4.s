	.file	"../tests0/eval-order-assign2.l4"
	.text
	.global	_c0_test
_c0_test:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdi
	MOVQ	0(%rdi), %rax
	MOVQ	%rax, %rax
	MOVQ	$0, 0(%rdi)
	MOVQ	%rax, %rax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L4:
	MOVL	$1, %ebx
	CMPL	$0, %ebx
	JL L3
	JMP L7
L7:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rbx
	MOVL	$1, %edi
	MOVL	$8, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, 0(%rbp)
	MOVQ	%rbx, %rbx
	MOVQ	%rbp, %rdi
	ANDL	$0, %eax
	CALL	_c0_test
	MOVQ	%rax, %rax
	MOVL	0(%rax), %edi
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVL	-4(%rbx), %eax
	CMPL	%eax, %edi
	JL L5
	JMP L3
L5:
	CMPL	$0, %edi
	JGE L6
	JMP L3
L6:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	MOVQ	%rbx, %rdi
	ADDQ	%rax, %rdi
	MOVQ	0(%rbp), %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdi)
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
