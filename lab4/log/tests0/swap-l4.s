	.file	"../tests0/swap.l4"
	.text
	.global	_c0_swap
_c0_swap:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rdx
	MOVQ	%rsi, %rsi
	MOVL	0(%rdx), %eax
	MOVL	%eax, %eax
	MOVL	0(%rsi), %edi
	MOVL	%edi, 0(%rdx)
	MOVL	%eax, 0(%rsi)
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L4:
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	MOVL	$3, 0(%rbp)
	MOVL	$7, 0(%rbx)
	MOVQ	%rbp, %rdi
	MOVQ	%rbx, %rsi
	ANDL	$0, %eax
	CALL	_c0_swap
	MOVL	0(%rbp), %eax
	MOVL	%eax, %edi
	IMULL	$10, %edi
	MOVL	0(%rbx), %eax
	ADDL	%edi, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L3:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
