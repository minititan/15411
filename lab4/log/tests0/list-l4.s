	.file	"../tests0/list.l4"
	.text
	.global	_c0_upto
_c0_upto:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVL	%edi, %eax
	MOVQ	$0, %rdi
	JMP L13
L13:
	MOVQ	%rdi, %rbp
	MOVQ	%rax, %rbx
	JMP L5
L5:
	CMPL	$0, %ebx
	JG L3
	JMP L4
L3:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	%ebx, 0(%rax)
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	%rbp, 0(%rax)
	MOVQ	%rdi, %rdi
	MOVL	%ebx, %eax
	SUBL	$1, %eax
	MOVL	%eax, %eax
	JMP L14
L14:
	MOVQ	%rdi, %rbp
	MOVQ	%rax, %rbx
	JMP L5
L4:
	MOVQ	%rbp, %rax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.global	_c0_sum
_c0_sum:
	SUBQ	$8, %rsp
L7:
	MOVQ	%rdi, %rdi
	MOVL	$0, %eax
	JMP L15
L15:
	MOVQ	%rax, %rsi
	MOVQ	%rdi, %rdi
	JMP L10
L10:
	CMPQ	$0, %rdi
	JNE L8
	JMP L9
L8:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	ADDL	%esi, %eax
	MOVL	%eax, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rax
	JMP L16
L16:
	MOVQ	%rsi, %rsi
	MOVQ	%rax, %rdi
	JMP L10
L9:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
L6:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L12:
	MOVL	$100, %edi
	ANDL	$0, %eax
	CALL	_c0_upto
	MOVQ	%rax, %rax
	MOVQ	%rax, %rax
	MOVQ	%rax, %rdi
	ANDL	$0, %eax
	CALL	_c0_sum
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L11:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
