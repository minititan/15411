	.file	"../tests0/intmap.l4"
	.text
	.global	_c0_lookup
_c0_lookup:
	SUBQ	$8, %rsp
L2:
	MOVQ	%rdi, %rax
	MOVL	%esi, %ecx
	MOVQ	%rdx, %rsi
	JMP L23
L23:
	MOVQ	%rax, %rdi
	JMP L5
L5:
	CMPQ	$0, %rdi
	JNE L6
	JMP L7
L6:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	%ecx, %eax
	JLE L3
	JMP L4
L7:
	JMP L4
L3:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	%ecx, %eax
	JE L8
	JMP L9
L8:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rsi)
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L9:
	JMP L10
L10:
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rax
	JMP L24
L24:
	MOVQ	%rax, %rdi
	JMP L5
L4:
	MOVL	$1, %eax
	NEGL	%eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_insert
_c0_insert:
	SUBQ	$8, %rsp
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$8, %rsp
L12:
	MOVQ	%rdi, %rdi
	MOVQ	%rsi, %rax
	MOVL	%edx, %r13d
	MOVL	%ecx, 0(%rsp)
	JMP L25
L25:
	MOVQ	%rax, %r12
	MOVQ	%rdi, %rbx
	JMP L15
L15:
	CMPQ	$0, %r12
	JNE L16
	JMP L17
L16:
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	%r13d, %eax
	JLE L13
	JMP L14
L17:
	JMP L14
L13:
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	CMPL	%r13d, %eax
	JE L18
	JMP L19
L18:
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rsp), %r15d
	MOVL	%r15d, 0(%rax)
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
L19:
	JMP L20
L20:
	TESTQ	%r12, %r12
	jz	_c0_call_raise
	MOVQ	%r12, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rax
	JMP L26
L26:
	MOVQ	%rax, %r12
	MOVQ	%rdi, %rbx
	JMP L15
L14:
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$0, %rax
	MOVL	0(%rsp), %r15d
	MOVL	%r15d, 0(%rax)
	TESTQ	%rbp, %rbp
	jz	_c0_call_raise
	MOVQ	%rbp, %rax
	ADDQ	$8, %rax
	MOVQ	%r12, 0(%rax)
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rdi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$0, %rax
	MOVL	%r13d, 0(%rax)
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVQ	%rdi, %rax
	ADDQ	$8, %rax
	MOVQ	%rbp, 0(%rax)
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	%rdi, 0(%rax)
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
L11:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L22:
	MOVL	$1, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbp
	MOVL	$1, %edi
	MOVL	$16, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rax, %rbx
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	$0, 0(%rax)
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rbx, %rdi
	MOVQ	%rax, %rsi
	MOVL	$2, %edx
	MOVL	$3, %ecx
	ANDL	$0, %eax
	CALL	_c0_insert
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rbx, %rdi
	MOVQ	%rax, %rsi
	MOVL	$6, %edx
	MOVL	$7, %ecx
	ANDL	$0, %eax
	CALL	_c0_insert
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rbx, %rdi
	MOVQ	%rax, %rsi
	MOVL	$4, %edx
	MOVL	$5, %ecx
	ANDL	$0, %eax
	CALL	_c0_insert
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rbx, %rdi
	MOVQ	%rax, %rsi
	MOVL	$8, %edx
	MOVL	$9, %ecx
	ANDL	$0, %eax
	CALL	_c0_insert
	TESTQ	%rbx, %rbx
	jz	_c0_call_raise
	MOVQ	%rbx, %rax
	ADDQ	$8, %rax
	MOVQ	0(%rax), %rax
	MOVQ	%rax, %rdi
	MOVL	$6, %esi
	MOVQ	%rbp, %rdx
	ANDL	$0, %eax
	CALL	_c0_lookup
	MOVL	0(%rbp), %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L21:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
