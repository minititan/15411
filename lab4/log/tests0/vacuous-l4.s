	.file	"../tests0/vacuous.l4"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L1:
	MOVQ	$0, %rax
	TESTQ	%rax, %rax
	jz	_c0_call_raise
	ADDQ	$0, %rax
	MOVL	0(%rax), %eax
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
