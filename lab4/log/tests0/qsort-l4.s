	.file	"../tests0/qsort.l4"
	.text
	.global	_c0_qsort
_c0_qsort:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L2:
	MOVQ	%rdi, %rbx
	MOVL	%esi, %r10d
	MOVL	%edx, %ebp
	CMPL	%ebp, %r10d
	JGE L23
	JMP L24
L23:
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L24:
	JMP L25
L25:
	MOVL	%r10d, %ecx
	MOVL	%ebp, %edx
	MOVQ	%rbx, %rdi
	MOVL	%ebp, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L21
	JMP L1
L21:
	CMPL	$0, %esi
	JGE L22
	JMP L1
L22:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, %r9d
	JMP L46
L46:
	MOVQ	%rdx, %r12
	MOVQ	%rcx, %r8
	JMP L7
L7:
	CMPL	%r12d, %r8d
	JL L5
	JMP L6
L5:
	MOVQ	%rbx, %rdi
	MOVL	%r8d, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L10
	JMP L1
L10:
	CMPL	$0, %eax
	JGE L11
	JMP L1
L11:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	0(%rax), %eax
	CMPL	%r9d, %eax
	JLE L8
	JMP L9
L8:
	MOVL	%r8d, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L47
L47:
	MOVQ	%r12, %rdi
	MOVQ	%rax, %rax
	JMP L20
L9:
	MOVQ	%rbx, %rsi
	MOVL	%r12d, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L16
	JMP L1
L16:
	CMPL	$0, %eax
	JGE L17
	JMP L1
L17:
	IMULL	$4, %eax
	MOVQ	%rsi, %rdx
	ADDQ	%rax, %rdx
	MOVQ	%rbx, %rsi
	MOVL	%r8d, %eax
	TESTQ	%rsi, %rsi
	jz	_c0_call_raise
	MOVL	-4(%rsi), %edi
	CMPL	%edi, %eax
	JL L18
	JMP L1
L18:
	CMPL	$0, %eax
	JGE L19
	JMP L1
L19:
	IMULL	$4, %eax
	ADDQ	%rsi, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rdx)
	MOVL	%r12d, %eax
	SUBL	$1, %eax
	MOVL	%eax, %edx
	MOVQ	%rbx, %rdi
	MOVL	%r8d, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L12
	JMP L1
L12:
	CMPL	$0, %esi
	JGE L13
	JMP L1
L13:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	MOVQ	%rdi, %rsi
	ADDQ	%rax, %rsi
	MOVQ	%rbx, %rcx
	MOVL	%edx, %edi
	TESTQ	%rcx, %rcx
	jz	_c0_call_raise
	MOVL	-4(%rcx), %eax
	CMPL	%eax, %edi
	JL L14
	JMP L1
L14:
	CMPL	$0, %edi
	JGE L15
	JMP L1
L15:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rcx, %rax
	MOVL	0(%rax), %eax
	MOVL	%eax, 0(%rsi)
	JMP L48
L48:
	MOVQ	%rdx, %rdi
	MOVQ	%r8, %rax
	JMP L20
L20:
	JMP L49
L49:
	MOVQ	%rdi, %r12
	MOVQ	%rax, %r8
	JMP L7
L6:
	MOVQ	%rbx, %rdi
	MOVL	%r12d, %eax
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %esi
	CMPL	%esi, %eax
	JL L3
	JMP L1
L3:
	CMPL	$0, %eax
	JGE L4
	JMP L1
L4:
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	%r9d, 0(%rax)
	MOVL	%r8d, %eax
	SUBL	$1, %eax
	MOVQ	%rbx, %rdi
	MOVL	%r10d, %esi
	MOVL	%eax, %edx
	ANDL	$0, %eax
	CALL	_c0_qsort
	MOVL	%r12d, %eax
	ADDL	$1, %eax
	MOVQ	%rbx, %rdi
	MOVL	%eax, %esi
	MOVL	%ebp, %edx
	ANDL	$0, %eax
	CALL	_c0_qsort
	MOVL	$0, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.global	_c0_is_sorted
_c0_is_sorted:
	SUBQ	$8, %rsp
L27:
	MOVQ	%rdi, %r8
	MOVL	%esi, %ecx
	MOVL	$0, %eax
	JMP L50
L50:
	MOVQ	%rax, %rsi
	JMP L30
L30:
	CMPL	%ecx, %esi
	JL L28
	JMP L29
L28:
	MOVQ	%r8, %rdx
	MOVL	%esi, %edi
	TESTQ	%rdx, %rdx
	jz	_c0_call_raise
	MOVL	-4(%rdx), %eax
	CMPL	%eax, %edi
	JL L33
	JMP L26
L33:
	CMPL	$0, %edi
	JGE L34
	JMP L26
L34:
	MOVL	%edi, %eax
	IMULL	$4, %eax
	ADDQ	%rdx, %rax
	MOVL	0(%rax), %eax
	CMPL	%esi, %eax
	JNE L31
	JMP L32
L31:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L32:
	JMP L35
L35:
	MOVL	%esi, %eax
	ADDL	$1, %eax
	MOVL	%eax, %eax
	JMP L51
L51:
	MOVQ	%rax, %rsi
	JMP L30
L29:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
L26:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L37:
	MOVL	$938, %ebp
	MOVL	$2, %ebx
	IMULL	%ebp, %ebx
	CMPL	$0, %ebx
	JL L36
	JMP L45
L45:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$4, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVL	%ebx, 0(%rax)
	ADDQ	$4, %rax
	MOVQ	%rax, %rbx
	MOVL	$0, %eax
	JMP L52
L52:
	MOVQ	%rax, %rdx
	JMP L40
L40:
	MOVL	$2, %eax
	IMULL	%ebp, %eax
	DECL	%eax
	CMPL	%eax, %edx
	JL L38
	JMP L39
L38:
	MOVQ	%rbx, %rdi
	MOVL	%edx, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L43
	JMP L36
L43:
	CMPL	$0, %esi
	JGE L44
	JMP L36
L44:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rdi, %rax
	MOVL	%edx, 0(%rax)
	MOVQ	%rbx, %rdi
	MOVL	%edx, %esi
	ADDL	$1, %esi
	TESTQ	%rdi, %rdi
	jz	_c0_call_raise
	MOVL	-4(%rdi), %eax
	CMPL	%eax, %esi
	JL L41
	JMP L36
L41:
	CMPL	$0, %esi
	JGE L42
	JMP L36
L42:
	MOVL	%esi, %eax
	IMULL	$4, %eax
	ADDQ	%rax, %rdi
	MOVL	$2, %esi
	IMULL	%ebp, %esi
	MOVL	%edx, %eax
	ADDL	$1, %eax
	SUBL	%esi, %eax
	NEGL	%eax
	MOVL	%eax, 0(%rdi)
	MOVL	%edx, %eax
	ADDL	$2, %eax
	MOVL	%eax, %eax
	JMP L53
L53:
	MOVQ	%rax, %rdx
	JMP L40
L39:
	MOVL	$2, %eax
	IMULL	%ebp, %eax
	DECL	%eax
	MOVQ	%rbx, %rdi
	MOVL	$0, %esi
	MOVL	%eax, %edx
	ANDL	$0, %eax
	CALL	_c0_qsort
	MOVL	$2, %eax
	IMULL	%ebp, %eax
	MOVQ	%rbx, %rdi
	MOVL	%eax, %esi
	ANDL	$0, %eax
	CALL	_c0_is_sorted
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
L36:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
