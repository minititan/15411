	.file	"../tests0/empty.l4"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L2:
	MOVL	$2000, %ebx
	CMPL	$0, %ebx
	JL L1
	JMP L3
L3:
	MOVL	$1, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %edi
	MOVL	$0, %esi
	ANDL	$0, %eax
	CALL	calloc
	MOVQ	%rax, %rax
	MOVQ	%rbx, 0(%rax)
	MOVL	$0, %eax
	POPQ	%rbx
	RET
L1:
	MOVL	$11, %eax
	MOVL	%eax, %edi
	ANDL	$0, %eax
	CALL	raise
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
