	.file	"../tests2/iron-test13.l2"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L3:
	MOVL	$0, %eax
	MOVL	$1, %eax
	MOVL	$0, %eax
	JMP L7
L7:
	MOVQ	$0, %rdi
	MOVQ	$1, %rsi
	MOVQ	$0, %rax
	JMP L6
L6:
	CMPL	$10, %edi
	JL L4
	JMP L5
L4:
	MOVL	%esi, %edx
	ADDL	%eax, %edx
	MOVL	%edx, %eax
	MOVL	%esi, %eax
	MOVL	%edx, %eax
	MOVL	%edi, %eax
	ADDL	$1, %eax
	MOVL	%eax, %edi
	JMP L8
L8:
	MOVQ	%rax, %rdi
	MOVQ	%rdx, %rsi
	MOVQ	%rsi, %rax
	JMP L6
L5:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
L1:
	MOVL	$11, %eax
	MOVL	$11, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
L2:
	MOVL	$8, %eax
	MOVL	$8, %edi
	ANDL	$0, %eax
	CALL	raise
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11,%rdi
	call raise
