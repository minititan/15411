	.file	"../tests2/tennessee-return05.l2"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L1:
	MOVL	$1, %r15d
	CMPL	$0, %r15d
	JGE L2
	JMP L3
L2:
	MOVL	$1, %r15d
	CMPL	$32, %r15d
	JL L4
	JMP L3
L3:
	MOVL	$0, %edi
	MOVL	$42, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	ADDQ	$8, %rsp
	RET
L4:
	MOVL	$2, %eax
	MOVL	$1, %ecx
	SHLL	%cl, %eax
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
