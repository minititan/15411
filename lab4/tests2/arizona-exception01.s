	.file	"../tests2/arizona-exception01.l2"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L1:
	MOVL	$3, %edi
	JMP L8
L8:
	MOVQ	%rdi, %rsi
L4:
	CMPL	$0, %esi
	JNE L2
	JMP L3
L2:
	MOVL	$0, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	CMPL	$3, %edi
	JE L5
	JMP L6
L5:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
L6:
	JMP L7
L7:
	MOVL	%esi, %eax
	SUBL	$1, %eax
	MOVL	%eax, %eax
	JMP L9
L9:
	MOVQ	%rax, %rsi
L3:
	MOVL	$3, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
	.global	_c0_call_raise
_c0_call_raise:
	mov $11, %rdi
	call raise
