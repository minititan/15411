type multemp = int * int
type t = multemp

let raise_error s = 
	ErrorMsg.error None s; raise ErrorMsg.Error

let sub_counter = Hashtbl.create 128

(* hashtable to hold sizes of mutlemps *)
let size_holder = Hashtbl.create 128

(* adds a mapping from temp to its size *)
let add_size (t : Temp.temp) (s : Ir.size) =
	(* being pedantic here but want to check *)
	let old_size = try (Some (Hashtbl.find size_holder t)) with Not_found -> None in
	match old_size with
	| None -> Hashtbl.add size_holder t s
	| Some(os) -> if (os <> s) then raise_error ("Sizes of same temp do not match: " ^ Temp.name t)
					else ()

(* retrieve size of a temp *)
let get_size (t : Temp.temp) = 
	try (Some (Hashtbl.find size_holder t)) with Not_found ->  None 

(* get the subtemp from the temp *)
let get_subtemp t = 
	try (Some (Hashtbl.find sub_counter (Temp.get_int t))) with Not_found -> None

(* converts a temp to a subtemp (assumes initial conversion) *)
let convert_temp t =
	let int_temp = Temp.get_int t in

	match get_subtemp t with
	| Some((t,s)) -> let () = Hashtbl.add sub_counter t (t, s+1) in (t, s+1)
	| None -> let () = Hashtbl.add sub_counter int_temp (int_temp, 0) in (int_temp, 0)

let create () =
    let temp = Temp.create () in
    convert_temp temp

let sub_compare (a, b) (a', b') = 
	if (a = a' && b = b') then 0
	else if (a != a') then compare a a' 
	else compare b b'

let name (t, t') = "t" ^ string_of_int t ^ "_" ^ string_of_int t'

let format ff t = Format.fprintf ff "%s" (name t)

module S =
  struct
    type t = multemp
    let compare = sub_compare
    let format = format
  end

module type SET = Printable.SET with type elt = t
module Set = Printable.MakeSet(S)

module type MAP = Printable.MAP with type key = t
module Map = Printable.MakeMap(S)
		
