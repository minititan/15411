
(* label set gives the jumps from the block. Can be empty *)
type block = 
  Label.label * Label.Set.t * Ir.instr list

type blockfunc = Ir.size option * Symbol.symbol * block list * (Label.Set.t * Label.Set.t) Label.Map.t
type blockprogram = blockfunc list

module type PRINT =
  sig
    val pp_blocks : block list -> string
    val pp_blockprogram : blockprogram -> string
  end

module Print : PRINT =
  struct

    let pp_block (l, lset, clist) = 
      let bstr = List.map Ir.Print.pp_instr clist |> (String.concat "") in
      let lbl_list = Label.Set.elements lset in
      let jmpstr = "=====\nJumpsTo(" ^
        (List.map Label.name lbl_list |> (String.concat ","))
        ^ ")\n"
      in
      "BLOCK " ^ Label.name l ^ "\n=====\n" ^ bstr ^ jmpstr

    let pp_blocks bs = 
      List.fold_left (fun s b -> s ^ pp_block b ^ "\n") "" bs

    let pp_blockprogram p =
      List.map (fun (sz, id, b, _) -> Symbol.name id ^ " {\n" ^ pp_blocks b ^ "}\n\n") p
      |> (String.concat "")
  end