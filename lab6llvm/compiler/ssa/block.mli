
(* label set gives the jumps from the block. Can be empty *)
type block = 
	Label.label * Label.Set.t * Ir.instr list

(* label map goes from label to label set where label set indicates blocks it jumps to *)
type blockfunc = Ir.size option * Symbol.symbol * block list * (Label.Set.t * Label.Set.t) Label.Map.t

type blockprogram = blockfunc list

module type PRINT =
  sig
  	val pp_blocks : block list -> string
    val pp_blockprogram : blockprogram -> string
  end

module Print : PRINT

