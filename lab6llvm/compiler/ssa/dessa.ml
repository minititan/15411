(* Converts multemps to temps *)
module MTM = Multemp.Map
module MTS = Multemp.Set
module TS = Temp.Set
module LM = Label.Map
module SIR = Ssair
module IR = Ir

let raise_error s = ErrorMsg.error None s; raise ErrorMsg.Error

let convert_op (s,op) mmap = 
  match op with
  | SIR.IMM i -> ((s, IR.IMM(i)), mmap)
  | SIR.REG r -> ((s, IR.REG(r)), mmap)
  | SIR.MTEMP t ->
    begin match MTM.find' t mmap with
      | Some(tmp) -> ((s, IR.TEMP(tmp)), mmap)
      | None -> let tmp = Temp.create() in
        ((s, IR.TEMP(tmp)), MTM.add t tmp mmap)
    end
  | SIR.DEREF(i, (s', t)) ->
    begin match t with
      | SIR.MTEMP(t') -> 
        begin match MTM.find' t' mmap with
          | Some(tmp) -> ((s, IR.DEREF(i, (s', IR.TEMP(tmp)))), mmap)
          | None -> let tmp = Temp.create() in
            ((s, IR.DEREF(i, (s', IR.TEMP(tmp)))), MTM.add t' tmp mmap)
        end
      | SIR.IMM(i') -> ((s, IR.DEREF(i, (s', IR.IMM(i')))), mmap)
      | SIR.REG(r) -> ((s, IR.DEREF(i, (s', IR.REG(r)))), mmap)
      | SIR.ARGTOP(i') -> ((s, IR.DEREF(i, (s', IR.ARGTOP(i')))), mmap)
      | SIR.ARGBOTTOM(i') -> ((s, IR.DEREF(i, (s', IR.ARGBOTTOM(i')))), mmap)
      | SIR.DEREF(_) -> raise_error "Cannot have deref of deref at this stage"
    end
  | SIR.ARGTOP i -> ((s, IR.ARGTOP(i)), mmap)
  | SIR.ARGBOTTOM i -> ((s, IR.ARGBOTTOM(i)), mmap)

exception Unexpected

let rec convert_ssair block mmap : Ir.instr list * Temp.t Multemp.Map.t = 
  match block with
  | [] -> ([], mmap)
  | ir::rest -> 
    begin match ir with
      | SIR.BINOP (op, op1, op2, op3) -> 
        let (nop1, m1) = convert_op op1 mmap in
        let (nop2, m2) = convert_op op2 m1 in
        let (nop3, m3) = convert_op op3 m2 in

        let (rest_con, fmap) = convert_ssair rest m3 in
        (IR.BINOP(op, nop1, nop2, nop3) :: rest_con, fmap)

      | SIR.CHECKDIV (op1, op2) ->
        let (nop1, m1) = convert_op op1 mmap in
        let (nop2, m2) = convert_op op2 m1 in
        let (rest_con, fmap) = convert_ssair rest m2 in
        (IR.CHECKDIV (nop1, nop2)::rest_con, fmap)

      | SIR.UNOP (op, d, op1) -> 
        let (nd, m1) = convert_op d mmap in
        let (nop, m2) = convert_op op1 m1 in
        let (rest_con, fmap) = convert_ssair rest m2 in

        (IR.UNOP(op, nd, nop) :: rest_con, fmap)

      | SIR.IF (op, op1, op2, l1, l2) -> 
        let (nop1, m1) = convert_op op1 mmap in
        let (nop2, m2) = convert_op op2 m1 in
        let (rest_con, fmap) = convert_ssair rest m2 in
        (IR.IF(op, nop1, nop2, l1, l2) :: rest_con, fmap)

      (* a cast becomes a move at this point because we have no need for it anymore *)
      | SIR.MOV (d, s) | SIR.CAST(d, s) -> 
        let (nd, m1) = convert_op d mmap in
        let (ns, m2) = convert_op s m1 in
        let (rest_con, fmap) = convert_ssair rest m2 in
        (IR.MOV(nd, ns) :: rest_con, fmap)

      | SIR.RET -> let (rest_con, fmap) = convert_ssair rest mmap in
            (IR.RET :: rest_con, fmap)

      | SIR.GOTO l -> 
          let (rest_con, fmap) = convert_ssair rest mmap in
            (IR.GOTO(l):: rest_con, fmap)
      | SIR.LABEL l -> 
            let (rest_con, fmap) = convert_ssair rest mmap in
            (IR.LABEL(l) :: rest_con, fmap)
      | SIR.CALL(id) -> 
            let (rest_con, fmap) = convert_ssair rest mmap in
            (IR.CALL(id) :: rest_con, fmap)

      | SIR.TAILCALL(id) ->
            let (rest_con, fmap) = convert_ssair rest mmap in
            (IR.TAILCALL(id) :: rest_con, fmap)

      | SIR.DIRECTIVE _ | SIR.COMMENT _ -> raise Unexpected

        | SIR.NOP -> let (rest_con, fmap) = convert_ssair rest mmap in
        (IR.NOP :: rest_con, fmap)
    end

let rec convert_blocks bl mtemp_map = 
  match bl with
    | [] -> []
    | (l, jumpset, ssa_irlist, _, _)::rest -> 
      let (new_irlist, new_map) = convert_ssair ssa_irlist mtemp_map in
      (l, jumpset, new_irlist) :: (convert_blocks rest new_map)


let convert bf = 
  let (sz, fname, blocks, jumpmap, in_params, out_params) = bf in
  let converted_blocks = convert_blocks blocks MTM.empty in
  (sz, fname, converted_blocks, jumpmap)

let dessafier bp = 
  let () = Temp.reset () in
  List.map convert bp

