(* Module to do copy and constant propagation on the multemp type.
  Also does constant folding and conditional jump elimination. *)

open Ssair
module MTM = Multemp.Map
module T = Tree
module MTS = Multemp.Set
module LM = Label.Map
module LS = Label.Set

let issafe = ref true
let jump_changed = ref false

let raise_error s = 
  ErrorMsg.error None s; raise ErrorMsg.Error


type inttemp = 
  | CONST of Int32.t
  | TMP of Multemp.t


class contextHolder = 
object
  val ref_map = MTM.empty
  val copy_map = MTM.empty

  (* adds a reference of t2 to t1, so in future cases t1 can be replaced with t2.
    If existing references exist for t1 then t2 points to those. Preference is given
    to have a reference for a constant *)
  method add_ref (s1, t1 : Ssair.operand) (s2, t2 : Ssair.operand) = 
    match t1, t2 with
    | MTEMP(t), IMM(i) -> {< ref_map = MTM.add t (CONST i) ref_map >}
    | MTEMP(t), MTEMP(t') ->
      let new_copy_map = 
        (match MTM.find' t' copy_map with
        | None -> MTM.add t t' copy_map
        | Some v -> MTM.add t v copy_map
        ) 

      in
      
      (match MTM.find' t' ref_map with
      | None -> {< ref_map = MTM.add t (TMP t') ref_map; copy_map = new_copy_map >}
      | Some(v) -> {< ref_map = MTM.add t v ref_map; copy_map = new_copy_map >}
      )

    | MTEMP(t), _ -> {<>}
    | REG(r), _ -> {<>}
    | ARGBOTTOM(i), _ -> {<>}
    | DEREF(_), _ -> {<>}
    | _ -> raise_error ("Cannot have this kind of reference: " ^ 
              Ssair.Print.pp_operand (s1, t1) ^ " <- " ^ Ssair.Print.pp_operand (s2, t2))

  method get_copy_ref t = 
    MTM.find' t copy_map

  (* prefers to retuns the constant binding if possible *)
  method get_value t : (inttemp option) = 
    MTM.find' t ref_map

end;;


(* converts the operand if a temp is present *)
let convert_op (s, op) env = 
  match op with
    | IMM(_) | REG(_) | ARGTOP(_) | ARGBOTTOM(_) -> (false, (s, op))
    
    | MTEMP(t) -> 
        (match env#get_value t with
        | None -> (false, (s, op))
        | Some(x) ->
          (match x with
          | CONST c -> (true, (s, (IMM(c))))
          | TMP tmp -> (true, (s, MTEMP(tmp)))
          )
        )

    (* for derefs we do not do constant propagation, only copy *)
    | DEREF(i, (s', t)) -> 
        (match t with
        | MTEMP(mt) -> 
          (match env#get_copy_ref mt with
          | None -> (false, (s, op))
          | Some(tmp) -> (true, (s, DEREF(i, (s', MTEMP(tmp)))))
          )
        | _ -> (false, (s, op))
        )

(* applies the operation to the constants if its not effectful *)
let apply_binop op c1 c2 = 
  match op with
  | T.ADD -> Some(Int32.add c1 c2)
    | T.SUB -> Some(Int32.sub c1 c2)
    | T.MUL -> Some(Int32.mul c1 c2)

    | T.DIV ->
      if (Int32.zero = c2 || (c1 = Int32.min_int && c2 = Int32.minus_one)) then
        None
      else
        Some (Int32.div c1 c2)

    | T.MOD ->
      if (Int32.zero = c2 || (c1 = Int32.min_int && c2 = Int32.minus_one)) then
        None
      else
        Some (Int32.rem c1 c2)

    | T.BAND -> Some(Int32.logand c1 c2)
    | T.BOR -> Some(Int32.logor c1 c2)
    | T.SAR ->
      if (!issafe || (Int32.compare c2 Int32.zero < 0) ||
               (Int32.compare c2 (Int32.of_int 31) > 0)) then 
        None
      else
        Some (Int32.shift_right c1 (Int32.to_int c2))

    | T.SAL ->
      if (!issafe || (Int32.compare c2 Int32.zero < 0) ||
               (Int32.compare c2 (Int32.of_int 31) > 0)) then 
        None
      else
        Some (Int32.shift_left c1 (Int32.to_int c2))

    | T.XOR -> Some(Int32.logxor c1 c2)


(* if non-effect binop between 2 constants then fold and transform to a move instruction *)
let reduce_binop ir = 
  match ir with
  | BINOP(op, d, op1, op2) ->
    
    (match (d, op1, op2) with

    (* can only reduce if both the operands are integers *)
    | (sd, dest), (s1, IMM(i1)), (s2, IMM(i2)) -> 
      let res = apply_binop op i1 i2 in
      
      (match res with
      | None -> None
      | Some(res_i) -> Some (MOV(d, (sd, IMM(res_i))))
      )

    | ((sd, dest), (s1, IMM(i1)), _) when (i1 = Int32.zero) ->
      (match op with
      | T.ADD -> Some (MOV(d, op2))
      | T.SUB -> Some (UNOP(T.NEG, d, op2))
      | T.MUL -> Some (MOV(d, (sd, IMM(Int32.zero))))
      | _ -> None
      )

    | ((sd, dest), _ , (s2, IMM(i2))) when (i2 = Int32.zero) ->
      (match op with
      | T.ADD -> Some (MOV(d, op1))
      | T.SUB -> Some (MOV(d, op1))
      | T.MUL -> Some (MOV(d, (sd, IMM(Int32.zero))))
      | _ -> None
      )

    | _ -> None
    )

  | _ -> raise_error "Not a binop to reduce"

let apply_unop op c =
  match op with
  | T.NEG -> Int32.neg c 
  | T.BNOT -> Int32.lognot c

let reduce_unop ir = 
  match ir with
  | UNOP(op, d, s) -> 
    (match d, s with
    | (sd, dest), (ss, IMM(si)) ->
      let res = apply_unop op si in
      Some (MOV(d, (sd, IMM(res))))

    | _ -> None 

    )

  | _ -> raise_error "Not a unop to reduce"


let remove_jump l1 l2 jmap = 
  let (in1, out1) = LM.find l1 jmap in
  let (in2, out2) = LM.find l2 jmap in
  LM.add l2 (LS.remove l1 in2, out2) (LM.add l1 (in1, LS.remove l2 out1) jmap)

let apply_cmpop op i1 i2 = 
  match op with
  | T.EQ -> i1 = i2
  | T.NOTEQ -> i1 <> i2
  | T.GREATER -> i1 > i2
  | T.GREATEREQ -> i1 >= i2
  | T.LESS -> i1 < i2
  | T.LESSEQ -> i1 <= i2

let reduce_cmp = function
  | IF (op, op1, op2, l1, l2) ->
    begin match op1, op2 with
      | (_, IMM(i1)), (_, IMM(i2)) ->
        let bool_result = apply_cmpop op i1 i2 in
        if bool_result then
          Some (GOTO(l1))
        else
          Some (GOTO(l2))
      | _ -> None
    end
  | _ -> raise_error "Not a cmpop to reduce"

exception Unexpected

(* The main thing to be careful about is that we cannot copy propagate when the temp being moved
  to is a parameter 
  TODO: WORRY ABOUT THE SIZES THAT YOU ARE PROPAGATING!!!! ALSO PAY ATTENTION IF 
    OPERAND IS A PARAMETER 
*)
let propagate block_list init_env jmap =
  
  let helper (env, change, blocks_sofar, jmap') (lb, jumps, irlist, inc, out) =
    
    (* looks at a single instruction and propagates the environment as well *)
    let my_helper (myenv, change_made, ir_sofar, jm) ir =
      match ir with
      | BINOP (op, op1, op2, op3) -> 
        let (ch1, cop2), (ch2, cop3)  = (convert_op op2 myenv, convert_op op3 myenv) in
        let nir = BINOP(op, op1, cop2, cop3) in

        (match reduce_binop nir with
          | None -> 
            
            (* cannot have immediate for div and mod *)
            (match op with
            | T.DIV | T.MOD -> 
              (match cop3 with
              | (s, IMM(c)) -> (myenv, change_made || ch1, ir_sofar @ [BINOP(op, op1, cop2, op3)], jm)
              | _ -> (myenv, change_made || ch1 || ch2, ir_sofar @ [nir], jm)
              )

            | _ -> (myenv, change_made || ch1 || ch2, ir_sofar @ [nir], jm)
            )

          | Some(reduce_ir) -> 
            (match reduce_ir with
            | MOV(d, s) -> 
              let new_env = myenv#add_ref d s in
              (new_env, true, ir_sofar @ [reduce_ir], jm)

            | _ -> (myenv, true, ir_sofar @ [reduce_ir], jm)
            )

        )

        | CHECKDIV (op1, op2) ->
          let (ch1, cop1), (ch2, cop2)  = (convert_op op1 myenv, convert_op op2 myenv) in
          let nir = CHECKDIV (cop1, cop2) in
          (myenv, change_made || ch1 || ch2, ir_sofar @ [nir], jm)

        | UNOP (op, d, op1) -> 
          let (ch, cop1) = (convert_op op1 myenv) in
          let nir = UNOP(op, d, cop1) in
                begin match reduce_unop nir with
          | None -> (myenv, change_made || ch, ir_sofar @ [nir], jm)
          | Some(new_ir) ->
            begin match new_ir with
            | MOV(d, s) -> 
            let new_env = myenv#add_ref d s in
            (new_env, true, ir_sofar @ [new_ir], jm)
          | BINOP (_, _, _, _) | UNOP (_, _, _) | DIRECTIVE _ | NOP
          | COMMENT _ | IF (_, _, _, _, _) | CALL _ | TAILCALL _
          | GOTO _ | LABEL _ | RET | CHECKDIV _ | CAST _ -> raise Unexpected
                    end
                end

        | IF (op, op1, op2, l1, l2) -> 
          let (ch1, cop1), (ch2, cop2)  = (convert_op op1 myenv, convert_op op2 myenv) in
          let nir = IF(op, cop1, cop2, l1, l2) in
          (match reduce_cmp nir with
          | None -> (myenv, change_made || ch1 || ch2, ir_sofar @ [nir], jm)
          | Some(reduce_ir) ->
            let () = jump_changed := true in
            (match reduce_ir with
            | GOTO(l) -> 
              if (l = l1) then 
                let njm = remove_jump lb l2 jm in
                (myenv, true, ir_sofar @ [reduce_ir], njm)
              else
                 let njm = remove_jump lb l1 jm in
                (myenv, true, ir_sofar @ [reduce_ir], njm)
            | BINOP (_, _, _, _) | UNOP (_, _, _) | MOV (_, _)
            | DIRECTIVE _ | COMMENT _
            | IF (_, _, _, _, _) | CALL _ | TAILCALL _
            | LABEL _ | RET | NOP | CHECKDIV _ | CAST _ -> raise Unexpected
            )
          )


        | MOV (d, s) -> 
          let (ch, cs) = convert_op s myenv in
          let new_ir = MOV(d, cs) in
          let new_env = myenv#add_ref d s in
          (new_env, change_made || ch, ir_sofar @ [new_ir], jm)

        | RET | GOTO _ | LABEL _ | CALL _ | TAILCALL _ | NOP | CAST _ ->
          (myenv, change_made, ir_sofar @ [ir], jm) 

        | DIRECTIVE _ | COMMENT _ -> raise Unexpected

    in
      let (final_env, changes, new_irlist, newjmap) = List.fold_left my_helper (env, change, [], jmap') irlist in
      let jumpouts = snd (LM.find lb newjmap) in 
      (final_env, change || changes, blocks_sofar @ [(lb, jumpouts, new_irlist, inc, out)], newjmap)

  in
    let (final_env, final_change, new_blocks, new_jmap) =  
        List.fold_left helper (init_env, false, [], jmap) block_list in

    (final_change, new_blocks, final_env, new_jmap)


(* recursively calls itself till no changes made *)
let rec apply_propagations change_made blocks env jmap = 
  if (change_made) then
    let (change, new_blocks, new_env, new_jmap) = propagate blocks env jmap in
    apply_propagations change new_blocks new_env new_jmap
  else
    (blocks, jmap)

(* optimizes a ssair blcok program *)
let optimize blocks is_unsafe jumpmap = 
  let () = issafe := (not is_unsafe) in
  let () = jump_changed := false in
  let env = new contextHolder in

  let (new_blocks, new_jmap) = 
    apply_propagations true blocks env jumpmap in

  (new_blocks, new_jmap, !jump_changed)







