import os

raw = open('failing', 'r').read()

filenames = [x.strip() for x in raw.splitlines()]

fns_with_sizes = [(x,os.path.getsize(x)) for x in filenames if x]
by_size = sorted(fns_with_sizes,key=lambda x: x[1])
print "five smallest failing"
for x in by_size[:5]:
  print "{} -> {}".format(x[1],x[0])

