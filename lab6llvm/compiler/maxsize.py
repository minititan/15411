import os
import glob

filenames = glob.glob("../tests[1,2]/*.l[1,2,3,4]")

fns_with_sizes = [(x,os.path.getsize(x)) for x in filenames if x]
by_size = list(reversed(sorted(fns_with_sizes,key=lambda x: x[1])))
print "five largest"
for x in by_size[:10]:
  print "{} -> {}".format(x[1],x[0])

