module T = Tree
open Llvmir

let format_cmpop = function
  | T.EQ -> "eq"
  | T.NOTEQ -> "ne"
  | T.GREATER -> "sgt"
  | T.GREATEREQ -> "sge"
  | T.LESS -> "slt"
  | T.LESSEQ -> "sle"

let format_binop = function
  | T.ADD -> "add"
  | T.SUB -> "sub"
  | T.MUL -> "mul"
  | T.DIV -> "sdiv"
  | T.MOD -> "srem"
  | T.BAND -> "and"
  | T.BOR -> "or"
  | T.SAR -> "ashr"
  | T.SAL -> "shl"
  | T.XOR -> "xor"

let rec format_lltype = function
  | Void -> "void"
  | I32 -> "i32"
  | I8  -> "i8"
  | I64 -> "i64"
  | I1 -> "i1"
  | Pointer typ -> format_lltype typ ^ "*"

let format_op = function
  | TEMP mt -> "%" ^ Multemp.name mt
  | CONST i -> Int32.to_string i

let format_philist (op, lbl) =
  let op_s = format_op op in
  let lbl_s = Label.name lbl in
  "[" ^ op_s ^ ", %" ^ lbl_s ^ "]"

let format_arg (typ, op) =
  let typ_s = format_lltype typ in
  let op_s = format_op op in
  typ_s ^ " " ^ op_s

let format_instr = function
  | PHI (dst_op, typ, phi_list) ->
      let phi_s = List.map format_philist phi_list |> (String.concat ", ") in
      let typ_s = format_lltype typ in
      format_op dst_op ^ " = phi " ^ typ_s ^ " " ^ phi_s

  | STORE (typ1, op1, typ2, op2) ->
    "store " ^ format_lltype typ1
    ^ " " ^ format_op op1 ^ ", "
    ^ format_lltype typ2 ^ " "
    ^ format_op op2 ^ ", align 4" (* src_size,src,dest_size,dst *)
  | CHECKNULL op ->
      "call void @check_null(i8* " ^ format_op op ^ ")"
  | CHECKDIV (op1, op2) ->
      "call void @check_div(i32 " ^ format_op op1 ^ ", i32 " ^ format_op op2 ^ ")"
  | LOAD (dst_op, src_type, src_op) ->
      let dst_op_s = format_op dst_op in
      let src_typ_s = format_lltype src_type in
      let src_op_s = format_op src_op in
      dst_op_s ^ " = load " ^ src_typ_s ^ "* " ^ src_op_s ^ ", align 4"
  | BIND (dst_op, typ, src_op) ->
      let dst_op_s = format_op dst_op in
      let typ_s = format_lltype typ in
      let src_op_s = format_op src_op in
      dst_op_s ^ " = add " ^ typ_s ^ " " ^ src_op_s ^ ", 0" (* hack: add 0 for move *)
  | BRANCH lbl -> "br label %" ^ Label.name lbl
  | BRANCHIF (op1, lbl1, lbl2) -> "br i1 " ^ format_op op1
      ^ ", label %" ^ Label.name lbl1 ^ ", label %" ^ Label.name lbl2
  | BITCAST (dst_op, src_typ, src_op, dst_typ) ->
      let dst_op_s = format_op dst_op in
      let src_typ_s = format_lltype src_typ in
      let src_op_s = format_op src_op in
      let dst_typ_s = format_lltype dst_typ in
      let cast_s =
        begin match (src_typ, dst_typ) with
         | (_, Pointer _) -> "inttoptr"
         | _ -> "zext"
        end
      in
      dst_op_s ^ " = " ^ cast_s ^ " " ^ src_typ_s ^ " " ^ src_op_s ^ " to " ^ dst_typ_s
  | CALL (dst_op, fn_typ, fn_name, arg_list, tail_possible) ->
      let dst_op_s = format_op dst_op in
      let fn_typ_s = format_lltype fn_typ in
      let fn_name_s = Symbol.name fn_name in
      let arg_s = List.map format_arg arg_list |> (String.concat ", ") in
      if tail_possible then
        dst_op_s ^ " = tail call " ^ fn_typ_s ^ "@" ^ fn_name_s ^ "(" ^ arg_s ^ ")\n  ret i32 0"
      else
        dst_op_s ^ " = call " ^ fn_typ_s ^ "@" ^ fn_name_s ^ "(" ^ arg_s ^ ")"
  | VOIDCALL (fn_name, arg_list, tail_possible) ->
      let fn_name_s = Symbol.name fn_name in
      let arg_s = List.map format_arg arg_list |> (String.concat ", ") in
      let tail_prefix = if tail_possible then "tail " else "" in
      tail_prefix ^ "call void @" ^ fn_name_s ^ "(" ^ arg_s ^ ")"
  | BINOP (bop, typ, dest, src1, src2) ->
      let binop_s = format_binop bop in
      let typ_s = format_lltype typ in
      let dest_s = format_op dest in
      let src1_s = format_op src1 in
      let src2_s = format_op src2 in
      dest_s ^ " = " ^ binop_s ^ " " ^ typ_s ^ " " ^ src1_s ^ ", " ^ src2_s
  | CMP (dst_op, cop, typ, op1, op2) ->
      let dst_op_s = format_op dst_op in
      let cop_s = format_cmpop cop in
      let typ_s = format_lltype typ in
      let op1_s = format_op op1 in
      let op2_s = format_op op2 in
      dst_op_s ^ " = icmp " ^ cop_s ^ " " ^ typ_s ^ " " ^ op1_s ^ ", " ^ op2_s
  | RET (typ, op) -> "ret " ^ format_lltype typ ^ " " ^ format_op op
  | RETVOID -> "ret void"

let tab_prefix s = "  " ^ s

let format_block (lbl, il) =
  let lbl_s = Label.name lbl ^ ":" in
  let il_s = List.map format_instr il
             |> (List.map tab_prefix)
             |> (String.concat "\t\n") in
  lbl_s ^ "\n" ^ il_s

let format_blockfunc (sym, ftype, arg_list, blocklist) =
  let block_str = List.map format_block blocklist |> (String.concat "\n") in
  let arg_str = List.map format_arg arg_list |> (String.concat ", ") in
  let sym = Symbol.name sym in
  let ftype_s = format_lltype ftype in
  let header = "; Function Attrs: nounwind uwtable\n" in
  header ^ "define " ^ ftype_s ^ " @" ^ sym ^ "(" ^ arg_str ^ ") #0 {\n"
  ^ block_str ^ "\n}"

let format llvmprog =
  List.map format_blockfunc llvmprog |> (String.concat "\n")
