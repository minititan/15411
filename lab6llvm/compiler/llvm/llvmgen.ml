module T = Tree
module SI = Ssair
module I = Ir
module L = Llvmir
module S = Storage
module MTS = Multemp.Set
module LM = Label.Map
module LS = Label.Set

let is_safe = ref false

let raise_error s = 
  ErrorMsg.error None s; raise ErrorMsg.Error

let convert_size s = 
  match s with
  | I.WORD -> L.I8
  | I.DWORD -> L.I32
  | I.QWORD -> L.I64

(* drop size here *)
let convert_op op = 
  match op with
  | SI.IMM(i) -> L.CONST(i)
  | SI.MTEMP(t) -> L.TEMP(t)
  | SI.DEREF(_, (_, _)) -> raise_error "have deref as operand in munch"
  | _ -> raise_error "Cannot have these operands"

exception EmptyMunchCall
exception NonCallMunchCall
exception SizeNotMatching

(* returns an argument list, function name, an option of dest for function
 * result, a flag indicating if tail call optimization is possible, and
 * remaining instructions. Should only be called with instruction list
 * beginning with a move to argbottom or a call.
 *)
let munch_call insts = 
  let rec get_args_out lst acc = 
    match lst with 
    | [] -> raise EmptyMunchCall
    | ir::rest -> 
      match ir with
      | SI.MOV((_, SI.ARGBOTTOM _), (s2, op)) -> 
        get_args_out rest (acc @ [(s2, op)])

      | _ -> (lst, acc)

  in

  let (rem, args) = get_args_out insts [] in

  match rem with
    | [] -> raise EmptyMunchCall
    (* this is broken from some reason. refactoring causes even more bugs... what.. *)
    (*| SI.CALL(fname) :: (SI.MOV((dsize,dest), (ssize, SI.REG(S.EAX))) :: [SI.RET]) -> 
      if dsize <> ssize then raise SizeNotMatching else
      (args, fname, Some(dest,ssize), true, [SI.RET]) (* non void tail call *)*)

    | SI.CALL(fname) :: (SI.MOV((dsize,dest), (ssize, SI.REG(S.EAX))) :: rest) -> 
      if dsize <> ssize then raise SizeNotMatching else
      (args, fname, Some(dest,ssize), false, rest)

    | SI.CALL(fname) :: [SI.RET] -> (* void tail call *)
      (args, fname, None, true, [SI.RET])

    | SI.CALL(fname) :: rest ->
      (args, fname, None, false, rest)

    | _ -> raise NonCallMunchCall

exception RegInMunchMove
exception ArgtopInMunchMove
exception ArgbottomInMunchMove
exception Unexpected

(* converts a move instruction *)
let munch_move ir = 
  match ir with
  | SI.MOV(dest, src) -> 
    (match dest with
    | (res_size, SI.DEREF(off_i, (ds, dt))) ->
      (match src with
      | (v_size, op) ->
        if (v_size <> res_size) then
          raise_error "sizes in deref dest move dont match"
        else
          let value_type = convert_size v_size in
          let store_type = L.Pointer(value_type) in
          let new_t_tostore = L.TEMP (Multemp.create()) in
          let offset_temp = L.TEMP (Multemp.create()) in

          let dest_prep =
            [
              L.BINOP(T.ADD, convert_size ds, offset_temp, convert_op dt, L.CONST(off_i));
              L.BITCAST(new_t_tostore, convert_size ds, offset_temp, store_type)
            ]
          in

          (match op with
          | SI.MTEMP(t) ->
              dest_prep @
              [
                L.STORE(value_type, L.TEMP(t), store_type, new_t_tostore)
              ]

          | SI.IMM(i) -> 
              dest_prep @
              [
                L.STORE(value_type, L.CONST(i), store_type, new_t_tostore)
              ]

          | SI.DEREF(off, (siz, tem)) ->
            let extra =
              if !is_safe then
                let null_check_temp = L.TEMP(Multemp.create()) in
                [
                  L.BITCAST(null_check_temp, convert_size siz, convert_op tem, L.Pointer(L.I8));
                  L.CHECKNULL(null_check_temp);
                ]
              else
                []
            in
            let src_temp = L.TEMP(Multemp.create()) in
            let temp_temp = L.TEMP(Multemp.create()) in
            extra @ [
              L.BINOP(T.ADD, convert_size siz, offset_temp, convert_op tem, L.CONST(off));
              L.BITCAST(src_temp, convert_size siz, offset_temp, L.Pointer(value_type));
              L.LOAD(temp_temp, store_type, src_temp);
              L.BITCAST(new_t_tostore, convert_size ds, convert_op dt, store_type);
              L.STORE(value_type, temp_temp, store_type, new_t_tostore)
            ]
          | SI.REG _ -> raise RegInMunchMove
          | SI.ARGTOP _ -> raise ArgtopInMunchMove
          | SI.ARGBOTTOM _ -> raise ArgbottomInMunchMove
          )
      )

    | (res_size, SI.MTEMP(dt)) ->
      (match src with
      | (v_size, op) ->
        if (v_size <> res_size) then
          raise_error "sizes in temp dest move dont match"
        else
          let value_type = convert_size v_size in
          let store_type = convert_size res_size in

          begin match op with
            | SI.MTEMP(_) | SI.IMM(_) -> 
              [L.BIND(L.TEMP(dt), value_type, convert_op op)]

            | SI.DEREF(off, (siz, tem)) ->
              let extra =
                let null_check_temp = L.TEMP(Multemp.create()) in
                [
                  L.BITCAST(null_check_temp, convert_size siz, convert_op tem, L.Pointer(L.I8));
                  (*L.CHECKNULL(null_check_temp);*)
                ]
              in
              let src_temp = L.TEMP(Multemp.create()) in
              let temp_temp = L.TEMP(Multemp.create()) in
              let offset_temp = L.TEMP(Multemp.create()) in
              extra @ [
              L.BINOP(T.ADD, convert_size siz, offset_temp, convert_op tem, L.CONST(off));
              L.BITCAST(src_temp, convert_size siz, offset_temp, L.Pointer(value_type));
              L.LOAD(temp_temp, store_type, src_temp);
              L.BIND(L.TEMP(dt), value_type, temp_temp)
              ]
            | SI.REG _ | SI.ARGTOP _ | SI.ARGBOTTOM _ -> raise Unexpected
          end
      )

    | _ -> raise_error "Move dest not a temp or deref"

    )
  | _ -> raise_error "Not a move instruction"


(* there cannot be derefs as operands so we dont worry about calling
  convert_op on the operands here *)
let munch_cmp ir = 
  match ir with
  | SI.IF(cop, (s1, op1), (s2, op2), l1, l2) ->
    if (s1 <> s2) then
      raise_error "Sizes not equal in comparison"
    else
      let res_temp = L.TEMP(Multemp.create()) in
      let top1, top2 = convert_op op1, convert_op op2 in
      let sz = convert_size s1 in
      [L.CMP(res_temp, cop, sz, top1, top2);
       L.BRANCHIF(res_temp, l1, l2)
      ]

  | _ -> raise_error "Not a comparison instruction to munch"

exception BinopSizeNotMatching
exception UnopSizeNotMatching

let munch_unop = function
    | SI.UNOP (unop, (dest_size, dest), (src_size, src)) ->
            if dest_size <> src_size then raise UnopSizeNotMatching else
            let zero = L.CONST (Int32.zero) in
            let neg1 = L.CONST (Int32.of_int (-1)) in
            begin match unop with
              | T.NEG  -> [L.BINOP (T.SUB, convert_size dest_size, convert_op dest, zero, convert_op src)]
              | T.BNOT -> [L.BINOP (T.XOR, convert_size dest_size, convert_op dest, neg1, convert_op src)]
            end

    | _ -> raise_error "Non-unop in munch_unop"

let munch_binop = function
  | SI.BINOP(bop, (dest_size, dest), (op1_size, op1), (op2_size, op2)) ->
    if dest_size <> op1_size || dest_size <> op2_size || op1_size <> op2_size then
      (* sizes should all match *)
      raise BinopSizeNotMatching
    else
      [L.BINOP (bop, convert_size dest_size, convert_op dest, convert_op op1, convert_op op2)]
                
  | _ -> raise_error "Not a binop to match"

let convert_arg = function
  | (size, op) -> (convert_size size, convert_op op)

(* converts every instruction into the llvm equivalent. Converted instructions
  are accumulated into acc and returned once the list is empty *)
let llvm_block_trans (block : Ssair.instr list) : Llvmir.instr list = 

  let rec helper (ir_list : Ssair.instr list) (acc : Llvmir.instr list) : Llvmir.instr list = 
    begin match ir_list with
    | [] -> acc
    | ir::rest -> 
      begin match ir with
        | SI.MOV(dst, src) ->
          begin match dst with

          (* check if its the beginning of function call and munch the call if so *)
          | (_, SI.ARGBOTTOM(_)) -> 
            let (args, fname, dest, tail_possible, rem) = munch_call (ir::rest) in
            begin match dest with
            | Some(d,dsize) -> helper rem (acc @ [L.CALL(convert_op d, convert_size dsize, fname, List.map convert_arg args, tail_possible)])
            | None -> helper rem (acc @ [L.VOIDCALL(fname, List.map convert_arg args, tail_possible)])
            end
          
          (* also check is its a move to eax indicating a return *)
          | (ret_size, SI.REG S.EAX) -> 
            begin match rest with
            | SI.RET::rest' -> 
              let (src_size, src_op) = src in
              if (ret_size <> src_size) then
                raise_error "sizes of move not equal"
              else
                begin match src_op with
                  | SI.MTEMP(t) -> helper rest' (acc @ [L.RET(convert_size ret_size, L.TEMP(t))])

                  | SI.IMM(i) -> helper rest' (acc @ [L.RET(convert_size ret_size, L.CONST(i))])

                  | SI.DEREF(off, (dt_size, dt)) -> 
                    let res_temp = L.TEMP(Multemp.create()) in
                    let temp_temp = L.TEMP(Multemp.create()) in
                    let offset_temp = L.TEMP(Multemp.create()) in
                    let cs = 
                      let store_type = L.Pointer(convert_size ret_size) in
                      [
                      L.BINOP(T.ADD, convert_size dt_size, offset_temp, convert_op dt, L.CONST(off));
                      L.BITCAST(temp_temp, convert_size dt_size, offset_temp, store_type);
                      L.LOAD(res_temp, store_type, temp_temp);
                      L.RET(convert_size ret_size, res_temp)
                      ]
                    in
                    helper rest' (acc @ cs)
                  | _ -> raise Unexpected
                end

            | _ -> raise_error "no return after a move into eax"
            end

          | _ ->
              let commands = munch_move ir in
              helper rest (acc @ commands)
          end 

        | SI.BINOP _ ->
          let commands = munch_binop ir in
          helper rest (acc @ commands)

        | SI.CAST(d_op, s_op) ->
          (match d_op, s_op with
          | (d_size, SI.MTEMP(dt)), (s_size, SI.MTEMP(st)) -> 
            let cmds = [L.BITCAST(L.TEMP(dt), convert_size s_size, L.TEMP(st), convert_size d_size)] in
            helper rest (acc @ cmds)

          | _ -> raise_error "Pattern not possible for CAST instruction"
          )

          (* ignore sizes, assume i32 *)
        | SI.CHECKDIV((_, op1), (_, op2)) ->
          let commands =
            [L.CHECKDIV (convert_op op1, convert_op op2)]
          in
          helper rest (acc @ commands)

        | SI.UNOP _ ->
          let commands = munch_unop ir in
          helper rest (acc @ commands)

        | SI.IF _ ->
          let commands = munch_cmp ir in
          helper rest (acc @ commands)

        | SI.GOTO l -> helper rest (acc @ [L.BRANCH(l)])

        | SI.CALL _ ->
          let (args, fname, dest, tail_possible, rem) = munch_call (ir::rest) in
          
          begin match dest with
            | Some(d,dsize) -> helper rem (acc @ [L.CALL(convert_op d, convert_size dsize, fname, List.map convert_arg args, tail_possible)])
            | None -> helper rem (acc @ [L.VOIDCALL(fname, List.map convert_arg args, tail_possible)])
          end

        | SI.LABEL _ -> helper rest acc

        (* if not void then it is after a mov to eax which will munch it *)
        | SI.RET -> helper rest (acc @ [L.RETVOID])

        | SI.TAILCALL _ | SI.DIRECTIVE _ | SI.COMMENT _ -> raise Unexpected

        | SI.NOP -> helper rest acc
      end
    end
  in
  helper block []

(* assumes its the first block *)
let get_args (bl, jset, block, _, _) =
  let rec collect_args b args = 
    match b with
    | SI.MOV (x, (s1, SI.ARGTOP(_))) :: rest -> 
      (match x with
      | (s, SI.MTEMP t) -> collect_args rest (args @ [(convert_size s, L.TEMP(t))])
      | _ -> raise_error "Move argtop into non-temp"
      )

    | _ -> args
  in
  collect_args (List.tl block) []

(* creates the phi moves required at the start of a basic block *)
let create_phi_commands bl jmap startmap endmap =
  let (in_blocks, out_blocks) = LM.find bl jmap in
  let start_params = LM.find bl startmap in
  Multemp.Set.fold (fun (p,c) irlist -> 
        let (size, phi_list) = 
          LS.fold (fun in_bl (s, l) -> 
                let mtset = LM.find in_bl endmap in
                let same_gen = MTS.filter (fun (p', c') -> p' = p) mtset in
                if (MTS.cardinal same_gen = 0) then (s, l)
                else
                  let (pa, ch) = MTS.choose same_gen in
                  let s' = Multemp.get_size (Temp.get_temp pa) in
                  (match s' with
                  | None -> raise_error "temp size not present in mapping"
                  | Some(sz) -> (Some (convert_size sz), (L.TEMP((pa, ch)), in_bl) :: l)
              	  )
              ) in_blocks (None, [])

        in
        
        if (List.length phi_list = 0) then
          irlist
        else
          (match size with
          | None -> raise_error "Size for multemp at end of block not found"
          | Some(s) -> L.PHI(L.TEMP((p,c)), s, phi_list) :: irlist
          )
      ) start_params []


let llvm_func_trans (sz, fname, blocks, jmap, startmap, endmap) = 
  let arg_list = get_args (List.hd blocks) in
  let trans_blocks = 
  		List.map (fun (bl, jset, block, in_mtemps, out_mtemps) -> 
                  	let phi_start = create_phi_commands bl jmap startmap endmap in
                  	(bl, phi_start @ (llvm_block_trans block))
                  ) (List.tl blocks)
  in
  
  match sz with
  | None -> (fname, L.Void, arg_list, trans_blocks)
  | Some(s) -> (fname, convert_size s, arg_list, trans_blocks)


(* converts a list of functinons to llvm functions *)
let llvmgen ~safe fl =
  let () = is_safe := safe in
  List.map llvm_func_trans fl 




