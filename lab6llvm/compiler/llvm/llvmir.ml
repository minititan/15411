module S = Storage
module T = Tree

type binop = T.binop
type cmpop = T.cmpop
type unop = T.unop

type lltype =
  | I32
  | I8 (* byte *)
  | I64 (* pointer *)
  | I1
  | Pointer of lltype
  | Void

type operand = 
	| TEMP of Multemp.multemp
	| CONST of Int32.t


(* note on store: store i32 5, i32* %1
 * stores 5 into the memory cell of size
 * i32 at %1 i.e. we have source, dest
 * like AT&T syntax
 *)

(* note: tail call dropped for now, as tail
 * recursion is already covered with a goto
 *)

(* The types mimic the patterns defined in the llvm reference *)
type instr =
  | PHI of operand * lltype * (operand * Label.label) list
  | STORE of lltype * operand * lltype * operand (* src_size,src,dest_size,dst *)
  | LOAD of operand * lltype * operand (* dst * src_type * src *)
  | BIND of operand * lltype * operand (* no move instruction in SSA, so bind instead *)
  | BRANCH of Label.label (* same as goto *)
  | BRANCHIF of operand * Label.label * Label.label (* true, false label order *)
  | BITCAST of operand * lltype * operand * lltype (* dst,src_size,src,dst_size *)
  | CALL of operand * lltype * Symbol.symbol * ((lltype * operand) list) * bool (* bool indicated tail call opimization possible *)
  | VOIDCALL of Symbol.symbol * ((lltype * operand) list) * bool (* also tail call here *)
  | BINOP of binop * lltype * operand * operand * operand
  | CMP of operand * cmpop * lltype * operand * operand
  | RET of lltype * operand
  | RETVOID
  | CHECKNULL of operand
  | CHECKDIV of operand * operand (* src1, src2 *)

type llvmblock = Label.label * instr list

(* function name, irlist, map for jumps
  map goes from block label to tuple of two sets. first set is incoming block labels and second
  is outgoing block labels *)
type llvmblockfunc = Symbol.symbol * lltype * ((lltype * operand) list) * llvmblock list

type llvmprogram = llvmblockfunc list




