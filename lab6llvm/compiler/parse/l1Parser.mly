%{
(* L1 Compiler
 * L1 grammar
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Now conforms to the L1 fragment of C0
 *
 * Modified: Maxime Serrano <mserrano@andrew.cmu.edu> Fall 2014
 * Should be more up-to-date with 2014 spec
 *
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

module P = Parsetree
module T = TypeHolder

%}

%token EOF
%token DOT ARROW
%token TYPEDEF IF ELSE WHILE FOR
%token STRUCT NULL ALLOC ALLOCARRAY
%token ASSERT TRUE FALSE
%token BOOL VOID
%token CHAR STRING
%token SEMI
%token <Int32.t> DECCONST 
%token <Int32.t> HEXCONST
%token <Symbol.symbol> IDENT
%token <Symbol.symbol> TYPIDENT
%token RETURN
%token INT
%token PLUS MINUS STAR SLASH PERCENT 
%token BITAND BITOR XOR BITNOT LOGICNOT LOGICAND LOGICOR
%token PLUSPLUS MINUSMINUS
%token SAR SAL EQUALS NOTEQUALS LESSEQUAL GREATEREQUAL LESS GREATER
%token QUESTION COLON
%token ASSIGN PLUSEQ MINUSEQ STAREQ SLASHEQ PERCENTEQ 
%token BITANDEQ BITOREQ XOREQ SAREQ SALEQ
%token LBRACE RBRACE
%token LPAREN RPAREN
%token LBRACKET RBRACKET
%token COMMA
%token CONTINUE BREAK
%token NEGATE

/* UNARY and ASNOP are dummy terminals.
 * We need dummy terminals if we wish to assign a precedence
 * to a rule that does not correspond to the precedence of
 * the rightmost terminal in that rule.
 * Implicit in this is that precedence can only be infered
 * terminals. Therefore, don't try to assign precedence to "rules"
 */

%type <Parsetree.program> program

%right ASSIGN PLUSEQ MINUSEQ STAREQ SLASHEQ PERCENTEQ BITANDEQ BITOREQ XOREQ SALEQ SAREQ
%right QUESTION COLON
%left LOGICOR
%left LOGICAND
%left BITOR
%left XOR
%left BITAND
%left EQUALS NOTEQUALS
%left LESS LESSEQUAL GREATER GREATEREQUAL
%left SAL SAR
%left PLUS MINUS
%left STAR SLASH PERCENT
%right LOGICNOT BITNOT UNARYMINUS PLUSPLUS MINUSMINUS
%nonassoc LPAREN RPAREN LBRACKET RBRACKET ARROW DOT

%start program

%%

program :
  /* empty */                      { P.Program None }
  | gdecl program                  { P.Program (Some ($1, $2)) }
  ;

gdecl :
  fdecl                            { $1 }
  | fdefn                          { $1 }
  | typedef                        { $1 }
  | sdecl                          { $1 }
  | sdefn                          { $1 }
  ;

fdecl :
  typ IDENT param_list SEMI       { P.Fdecl ($1, $2, $3) }
  ;

fdefn :
  typ IDENT param_list block      { P.Fdefn ($1, $2, $3, $4) }
  ;

sdecl :
  STRUCT IDENT SEMI                { P.Sdecl $2 }
  | STRUCT TYPIDENT SEMI             { P.Sdecl $2 }
  ;

sdefn : 
  STRUCT IDENT LBRACE field_list RBRACE SEMI    { P.Sdefn($2, $4) }
  | STRUCT TYPIDENT LBRACE field_list RBRACE SEMI    { P.Sdefn($2, $4) }
  ;

field :
  typ IDENT SEMI                       { P.Field($1, $2) }
  | typ TYPIDENT SEMI                    { P.Field($1, $2) }
  ;

field_list :
  /* empty */                       { P.FL None }
  | field field_list                { P.FL (Some ($1, $2)) }
  ;

param :
  typ IDENT                       { P.Param ($1, $2) }
  ;

param_list_follow :
  /* empty */                     { P.PLF None }
  | COMMA param param_list_follow { P.PLF (Some ($2, $3)) }
  ;

param_list :
  LPAREN RPAREN                           { P.PL None }
  | LPAREN param param_list_follow RPAREN { P.PL (Some ($2, $3)) }
  ;

typedef :
  TYPEDEF typ IDENT SEMI          { T.get_typedef $2 $3 }
  ;

typ :
  INT                             { P.INT }
  | BOOL                          { P.BOOL }
  | TYPIDENT                      { T.get_type $1 }
  | VOID                          { P.VOID }
  | typ STAR                      { P.POINTER $1 }
  | typ LBRACKET RBRACKET         { P.ARRPOINTER $1 }
  | STRUCT IDENT                  { P.STRUCT $2 }
  | STRUCT TYPIDENT               { P.STRUCT $2 }
  ;

block :
  LBRACE stmts RBRACE              { P.Block $2 }
  ;

stmts :
  /* empty */                      { P.Seq None }
  | stmt stmts                     { P.Seq (Some ($1, $2)) }
  ;

stmt :
  simp SEMI                        { P.StmSimp $1 }
  | control                        { P.StmControl $1 }
  | block                          { P.StmBlock $1 }
  ;

decl :
  typ IDENT                        { P.Decl ($1, $2) }
  | typ IDENT ASSIGN exp           { P.DeclAsn ($1, $2, $4) }
  ;

simp :
  exp asop exp                  { P.SimpAsop ($1, $2, $3) }
  | STAR exp postop             { ErrorMsg.error None "for compatibility with C, please write (*e)++ instead of *e++"; raise ErrorMsg.Error }
  | exp postop                  { P.SimpPostop ($1, $2) }
  | decl                           { P.SimpDecl $1 }
  | exp                            { P.SimpExp $1 }
  ;

simpopt :
  /* empty */                      { P.SimpOpt None }
  | simp                           { P.SimpOpt (Some $1) }
  ;

elseopt :
  /* empty */                      { P.ElseOpt None }
  | ELSE stmt                      { P.ElseOpt (Some $2) }
  ;

control :
  IF LPAREN exp RPAREN stmt elseopt                        { P.If($3, $5, $6) }
  | WHILE LPAREN exp RPAREN stmt                           { P.While($3, $5) }
  | FOR LPAREN simpopt SEMI exp SEMI simpopt RPAREN stmt   { P.For ($3, $5, $7, $9) }
  | RETURN exp SEMI                                        { P.Return $2 }
  | RETURN SEMI                                            { P.ReturnVoid }
  | ASSERT LPAREN exp RPAREN SEMI                          { P.Assert $3 }
  ;

arg_list_follow :
  /* empty */                 { P.ALF None }
  | COMMA exp arg_list_follow { P.ALF (Some ($2, $3)) }
  ;

arg_list :
  LPAREN RPAREN                       { P.AL None }
  | LPAREN exp arg_list_follow RPAREN { P.AL (Some ($2, $3)) }
  ;


/* expressions are organized into blocks by precedence */
exp : 
  LPAREN exp RPAREN                        { $2 }
 | IDENT                                      { P.Var $1 }
 | ALLOC LPAREN typ RPAREN                    { P.Alloc($3) }
 | ALLOCARRAY LPAREN typ COMMA exp RPAREN     { P.AllocArr($3, $5) }
 | IDENT arg_list                             { P.Call ($1, $2) }
 | NULL                                       { P.Null }
 | exp DOT IDENT                           { P.Dot($1, $3) }
 | exp DOT TYPIDENT                        { P.Dot($1, $3) }
 | exp ARROW IDENT                         { P.Arrow($1, $3) }
 | exp ARROW TYPIDENT                      { P.Arrow($1, $3) }
 | STAR exp                                { P.Deref($2) }


 | exp LBRACKET exp RBRACKET            { P.ArrayIndex($1, $3) }

 | intconst                      { $1 }

 | TRUE                          { P.True } 
 | FALSE                         { P.False }

 | LOGICNOT exp %prec LOGICNOT   { P.UnopExp (P.LOGICNOT, $2) }
 | BITNOT exp %prec BITNOT       { P.UnopExp (P.BITNOT, $2) }
 | MINUS exp %prec UNARYMINUS    { P.UnopExp (P.NEGATIVE, $2) }

 | exp STAR exp                  { P.BinopExp (P.TIMES, $1, $3) }
 | exp SLASH exp                 { P.BinopExp (P.DIVIDEDBY, $1, $3) }
 | exp PERCENT exp               { P.BinopExp (P.MODULO, $1, $3) }
 | exp PLUS exp                  { P.BinopExp (P.PLUS, $1, $3) }
 | exp MINUS exp                 { P.BinopExp (P.MINUS, $1, $3) }
 | exp SAL exp                   { P.BinopExp (P.SAL, $1, $3) }
 | exp SAR exp                   { P.BinopExp (P.SAR, $1, $3) }
 | exp LESS exp                  { P.BinopExp (P.LESS, $1, $3) }
 | exp LESSEQUAL exp             { P.BinopExp (P.LESSEQUAL, $1, $3) }
 | exp GREATER exp               { P.BinopExp (P.GREATER, $1, $3) }
 | exp GREATEREQUAL exp          { P.BinopExp (P.GREATEREQUAL, $1, $3) }
 | exp EQUALS exp                { P.BinopExp (P.EQUALS, $1, $3) }
 | exp NOTEQUALS exp             { P.BinopExp (P.NOTEQUALS, $1, $3) }
 | exp BITAND exp                { P.BinopExp (P.BITAND, $1, $3) }
 | exp XOR exp                   { P.BinopExp (P.XOR, $1, $3) }
 | exp BITOR exp                 { P.BinopExp (P.BITOR, $1, $3) }
 | exp LOGICAND exp              { P.BinopExp (P.LOGICAND, $1, $3) }
 | exp LOGICOR exp               { P.BinopExp (P.LOGICOR, $1, $3) }
 | exp QUESTION exp COLON exp    { P.Condition ($1, $3, $5) }
 ;


intconst :
  DECCONST                       { P.ConstExp $1 }
  | HEXCONST                     { P.ConstExp $1 }
  ;

postop :
  PLUSPLUS                       { P.PLUSPLUS }
  | MINUSMINUS                   { P.MINUSMINUS } 
  ;

asop :
 ASSIGN                          { P.ASSIGN }
 | PLUSEQ                        { P.PLUSEQ }
 | MINUSEQ                       { P.MINUSEQ }
 | STAREQ                        { P.STAREQ }
 | SLASHEQ                       { P.SLASHEQ }
 | PERCENTEQ                     { P.PERCENTEQ }
 | BITANDEQ                      { P.BITANDEQ }
 | BITOREQ                       { P.BITOREQ }
 | XOREQ                         { P.XOREQ }
 | SAREQ                         { P.SAREQ }
 | SALEQ                         { P.SALEQ }
 ;

%%