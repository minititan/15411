#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
#include <limits.h>

void check_null(void* x)
{
  if (x == NULL)
  {
    raise(SIGSEGV);
  }
}

//sometimes pointers are 64 bit values
void check_null64(int64_t x)
{
  if (x == 0)
  {
    raise(SIGSEGV);
  }
}

void check_div(int32_t x, int32_t y)
{
  if (y == 0)
  {
    raise(SIGFPE);
  }
  if (x == INT_MIN && y == -1)
  {
    raise(SIGFPE);
  }
}

//A is given i64_t so that it is consistent with the arithmetic
//we do on 64 bit integers when not using LLVM in our IR
void check_array(int64_t A, int32_t offset, int32_t size)
{
  if (A == 0)
  {
    raise(SIGSEGV);
  }
  if (offset < 0)
  {
    raise(SIGSEGV);
  }
  int32_t num_elements = *(int32_t*)((int8_t*)A-size);
  if (offset >= num_elements)
  {
    raise(SIGSEGV);
  }
}
