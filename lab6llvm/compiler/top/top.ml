(* L1 Compiler
 * Top Level Environment
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

open Unix

let say = print_endline

let flag_verbose = Flag.flag "verbose"
let flag_ast = Flag.flag "ast"
let flag_ir1 = Flag.flag "ir1"
let flag_ir2 = Flag.flag "ir2"
let flag_assem = Flag.flag "assem"
let flag_parse = Flag.flag "parse"
let flag_irinst = Flag.flag "irinst"
let flag_llir = Flag.flag "llir"
let flag_coloring = Flag.flag "coloring"
let flag_optimize = Flag.flag "optimize"
let flag_livelist = Flag.flag "livelist"
let flag_unsafe = Flag.flag "unsafe"
let flag_llvm = Flag.flag "llvm"

let library_file = ref ""
let optimization_level = ref 0

let reset_flags () =
  List.iter Flag.unset [flag_verbose; flag_ast; flag_ir1; flag_ir2; flag_assem;
  flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist;
  flag_unsafe; flag_llvm]

let set flag = Arg.Unit (fun () -> Flag.set flag)

let set_all () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir1; flag_ir2;
    flag_assem; flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize;
    flag_livelist; flag_llvm])

let set_code () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir1; flag_ir2;
    flag_assem; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist])

let options =
  [("--verbose", set flag_verbose, "verbose message");
   ("-v", set flag_verbose, "verbose message");
   ("-l", Arg.String (fun s -> library_file := s), "library file (.h0)");
   ("-O0", Arg.Unit (fun () -> optimization_level := 0), "optimization level 0");
   ("-O1", Arg.Unit (fun () -> optimization_level := 1), "optimization level 1");
   ("-O2", Arg.Unit (fun () -> optimization_level := 2), "optimization level 2");
   ("--llvm", set flag_llvm, "use llvm backend");
   ("--unsafe", set flag_unsafe, "don't check null pointers");
   ("--safe", Arg.Unit (fun () -> ()), "make all memory checks");
   ("--debug-parse", set flag_parse, "debug the parser");
   ("--dump-ast", set flag_ast, "pretty print the AST");
   ("--dump-ir1", set flag_ir1, "pretty print IR1");
   ("--dump-ir2", set flag_ir2, "pretty print IR2");
   ("--dump-irinst", set flag_irinst, "pretty print the IR instructions");
   ("--dump-llir", set flag_llir, "pretty print the LLIR instructions");
   ("--dump-coloring", set flag_coloring, "pretty print the coloring map");
   ("--dump-assem", set flag_assem, "pretty print the assembly");
   ("--dump-opt", set flag_optimize, "pretty print the optimized assembly");
   ("--dump-livelist", set flag_livelist, "pretty print the live list");
   ("--all", set_all() , "set all print flags (super verbose mode)");
   ("--code", set_code(), "set all flags related to code generation")
 ]

exception EXIT

let stem s =
  try
    let dot = String.rindex s '.' in
    String.sub s 0 dot
  with Not_found -> s

let main args =
  try
    let header = "Usage: compile [OPTION...] SOURCEFILE\nwhere OPTION is" in
    let usageinfo () = Arg.usage options header in
    let errfn msg = say (msg ^ "\n"); usageinfo (); raise EXIT in

    let _ = Temp.reset () in
    let _ = reset_flags () in

    let _ = if Array.length args < 2 then (usageinfo (); raise EXIT) in

    (* Parse the arguments.  Non-keyword arguments accumulate in fref,
       and then is assigned to files. *)
    let files =
      let fref = ref []
      and current = ref 0 in
      let () = Arg.parse_argv ~current args options
          (fun s -> fref := s::!fref) header in
      List.rev !fref in

    let source = match files with
    | [] -> errfn "Error: no input file"
    | [filename] -> filename
    | _ -> errfn "Error: more than one input file" in

    (* Parse Header *)
    let library_ast = if !library_file = "" then
      []
    else
      let _ = Flag.guard flag_verbose say ("Parsing header... " ^ !library_file) in
      let _ = Flag.guard flag_parse
          (fun _ -> ignore (Parsing.set_trace true)) () in
      let library_ast = !library_file
          |> Parse.parse
          |> (Elaboration.trans_program ~is_main_file:false)
      in
      library_ast
    in

    (* Parse and Elaborate *)
    let _ = Flag.guard flag_verbose say ("Parsing... " ^ source) in
    let _ = Flag.guard flag_parse (fun () -> ignore (Parsing.set_trace true)) () in
    let ast = Parse.parse source |> (Elaboration.trans_program ~is_main_file:true) in
    let _ = Flag.guard flag_ast (fun () -> say (Ast.Print.pp_program ast)) () in

    (* Typecheck *)
    let typed_ast = StaticChecker.check (library_ast,ast) in
    let _ = Flag.guard flag_verbose say "Passed Type Checks.\n" in

    (* Expression tree *)
    let _ = Flag.guard flag_verbose say "Typed AST -> Command List...\n" in
    let base_tree = Trans.translate typed_ast ~is_unsafe:(Flag.isset flag_unsafe) ~is_llvm:(Flag.isset flag_llvm) in
    let _ = Flag.guard flag_ir1 (fun () -> say (Tree.Print.pp_program base_tree)) () in

    (* IR *)
    let _ = Flag.guard flag_verbose say "Command List -> IR...\n" in
    let base_ir = Codegen.codegen base_tree in
    let _ = Flag.guard flag_ir1 (fun () -> say (Ir.Print.pp_program base_ir)) () in

    (* Basic Blocks *)
    let _ = Flag.guard flag_verbose say "IR -> Basic Blocks...\n" in
    let basic_blocks = Blockgen.block_convert base_ir in
    let _ = Flag.guard flag_ir2 (fun () -> say (Block.Print.pp_blockprogram basic_blocks)) () in

    if not (Flag.isset flag_llvm) then
      let optimized_blocks =
        if (!optimization_level = 2) then
          (* initial neededness *)
          let _ = Flag.guard flag_verbose say "Initial neededness...\n" in
          let init_opt_blocks = Allocator.get_needed_of_blockprogram basic_blocks in
          let _ = Flag.guard flag_ir2 (fun () -> say (Block.Print.pp_blockprogram init_opt_blocks)) () in

          (* SSA conversion *)
          let _ = Flag.guard flag_verbose say "BB -> SSA...\n" in
          let ssa_blocks =
            Ssagen.genssa
              init_opt_blocks
              ~is_unsafe:(Flag.isset flag_unsafe)
              ~is_llvm:(Flag.isset flag_llvm)
          in
          let _ = Flag.guard flag_ir2 (fun () -> say (Ssair.Print.pp_ssaprogram ssa_blocks)) () in

          (* DESSA (still in ssa form but now has temps) *)
          let _ = Flag.guard flag_verbose say "SSA -> De-SSA...\n" in
          let dessa_blocks = Dessa.dessafier ssa_blocks in
          let _ = Flag.guard flag_ir2 (fun () -> say (Block.Print.pp_blockprogram dessa_blocks)) () in

          (* Neededness Analysis *)
          let _ = Flag.guard flag_verbose say "Neededness...\n" in
          let needed_blocks = Allocator.get_needed_of_blockprogram dessa_blocks in
          let _ = Flag.guard flag_ir2 (fun () -> say (Block.Print.pp_blockprogram needed_blocks)) () in
          needed_blocks
        else
          basic_blocks
      in

      (* Liveness Analysis *)
      let _ = Flag.guard flag_verbose say "Performing Regalloc...\n" in
      let storage_map = Allocator.storage_maps_of_blockprogram optimized_blocks in
      let _ = Flag.guard flag_coloring (fun () -> say (Allocator.Print.pp_storagemap storage_map)) () in

      (* LLIR *)
      let _ = Flag.guard flag_verbose say "Basic Blocks -> LLIR...\n" in
      let llir = Llirgen.llirgen optimized_blocks storage_map in
      let _ = Flag.guard flag_llir (fun () -> say (Llir.Print.pp_program llir)) () in

      let _ = Flag.guard flag_verbose say "LLIR -> Assembly..." in
      let assem = Assemgen.assembly_gen llir storage_map in
      let _ = Flag.guard flag_assem
          (fun () -> say (Assem.Print.pp_program assem)) () in

      let _ = Flag.guard flag_verbose say "Optimizing.." in
      let assem_opt =
        if (!optimization_level = 2) then
          Optimize.optimize assem
        else
          assem
      in
      let _ = Flag.guard flag_verbose (fun () -> say (Assem.Print.pp_program assem_opt)) () in

      (* Add assembly header and footer *)
      let assem_final =
        Assem.DIRECTIVE(".file\t\"" ^ source ^ "\"")::[Assem.DIRECTIVE(".text")]
        @ assem_opt
        @ [Assem.DIRECTIVE ".ident\t\"15-411 L3 compiler\""] in

      let code = String.concat "" (List.map Assem.format assem_final) in

      (* Output assembly *)
      let afname = stem source ^ ".s" in
      let _ = Flag.guard flag_verbose
          say ("Writing assembly to " ^ afname ^ " ...") in
      let _ = SafeIO.withOpenOut afname
          (fun afstream -> output_string afstream code) in
      (* Return success status *)
      0
    else
      (* SSA conversion *)
      let _ = Flag.guard flag_verbose say "BB -> SSA...\n" in
      let ssa_blocks =
        Ssagen.genssa
          basic_blocks
          ~is_unsafe:(Flag.isset flag_unsafe)
          ~is_llvm:(Flag.isset flag_llvm) (* should be true here *)
      in
      let _ = Flag.guard flag_ir2 (fun () -> say (Ssair.Print.pp_ssaprogram ssa_blocks)) () in

      let llvm_prog = Llvmgen.llvmgen ~safe:(not (Flag.isset flag_unsafe)) ssa_blocks in
      let llvm_str = Llvm.format llvm_prog in
      let header = "; ModuleID = '" ^ source ^ "'\n" ^
        "target datalayout = \"e-p:64:64:64-i1:8:8-i8:8" ^
        ":8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:" ^
        "64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-" ^
        "S128\"\ntarget triple = \"x86_64-pc-linux-gnu\"\n\n" in

      let declarations =
        [
          "void @raise(i32)";
          "void @abort()";
          "i32 @itof(i32)";
          "i32 @ftoi(i32)";
          "i32 @fadd(i32, i32)";
          "i32 @fsub(i32, i32)";
          "i32 @fmul(i32, i32)";
          "i32 @fdiv(i32, i32)";
          "i32 @fless(i32, i32)";
          "void @print_fpt(i32)";
          "void @print_int(i32)";
          "void @print_hex(i32)";
          "void @check_null(i8*)";
          "void @check_div(i32, i32)";
          "i64 @calloc(i32, i32)"; (* we treat pointers returned by calloc as 64bit ints *)
          "void @check_array(i64, i32, i32)";
          "void @check_null64(i64)"
        ]
        |> (List.map (fun s -> "; Function Attrs: nounwind\ndeclare " ^ s ^ " #1"))
        |> String.concat "\n"
      in

      let footer = "\n\nattributes #0 = { nounwind uwtable \"less-precise-" ^
        "fpmad\"=\"false\" \"no-frame-pointer-elim\"=\"true\" \"no-frame" ^
        "-pointer-elim-non-leaf\" \"no-infs-fp-math\"=\"false\" \"no-nans-" ^
        "fp-math\"=\"false\" \"stack-protector-buffer-size\"=\"8\" \"unsafe" ^
        "-fp-math\"=\"false\" \"use-soft-float\"=\"false\" }\n\n!llvm.ident =" ^
        " !{!0}\n\n!0 = metadata !{metadata !\"Louisiana LLVM C0\"}" in

      (* Add assembly header and footer *)
      let llvm_final =
        header
        ^ declarations
        ^ llvm_str
        ^ footer
      in

      (* Output assembly *)
      let afname = stem source ^ ".ll" in
      let _ = Flag.guard flag_verbose
          say ("Writing llvm ir to " ^ afname ^ " ...") in
      let _ = SafeIO.withOpenOut afname
          (fun afstream -> output_string afstream llvm_final) in

      (* we get 100/100 with just -simplifycfg and -die
       * That's no fun though considering all the optimizations
       * we could have! Here's just a small subset of -O1
       * that doesn't kill our safety requirements for C0.
       * *)
      if !optimization_level = 2 then
        let options =
          if Flag.isset flag_unsafe then ["-O3"] else
          ["-deadargelim"; "-simplifycfg"; "-jump-threading";
          "-correlated-propagation"; "-simplifycfg"; "-tailcallelim";
          "-simplifycfg"; "-reassociate"; "-loops"; "-loop-simplify";
          "-lcssa"; "-loop-rotate"; "-licm"; "-lcssa"; "-loop-unswitch";
          "-loop-simplify"; "-loop-unroll"; "-simplifycfg"; "-adce";
          "-strip-dead-prototypes"; "-domtree"; "-verify"]
        in

        let cmd = "opt " ^ (String.concat " " options) ^ " " ^ afname ^ " -o " ^ stem source ^ ".bc" in
      (* let () = Printf.printf "Running command: %s\n" cmd in *)
        let _ = system cmd in
        0
      else
        0

  with
    ErrorMsg.Error -> say "Compilation failed"; 1
  | EXIT -> 1
  | Arg.Help x -> prerr_string x; 1
  | e -> prerr_string (Printexc.to_string e); 1


let test s =
  main (Array.of_list (""::(Str.split (Str.regexp "[ \t\n]+") s)))
