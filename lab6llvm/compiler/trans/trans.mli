(* note from trans.ml:
 * each symbol should map to a struct_info,
 * a list of the fields in the structure in order
 * note that field's definition comes from TypedAst *)

val translate : TypedAst.typprogram * Symbol.symbol Symbol.Map.t -> is_unsafe:bool -> is_llvm:bool -> Tree.irprogram
