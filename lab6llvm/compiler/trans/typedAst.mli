type ident = Ast.ident

type oper = Ast.oper

type primtype =
  | Int
  | Bool
  | Void
  | PrId of ident
  | Struct of ident
  | Pointer of primtype
  | AnyPointer
  | Array of primtype

type param = primtype * ident

type field = primtype * ident

type params = param list

type simpexp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * typexp * typexp
  | UnopExp of oper * typexp 
  | Condition of typexp * typexp * typexp
  | True
  | False
  | Call of ident * typexp list
  | Null
  | FieldDeref of typexp * ident (* for structs *)
  | Deref of typexp
  | AllocCall of primtype
  | AllocArrCall of primtype * typexp
  | ArrIndex of typexp * typexp
and typexp = 
  primtype * simpexp

type typstm =
  | Declare of primtype * ident * typstm
  | Assign of typexp * typexp
  | AssignOp of typexp * typexp
  | If of typexp * typstm * typstm
  | While of typexp * typstm
  | Assert of typexp
  | Return of typexp
  | ReturnVoid
  | Seq of typstm * typstm
  | Expr of typexp
  | Nop

type typtopstm = 
  | Fdefn of primtype * ident * params * typstm
  | Fdecl of primtype * ident * params
  | Typedef of primtype * ident
  | StructDecl of ident
  | StructDefn of ident * (field list)

type typprogram = typtopstm list
