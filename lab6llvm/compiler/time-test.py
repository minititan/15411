import subprocess
import glob
import os
import json
import sys

#this code is ugly and should only be used, not looked at

d = {}
llvm = ["--llvm"]
unsafe = ["--unsafe"]
O2 = ["-O2"]
d["O0-safe-llvm"] = llvm
d["O0-unsafe-llvm"] = llvm+unsafe
d["O0-safe-x86"] = []
d["O0-unsafe-x86"] = unsafe
d["O2-safe-llvm"] = O2+llvm
d["O2-unsafe-llvm"] = O2+llvm+unsafe
d["O2-safe-x86"] = O2
d["O2-unsafe-x86"] = O2+unsafe

#filenames = glob.glob("../bench[0,1]/*.l[1,2,3,4]")

def is_return(fn):
  with open(fn,'r') as f:
    return "return" in f.readline()


def get_time(output):
    res = float(output.splitlines()[-1].split(",")[0])
    return res

def time_args(filenames,arg_list,trials,is_llvm,is_opt):
    print "is_llvm",is_llvm
    print "is_opt",is_opt
    #time how long it takes for bin/l4c arg_list
    #on the directory test_cases for each generated binary
    #see make_test for how to generate binary
    total = 0.0
    size = len(filenames)
    for i,filename in enumerate(filenames):
        print "[{}/{}]".format(i+1,size)
        raw_name = ".".join(filename.split(".")[:-1])
        cmd1 = ["bin/l4c","-l","15411.h0",filename]+arg_list
        if is_llvm:
            if is_opt:
              cmd2 = ["llc","-march=x86-64",raw_name+".bc","-o",raw_name+".s"]
            else:
              cmd2 = ["llc","-march=x86-64",raw_name+".ll","-o",raw_name+".s"]
        cmd3 = ["gcc","-m64",raw_name+".s","15411.c","l4rt.c","l4lib.c","-o",raw_name]
        cmd4 = ["perf","stat","-x,","-e","instructions:u",raw_name] #my own perf
        if True:
            print " ".join(cmd1)
            if is_llvm:
              print " ".join(cmd2)
            print " ".join(cmd3)
            print " ".join(cmd4)
            print ""
        subprocess.call(cmd1)
        if is_llvm:
            subprocess.call(cmd2)
        subprocess.call(cmd3)
        total += sum(get_time(subprocess.check_output(cmd4,stderr=subprocess.STDOUT)) for x in range(trials))/trials
        os.remove(raw_name)
        os.remove(raw_name+".s")
        if is_llvm:
          os.remove(raw_name+".ll")
          if is_opt:
              os.remove(raw_name+".bc")
    return int(total)

def run_tests(current_lab):
  filenames = [fn for fn in glob.glob("../tests[1,2]/*.{}".format(current_lab)) if is_return(fn)]
  #filenames = [fn for fn in glob.glob("../../lab4/tests3/*.{}".format(current_lab)) if is_return(fn)]
  results = {}
  trials = 1
  for name,arg_list in d.iteritems():
      results[name] = time_args(filenames,arg_list,trials,"llvm" in name,"O2" in name)
  print results
  open('results_{}'.format(current_lab),'w').write(json.dumps(results))

for lab in ["l1","l2","l3","l4"]:
  run_tests(lab)

