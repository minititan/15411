module IR = Ir
module S = Storage
module IM = Regalloc_util.IntMap
open Llir
open Regalloc_util

let raise_error msg = (ErrorMsg.error None msg; raise ErrorMsg.Error)

exception WrongColor

let color_to_storage c =
  if (c >= 0) then raise WrongColor
  else if c < stack_min_temp then
    S.STACK(stack_min_temp - c)
  else
    S.REG(temp_to_reg c)

let get_store t storage_map =
  let c = IM.find (Temp.get_int t) storage_map in
  color_to_storage c

let munch_size = function
  | IR.WORD -> WORD
  | IR.DWORD -> DWORD
  | IR.QWORD -> QWORD

let munch_operand storage_map op =
  let rec munch_simpop = function
    | IR.TEMP t ->
      (match get_store t storage_map with
      | S.STACK i -> STACK i
      | S.REG r -> REG r
      )
    | IR.REG r -> REG r
    | IR.IMM i -> IMM i
    | IR.ARGTOP i -> ARGTOP i
    | IR.ARGBOTTOM i -> ARGBOTTOM i
    | IR.DEREF (offset, (size', t)) ->   
          (match t with
          | IR.TEMP(tmp) -> 
              let tmp' = (match get_store tmp storage_map with
                            | S.STACK i -> STACK i
                            | S.REG r -> REG r
                         )
              in

              DEREF (offset, (munch_size size', tmp'))
          
          | _ -> DEREF(offset, (munch_size size', munch_simpop t))
          )

  and munch_operand' (size, op) =
    (munch_size size, munch_simpop op)
  in
  munch_operand' op

exception DestinationImm
exception ArginBinop


let biggest_size s1 s2 s3 =
  match (s1, s2, s3) with
  | (QWORD, _, _) | (_, QWORD, _) | (_, _, QWORD) -> QWORD
  | _ -> DWORD

let munch_binop storage_map binop d t1 t2 =
  let (size_t1,t1_op) as t1 = munch_operand storage_map t1
  and (size_t2,t2_op) as t2 = munch_operand storage_map t2
  and (size_d,d_op) as d = munch_operand storage_map d in
  let size_d' = biggest_size size_t1 size_t2 size_d in (* in case we don't use t1 (same as t2), make sure to promote d to a larger size *)
  (match binop with
   | Tree.DIV -> [MOV ((size_t1,REG S.EAX), t1); CLTD; UNOP (IDIV, t2); MOV ((size_d',d_op), (size_d,REG S.EAX))]
   | Tree.MOD -> [MOV ((size_t1,REG S.EAX), t1); CLTD; UNOP (IDIV, t2); MOV ((size_d',d_op), (size_d,REG S.EDX))]

   | Tree.ADD ->
      if d_op = t2_op then 
        (match t1_op with
        | IMM (i) -> if (i = Int32.one) then [UNOP(INC, d)]
                    else [BINOP (ADD, (size_d',d_op), t1)]
        | _ -> [BINOP (ADD, (size_d',d_op), t1)] 
        )

      else if d_op = t1_op then 
        (match t2_op with
          | IMM (i) -> if (i = Int32.one) then [UNOP(INC, d)]
                        else [BINOP (ADD, (size_d',d_op), t2)] 
          | _ -> [BINOP (ADD, (size_d',d_op), t2)] 
        )

      else [MOV ((size_d',d_op), t1); BINOP (ADD, (size_d',d_op), t2)] (* split up 3 addr *)

   | Tree.SUB -> 
      if d_op = t2_op then [BINOP (SUB, (size_d',d_op), t1); UNOP(NEG, d)] 
      
      else if d_op = t1_op then 
        (match t2_op with
        | IMM (i) -> if (i = Int32.one) then [UNOP(DEC, d)]
                      else [BINOP (SUB, (size_d',d_op), t2)]
        | _ -> [BINOP (SUB, (size_d',d_op), t2)]
        )

      else [MOV ((size_d',d_op), t1); BINOP (SUB, (size_d',d_op), t2)]

   | Tree.MUL -> 
      if d_op = t2_op then [BINOP (MUL, (size_d',d_op), t1)] 
      else if d_op = t1_op then [BINOP (MUL, (size_d',d_op), t2)] 
      else [MOV ((size_d',d_op), t1); BINOP (MUL, (size_d',d_op), t2)]

   | Tree.BAND -> 
      if d_op = t2_op then [BINOP (BAND, (size_d',d_op), t1)] 
      else if d_op = t1_op then [BINOP (BAND, (size_d',d_op), t2)] 
      else [MOV ((size_d',d_op), t1); BINOP (BAND, (size_d',d_op), t2)]

   | Tree.BOR -> 
      if d_op = t2_op then [BINOP (BOR, (size_d',d_op), t1)] 
      else if d_op = t1_op then [BINOP (BOR, (size_d',d_op), t2)] 
      else [MOV ((size_d',d_op), t1); BINOP (BOR, (size_d',d_op), t2)]

   | Tree.SAR ->
   (* if d is stack
    t1 in r14
    t2 in ecx
    shift r14 by cl
    store r14 into d

    if d is reg
    t1 in d
    t2 in ecx
    shift d by cl

    if d is imm
    fail *)
      (match d_op with
        | STACK _ -> [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
        | REG _ ->
          if d_op = t2_op then
            [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
          else
            [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (size_d',d_op), (WORD,REG S.CL))]
        | DEREF (offset, (size, oper)) ->
          (match oper with
          | STACK _ -> [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (size_d',d_op), (WORD,REG S.CL))]
          | REG _ ->
            if d_op = t2_op then
              [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
            else
              [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAR, (size_d',d_op), (WORD,REG S.CL))]
          | _ -> raise_error "impossible case in Tree.SAR"
          )
        | IMM _ -> raise DestinationImm
        | ARGTOP _ | ARGBOTTOM _ -> raise ArginBinop
      )

   | Tree.SAL ->
      (match d_op with
        | STACK _ -> [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
        | REG _ ->
          if d_op = t2_op then
            [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
          else
            [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (size_d',d_op), (WORD,REG S.CL))]
        | DEREF (offset, (size, oper)) ->
          (match oper with
          | STACK _ -> [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (size_d',d_op), (WORD,REG S.CL))]
          | REG _ ->
            if d_op = t2_op then
              [MOV ((size_t1,REG S.R14), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (DWORD,REG S.R14), (WORD,REG S.CL)); MOV ((size_d',d_op), (size_d,REG S.R14))]
            else
              [MOV ((size_d',d_op), t1); MOV ((size_t2,REG S.ECX), t2); BINOP (SAL, (size_d',d_op), (WORD,REG S.CL))]
          | _ -> raise_error "impossible case in Tree.SAL"
          )
        | IMM _ -> raise DestinationImm
        | ARGTOP _ | ARGBOTTOM _ -> raise ArginBinop
      )
   | Tree.XOR -> if d_op = t2_op then [BINOP (XOR, (size_d',d_op), t1)] else if d_op = t1_op then [BINOP (XOR, (size_d',d_op), t2)] else [MOV ((size_d',d_op), t1); BINOP (XOR, (size_d',d_op), t2)]
 )

let munch_unop storage_map u d t1 =
  let (d, t1) = (munch_operand storage_map d, munch_operand storage_map t1) in
  (match u with
  | Tree.NEG -> [MOV(d, t1); UNOP (NEG, d)]
  | Tree.BNOT -> [MOV(d, t1); UNOP (BNOT, d)]
  )

let munch_if storage_map cop o1 o2 l1 l2 =
  let o1' = munch_operand storage_map o1 in
  let o2' = munch_operand storage_map o2 in
  let cop' = (match cop with
                | Tree.EQ -> EQ
                | Tree.NOTEQ -> NOTEQ
                | Tree.GREATER -> GREATER
                | Tree.GREATEREQ -> GREATEREQ
                | Tree.LESS -> LESS
                | Tree.LESSEQ -> LESSEQ
                ) in 

              [IF (cop', o1', o2', l1); GOTO l2]

let munch_instr storage_map = function
  | IR.BINOP (b, d, t1, t2) -> munch_binop storage_map b d t1 t2
  | IR.UNOP (u, d, t1) -> munch_unop storage_map u d t1
  | IR.MOV (d, s) | IR.CAST (d, s) ->
    (let d' = munch_operand storage_map d
    and s' = munch_operand storage_map s in
    [MOV (d', s')])
  | IR.DIRECTIVE s -> [DIRECTIVE s]
  | IR.COMMENT s -> [COMMENT s]
  | IR.IF (cop, o1, o2, l1, l2) -> munch_if storage_map cop o1 o2 l1 l2
  | IR.GOTO l -> [GOTO l]
  | IR.LABEL l -> [LABEL l]
  | IR.RET -> [RET]
  | IR.NOP -> []

  | IR.CALL(fname) -> [CALL(fname)]
  | IR.TAILCALL(fname) -> [TAILCALL(fname)]
  | IR.CHECKDIV _ -> [] (* x86_64 traps for us. this is used in LLVM *)

let block_llir irlist storage_map =
    List.flatten (List.map (munch_instr storage_map) irlist)

let func_llir blist storage_map =
  List.flatten (List.map (fun (l, lset, irlist) -> block_llir irlist storage_map) blist)

let llirgen flist (storage_map : int Regalloc_util.IntMap.t Symbol.Map.t) =
  List.map (fun (sz, fname, blist, jmap) -> (fname, func_llir blist (Symbol.Map.find fname storage_map))) flist

