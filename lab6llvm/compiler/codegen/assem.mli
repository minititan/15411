(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | ANDOP
  | OR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type size =
  | WORD
  | DWORD
  | QWORD

type operand =
  | IMM of size * Int32.t
  | REG of size * Storage.reg
  | STACK of size * int
  | DEREF of size * int * operand (* offset * operand *)

val get_size : operand -> size

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | CMP of operand * operand
  | TEST of operand * operand
  | JZFAIL
  | JUMP of cmpop * Label.label (* conditional jump *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET
  | DIRECTIVE of string
  | COMMENT of string
  | FUNCHEADER of string
  | PUSH of operand
  | POP of operand
  | CALL of Symbol.symbol
  | CALLJUMP of Symbol.symbol
  | AND of operand * operand

type program = instr list

val format : instr -> string

module type PRINT =
  sig
    val pp_instr : instr -> string
    val pp_instrs : instr list -> string
    val pp_program : program -> string
  end

module Print : PRINT
