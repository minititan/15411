(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(*
TODO: include unsigned jumps?
*)

module S = Storage

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | ANDOP (* overloaded AND *)
  | OR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type size =
  | WORD
  | DWORD
  | QWORD

type operand =
  | IMM of size * Int32.t
  | REG of size * Storage.reg
  | STACK of size * int
  | DEREF of size * int * operand

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | CMP of operand * operand
  | TEST of operand * operand
  | JZFAIL
  | JUMP of cmpop * Label.label (* conditional jump *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET
  | DIRECTIVE of string
  | COMMENT of string
  | FUNCHEADER of string
  | PUSH of operand
  | POP of operand
  | CALL of Symbol.symbol
  | CALLJUMP of Symbol.symbol
  | AND of operand * operand

type program = instr list

(* functions that format assembly output *)

let format_unop size unop =
  (match size with
    | QWORD -> (match unop with
      | IDIV -> "IDIVQ"
      | NEG -> "NEGQ"
      | BNOT -> "NOTQ"
      | INC -> "INCQ"
      | DEC -> "DECQ"
    )
    | DWORD -> (match unop with
      | IDIV -> "IDIVL"
      | NEG -> "NEGL"
      | BNOT -> "NOTL"
      | INC -> "INCL"
      | DEC -> "DECL"
    ) (* should word cases happen? *)
    | WORD -> (match unop with
      | IDIV -> "IDIV"
      | NEG -> "NEG"
      | BNOT -> "NOT"
      | INC -> "INC"
      | DEC -> "DEC"
    )
  )

let format_binop size bop =
  (match size with
  | QWORD -> (match bop with
    | ADD -> "ADDQ"
    | SUB -> "SUBQ"
    | MUL -> "IMULQ"
    | ANDOP -> "ANDQ"
    | OR -> "ORQ"
    | SAR -> "SARQ"
    | SAL -> "SHLQ" (* no "arirthmetic" shift left *)
    | XOR -> "XORQ"
  )
  | DWORD -> (match bop with
    | ADD -> "ADDL"
    | SUB -> "SUBL"
    | MUL -> "IMULL"
    | ANDOP -> "ANDL"
    | OR -> "ORL"
    | SAR -> "SARL"
    | SAL -> "SHLL"
    | XOR -> "XORL"
  )
  | WORD -> (match bop with
    | ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "IMUL"
    | ANDOP -> "AND"
    | OR -> "OR"
    | SAR -> "SAR"
    | SAL -> "SHL"
    | XOR -> "XOR"
  )
  )

let (@@) f x = f x

let rec format_operand = function
  | IMM (_, n)  -> "$" ^ Int32.to_string n
  | STACK (_, s) -> string_of_int s ^ "(%rsp)"
  | REG (size, r) ->
    (match size with
     | WORD -> S.format_reg @@ S.reg_to_16 r
     | DWORD -> S.format_reg @@ S.reg_to_32 r
     | QWORD -> S.format_reg @@ S.reg_to_64 r
    )
  | DEREF (_, i, op) -> 
      match op with
      | IMM(_,c) -> string_of_int i ^ "(" ^ Int32.to_string c ^ ")"
      | _ -> string_of_int i ^ "(" ^ format_operand op ^ ")"

let format_label = Label.name

let get_size = function
  | IMM (s, _) -> s
  | REG (s, _) -> s
  | STACK (s, _) -> s
  | DEREF (s, _, _) -> s

let biggest_size s1 s2 =
  match (get_size s1, get_size s2) with
  | (QWORD, _) | (_, QWORD) -> QWORD
  | _ -> DWORD

let update_size size = function
  | IMM (_, i) -> IMM (size, i)
  | REG (_, r) -> REG (size, r)
  | STACK (_, s) -> STACK (size, s)
  | DEREF (_, i, o) -> DEREF (size, i, o)

let format = function
  | BINOP (oper, d, s) ->
    (match oper with
    | SAL | SAR ->
      "\t" ^ format_binop (get_size d) oper
      ^ "\t" ^ format_operand s
      ^ ", " ^ format_operand d ^ "\n"
    | XOR | OR | ANDOP | ADD | SUB | MUL ->
      let biggest = biggest_size d s in
      let d' = update_size biggest d in
      let s' = update_size biggest s in
      "\t" ^ format_binop biggest oper
      ^ "\t" ^ format_operand s'
      ^ ", " ^ format_operand d' ^ "\n"
    )
  | UNOP (oper, d) ->
      "\t" ^ format_unop (get_size d) oper
      ^ "\t" ^ format_operand d ^ "\n"
  | MOV (d, s) ->
      let biggest = biggest_size d s in
      let d' = update_size biggest d in
      let s' = update_size biggest s in
      let ins = (match biggest with
        | QWORD -> "MOVQ"
        | DWORD -> "MOVL"
        | WORD -> "MOVL"
      ) in
      "\t" ^ ins
      ^ "\t" ^ format_operand s'
      ^ ", " ^ format_operand d' ^ "\n"

  | PUSH op -> 
      let ins = (match get_size op with
        | QWORD -> "PUSHQ"
        | DWORD -> "PUSHL"
        | WORD -> "PUSH"
      ) in
      "\t" ^ ins
      ^ "\t" ^ (format_operand op) ^ "\n"

  | POP op ->
      let ins = (match get_size op with
        | QWORD -> "POPQ"
        | DWORD -> "POPL"
        | WORD -> "POP"
      ) in
      "\t" ^ ins
      ^ "\t" ^ (format_operand op) ^ "\n"

  | CALL func ->
      "\tCALL\t" ^ Symbol.name func ^ "\n"

  | CALLJUMP func ->
      "\tJMP\t" ^ Symbol.name func ^ "\n"

  | DIRECTIVE str ->
      "\t" ^ str ^ "\n"
  | COMMENT str ->
      "\t" ^ "/* " ^ str ^ "*/\n"
  | FUNCHEADER name ->
      let name' = name in
      "\n\t.globl\t" ^ name' ^ "\n\n"
      ^ name' ^ ":\n"
  | CLTD ->
      "\t" ^ "CLTD" ^ "\n"
  | RET ->
      "\t" ^ "RET" ^ "\n"
  | GOTO l ->
      "\t" ^ "JMP " ^ format_label l ^ "\n"
  | LABEL l ->
      format_label l ^ ":" ^ "\n"
  | CMP (o1, o2) ->
      let biggest = biggest_size o2 o1 in
      let o2' = update_size biggest o2 in
      let o1' = update_size biggest o1 in
      let ins = (match biggest with
        | QWORD -> "CMPQ"
        | DWORD -> "CMPL"
        | WORD -> "CMP"
      ) in
      "\t" ^ ins ^
      "\t" ^ format_operand o2' ^ ", " ^ format_operand o1' ^ "\n"
  | TEST (o1, o2) ->
      let biggest = biggest_size o2 o1 in
      let o2' = update_size biggest o2 in
      let o1' = update_size biggest o1 in
      let ins = (match biggest with
        | QWORD -> "TESTQ"
        | DWORD -> "TESTL"
        | WORD -> "TEST"
      ) in
      "\t" ^ ins ^
      "\t" ^ format_operand o2' ^ ", " ^ format_operand o1' ^ "\n"
  | JZFAIL -> "\tjz\t_c0_call_raise\n"
  | JUMP (cop, l) ->
    let jinst = (match cop with
      | EQ -> "JE"
      | NOTEQ -> "JNE"
      | GREATER -> "JG"
      | GREATEREQ -> "JGE"
      | LESS -> "JL"
      | LESSEQ -> "JLE") in
    "\t" ^ jinst ^ " " ^ format_label l ^ "\n"

  | AND (op1, op2) ->
      let biggest = biggest_size op2 op1 in
      let op2' = update_size biggest op2 in
      let op1' = update_size biggest op1 in
      let ins = (match biggest with
        | QWORD -> "ANDQ"
        | DWORD -> "ANDL"
        | WORD -> "AND"
      ) in
      "\t" ^ ins ^ "\t" ^ format_operand op2' ^ ", " ^ format_operand op1' ^ "\n"

module type PRINT =
  sig
    val pp_instr : instr -> string
    val pp_instrs : instr list -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct
    let pp_instr = format

    let rec pp_instrs = function
      [] -> ""
    | instr::instrs' -> pp_instr instr ^ pp_instrs instrs'

    let pp_program instrs = "Assembly code {\n" ^ pp_instrs instrs ^ "}\n"
  end
