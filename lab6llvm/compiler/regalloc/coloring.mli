open Regalloc_util

(* val get_color_map : IntSet.t array -> (IntSet.t * IntSet.t *IntSet.t) list -> int IntMap.t *)

val gen_coloring : IntSet.t IntMap.t -> int IntMap.t

module type PRINT =
  sig
    val pp_colormap : int IntMap.t -> string
  end

module Print : PRINT