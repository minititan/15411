val storage_map_of_blockfunc : Block.blockfunc ->
	(Symbol.symbol * int Regalloc_util.IntMap.t)

val storage_maps_of_blockprogram : Block.blockprogram ->
	int Regalloc_util.IntMap.t Symbol.Map.t


val get_needed_of_blockfunc : Block.blockfunc -> Block.blockfunc

val get_needed_of_blockprogram : Block.blockprogram -> Block.blockprogram

(* takes a blockprogram and returns a map from label to live temps at the beginning of the block *)
val get_block_live : Block.blockprogram -> Regalloc_util.IntSet.t Label.Map.t

val format : int -> Storage.str -> string

module type PRINT =
  sig
    val pp_storagemap : int Regalloc_util.IntMap.t Symbol.Map.t -> string
  end

module Print : PRINT
