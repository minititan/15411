(* module to implement register or stack allocation *)

open Regalloc_util

module SM = Symbol.Map
module L = Liveness_gen
module N = Neededness
module C = Coloring

let get_needed_of_blockfunc (sym, bl, jmap) =
  let (i_list,line_map) = L.instruction_line_list bl in
  let (def_list,needed_list) = L.seed i_list |> N.propagate_neededness in
  let needed_map = N.get_needed_map (i_list,def_list,needed_list) in
  let new_blocks = N.get_new_blocks (bl,line_map,needed_map) in
  (sym, new_blocks, jmap)

let get_needed_of_blockprogram bl =
  List.map get_needed_of_blockfunc bl

let storage_map_of_blockfunc (sym, bl, _) =
  let colored =
    let (i_list,_) = L.instruction_line_list bl in
    i_list
    |> L.seed
    |> L.propagate_liveness
    |> L.gen_interference
    |> C.gen_coloring
  in
  (sym, colored)

let storage_maps_of_blockprogram p =
  let sm_of_list l = List.fold_left (fun sm (k,v) -> SM.add k v sm) SM.empty l in
  sm_of_list (List.map storage_map_of_blockfunc p)

exception NotLabel

let live_map_of_blockfunc (_,bl, _) =
  let (i_list,_) = L.instruction_line_list bl in
  let (_,live_list,_) = L.seed i_list |> L.propagate_liveness in
  let zipped = zip ((i_list),(live_list)) in
  let labels_only = List.filter (fun ((_,i),_) -> match i with Ir.LABEL _ -> true | _ -> false) zipped in
  let live_lines_map = List.fold_left (fun m ((_,l),lv) -> match l with Ir.LABEL lbl -> Label.Map.add lbl lv m | _ -> raise NotLabel) Label.Map.empty labels_only in
  live_lines_map

let merge_maps m1 m2 =
  List.fold_left (fun m1' (m2k,m2v) -> Label.Map.add m2k m2v m1') m1 (Label.Map.bindings m2)

(* Block.blockprogram -> LiveSet Label.Map.t *)
let get_block_live bp =
  let sm_of_list l = List.fold_left (fun lm lm' -> merge_maps lm lm') Label.Map.empty l in
  sm_of_list (List.map live_map_of_blockfunc bp)

let format k v = 
  let s = Printf.sprintf "t%d -> " k in
  match v with 
  | Storage.STACK(o) -> let s2 = Printf.sprintf "STACK(%d)\n" o in s ^ s2
  | Storage.REG(r) -> s ^ Storage.format_reg r ^ "\n" 

module type PRINT =
  sig
    val pp_storagemap : int IntMap.t Symbol.Map.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_storagemap m =
      let bndgs = Symbol.Map.bindings m in
      let int_storage_map_to_string ism =
        let bndgs_ism = IntMap.bindings ism in
        let ism_binding_to_string (i,stor) =
          (string_of_int i) ^ " -> " ^ (string_of_int stor)
        in
        String.concat "\n" (List.map ism_binding_to_string bndgs_ism)
      in
      let bndg_to_string (s,int_storage_map) =
        let ism_string = int_storage_map_to_string int_storage_map in
        Symbol.name s ^ ":\n" ^ ism_string
      in
      "Register allocation mappings:\n" ^ String.concat "\n" (List.map bndg_to_string bndgs) ^ "\n\n"
  end
