(* Implementation of register allocator *)

(* Lab 3: Note we are doing this starting from IR (Tree.cmd) now. *)

open Regalloc_util
module IR = Ir
module T = Tree
module S = Storage
module LM = Label.Map

type live = IntSet.t
type define = IntSet.t
type use = IntSet.t
type successor = IntSet.t
type necessary = IntSet.t
type line = int

exception WrongPattern of string

let (|>) x f = f x

let raise_error s = ErrorMsg.error None s; raise ErrorMsg.Error

(* (Definition, Use set, Successor, Neccessary set) *)
type predicates = line * define * use * necessary * successor

type numbered_commands = (line * Ir.instr) list

let list_to_set = List.fold_left (fun s x -> IntSet.add x s) IntSet.empty

let label_map_from_instructions (inst_l_with_lines : numbered_commands) =
  List.fold_left (fun lm (i,l) ->
    match l with IR.LABEL l' -> LM.add l' i lm | _ -> lm)
  LM.empty inst_l_with_lines

let block_to_instr_list (l,_,cl) =
  List.mapi (fun i c -> (l,i,c)) cl

(* main_line_number * ((label * block_line_number * inst) list) *)
let blockfunc_to_instr_list bf =
  List.map block_to_instr_list bf
  |> List.concat
  |> enumerate

let intset_of_rlist =
  List.fold_left (fun s e -> IntSet.add (reg_to_temp e) s) IntSet.empty

(* set of caller save registers *)
let caller_save =
  intset_of_rlist [S.EAX; S.EDI; S.ESI; S.EDX; S.ECX; S.R8; S.R9; S.R10; S.R11]

let merge_def_use (d1,u1) (d2,u2) = (IntSet.union d1 d2, IntSet.union u1 u2)

let argnum_to_regset =
  let reg_to_set r = IntSet.singleton (reg_to_temp r) in
  function
  | 0 -> reg_to_set S.EDI
  | 1 -> reg_to_set S.ESI
  | 2 -> reg_to_set S.EDX
  | 3 -> reg_to_set S.ECX
  | 4 -> reg_to_set S.R8
  | 5 -> reg_to_set S.R9
  | _ -> IntSet.empty               

let get_deref_op = function
  | (_, IR.DEREF (_, (_, tmp))) -> 
      
      (match tmp with
      | IR.TEMP(t) -> IntSet.singleton (Temp.get_int t)
      | _ -> IntSet.empty 
      )

  | _ -> IntSet.empty

let deref_of_temps = List.fold_left (fun s x -> IntSet.union s (get_deref_op x)) IntSet.empty

let get_temp_list = function
  | IR.BINOP (_, d, s1, s2) -> [d;s1;s2]
  | IR.UNOP(_, d, s) -> [d;s]
  | IR.DIRECTIVE _ | IR.COMMENT _ -> []
  | IR.MOV (d, s) -> [d;s]
  | IR.IF (_, o1, o2, _, _) -> [o1;o2]
  | IR.CHECKDEREF o -> [o]
  | IR.CALL _ -> []
  | IR.TAILCALL _ -> []
  | IR.GOTO l -> []
  | IR.LABEL l -> []
  | IR.NOP -> []
  | IR.RET -> []

let get_deref_ins ins = get_temp_list ins |> deref_of_temps

let temp_to_set (_, op) = match op with
  | IR.IMM _ -> IntSet.empty
  | IR.REG r -> IntSet.singleton (reg_to_temp r)
  | IR.TEMP t -> IntSet.singleton (Temp.get_int t)
  | IR.ARGTOP a | IR.ARGBOTTOM a -> argnum_to_regset a
  (* we use get_deref_use for the deref case as this
     should always be a use, so do nothing here *)
  | IR.DEREF (offset, (size, tmp)) -> IntSet.empty

let temps_to_set =
  List.fold_left (fun s x -> IntSet.union (temp_to_set x) s) IntSet.empty

exception LabelMissing

let drop_size (_, i) = i

(* return (line, def, use, needed, succ) *)
(* Generates predicates for an instruction *)
let predicate_gen lm (line, instr) : predicates =
  let label_to_line l = try LM.find l lm with Not_found -> raise_error ("Label Missing: " ^ Label.name l) in
  let empty = IntSet.empty in
  let next_line = IntSet.singleton (line+1) in
  let (d,u,n,s) =
   begin match instr with
    | IR.BINOP (b, d, s1, s2) ->
      begin match b with
       | T.DIV | T.MOD ->
        let regs_used = list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX] in
        let source_temps = temps_to_set [s1; s2] in
        (IntSet.union regs_used (temp_to_set d),
         IntSet.union regs_used source_temps,
         IntSet.union regs_used source_temps,
         next_line)
       | T.SAR | T.SAL ->
        (IntSet.add (reg_to_temp S.ECX) (temp_to_set d),
         IntSet.add (reg_to_temp S.ECX) (temps_to_set [s1; s2]),
         empty,
         next_line)
       | T.ADD | T.SUB | T.MUL | T.BAND | T.BOR | T.XOR ->
       (temp_to_set d, temps_to_set [s1; s2], empty, next_line)
      end
    (* is this correct? *)
    | IR.UNOP(_, d, s) -> (temp_to_set d, temp_to_set s, empty, next_line)
    (* TODO: we should actually use directives and comments. *)
    | IR.DIRECTIVE _ | IR.COMMENT _ -> raise_error "Invalid instruction."
    | IR.MOV (d, s) ->
      let nec = begin match drop_size d with
        | IR.ARGBOTTOM _ -> temps_to_set [d;s]
        | IR.DEREF _ -> temp_to_set s
        | _ -> empty
      end
      in
      (temp_to_set d, temp_to_set s, nec, next_line)
    | IR.IF (_, o1, o2, l1, l2) ->
      let l1line = label_to_line l1 in
      let l2line = label_to_line l2 in
      let used_nec = temps_to_set [o1; o2] in
      (empty, used_nec, used_nec, list_to_set [l1line; l2line; line+1])
    | IR.CHECKDEREF o -> (empty, temp_to_set o, temp_to_set o, next_line)
    | IR.CALL _ -> (caller_save, empty, empty, next_line)
    | IR.TAILCALL _ -> (empty, empty, empty, empty)
    | IR.GOTO l ->
      (empty, empty, empty, IntSet.singleton (label_to_line l))
    | IR.LABEL l -> (empty, empty, empty, next_line)
    | IR.NOP -> (empty, empty, empty, next_line)
    | IR.RET ->
      let ret_used = IntSet.singleton (reg_to_temp S.EAX) in
      (empty, ret_used, ret_used, empty)
   end
  in
  let deref_temps = (get_deref_ins instr) in
  let u' = IntSet.union u deref_temps in
  let n' = IntSet.union n deref_temps in
  (line,d,u',n',s)

let instruction_line_list (bl : Block.block list) : numbered_commands * line IntMap.t Label.Map.t =
  let numbered_list = blockfunc_to_instr_list bl in
  let block_label_map_init =
    List.fold_left
    (fun m lbl -> Label.Map.add lbl IntMap.empty m)
    Label.Map.empty
    (List.map (fun (lbl,_,_) -> lbl) bl)
  in
  let line_to_blockline_map =
    List.fold_left
    (fun m (main_line_number,(lbl,block_line_number,_)) ->
      Label.Map.add lbl (IntMap.add block_line_number main_line_number (Label.Map.find lbl m)) m
    )
    block_label_map_init numbered_list
  in
  let instruction_list =
    List.map
    (fun (main_line_number,(_,_,inst)) -> (main_line_number,inst))
    numbered_list
  in
  (instruction_list,line_to_blockline_map)

let seed l = List.map (predicate_gen (label_map_from_instructions l)) l

let intset_of_define x = x
let intset_of_use x = x
let intset_of_live x = x

let propagated_to_list def_list use_list live_list =
  let fixup = List.map (fun (i,sl) -> string_of_int i ^ " -> {" ^ (String.concat ", " sl) ^ "}") in
  let def_list_s =
    def_list
    |> (List.map intset_of_define)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let use_list_s =
    use_list
    |> (List.map intset_of_use)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let live_list_s =
    live_list
    |> (List.map intset_of_live)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  "Def list:\n" ^ def_list_s ^ "\nUse list:\n" ^ use_list_s ^ "\nLive list:\n" ^ live_list_s

(* we can refactor update_interference to
   use this down below when we have time *)
let update_mapset line pred_map successor_line =
  let pred = IntMap.find successor_line pred_map in
  let pred' = IntSet.add line pred in
  IntMap.add successor_line pred' pred_map

let preds_to_string preds =
  let bndgs = IntMap.bindings preds in
  let bnd_to_string (ln,int_set) =
    let int_list = IntSet.elements int_set in
    "Line " ^ (string_of_int ln) ^ " -> {" ^ String.concat "," (List.map string_of_int int_list) ^ "}"
  in
  let combine s1 s2 = s1 ^ "\n" ^ s2 in
  List.fold_left combine "" (List.map bnd_to_string bndgs)

let rec propagate_var_liveness predecessors var pred_map ln =
  let preds = IntMap.find ln predecessors in
  let (d,u,s,l) = IntMap.find ln pred_map in
  if (IntSet.mem var d && not (IntSet.mem var u)) || IntSet.mem var l then (* done propagating *)
    pred_map
  else
    let l' = IntSet.add var l in
    let pred_map' = IntMap.add ln (d,u,s,l') pred_map in
    IntSet.fold (fun ln' pm -> propagate_var_liveness predecessors var pm ln') preds pred_map'

let rec process_line_liveness predecessors pred_map ln =
  let (d,u,s,l) = IntMap.find ln pred_map in
  let new_vars = IntSet.diff u l in
  if new_vars = IntSet.empty then
    pred_map
  else
    let pred_map' = IntSet.fold (fun new_var pm -> propagate_var_liveness predecessors new_var pm ln) new_vars pred_map in
    let preds = IntMap.find ln predecessors in
    IntSet.fold (fun pred pm' -> process_line_liveness predecessors pm' pred) preds pred_map'

let predecessors_of_pred_list (pred_list : predicates list) =
  let insert_empty_set s x = IntMap.add x IntSet.empty s in
  (* why does this have to be not -1?? *)
  let range_len_list = (0 -- (List.length pred_list)) in
  let init = List.fold_left insert_empty_set IntMap.empty range_len_list in
  let add_map_for_successors current_map (line,_,_,_,successors) =
    List.fold_left
    (update_mapset line) current_map (IntSet.elements successors)
  in
  List.fold_left add_map_for_successors init pred_list

let debug = false

(* gets all temps used in the pred_list *)
let all_temps pred_list =
  let def_use_list = List.map (fun (_,d,u,_,_) -> (d,u)) pred_list in
  let used d u = IntSet.union d u in
  List.fold_left (fun all_temps (d,u) -> IntSet.union all_temps (used d u)) IntSet.empty def_use_list

(* val propagate_liveness : predicates list -> (define list) * (live list) *)
let propagate_liveness (pred_list : predicates list) =
  let def_list = List.map (fun (_,d,_,_,_) -> d) pred_list in
  (* line -> (def,use,succ,live) *)
  let predecessors = predecessors_of_pred_list pred_list in
  (* Predecessor generation seems fine. *)
  let predicates_to_live_map im (l,d,u,_,s) = IntMap.add l (d,u,s,IntSet.empty) im in
  let pred_map_init = List.fold_left predicates_to_live_map IntMap.empty pred_list in
  let lines = List.rev (0 -- (List.length pred_list - 1)) in
  let liveness_information = List.fold_left (process_line_liveness predecessors) pred_map_init lines in
  (* IntMap.bindings returns in ascending order by key. *)
  let live_list =
    List.map (fun (ln,(d,u,s,l)) -> l) (IntMap.bindings liveness_information) in
  let () = if debug then
    let use_list = List.map (fun (_,_,u,_,_) -> u) pred_list in
    Printf.printf "%s\n\n" (propagated_to_list def_list use_list live_list) else () in
  (def_list, live_list, all_temps pred_list)

exception NoCommand
exception InvalidGenInterfaceArgs

let align_lists (l1,l2) =
  let l1_length = List.length l1 and l2_length = List.length l2 in
  if l1_length <> l2_length || l1_length = 0 || l2_length = 0
    then raise InvalidGenInterfaceArgs else
  (* drop front of l2 and end of l1 then zip *)
  let l1' = take (l1_length-1) l1 and l2' = drop 1 l2 in
  zip (l1', l2')

(* val gen_interference : define list -> live list -> IntSet.t IntMap.t *)
let gen_interference (def_list, live_list, all_temps) =
  let zipped = align_lists (def_list, live_list) in

  (* adds edges t1->t2 and t2->t1 *)
  let update_interference inter_map (t1,t2) =
    let t1_new = IntSet.add t2 (try IntMap.find t1 inter_map with Not_found -> IntSet.empty) in
    let t2_new = IntSet.add t1 (try IntMap.find t2 inter_map with Not_found -> IntSet.empty) in
    let inter_map' = IntMap.add t1 t1_new inter_map in
    IntMap.add t2 t2_new inter_map'
  in

  let mark_interference inter_map (def_set, live_set) =
    IntSet.fold
      (fun def_temp im ->
      List.fold_left
        (fun im' live_temp -> update_interference im' (def_temp, live_temp))
        im (IntSet.elements (IntSet.remove def_temp live_set)))
    def_set
    inter_map
  in

  let initial_mapping =
    IntSet.fold
    (fun i int_map -> IntMap.add i IntSet.empty int_map)
    all_temps IntMap.empty
  in

  List.fold_left mark_interference initial_mapping zipped

let print_set s =
  let mid_s =
    match IntSet.elements s with
      | [] -> "{"
      | [x] -> "{" ^ (string_of_int x)
      | x::xs -> "{" ^ (string_of_int x)
        ^ List.fold_left
          (fun s i -> let new_s = Printf.sprintf ",%d" i in s ^ new_s )
          "" xs
    in
  mid_s ^ "}"

module type PRINT =
  sig
  val pp_livelist : IntSet.t list -> string
  end

module Print : PRINT =
  struct
  let pp_livelist l =
    let combine s liveset = let set_s = print_set liveset in s ^ set_s ^ "\n" in
    "Live list {\n" ^ List.fold_left combine "" l ^ "}\n"
  end
