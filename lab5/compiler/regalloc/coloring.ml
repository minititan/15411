(* Module that gives a coloring of a list of vertices *)

open Regalloc_util

(* Searches for lowest color not taken by neighbors in g not in l 
   If vertex is register then simply returns number since register
   is color itself. *)
let get_color tmp (conflict_map : Regalloc_util.IntSet.t Regalloc_util.IntMap.t) color_map =
  if tmp < 0 then
    tmp (* pre colored *)
  else
    let neighbors = IntMap.find tmp conflict_map in

    let fold_helper tmp' color_set =
      if tmp' < 0 then IntSet.add tmp' color_set else
      try IntSet.add (IntMap.find tmp' color_map) color_set with Not_found -> color_set
    in

    let neighbor_colors = IntSet.fold fold_helper neighbors IntSet.empty in
    let available_colors = List.rev (IntSet.elements neighbor_colors) in
    let rec get_first_available c' = function
      | [] -> c'
      | x::l' -> if x = c' then get_first_available (x-1) l' else c'
    in
    get_first_available (-1) available_colors

(* Returns a mapping from vertex to color assigned in a 
   greedy coloring based on ordering given
 *)
let get_coloring simplicial_order conflict_map =
  let rec color_helper simplicial_order color_map = 
    match simplicial_order with
    | [] -> color_map
    | tmp::l ->
      let c = get_color tmp conflict_map color_map in
      color_helper l (IntMap.add tmp c color_map)
  in
  color_helper simplicial_order IntMap.empty

let simplicial_ordering (conflict_map : IntSet.t IntMap.t) =
  let conflict_map_size = IntMap.cardinal conflict_map in
  let max_cardinality_tbl = Hashtbl.create conflict_map_size in
  let find' x = try (Some (Hashtbl.find max_cardinality_tbl x)) with Not_found -> None in
  let () = IntMap.iter (fun k _ -> Hashtbl.add max_cardinality_tbl k 0) conflict_map in
  let max_pair () =
    let combine (k : int) (v : int) = function
      | None -> Some (k,v)
      | Some (k',v') -> if v > v' then Some (k,v) else Some (k',v')
    in
    Hashtbl.fold combine max_cardinality_tbl None
  in
  (* increment all interfering with k, remove k *)
  let update_ordering k =
    let interferes_with_k = IntMap.find k conflict_map in
    (* the adjacency table is undirected, so we can just fold over these values *)
    let update_val tmp =
      (* as temps are removed, we won't find them when we look them up *)
      (match find' tmp with
      | Some crdnl -> Hashtbl.replace max_cardinality_tbl tmp (crdnl+1)
      | None -> ())
    in
    let () = IntSet.iter update_val interferes_with_k in
    Hashtbl.remove max_cardinality_tbl k
  in
  let rec gen_simplicial l =
    (match (max_pair ()) with
     | None -> l
     | Some (k,_) -> let () = update_ordering k in gen_simplicial (k::l)
    )
  in
  gen_simplicial []

let so_to_string so = String.concat "," (List.map string_of_int so)

let debug = false

(* IntSet.t IntMap.t -> int IntMap.t *)
(* assume an undirected map: v in map[k] -> k in map[v] *)
let gen_coloring (conflict_map : IntSet.t IntMap.t) =
  let simplicial_order = simplicial_ordering conflict_map in
  let _ = if debug then Printf.printf "\nSimplicial ordering: [%s]\n" (so_to_string simplicial_order) else () in
  get_coloring simplicial_order conflict_map

let format k v  = 
  Printf.sprintf "%d -> %d\n" k v

module type PRINT =
  sig
    val pp_colormap : int IntMap.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_colormap m =
      let (k, mincolor) = IntMap.min_binding m in
    let info = Printf.sprintf "Min binding: %d, %d\n" k mincolor in
      "Color mapping {\n" ^
      IntMap.fold (fun k v s -> s ^ (format k v)) m ""
      ^ info ^ "}\n"
  end
