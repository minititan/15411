
module P = Parsetree
module SM = Symbol.Map

let typeholder = 
object
  val mutable typemap = SM.empty

  method add_typedef the_type name = 
    match SM.find' name typemap with 
    | Some _ -> ErrorMsg.error None ("Ident used as typedef"); raise ErrorMsg.Error
    | None -> typemap <- SM.add name the_type typemap

  method is_type name = 
    SM.mem name typemap

  method get_type name = 
    SM.find name typemap

end

let get_type name = 
	typeholder#get_type name 

let get_typedef t name = 
  let () = typeholder#add_typedef t name in 
      P.Typedef(t, name)


let is_typedef name = 
  typeholder#is_type name