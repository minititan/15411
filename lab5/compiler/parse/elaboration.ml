(* elaboration.ml
 * Ned Williamson
 * Parse Tree -> AST
 *)

module P = Parsetree
module A = Ast

let rec trans_primtype = function
  | P.BOOL -> A.Bool
  | P.INT -> A.Int
  | P.PrId i -> A.PrId i
  | P.VOID -> A.Void
  | P.POINTER t -> A.Pointer (trans_primtype t)
  | P.ARRPOINTER t -> A.Array (trans_primtype t)
  | P.STRUCT id -> A.Struct id 

let trans_oper = function
  | P.PLUS -> A.PLUS
  | P.MINUS -> A.MINUS
  | P.TIMES -> A.TIMES
  | P.DIVIDEDBY -> A.DIVIDEDBY
  | P.MODULO -> A.MODULO
  | P.NEGATIVE -> A.NEGATIVE
  | P.BITAND -> A.BITAND
  | P.BITOR -> A.BITOR
  | P.BITNOT -> A.BITNOT
  | P.SAR -> A.SAR
  | P.SAL -> A.SAL
  | P.XOR -> A.XOR
  | P.LOGICAND -> A.LOGICAND
  | P.LOGICOR -> A.LOGICOR
  | P.LOGICNOT -> A.LOGICNOT
  | P.EQUALS -> A.EQUALS
  | P.NOTEQUALS -> A.NOTEQUALS
  | P.GREATER -> A.GREATER
  | P.GREATEREQUAL -> A.GREATEREQUAL
  | P.LESS -> A.LESS
  | P.LESSEQUAL -> A.LESSEQUAL

let unwrap_decl = function
  | P.SimpOpt None -> None
  | P.SimpOpt (Some s) ->
    (match s with
      | P.SimpDecl d -> Some d
      | P.SimpAsop _ | P.SimpPostop _ | P.SimpExp _ -> None)

exception DeclInStep

let rec trans_control = function
  | P.If (e, s, elo) ->
    (*if e = P.True then trans_stm s else
    if e = P.False then trans_elseopt elo else *)
    A.If (trans_exp e, trans_stm s, trans_elseopt elo)
  | P.While (e, s) -> A.While (trans_exp e, trans_stm s)
  | P.For (init, e, step, body) ->
    if (match unwrap_decl step with Some _ -> true | None -> false)
      then raise DeclInStep else
    (match unwrap_decl init with
      | None -> A.Seq(trans_simpopt init,
          A.While(trans_exp e, A.Seq(trans_stm body,
            trans_simpopt step)))
      | Some d -> (match d with
        | P.Decl (t, i) -> A.Declare(trans_primtype t, i,
            A.While(trans_exp e, A.Seq(trans_stm body, trans_simpopt step)))

        | P.DeclAsn (t, i, e2) ->
            let (isin, te2) = special_trans_exp i e2 in
            A.Declare( trans_primtype t, i, A.Seq(A.Assign (A.Var i, isin, te2),
              A.While(trans_exp e, A.Seq(trans_stm body,
                trans_simpopt step))))
        )
      )
  | P.Return e -> A.Return (trans_exp e)
  | P.ReturnVoid -> A.ReturnVoid
  | P.Assert e -> A.Assert (trans_exp e)
  
and trans_elseopt = function
  | P.ElseOpt None -> A.Nop
  | P.ElseOpt (Some s) -> trans_stm s

and trans_memexp = function
  | P.Deref(l) -> A.Deref(trans_exp l)
  | P.Var(i) -> A.Var(i)
  | P.Dot(l, i) -> A.FieldDeref(trans_exp l, i)
  | P.Arrow(l, i) -> A.FieldDeref(A.Deref(trans_exp l), i)
  | P.ArrayIndex(l, e) -> A.ArrIndex(trans_exp l, trans_exp e)
  | _ -> ErrorMsg.error None ("This type exp cannot be assigned to"); raise ErrorMsg.Error

and trans_simp = function
  | P.SimpAsop (me, o, e) ->
      let ltran = trans_memexp me in
      let e' = trans_exp e in
    
    (match o with
      | P.ASSIGN -> A.Assign (ltran, false,  e')
      | P.PLUSEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.PLUS, ltran, e')))
      | P.MINUSEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.MINUS, ltran, e')))
      | P.STAREQ -> A.AssignOp (ltran, false, (A.BinopExp (A.TIMES, ltran, e')))
      | P.SLASHEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.DIVIDEDBY, ltran, e')))
      | P.PERCENTEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.MODULO, ltran, e')))
      | P.BITANDEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.BITAND, ltran, e')))
      | P.BITOREQ -> A.AssignOp (ltran, false, (A.BinopExp (A.BITOR, ltran, e')))
      | P.XOREQ -> A.AssignOp (ltran, false, (A.BinopExp (A.XOR, ltran, e')))
      | P.SAREQ -> A.AssignOp (ltran, false, (A.BinopExp (A.SAR, ltran, e')))
      | P.SALEQ -> A.AssignOp (ltran, false, (A.BinopExp (A.SAL, ltran, e')))
    )

  | P.SimpPostop (l, p) ->
    let ltran = trans_memexp l  in
    (match p with
      | P.PLUSPLUS -> A.AssignOp (ltran, false, (A.BinopExp (A.PLUS, ltran,
        A.ConstExp Int32.one)))
      | P.MINUSMINUS -> A.AssignOp (ltran, false, (A.BinopExp (A.MINUS, ltran,
        A.ConstExp Int32.one)))
    )

  | P.SimpDecl d -> trans_decl d

  (* never deal with resolving ambiguity here since its taken care of in parser *)
  | P.SimpExp e -> 
      let transe = trans_exp e in
      A.Expr(transe)

(* we have to do this later now due to operands having side effects *)
(* and construct_shift_tree op te1 te2 = A.BinopExp(trans_oper op, te1, te2) *)
  (*
  A.Condition(
    (* check bounds *)
    A.BinopExp(A.LOGICAND,
      A.BinopExp(A.LESS, te2, A.ConstExp(Int32.of_int 32)),
      A.BinopExp(A.GREATEREQUAL, te2, A.ConstExp(Int32.of_int 0))),
    (* good case *)
    A.BinopExp(trans_oper op, te1, te2),
    (* raise exception *)
    A.BinopExp(A.DIVIDEDBY, A.ConstExp(Int32.of_int 42),A.ConstExp(Int32.of_int 0)))
  *)

and trans_alf = function
  | P.ALF None -> []
  | P.ALF (Some (e, alf)) -> (trans_exp e)::trans_alf(alf)

and special_trans_alf id = function
  | P.ALF None -> (false, [])
  | P.ALF (Some (e, alf)) -> let (isin1, te) = special_trans_exp id e in
                              let (isin2, talf) = special_trans_alf id alf in
                              (isin1 || isin2, te::talf)

and trans_al = function
  | P.AL None -> []
  | P.AL (Some (e, alf)) -> (trans_exp e)::trans_alf(alf)

and special_trans_al id = function
  | P.AL None -> (false, [])
  | P.AL (Some (e, alf)) -> 
      let (isin1, te) = special_trans_exp id e in
      let (isin2, talf) = special_trans_alf id alf in
      (isin1 || isin2, te::talf)

and trans_exp = function
  | P.ConstExp i -> A.ConstExp i
  | P.True -> A.True
  | P.False -> A.False
  | P.Var i -> A.Var i
  | P.UnopExp (o, e) -> A.UnopExp (trans_oper o, trans_exp e)
  | P.BinopExp (o, e1, e2) ->
    let o' = trans_oper o in
    let fold fn = begin match (trans_exp e1, trans_exp e2) with
      | (A.ConstExp i1, A.ConstExp i2) -> A.ConstExp (fn i1 i2)
      | (e1',e2') -> A.BinopExp (o', e1', e2')
    end
    in
    let fold_div fn = begin match (trans_exp e1, trans_exp e2) with
      | (A.ConstExp i1, A.ConstExp i2) when i2 <> Int32.zero -> A.ConstExp (fn i1 i2)
      | (e1',e2') -> A.BinopExp (o', e1', e2')
    end
    in
    let fold_shift fn = begin match (trans_exp e1, trans_exp e2) with
      | (A.ConstExp i1, A.ConstExp i2) when (i2 >= Int32.zero && i2 < (Int32.of_int 32)) -> A.ConstExp (fn i1 i2)
      | (e1',e2') -> A.BinopExp (o', e1', e2')
    end
    in
    let fold_bool true_cmp = begin match (trans_exp e1, trans_exp e2) with
      | (A.ConstExp i1, A.ConstExp i2) -> if true_cmp i1 i2 then A.True else A.False
      | (e1',e2') -> A.BinopExp (o', e1', e2')
    end
    in
    begin match o' with
      | A.LOGICAND -> A.Condition(trans_exp e1, trans_exp e2, A.False)
      (* fold on true/false here on in bool_jmp? *)
      | A.LOGICOR -> A.Condition(trans_exp e1, A.True, trans_exp e2)
      | A.PLUS -> fold Int32.add
      | A.MINUS -> fold Int32.sub
      | A.TIMES -> fold Int32.mul
      | A.DIVIDEDBY -> fold_div Int32.div
      | A.MODULO -> fold_div Int32.rem
      | A.BITAND -> fold Int32.logand
      | A.BITOR -> fold Int32.logor
      | A.SAR -> fold_shift (fun i1 i2 -> Int32.shift_right i1 (Int32.to_int i2))
      | A.SAL -> fold_shift (fun i1 i2 -> Int32.shift_left i1 (Int32.to_int i2))
      | A.XOR -> fold Int32.logxor
      | A.EQUALS -> fold_bool (fun i1 i2 -> i1 = i2)
      | A.NOTEQUALS -> fold_bool (fun i1 i2 -> i1 <> i2)
      | A.GREATER -> fold_bool (fun i1 i2 -> i1 > i2)
      | A.GREATEREQUAL -> fold_bool (fun i1 i2 -> i1 >= i2)
      | A.LESS -> fold_bool (fun i1 i2 -> i1 < i2)
      | A.LESSEQUAL -> fold_bool (fun i1 i2 -> i1 <= i2)
      (* unary negate and bitnot aren't foldable -
        we can lose bool status, so this is done later,
        I think in trans. *)
      | o' -> A.BinopExp (o', trans_exp e1, trans_exp e2)
    end


  | P.Condition (e1, e2, e3) ->
      let e1' = trans_exp e1
      and e2' = trans_exp e2
      and e3' = trans_exp e3 in
      A.Condition (e1', e2', e3')

  | P.Call (i, al) -> A.Call (i, trans_al al)

  | P.Null -> A.Null
  | P.Dot(exp, id) -> A.FieldDeref(trans_exp exp, id)
  | P.Arrow(exp, id) -> A.FieldDeref(A.Deref(trans_exp exp), id)
  | P.Alloc(ptype) -> A.AllocCall(trans_primtype ptype)
  | P.AllocArr(ptype, exp) -> A.AllocArrCall(trans_primtype ptype, trans_exp exp)
  | P.ArrayIndex(e1, e2) -> A.ArrIndex(trans_exp e1, trans_exp e2)
  | P.Deref(e) -> A.Deref(trans_exp e)

and trans_decl = function
  | P.Decl (t, i) -> A.Declare (trans_primtype t, i, A.Nop)

  | P.DeclAsn (t, i, e) -> 
      let (isin, trans_e) = special_trans_exp i e in
      A.Declare (trans_primtype t, i, A.Assign(A.Var i, isin, trans_e))

(* DIRTIEST HACK EVER TO TAKE CARE OF ASSINGMENT OF SAME NAME FUNCTION 
    Returns a tuple of boolean, translation where boolean indicates
    if the id is called *)
and special_trans_exp id = function
  | P.ConstExp i -> (false, A.ConstExp i)

  | P.True -> (false, A.True)

  | P.False -> (false, A.False)

  | P.Var i -> (false, A.Var i)

  | P.UnopExp (o, e) -> 
      let (isin, te) = special_trans_exp id e in
      (isin, A.UnopExp (trans_oper o, te))

  | P.BinopExp (o, e1, e2) ->
      let (isin1, te1), (isin2, te2) = special_trans_exp id e1, special_trans_exp id e2 in
      (match trans_oper o with
        | A.LOGICAND -> (isin1 || isin2, A.Condition(te1, te2, A.False))
        | A.LOGICOR -> (isin1 || isin2, A.Condition(te1, A.True, te2))
        | o' -> (isin1 || isin2, A.BinopExp (o', te1, te2))
      )

  | P.Condition (e1, e2, e3) ->
      let (isin1, e1') = special_trans_exp id e1
      and (isin2, e2') = special_trans_exp id e2
      and (isin3, e3') = special_trans_exp id e3 in
      (isin1 || isin2 || isin3, A.Condition (e1', e2', e3'))

  | P.Call (i, al) -> 
      let (isin, al_t) = special_trans_al id al in
        (i = id || isin, A.Call (i, al_t) )


  | P.Null -> (false, A.Null)

  | P.Dot(exp, id) ->
        let (isin, te) = special_trans_exp id exp in
         (isin, A.FieldDeref(te, id))

  | P.Arrow(exp, id) -> 
        let (isin, te) = special_trans_exp id exp in
          (isin, A.FieldDeref(A.Deref(te), id))


  | P.Alloc(ptype) -> (false, A.AllocCall(trans_primtype ptype))

  | P.AllocArr(ptype, exp) -> 
        let (isin, te) =  special_trans_exp id exp  in
          (isin, A.AllocArrCall(trans_primtype ptype, te))

  | P.ArrayIndex(e1, e2) -> 
          let (isin1, te1), (isin2, te2) = special_trans_exp id e1, special_trans_exp id e2 in
          (isin1 || isin2, A.ArrIndex(te1, te2))

  | P.Deref(e) -> 
        let (isin, te) = special_trans_exp id e in
        (isin, A.Deref(te))


and trans_stm : P.stmt -> A.stm = function
  | P.StmSimp s -> trans_simp s
  | P.StmControl c -> trans_control c
  | P.StmBlock b -> trans_block b

and trans_simpopt = function
  | P.SimpOpt None -> A.Nop
  | P.SimpOpt (Some s) -> trans_simp s

and trans_block = function
  | P.Block b -> trans_stms b

and trans_stms = function
  | P.Seq None -> A.Nop
  | P.Seq (Some (s, ss)) ->
    (match s with
      | P.StmSimp smp ->
        (match smp with
          | P.SimpDecl dcl -> 

            (match dcl with
            | P.Decl (t, i) -> A.Declare (trans_primtype t, i, trans_stms ss)

            | P.DeclAsn (t, i, e) -> 
                let (isin, t_e) = special_trans_exp i e in
                A.Declare (trans_primtype t, i,
                                    A.Seq(A.Assign(A.Var i, isin, t_e), trans_stms ss))
            )

          | P.SimpAsop _ | P.SimpPostop _ | P.SimpExp _ -> 
                A.Seq(trans_stm s, trans_stms ss)
              
        )

      | P.StmControl _ | P.StmBlock _ -> A.Seq(trans_stm s, trans_stms ss)
              
    )


let trans_param = function
  | P.Param (t, i) -> (trans_primtype t, i)

let rec trans_plf = function
  | P.PLF None -> []
  | P.PLF (Some (p, plf)) -> (trans_param p)::(trans_plf plf)

let trans_pl : P.param_list -> A.param list = function
  | P.PL None -> []
  | P.PL (Some (p, plf)) -> (trans_param p)::(trans_plf plf)

let trans_field = function
  | P.Field(ptype, id) -> (trans_primtype ptype, id)

let rec trans_fl = function
  | P.FL(None) -> []
  | P.FL(Some(f, fl)) -> trans_field f :: trans_fl fl

let trans_gdecl = function
  | P.Fdecl (t, i, pl) ->
      let t' = trans_primtype t in
      let pl' = trans_pl pl in
      A.Fdecl (t', i, pl')

  | P.Fdefn (t, i, pl, b) ->
      let t' = trans_primtype t in
      let pl' = trans_pl pl in
      let s = trans_block b in
      A.Fdefn (t', i, pl', s)

  | P.Typedef (t, i) ->
      A.Typedef (trans_primtype t, i)

  | P.Sdecl(sname) -> A.Structdecl(sname)

  | P.Sdefn(sname, flist) -> 
      A.Structdefn(sname, trans_fl flist)

let rec trans_program' = function
  | P.Program p ->
    match p with
    | None -> []
    | Some (g, p') -> (trans_gdecl g)::(trans_program' p')

let trans_program ~is_main_file = function
  | P.Program p ->
    match p with
    | None -> [] (* is main declared implicitly for an empty file? *)
    | Some (g, p') ->
      if is_main_file then
        (A.Fdecl (A.Int, Symbol.symbol "main", []))::(trans_gdecl g)::(trans_program' p')
      else
        (trans_gdecl g)::(trans_program' p')



