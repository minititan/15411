(* Goes from the transated ast tree to an intermediate representation
  consisting of straight line instructions. This is then converted to a block
  format later for optimizations.
 *)

module T = Tree
module S = Storage
module I = Ir

let raise_error msg = (ErrorMsg.error None msg; raise ErrorMsg.Error)

let munch_texp = function
  | T.WORD e' -> (I.WORD, e')
  | T.DWORD e' -> (I.DWORD, e')
  | T.QWORD e' -> (I.QWORD, e')

let process_deref e_size = function
  | T.Deref (offset, texp') -> (match munch_texp texp' with
    | (temp_size, T.Temp t) -> (e_size, I.DEREF(offset, (temp_size, I.TEMP(t))))
    | _ -> raise_error "Dereferencing a non-temp."
    )
  | _ -> raise_error "Non-deref in process_deref."

let get_operand texp =
  let (e_size, e_exp) = munch_texp texp in (match e_exp with
  | T.Const i -> (e_size, I.IMM i)
  | T.Temp t -> (e_size, I.TEMP t)
  | T.Deref (_, _) -> process_deref e_size e_exp
  | T.Binop _ | T.Unop _ | T.Call _ -> raise_error "Invalid expression in munch_operand."
  )

(* used when doing mapi over arguments for call *)
let process_arg i e =
  let (size_e,exp_e) = get_operand e in
  [I.MOV((size_e, I.ARGBOTTOM i), (size_e, exp_e))]

let process_tail_arg arg temp = 
  let arg_op = get_operand arg in
  let temp_op = get_operand temp in
  I.MOV(temp_op, arg_op)

let munch_mov src dst =
  let (src_size, src_exp) = munch_texp src in
  match src_exp with
  | T.Const i -> [I.MOV(get_operand dst, (src_size, I.IMM i))]

  | T.Temp t -> [I.MOV(get_operand dst, (src_size, I.TEMP t))]

  (* don't use source size in binop *)
  | T.Binop (op, e1, e2) -> [I.BINOP(op, get_operand dst, get_operand e1, get_operand e2)]

  | T.Unop (op, e1) -> [I.UNOP(op, get_operand dst, get_operand e1)]

  | T.Call (id, elist) ->
    let setup_args = List.flatten (List.mapi process_arg elist) in
    setup_args @ [I.CALL(id); I.MOV(get_operand dst, (src_size, I.REG(S.EAX)))]

  | T.Deref _ -> [I.MOV(get_operand dst, process_deref src_size src_exp)]

let munch_command temp_arg_list first_label = function
  | T.Mov (dst, src) -> munch_mov src dst

  | T.If(T.Cmp(cmpop, e1, e2), l1, l2) ->
    [I.IF(cmpop, get_operand e1, get_operand e2, l1, l2)]

  | T.Goto(l) -> [I.GOTO(l)]

  | T.CheckDeref(t) -> [I.CHECKDEREF(get_operand t)]

  | T.Label(l) -> [I.LABEL(l)]

  | T.ReturnVoid -> [I.RET]

  | T.VoidCall(id, elist) ->
    let irlst = List.flatten (List.mapi process_arg elist) in
    irlst @ [I.CALL(id)]

  | T.TailCall(_, elist) ->
    let irlst = List.map2 process_tail_arg elist temp_arg_list in
    irlst @ [I.GOTO(first_label)]

  | T.Return ret_val ->
    let (ret_size, ret_op) = get_operand ret_val in
    [I.MOV((ret_size, I.REG S.EAX), (ret_size, ret_op)); I.RET]

let create_arg_moves temp_arg_list =
  (* should we be using a different sized comparison? *)
  let process_toparg i texp =
    let (typ_size, tmp) = (match texp with
      | T.WORD  (T.Temp t) -> (I.DWORD, t)
      | T.DWORD (T.Temp t) -> (I.DWORD,  t)
      | T.QWORD (T.Temp t) -> (I.QWORD, t)
      | _ -> raise_error "Non-temp found in toparg."
    ) in
    I.MOV((typ_size, I.TEMP tmp), (typ_size, I.ARGTOP i))
  in
  List.mapi process_toparg temp_arg_list

let munch_func (_, id, temp_arg_list, cmdlist) = 
  let arg_moves = create_arg_moves temp_arg_list in

  match cmdlist with
  | cmd::restcmds -> 

    (* first cmd is the label *)
    (match cmd with
    | T.Label(first_label) -> 
      let arg_moves_block = [I.LABEL(Label.create())] @ arg_moves @ [I.GOTO(first_label)] in

      (id, arg_moves_block @ 
                      (List.flatten (List.map (munch_command temp_arg_list first_label) (cmd::restcmds) )) )

    | _ -> raise_error "first command is not a label"
    )

  | [] -> (id, [])

let codegen flist =
  List.map munch_func flist
