module S = Storage
module T = Tree

type binop = T.binop
type cmpop = T.cmpop
type unop = T.unop

type size = Ir.size

type simpoper =
  | IMM of Int32.t
  | REG of Storage.reg
  | MTEMP of Multemp.multemp
  | DEREF of Int32.t * (size * simpoper)
  | ARGTOP of int
  | ARGBOTTOM of int
and operand =
  size * simpoper

type instr =
  | BINOP of binop * operand * operand * operand
  | UNOP of unop * operand * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label * Label.label (* if true goto Label.label *)
  | CALL of Symbol.symbol
  | TAILCALL of Symbol.symbol
  | GOTO of Label.label
  | CHECKDEREF of operand
  | LABEL of Label.label
  | RET
  | NOP


type ssablock = 
  Label.label * Label.Set.t * instr list * Multemp.Set.t * Multemp.Set.t


(* function name, irlist, map for jumps
  map goes from block label to tuple of two sets. first set is incoming block labels and second
  is outgoing block labels *)
type ssablockfunc = Symbol.symbol * ssablock list * (Label.Set.t * Label.Set.t) Label.Map.t

type ssaprogram = ssablockfunc list


module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_cmpop : cmpop -> string
    val pp_instr : instr -> string
    val pp_ssaprogram : ssaprogram -> string
  end

module Print : PRINT =
  struct

    let rec pp_actual_operand op = 
      match op with
      | IMM i -> Int32.to_string i
      | REG r -> S.format_reg r
      | MTEMP t -> Multemp.name t
      | DEREF(i, (s, t)) -> Int32.to_string i ^ "(" ^ Ir.Print.pp_size s ^ pp_actual_operand t ^ ")"
      | ARGTOP i -> "argtop" ^ string_of_int i
      | ARGBOTTOM i -> "argbottom" ^ string_of_int i
    
    let pp_operand (_, op) = pp_actual_operand op (* ignore size *)

    let pp_binop op = 
      match op with
      | T.ADD -> "ADD"
      | T.SUB -> "SUB"
      | T.MUL -> "MUL"
      | T.DIV -> "DIV"
      | T.MOD -> "MOD"
      | T.BAND -> "AND"
      | T.BOR -> "OR"
      | T.SAR -> "SAR"
      | T.SAL -> "SAL"
      | T.XOR -> "XOR"

    let pp_unop op = 
      match op with
      | T.NEG -> "NEG"
      | T.BNOT -> "NOT"

    let pp_cmpop op = 
    match op with
      | T.EQ -> "EQ"
      | T.NOTEQ -> "NEQ"
      | T.GREATER -> "GREAT"
      | T.GREATEREQ -> "GREATEQ"
      | T.LESS -> "LESS"
      | T.LESSEQ -> "LESSEQ"

    let pp_label = Label.name

    let pp_instr = function
    | BINOP (op, op1, op2, op3) -> pp_binop op ^ " " ^ pp_operand op1 ^ 
                                    " <- " ^ pp_operand op2 ^ "," ^ pp_operand op3 ^ "\n"

    | UNOP (op, d, op1) -> pp_unop op ^ " " ^ pp_operand d ^ " <- " ^ pp_operand op1 ^ "\n"
    | IF (op, op1, op2, l1, l2) -> "CMP " ^ pp_cmpop op ^ " (" ^ pp_operand op1 ^ ", " ^ pp_operand op2 ^ "): " ^ 
                              "GOTO " ^ (Label.name l1)  ^ " ELSE GOTO " ^ (Label.name l2) ^ "\n"
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ " <- " ^ pp_operand s ^ "\n"
    | CHECKDEREF e -> "CHECKDEREF " ^ pp_operand e ^ "\n"
    | DIRECTIVE s -> "DIRECTIVE: " ^ s ^ "\n"
    | COMMENT s -> "COMMENT: " ^ s ^ "\n"
    | RET -> "RET" ^ "\n"
    | GOTO l -> "GOTO " ^ pp_label l ^ "\n"
    | LABEL l -> "LABEL " ^ pp_label l ^ "\n"
    | CALL(id) -> "CALL " ^ Symbol.name id ^ "\n"
    | TAILCALL(id) -> "TAILCALL " ^ Symbol.name id ^ "\n"
    | NOP -> "NOP\n"
    
    let pp_block (_, lset, clist, inc, outg) = (* don't show label *)
      let instr = Multemp.Set.fold (fun e s -> s ^ "," ^ Multemp.name e) inc "IN: " in
      let outstr = Multemp.Set.fold (fun e s -> s ^ "," ^ Multemp.name e) outg "OUT: " in
      let bstr = List.fold_left (fun s c -> s ^ pp_instr c) "" clist in
      let jmpstr = (Label.Set.fold (fun lb s -> s ^ Label.name lb ^ ",") lset "JumpsTo(") ^ ")\n" in
      instr ^ "\n=====\n" ^ bstr ^ "=====\n" ^ outstr ^ "\n" ^ jmpstr ^ "\n"

    let pp_blocks bs = 
      List.fold_left (fun s b -> s ^ pp_block b ^ "\n") "" bs

    (* ignore label map *)
    let pp_func (id, b, _) = Symbol.name id ^ " {\n" ^ pp_blocks b ^ "}\n\n"

    let pp_ssaprogram p =
      p
      |> (List.map pp_func)
      |> (String.concat "")
  end
