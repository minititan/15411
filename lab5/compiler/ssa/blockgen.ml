

module I = Ir
module B = Block
module LS = Label.Set
module LM = Label.Map
module SS = Temp.Set
module SM = Symbol.Map


let rec scan_till_label = function
  | [] -> []
  | (I.LABEL l)::rest -> (I.LABEL l)::rest
  | _::insts -> scan_till_label insts

(* scans till the first goto or if and returns set of jumps from there 
  and the command list till there. Also returns a set containing the temps used in the block *)
let rec get_block sofar l first_label = 
  match l with
  | [] -> (LS.empty, sofar, [])

  | instr::[] ->
    begin match instr with
    | I.GOTO(lab) -> (LS.singleton lab, sofar @ [instr], [])
    | I.IF(_, _, _, lab1, lab2) ->
      (LS.add lab1 (LS.singleton lab2), sofar @ [instr], [])

    | _ -> (LS.empty, sofar @ [instr], [])
    end

  | instr::l' -> 
    begin match instr with
    | I.GOTO lab -> (LS.singleton lab, sofar @ [instr], l')
    | I.IF(_, _, _, lab1, lab2) -> 
      (LS.add lab1 (LS.singleton lab2), sofar @ [instr], l')

    (* for a ret we scan ahead till we see a label and drop all of the stuff
      till there *)
    | I.RET -> 
      let new_l = scan_till_label l' in
      (LS.empty, sofar @ [instr], new_l)

    | I.TAILCALL _ -> 
      let new_l = scan_till_label l' in
      (LS.singleton (first_label), sofar @ [instr], new_l)

    | _ -> get_block (sofar @ [instr]) l' first_label
    end


let rec get_blocks instrlist first_label = 
  match instrlist with
  | instr::l' ->
    begin match instr with
    | I.LABEL(l) ->
      let (exit_labels, block_instrs, rem) = get_block [] l' first_label in
      (l, exit_labels, instr::block_instrs)::(get_blocks rem first_label)

    | _ -> ErrorMsg.error None ("Block has to begin with Label!: "); raise ErrorMsg.Error
    end

  | [] -> []

let addJump b1 b2 m =
  if (b1 = b2) then
    let (inc, out) = LM.find b1 m in
    LM.add b1 (LS.add b1 inc, LS.add b1 out) m
  else
    let (b1_in, b1_out) = LM.find b1 m in
    let (b2_in, b2_out) = LM.find b2 m in
    let n1 = LM.add b1 (b1_in, LS.add b2 b1_out) m in
    LM.add b2 (LS.add b1 b2_in, b2_out) n1


let addJumps block jump_set jumpmap =
  let new_jmap = LS.fold (fun b m -> addJump block b m) jump_set jumpmap in
  new_jmap

let initJumpMap bp = 
  List.fold_left (fun m (l,_, _) -> LM.add l (LS.empty, LS.empty) m) LM.empty bp


let get_jumps_and_blocks bl = 
  let init_jmap = initJumpMap bl in
  let mid_jmap = List.fold_left (fun jmap (l, jset, _) -> addJumps l jset jmap) init_jmap bl in
  (bl, mid_jmap)

exception NoLabel

let rec get_first_label = function
  | (I.LABEL l) :: _ -> l
  | _ :: l' -> get_first_label l'
  | [] -> raise NoLabel

let block_convert l =
  let first_label_map = List.fold_left (fun m (id,il) -> SM.add id (get_first_label il) m) SM.empty l in
  let block_funcs = List.map (fun (id, instrlist) -> 
                  let bl = get_blocks instrlist (SM.find id first_label_map) in
                  let (new_bl, jmap) = get_jumps_and_blocks bl in
                  (id, new_bl, jmap)) l 
  in
  
  block_funcs
