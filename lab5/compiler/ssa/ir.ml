(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(* Ned: from assem.ml
 * We want 3-addr for our IR and 2-addr for assem,
 * so we use this to define a different instruction
 * type.
 *
 * Therefore we have these instruction types, each
 * progressively lower lever: IR, LLIR, AS.
 *)

module S = Storage
module T = Tree

type binop = T.binop
type cmpop = T.cmpop
type unop = T.unop

type size = 
  | WORD
  | DWORD
  | QWORD

type simpoper =
  | IMM of Int32.t
  | REG of Storage.reg
  | TEMP of Temp.temp
  | DEREF of Int32.t * (size * simpoper)
  | ARGTOP of int
  | ARGBOTTOM of int
and operand =
  size * simpoper

type instr =
  | BINOP of binop * operand * operand * operand
  | UNOP of unop * operand * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label * Label.label (* if true goto Label.label *)
  | CALL of Symbol.symbol
  | TAILCALL of Symbol.symbol
  | GOTO of Label.label
  | CHECKDEREF of operand
  | LABEL of Label.label
  | NOP
  | RET

type irfunction = Symbol.symbol * instr list

type irprogram = irfunction list

module type PRINT =
  sig
    val pp_size : size -> string
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_cmpop : cmpop -> string
    val pp_instr : instr -> string
    val pp_program : irprogram -> string
  end

exception Unexpected

module Print : PRINT =
  struct

    let pp_size = function
    | WORD -> "W."
    | DWORD -> "D."
    | QWORD -> "Q."

    let pp_operand (sz, op) =
    pp_size sz ^ (match op with
    | IMM i -> Int32.to_string i
    | REG r -> S.format_reg r
    | TEMP t -> Temp.name t
    | DEREF(i, (sz, t)) -> let str = (match t with | TEMP(t') -> Temp.name t' | _ -> raise Unexpected)
                            in Int32.to_string i ^ "(" ^ pp_size sz ^ str ^ ")"
    | ARGTOP i -> "argtop" ^ string_of_int i
    | ARGBOTTOM i -> "argbottom" ^ string_of_int i
    )

    let pp_binop op = 
      match op with
      | T.ADD -> "ADD"
      | T.SUB -> "SUB"
      | T.MUL -> "MUL"
      | T.DIV -> "DIV"
      | T.MOD -> "MOD"
      | T.BAND -> "AND"
      | T.BOR -> "OR"
      | T.SAR -> "SAR"
      | T.SAL -> "SAL"
      | T.XOR -> "XOR"

    let pp_unop op = 
      match op with
      | T.NEG -> "NEG"
      | T.BNOT -> "NOT"

    let pp_cmpop op = 
    match op with
      | T.EQ -> "EQ"
      | T.NOTEQ -> "NEQ"
      | T.GREATER -> "GREAT"
      | T.GREATEREQ -> "GREATEQ"
      | T.LESS -> "LESS"
      | T.LESSEQ -> "LESSEQ"

    let pp_label = Label.name

    let pp_instr = function
    | BINOP (op, op1, op2, op3) -> pp_binop op ^ " " ^ pp_operand op1 ^ 
                                    " <- " ^ pp_operand op2 ^ "," ^ pp_operand op3 ^ "\n"

    | UNOP (op, d, op1) -> pp_unop op ^ " " ^ pp_operand d ^ " <- " ^ pp_operand op1 ^ "\n"
    | IF (op, op1, op2, l1, l2) -> "CMP " ^ pp_cmpop op ^ " (" ^ pp_operand op1 ^ ", " ^ pp_operand op2 ^ "): " ^ 
                              "GOTO " ^ (Label.name l1)  ^ " ELSE GOTO " ^ (Label.name l2) ^ "\n"
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ " <- " ^ pp_operand s ^ "\n"
    | CHECKDEREF e -> "CHECKDEREF " ^ pp_operand e ^ "\n"
    | DIRECTIVE s -> "DIRECTIVE: " ^ s ^ "\n"
    | COMMENT s -> "COMMENT: " ^ s ^ "\n"
    | RET -> "RET" ^ "\n"
    | GOTO l -> "GOTO " ^ pp_label l ^ "\n"
    | LABEL l -> "LABEL " ^ pp_label l ^ "\n"
    | CALL(id) -> "CALL " ^ Symbol.name id ^ "\n"
    | TAILCALL(id) -> "TAILCALL " ^ Symbol.name id ^ "\n"
    | NOP -> "NOP\n"
    
    let rec pp_function = function
    | [] -> ""
    | instr::instrs' -> pp_instr instr ^ (pp_function instrs')

    let pp_program p = 
      let fstr = List.map (fun (id, ilist) -> Symbol.name id ^ " {" ^ pp_function ilist ^ "}\n" ) p in
      List.fold_left (fun s fs -> s ^ fs) "" fstr
  end
