(* Module that takes in a sequence of 2 addr low-level IR instructions
	and the mapping from temps to colors and returns the final assembly
 *)

val assembly_gen : Llir.program -> int Regalloc_util.IntMap.t Symbol.Map.t -> Assem.program
