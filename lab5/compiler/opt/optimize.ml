module AS = Assem

module S = Storage

(* drop self moves *)
let rec optimize = function
  | (AS.MOV (AS.REG (_, r1), AS.REG (_, r2))) :: insts when r1 = r2 -> optimize insts
  | inst::insts -> inst::(optimize insts)
  | [] -> []
