	.file	"../tests0/return10.l3"
	.text
	.global	_c0_fac
_c0_fac:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	$1, %edi
L4:
	CMPL	$1,%eax
	JG L2
	JMP L3
L2:
	IMULL	%eax, %edi
	DECL	%eax
	JMP L4
L3:
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$5, %edi
	AND	$0,%al
	CALL	_c0_fac
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
