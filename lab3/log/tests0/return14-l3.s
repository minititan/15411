	.file	"../tests0/return14.l3"
	.text
	.global	_c0_log2
_c0_log2:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %edi
	MOVL	$0, %esi
L4:
	CMPL	$1,%edi
	JG L2
	JMP L3
L2:
	INCL	%esi
	MOVL	$2, %ecx
	MOVL	%edi, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %edi
	JMP L4
L3:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$1024, %edi
	AND	$0,%al
	CALL	_c0_log2
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
