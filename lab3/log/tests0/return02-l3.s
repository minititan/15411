	.file	"../tests0/return02.l3"
	.text
	.global	_c0_gcd
_c0_gcd:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %edi
	MOVL	%esi, %esi
L4:
	CMPL	$0,%esi
	JNE L2
	JMP L3
L2:
	MOVL	%esi, %ecx
	MOVL	%edi, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %ecx
	MOVL	%esi, %edi
	MOVL	%ecx, %esi
	JMP L4
L3:
	MOVL	%edi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$24, %edi
	MOVL	$56, %esi
	AND	$0,%al
	CALL	_c0_gcd
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
