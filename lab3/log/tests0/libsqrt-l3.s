	.file	"../tests0/libsqrt.l3"
	.text
	.global	_c0_fabs
_c0_fabs:
	PUSHQ	%rbx
L1:
	MOVL	%edi, %ebx
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L2
	JMP L3
L2:
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fsub
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	RET
	JMP L4
L3:
	MOVL	%ebx, %eax
	POPQ	%rbx
	RET
	JMP L4
L4:
	.global	_c0_fsqrt
_c0_fsqrt:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$16, %rsp
L5:
	MOVL	%edi, %ebx
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L11
	JMP L12
L11:
	MOVL	$1, %eax
	NEGL	%eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	JMP L13
L12:
	JMP L13
L13:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %ebp
	MOVL	$100000, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebp, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %ebp
	MOVL	$100000, %r12d
	MOVL	$0, %r13d
	MOVL	%ebx, 0(%rsp)
L8:
	MOVL	0(%rsp), %edi
	MOVL	0(%rsp), %esi
	AND	$0,%al
	CALL	fmul
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fsub
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fabs
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%ebp, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L10
	JMP L9
L9:
	CMPL	%r12d,%r13d
	JL L6
	JMP L7
L10:
	JMP L7
L6:
	MOVL	$2, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	0(%rsp), %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, 8(%rsp)
	MOVL	$2, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	0(%rsp), %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %eax
	MOVL	8(%rsp), %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fadd
	MOVL	%eax, 0(%rsp)
	INCL	%r13d
	JMP L8
L7:
	MOVL	0(%rsp), %eax
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
L14:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %ebx
	MOVL	$100000, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %ebx
	MOVL	$27, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fsqrt
	MOVL	%eax, %ebp
	MOVL	%ebp, %edi
	MOVL	%ebp, %esi
	AND	$0,%al
	CALL	fmul
	MOVL	%eax, %r12d
	MOVL	%r12d, %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fadd
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	ftoi
	MOVL	%eax, %ebx
	MOVL	%ebp, %edi
	AND	$0,%al
	CALL	print_fpt
	MOVL	%r12d, %edi
	AND	$0,%al
	CALL	print_fpt
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	print_int
	MOVL	%ebx, %eax
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	RET
	.ident	"15-411 L3 compiler"
