	.file	"../tests0/libexp.l3"
	.text
	.global	_c0_fabs
_c0_fabs:
	PUSHQ	%rbx
L1:
	MOVL	%edi, %ebx
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L2
	JMP L3
L2:
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fsub
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	RET
	JMP L4
L3:
	MOVL	%ebx, %eax
	POPQ	%rbx
	RET
	JMP L4
L4:
	.global	_c0_fexp
_c0_fexp:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$16, %rsp
L5:
	MOVL	%edi, %ebx
	MOVL	$100000, %ebp
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %r12d
	MOVL	$100000, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%r12d, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %r12d
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %r13d
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, 0(%rsp)
	MOVL	$1, 8(%rsp)
L8:
	MOVL	0(%rsp), %edi
	AND	$0,%al
	CALL	_c0_fabs
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%r12d, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L10
	JMP L9
L9:
	CMPL	%ebp,8(%rsp)
	JLE L6
	JMP L7
L10:
	JMP L7
L6:
	MOVL	0(%rsp), %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fmul
	MOVL	%eax, 0(%rsp)
	MOVL	8(%rsp), %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	0(%rsp), %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, 0(%rsp)
	MOVL	%r13d, %edi
	MOVL	0(%rsp), %esi
	AND	$0,%al
	CALL	fadd
	MOVL	%eax, %r13d
	INCL	8(%rsp)
	JMP L8
L7:
	MOVL	%r13d, %eax
	ADDQ	$16, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L11:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %ebx
	MOVL	$100000, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %ebx
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fexp
	MOVL	%eax, %ebp
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebp, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fsub
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fabs
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	MOVL	%ebx, %esi
	AND	$0,%al
	CALL	fless
	MOVL	%eax, %eax
	CMPL	$0,%eax
	JNE L14
	JMP L15
L15:
	AND	$0,%al
	CALL	abort
L14:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fexp
	MOVL	%eax, %ebx
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	ftoi
	MOVL	%eax, %eax
	CMPL	$2,%eax
	JE L12
	JMP L13
L13:
	AND	$0,%al
	CALL	abort
L12:
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	ftoi
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
