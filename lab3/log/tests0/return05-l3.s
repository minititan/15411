	.file	"../tests0/return05.l3"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	$10, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %ebx
	MOVL	$5, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fadd
	MOVL	%eax, %ebp
	MOVL	$2, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebp, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fmul
	MOVL	%eax, %ebx
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	print_fpt
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	ftoi
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
