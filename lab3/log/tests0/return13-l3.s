	.file	"../tests0/return13.l3"
	.text
	.global	_c0_will_return
_c0_will_return:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	CMPL	$0,%eax
	JE L2
	JMP L3
L2:
	MOVL	$1, %eax
	ADDQ	$8, %rsp
	RET
	JMP L4
L3:
	INCL	%eax
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
	JMP L4
L4:
	DECL	%eax
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$0, %eax
	MOVL	%eax, %edi
	ADDL	$0, %edi
	CMPL	$0,%edi
	JNE L6
	JMP L7
L6:
	INCL	%eax
	CMPL	$0,%eax
	JNE L9
	JMP L10
L9:
	MOVL	$2, %eax
	ADDQ	$8, %rsp
	RET
	JMP L11
L10:
	MOVL	$3, %eax
	ADDQ	$8, %rsp
	RET
	JMP L11
L11:
	JMP L8
L7:
	JMP L8
L8:
	MOVL	$4, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
