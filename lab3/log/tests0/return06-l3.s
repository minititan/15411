	.file	"../tests0/return06.l3"
	.text
	.global	_c0_nop
_c0_nop:
	SUBQ	$8, %rsp
L1:
	ADDQ	$8, %rsp
	RET
	.global	_c0_raise
_c0_raise:
	SUBQ	$8, %rsp
L2:
	MOVL	$0, %edi
	MOVL	$1, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	ADDQ	$8, %rsp
	RET
	.global	_c0_abort
_c0_abort:
	SUBQ	$8, %rsp
L3:
	JMP L5
L5:
	AND	$0,%al
	CALL	abort
L4:
	ADDQ	$8, %rsp
	RET
	.global	_c0_okay
_c0_okay:
	SUBQ	$8, %rsp
L6:
	JMP L7
L8:
	AND	$0,%al
	CALL	abort
L7:
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L9:
	AND	$0,%al
	CALL	_c0_nop
	JMP L10
L10:
	AND	$0,%al
	CALL	_c0_okay
	JMP L12
L11:
	JMP L12
L12:
	MOVL	$128, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
