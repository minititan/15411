	.file	"../tests0/return08.l3"
	.text
	.global	_c0_foo
_c0_foo:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	CMPL	$0,%edi
	JNE L2
	JMP L3
L2:
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	JMP L4
L3:
	MOVL	$10, %eax
	ADDQ	$8, %rsp
	RET
	JMP L4
L4:
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L5:
	MOVL	$0, %edi
	MOVL	$1, %esi
	AND	$0,%al
	CALL	_c0_foo
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
