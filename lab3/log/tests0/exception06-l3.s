	.file	"../tests0/exception06.l3"
	.text
	.global	_c0_f
_c0_f:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	MOVL	%edx, %esi
	MOVL	%ecx, %edx
	MOVL	%r8d, %ecx
	ADDL	%edi, %eax
	ADDL	%esi, %eax
	ADDL	%edx, %eax
	ADDL	%ecx, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_the_void
_c0_the_void:
	SUBQ	$8, %rsp
L2:
L5:
	JMP L3
L3:
	JMP L5
L4:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L6:
	MOVL	$7, %ebx
	ADDL	$4, %ebx
	MOVL	$0, %edi
	MOVL	$1, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %ebp
	AND	$0,%al
	CALL	_c0_the_void
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%ebp, %esi
	MOVL	$6, %edx
	MOVL	%eax, %ecx
	MOVL	$15, %r8d
	AND	$0,%al
	CALL	_c0_f
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
