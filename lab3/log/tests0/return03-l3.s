	.file	"../tests0/return03.l3"
	.text
	.global	_c0_fib
_c0_fib:
	SUBQ	$8, %rsp
	PUSHQ	%rbp
	PUSHQ	%rbx
L1:
	MOVL	%edi, %ebx
	CMPL	$0,%ebx
	JL L7
	JMP L8
L7:
	MOVL	$0, %edi
	MOVL	$1, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%edi, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	JMP L9
L8:
	JMP L9
L9:
	CMPL	$0,%ebx
	JE L5
	JMP L6
L5:
	JMP L2
L6:
	CMPL	$1,%ebx
	JE L2
	JMP L3
L2:
	MOVL	$1, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	JMP L4
L3:
	MOVL	%ebx, %eax
	SUBL	$1, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fib
	MOVL	%eax, %ebp
	MOVL	%ebx, %eax
	SUBL	$2, %eax
	MOVL	%eax, %edi
	AND	$0,%al
	CALL	_c0_fib
	MOVL	%eax, %eax
	ADDL	%ebp, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	POPQ	%rbp
	ADDQ	$8, %rsp
	RET
	JMP L4
L4:
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L10:
	MOVL	$8, %edi
	AND	$0,%al
	CALL	_c0_fib
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
