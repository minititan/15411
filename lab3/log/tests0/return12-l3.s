	.file	"../tests0/return12.l3"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L1:
	MOVL	$0, %edi
	AND	$0,%al
	CALL	_c0_foo
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_foo
_c0_foo:
	SUBQ	$8, %rsp
L2:
	MOVL	%edi, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
