	.file	"../tests0/exception02.l3"
	.text
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L1:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %ebx
	MOVL	$0, %edi
	AND	$0,%al
	CALL	itof
	MOVL	%eax, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %esi
	AND	$0,%al
	CALL	fdiv
	MOVL	%eax, %ebx
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	print_fpt
	MOVL	%ebx, %edi
	AND	$0,%al
	CALL	print_hex
	MOVL	%ebx, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
