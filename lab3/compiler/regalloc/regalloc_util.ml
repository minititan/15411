(* Holds utility stuff for register allocation phase *)

module LLIR = Llir
module S = Storage

(* a prettier way to chain functions together *)
let (|>) x f = f x

module type IntSet_type = Printable.SET with type elt = int

(* Using printable sets for debugging *)
module IntSet = Printable.MakeSet(struct 
                                    type t = int 
                                    let compare = compare 
                                    let format ff e = Format.fprintf ff "%d" (e)
                                  end)

module type IntMap_type = Printable.MAP with type key = int

(* Using printable sets for debugging *)
module IntMap = Printable.MakeMap(struct 
                                    type t = int 
                                    let compare = compare 
                                    let format ff e = Format.fprintf ff "%d" (e)
                                  end)


exception WrongPattern of string

let enumerate l =
  List.mapi (fun i x -> (i,x)) l

let min_temp = -14
let stack_min_temp = -13
let word_size = 8

(* Function that maps from registers to negative temps *)
let reg_to_temp = function
  | S.EAX | S.RAX -> -1
  | S.EDI | S.RDI -> -2
  | S.ESI | S.RSI -> -3
  | S.EDX | S.RDX -> -4
  | S.ECX | S.RCX -> -5
  | S.R8  | S.R8D -> -6
  | S.R9  | S.R9D -> -7
  | S.R10 | S.R10D -> -8
  | S.R11 | S.R11D -> -9
  | S.EBX | S.RBX -> -10
  | S.EBP | S.RBP -> -11
  | S.R12 | S.R12D -> -12
  | S.R13 | S.R13D -> -13
  | S.R14 | S.R14D -> -14
  | S.R15 | S.R15D -> -15
  | _ -> raise (WrongPattern "Reserved register being used!")


let temp_to_reg = function
  | -1 -> S.EAX
  | -2 -> S.EDI
  | -3 -> S.ESI
  | -4 -> S.EDX
  | -5 -> S.ECX
  | -6 -> S.R8
  | -7 -> S.R9
  | -8 -> S.R10
  | -9 -> S.R11
  | -10 -> S.EBX
  | -11 -> S.EBP
  | -12 -> S.R12D
  | -13 -> S.R13D
  | -14 -> S.R14D
  | -15 -> S.R15D
  | x -> let s = Printf.sprintf "Integer %d not assigned to register!" x in 
              raise (WrongPattern s)

(* caller saved: rax, rdi, rsi, rdx, rcx, r8, r9, r10, r11
 * callee saved: rbx, rbp, r12, r13, r14, r15
 *)