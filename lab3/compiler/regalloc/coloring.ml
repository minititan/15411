(* Module that gives a coloring of a list of vertices *)

open Graph
open Regalloc_util

(*
module Int = struct
  type t = int
  let compare = compare
  let hash = Hashtbl.hash
  let equal = (=)
  let default = 0
end

module G = Imperative.Graph.Concrete(Int)
module CQT = Cliquetree.CliqueTree(G)

let g = G.create()

let connect_v v v' = 
  G.add_edge g v v'

(* Connects 2 sets of vertices completely *)
let connect_all s1 s2 = 
  let l1 = IntSet.elements s1 in
  let l2 = IntSet.elements s2 in
  List.iter (fun v -> List.iter (fun v' -> connect_v v v' ) l2) l1

(* Add all defined vertices *)
let add_all_vertex pred_list = 
  let iter_help (def_set, use_set, succ_set) = 
    List.iter (fun v -> G.add_vertex g v) (IntSet.elements def_set)
  in
    List.iter (iter_help) pred_list

*)

(*
(* creates the graph using the list of live variables and returns it *)
let create_graph liveset_arr pred_list =
  let _ = add_all_vertex pred_list in
  let l = List.length pred_list in
  (* adds the edge b/w v and v' to g*)
  List.iteri (fun i (def, _, _) -> 
          if (i = l-1) then ()
          else connect_all def liveset_arr.(i+1)) pred_list
*)

(* Use Ocaml's CliqueTree module to get a perfect elimination
   ordering from the graph 
 *)
(*
let get_ordering () = 
  (*if (CQT.is_chordal g) then
    let _ = prerr_endline "IM HERE" in 
    let (ordering, x, y) = CQT.mcs_clique g in
      ordering
  else*)
    IntSet.elements (G.fold_vertex (IntSet.add) g IntSet.empty)
*)

(* Searches for lowest color not taken by neighbors in g not in l 
   If vertex is register then simply returns number since register
   is color itself. 
*)

let get_color tmp (conflict_map : Regalloc_util.IntSet.t Regalloc_util.IntMap.t) color_map =
  if tmp < 0 then
    tmp (* pre colored *)
  else 
    let neighbors = IntMap.find tmp conflict_map in

    let fold_helper tmp' color_set =
      if (IntMap.mem tmp' color_map) then
        let this_neighbor_color = IntMap.find tmp' color_map in
        IntSet.add this_neighbor_color color_set
      else
        color_set
    in

    let neighbor_colors = IntSet.fold fold_helper neighbors IntSet.empty in

    let rec get_available_color color_set c = 
      if (IntSet.mem c color_set) then
        get_available_color color_set (c-1)
      else
        c
    in
    get_available_color neighbor_colors (-1)

(* Returns a mapping from vertex to color assigned in a 
   greedy coloring based on ordering given
 *)
let get_coloring simplicial_order conflict_map =
  let rec color_helper simplicial_order color_map = 
    match simplicial_order with
    | [] -> color_map
    | tmp::l ->
      let c = get_color tmp conflict_map color_map in
      color_helper l (IntMap.add tmp c color_map)
  in
  color_helper simplicial_order IntMap.empty

(*
let get_color_map live_list pred_list = 
    let () = create_graph live_list pred_list in
  let ordering = get_ordering () in
  let mapping = get_coloring ordering in
  mapping
*)

let simplicial_ordering conflict_map =
  let max_pair m =
    let combine k v = function
      | None -> Some (k,v)
      | Some (k',v') -> if v > v' then Some (k,v) else Some (k',v')
    in
    IntMap.fold combine m None
  in
  let init = IntMap.map (fun _ -> 0) conflict_map in
  let update_ordering k ordering_m =
    let update_val e = if IntMap.mem e conflict_map then e+1 else e in
    IntMap.remove k (IntMap.map update_val ordering_m)
  in
  let rec gen_simplicial ordering_m l =
    (match max_pair ordering_m with
     | None -> l
     | Some (k,_) -> gen_simplicial (update_ordering k ordering_m) (k::l)
    )
  in
  List.rev (gen_simplicial init [])

let so_to_string so = String.concat "," (List.map string_of_int so)

let debug = false

(* IntSet.t IntMap.t -> int IntMap.t *)
(* assume an undirected map: v in map[k] -> k in map[v] *)
let gen_coloring (conflict_map : IntSet.t IntMap.t) =
  let simplicial_order = simplicial_ordering conflict_map in
  let _ = if debug then Printf.printf "\nSimplicial ordering: [%s]\n" (so_to_string simplicial_order) else () in
  get_coloring simplicial_order conflict_map

let format k v  = 
  Printf.sprintf "%d -> %d\n" k v

module type PRINT =
  sig
    val pp_colormap : int IntMap.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_colormap m =
      let (k, mincolor) = IntMap.min_binding m in
    let info = Printf.sprintf "Min binding: %d, %d\n" k mincolor in
      "Color mapping {\n" ^
      IntMap.fold (fun k v s -> s ^ (format k v)) m ""
      ^ info ^ "}\n"
  end
