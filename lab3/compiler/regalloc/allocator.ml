(* module to implement register or stack allocation *)

open Regalloc_util

module SM = Symbol.Map
module L = Liveness_gen
module C = Coloring

(* interface for this updated, now will do the entire blockfunc -> storage map process
 * it hasn't been rewritten yet *)

(*
let get_storage_map color_map =
  let (k, some_v) = IntMap.choose color_map in
  let min_color = IntMap.fold (fun k v min_c ->
    if (v < min_c) then v else min_c) color_map some_v in
  if (min_color < min_temp) then
    IntMap.map (color_to_storage true) color_map
  else
    IntMap.map (color_to_storage false) color_map
*)


(* notes:
3 address -> regalloc

calling conventions
call: define caller save
div: use eax and define eax, edx
mod: use eax, edx and define eax, edx

preference to caller saved over callee, keep track of callee-saved that are used
*)

let interference_to_string interference =
  let bndgs = IntMap.bindings interference in
  let bnd_to_string (temp,int_set) =
    let int_list = IntSet.elements int_set in
    "Temp " ^ (string_of_int temp) ^ " -> {" ^ String.concat "," (List.map string_of_int int_list) ^ "}"
  in
  let combine s1 s2 = s1 ^ "\n" ^ s2 in
  List.fold_left combine "" (List.map bnd_to_string bndgs)

let debug = L.debug

let storage_map_of_blockfunc (sym,bl) =
  let inslist_numbered = L.instruction_line_list bl in
  let seeded = L.seed inslist_numbered in
  let (def_list, live_list, all_temps) = L.propagate seeded in
  let interfered = L.gen_interference def_list live_list all_temps in
  let _ = if debug then Printf.printf "Interference: %s\n" (interference_to_string interfered) else () in
  let colored = C.gen_coloring interfered in
  (sym, colored)

let sm_of_list l = List.fold_left (fun sm (k,v) -> SM.add k v sm) SM.empty l

let storage_maps_of_blockprogram p =
  sm_of_list (List.map storage_map_of_blockfunc p)

let format k v = 
  let s = Printf.sprintf "t%d -> " k in
  match v with 
  | Storage.STACK(o) -> let s2 = Printf.sprintf "STACK(%d)\n" o in s ^ s2
  | Storage.REG(r) -> s ^ Storage.format_reg r ^ "\n" 

module type PRINT =
  sig
    val pp_storagemap : int IntMap.t Symbol.Map.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_storagemap m =
      let bndgs = Symbol.Map.bindings m in
      let int_storage_map_to_string ism =
        let bndgs_ism = IntMap.bindings ism in
        let ism_binding_to_string (i,stor) =
          (string_of_int i) ^ " -> " ^ (string_of_int stor)
        in
        String.concat "\n" (List.map ism_binding_to_string bndgs_ism)
      in
      let bndg_to_string (s,int_storage_map) =
        let ism_string = int_storage_map_to_string int_storage_map in
        Symbol.name s ^ ":\n" ^ ism_string
      in
      "Register allocation mappings:\n" ^ String.concat "\n" (List.map bndg_to_string bndgs) ^ "\n\n"
  end

(*
let get_storage_map instr_list = 
  let liveness_list = L.liveness_generator instr_list in 
  let color_map = C.get_color_map liveness_list in
  let (k, min_color) = IntMap.min_binding color_map in

  if (min_color < min_temp) then
    (* since we need a register to do the stack moves *)
    let stack_spots = min_temp - min_color in

    (* We don't have enough registers so first find
      stack_spots number of color class
      with the least elements *)
    let color_to_vertex = collect_vals color_map in

    let color_cmp (c1, vxs1) (c2, vxs2) = 
      (* if the color class contains register it must always lose comparison *)
      let vxs1_has_reg = BatList.exists (fun x -> x < 0) vxs1 in
      let vxs2_has_reg = BatList.exists (fun x -> x < 0) vxs2 in
      let vxs1_l = List.length vxs1 in
      let vxs2_l = List.length vxs2 in
      if (vxs1_has_reg and vxs2_has_reg) then 0
      else if (vxs1_has_reg) then 1
      else if (vxs2_has_reg) then -1
      else if (vxs1_l > vxs2_l) then 1
      else if (vxs2_l > vxs1_l) then -1
      else 0
    in

    (* We sort it to get the sizes of the color classes to pick
      the smallest color classes that dont contain a register 
     to store on the stack *)
    let rev_color_sizes = List.sort color_cmp (IntMap.bindings color_to_vertex)

    in

    (* We now have a list of (color, temp list) sorted by sizes of temp list
      So we now replace the smallest ones by stack storage *)
    let (stack_list)
    in storage_map
  else
    IntMap.empty
*)