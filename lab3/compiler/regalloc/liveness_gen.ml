(* Implementation of register allocator *)

(* Lab 3: Note we are doing this starting from IR (Tree.cmd) now. *)

open Regalloc_util
module IR = Ir
module T = Tree
module S = Storage
module LM = Label.Map

type live = IntSet.t
type define = IntSet.t
type use = IntSet.t
type successor = IntSet.t
type line = int

exception WrongPattern of string

let (|>) x f = f x

(* From Batteries *)
(******************)
type 'a mut_list =  {
  hd: 'a;
  mutable tl: 'a list
}

external inj : 'a mut_list -> 'a list = "%identity"

module Acc = struct
  let dummy () =
    { hd = Obj.magic (); tl = [] }
  let create x =
    { hd = x; tl = [] }
  let accum acc x =
    let cell = create x in
    acc.tl <- inj cell;
    cell
end

let rec drop n = function
  | _ :: l when n > 0 -> drop (n-1) l
  | l -> l

let take n l =
  let rec loop n dst = function
    | h :: t when n > 0 ->
      loop (n - 1) (Acc.accum dst h) t
    | _ ->
      ()
  in
  let dummy = Acc.dummy () in
  loop n dummy l;
  dummy.tl

(******************)

let raise_error s = ErrorMsg.error None s; raise ErrorMsg.Error

(* (Definition, Use set, Successor set) *)
type predicates = line * define * use * successor

type numbered_commands = (line * Ir.instr) list

let list_to_set = List.fold_left (fun s x -> IntSet.add x s) IntSet.empty

let label_map_from_instructions (inst_l_with_lines : numbered_commands) =
  List.fold_left (fun lm (i,l) ->
    match l with IR.LABEL l' -> LM.add l' i lm | _ -> lm)
  LM.empty inst_l_with_lines

let block_to_instr_list bf =
  List.concat (List.map (fun (l,_,cl) -> cl) bf)

let intset_of_rlist =
  List.fold_left (fun s e -> IntSet.add (reg_to_temp e) s) IntSet.empty

(* set of caller save registers *)
let caller_save =
  intset_of_rlist [S.EAX; S.EDI; S.ESI; S.EDX; S.ECX; S.R8; S.R9; S.R10; S.R11]

let merge_def_use (d1,u1) (d2,u2) = (IntSet.union d1 d2, IntSet.union u1 u2)

let argnum_to_regset =
  let reg_to_set r = IntSet.singleton (reg_to_temp r) in
  function
  | 0 -> reg_to_set S.EDI
  | 1 -> reg_to_set S.ESI
  | 2 -> reg_to_set S.EDX
  | 3 -> reg_to_set S.ECX
  | 4 -> reg_to_set S.R8
  | 5 -> reg_to_set S.R9
  | _ -> IntSet.empty

let temp_to_set = function
  | IR.IMM _ -> IntSet.empty
  | IR.REG r -> IntSet.singleton (reg_to_temp r)
  | IR.TEMP t -> IntSet.singleton (Temp.get_int t)
  | IR.ARGTOP a | IR.ARGBOTTOM a -> argnum_to_regset a

let temps_to_set =
  List.fold_left (fun s x -> IntSet.union (temp_to_set x) s) IntSet.empty

exception LabelMissing

(* return (line, def, use, succ) *)
(* Generates predicates for an instruction *)
let predicate_gen lm (line, instr) : predicates =
  (* We use the exception version of find because this
   * should always work.
   *)
  let label_to_line l = try LM.find l lm with Not_found -> raise LabelMissing in
  let empty = IntSet.empty in
  let next_line = IntSet.singleton (line+1) in
  let (d,u,s) =
   (match instr with
    | IR.BINOP (b, d, s1, s2) ->
      (match b with
       | T.DIV | T.MOD ->
        let regs_used = list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX] in
        (IntSet.union regs_used (temp_to_set d),
         IntSet.union regs_used (temps_to_set [s1; s2]),
         next_line)
       | T.SAR | T.SAL ->
        (IntSet.add (reg_to_temp S.ECX) (temp_to_set d),
         IntSet.add (reg_to_temp S.ECX) (temps_to_set [s1; s2]),
         next_line)
       | T.ADD | T.SUB | T.MUL | T.BAND | T.BOR | T.XOR ->
       (temp_to_set d, temps_to_set [s1; s2], next_line)
      )

    (* is this correct? *)
    | IR.UNOP(_, d, s) -> (temp_to_set d, temp_to_set s, next_line)

    (* TODO: we should actually use directives and comments. *)
    | IR.DIRECTIVE _ | IR.COMMENT _ -> raise_error "Invalid instruction."

    | IR.MOV (d, s) -> (temp_to_set d, temp_to_set s, next_line)

    | IR.IF (_, o1, o2, l1, l2) ->
      let l1line = label_to_line l1 in
      let l2line = label_to_line l2 in
      (empty, temps_to_set [o1; o2], list_to_set [l1line; l2line; line+1])

    | IR.CALL _ -> (caller_save, empty, next_line)

    | IR.GOTO l ->
      (IntSet.empty, IntSet.empty, IntSet.singleton (label_to_line l))

    | IR.LABEL l -> (IntSet.empty, IntSet.empty, next_line)

    | IR.RET -> (empty, IntSet.singleton (reg_to_temp S.EAX), empty)
   )
  in
  (line,d,u,s)

let instruction_line_list bl = enumerate (block_to_instr_list bl)

let seed l = List.map (predicate_gen (label_map_from_instructions l)) l

let intset_of_define x = x
let intset_of_use x = x
let intset_of_live x = x

let propagated_to_list def_list use_list live_list =
  let fixup = List.map (fun (i,sl) -> string_of_int i ^ " -> {" ^ (String.concat ", " sl) ^ "}") in
  let def_list_s =
    def_list
    |> (List.map intset_of_define)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let use_list_s =
    use_list
    |> (List.map intset_of_use)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  let live_list_s =
    live_list
    |> (List.map intset_of_live)
    |> (List.map IntSet.elements)
    |> (List.map (List.map string_of_int))
    |> enumerate
    |> fixup
    |> (String.concat "\n") in
  "Def list:\n" ^ def_list_s ^ "\nUse list:\n" ^ use_list_s ^ "\nLive list:\n" ^ live_list_s

(* we can refactor update_interference to
   use this down below when we have time *)
let update_mapset line pred_map successor_line =
  let pred = IntMap.find successor_line pred_map in
  let pred' = IntSet.add line pred in
  IntMap.add successor_line pred' pred_map

let preds_to_string preds =
  let bndgs = IntMap.bindings preds in
  let bnd_to_string (ln,int_set) =
    let int_list = IntSet.elements int_set in
    "Line " ^ (string_of_int ln) ^ " -> {" ^ String.concat "," (List.map string_of_int int_list) ^ "}"
  in
  let combine s1 s2 = s1 ^ "\n" ^ s2 in
  List.fold_left combine "" (List.map bnd_to_string bndgs)

let rec propagate_var predecessors var pred_map ln =
  let preds = IntMap.find ln predecessors in
  let (d,u,s,l) = IntMap.find ln pred_map in
  if (IntSet.mem var d && not (IntSet.mem var u)) || IntSet.mem var l then (* done propagating *)
    pred_map
  else
    let l' = IntSet.add var l in
    let pred_map' = IntMap.add ln (d,u,s,l') pred_map in
    IntSet.fold (fun ln' pm -> propagate_var predecessors var pm ln') preds pred_map'

let rec process_line predecessors pred_map ln =
  let (d,u,s,l) = IntMap.find ln pred_map in
  let new_vars = IntSet.diff u l in
  if new_vars = IntSet.empty then
    pred_map
  else
    (*let l' = IntSet.union l u in
    let pred_map' = IntMap.add ln (d,u,s,l') pred_map in*)
    let pred_map' = IntSet.fold (fun new_var pm -> propagate_var predecessors new_var pm ln) new_vars pred_map in
    let preds = IntMap.find ln predecessors in
    IntSet.fold (fun pred pm' -> process_line predecessors pm' pred) preds pred_map'

(* From: http://stackoverflow.com/questions/243864/
   what-is-the-ocaml-idiom-equivalent-to-pythons-range-function *)
let (--) i j =
  let rec aux n acc = if n < i then acc else aux (n-1) (n :: acc)
  in aux j []

let predecessors_of_pred_list pred_list =
  let insert_empty_set s x = IntMap.add x IntSet.empty s in
  (* why does this have to be not -1?? *)
  let range_len_list = (0 -- (List.length pred_list)) in
  let init = List.fold_left insert_empty_set IntMap.empty range_len_list in
  let add_map_for_successors current_map (line,_,_,successors) =
    List.fold_left
    (update_mapset line) current_map (IntSet.elements successors)
  in
  List.fold_left add_map_for_successors init pred_list

let debug = false

let all_temps pred_list =
  let def_use_list = List.map (fun (_,d,u,_) -> (d,u)) pred_list in
  let used d u = IntSet.union d u in
  List.fold_left (fun all_temps (d,u) -> IntSet.union all_temps (used d u)) IntSet.empty def_use_list

(* val propagate : predicates list -> (define list) * (live list) *)
let propagate pred_list =
  let def_list = List.map (fun (_,d,_,_) -> d) pred_list in
  (* line -> (def,use,succ,live) *)
  let predecessors = predecessors_of_pred_list pred_list in
  (* Predecessor generation seems fine. *)
  let predicates_to_live_map im (l,d,u,s) = IntMap.add l (d,u,s,IntSet.empty) im in
  let pred_map_init = List.fold_left predicates_to_live_map IntMap.empty pred_list in
  let lines = List.rev (0 -- (List.length pred_list - 1)) in
  let liveness_information = List.fold_left (process_line predecessors) pred_map_init lines in
  (* IntMap.bindings says it returns in ascending
   * order by key... it better be right. *)
  let live_list =
    List.map (fun (ln,(d,u,s,l)) -> l) (IntMap.bindings liveness_information) in
  let _ = if debug then
    let use_list = List.map (fun (_,_,u,_) -> u) pred_list in
    Printf.printf "%s\n\n" (propagated_to_list def_list use_list live_list) else () in
  (def_list, live_list, all_temps pred_list)

exception NoCommand
exception InvalidGenInterfaceArgs
exception SizeNotMatching

let rec zip = function
  | ((x::l),(x'::l')) -> (x,x')::(zip (l,l'))
  | ([],[]) -> []
  | (_::_, []) | ([], _::_) -> raise SizeNotMatching

(* val gen_interference : define list -> live list -> IntSet.t IntMap.t *)
let gen_interference def_list live_list all_temps =
  let def_list_length = List.length def_list
  and live_list_length = List.length live_list in
  if def_list_length <> live_list_length
    || def_list_length = 0
    || live_list_length = 0
    then raise InvalidGenInterfaceArgs else
  (* drop front of live_list and end of def_list then zip *)
  let def_list' = take (def_list_length-1) def_list
  and live_list' = drop 1 live_list in
  let zipped = zip (def_list', live_list') in

  (* adds edges t1->t2 and t2->t1 *)
  let update_interference inter_map (t1,t2) =
    let t1_new = IntSet.add t2 (try IntMap.find t1 inter_map with Not_found -> IntSet.empty) in
    let t2_new = IntSet.add t1 (try IntMap.find t2 inter_map with Not_found -> IntSet.empty) in
    let inter_map' = IntMap.add t1 t1_new inter_map in
    IntMap.add t2 t2_new inter_map'
  in

  (* I'm not worrying about mov vs. binop. Is that a problem? *)
  let mark_interference inter_map (def_set, live_set) =
    IntSet.fold
      (fun def_temp im ->
      List.fold_left
        (fun im' live_temp -> update_interference im' (def_temp, live_temp))
        im (IntSet.elements (IntSet.remove def_temp live_set)))
    def_set
    inter_map
  in

  let initial_mapping =
    IntSet.fold
    (fun i int_map -> IntMap.add i IntSet.empty int_map)
    all_temps IntMap.empty
  in

  List.fold_left mark_interference initial_mapping zipped

let print_set s =
  let mid_s =
    match IntSet.elements s with
      | [] -> "{"
      | [x] -> "{" ^ (string_of_int x)
      | x::xs -> "{" ^ (string_of_int x)
        ^ List.fold_left
          (fun s i -> let new_s = Printf.sprintf ",%d" i in s ^ new_s )
          "" xs
    in
  mid_s ^ "}"

module type PRINT =
  sig
  val pp_livelist : IntSet.t list -> string
  end

module Print : PRINT =
  struct
  let pp_livelist l =
    let combine s liveset = let set_s = print_set liveset in s ^ set_s ^ "\n" in
    "Live list {\n" ^ List.fold_left combine "" l ^ "}\n"
  end
