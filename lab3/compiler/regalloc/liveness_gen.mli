(* Module to implement liveness generation *)

open Regalloc_util

type live = IntSet.t
type define = IntSet.t
type use = IntSet.t
type successor = IntSet.t
type line = int

type predicates = line * define * use * successor
type numbered_commands = (line * Ir.instr) list

val intset_of_define : define -> IntSet.t
val intset_of_use : use -> IntSet.t
val intset_of_live : live -> IntSet.t

val label_map_from_instructions : numbered_commands -> int Label.Map.t

val block_to_instr_list : Block.block list -> Ir.instr list

val debug : bool

val seed : numbered_commands -> predicates list

val propagate : predicates list -> (define list) * (live list) * IntSet.t

(* val liveness_generator : Block.blockfunc -> IntSet.t list * (live list) *)

val instruction_line_list : Block.block list -> numbered_commands

(* end result is temp -> temp set map of interferences *)
val gen_interference : define list -> live list -> IntSet.t -> IntSet.t IntMap.t

module type PRINT = 
  sig
    val pp_livelist : IntSet.t list -> string 
  end

module Print : PRINT
