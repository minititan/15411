val storage_map_of_blockfunc : Block.blockfunc ->
	(Symbol.symbol * int Regalloc_util.IntMap.t)

val storage_maps_of_blockprogram : Block.blockprogram ->
	int Regalloc_util.IntMap.t Symbol.Map.t

val format : int -> Storage.str -> string

module type PRINT =
  sig
    val pp_storagemap : int Regalloc_util.IntMap.t Symbol.Map.t -> string
  end

module Print : PRINT
