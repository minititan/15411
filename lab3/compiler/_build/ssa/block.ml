
(* label set gives the jumps from the block. Can be empty *)
type block = 
	Label.label * Label.Set.t * Ir.instr list

type blockfunc = Symbol.symbol * block list

type blockprogram = blockfunc list

module type PRINT =
  sig
    val pp_blockprogram : blockprogram -> string
  end

module Print : PRINT =
  struct

  	let pp_block (l, lset, clist) = 
  		let bstr = List.fold_left (fun s c -> s ^ Ir.Print.pp_instr c) "" clist in
  		let jmpstr = (Label.Set.fold (fun lb s -> s ^ Label.name lb ^ ",") lset "=====\nJumpsTo(") ^ ")\n" in
  		"BLOCK " ^ Label.name l ^ "\n=====\n" ^ bstr ^ jmpstr

  	let pp_blocks bs = 
  		List.fold_left (fun s b -> s ^ pp_block b ^ "\n") "" bs

  	let pp_blockprogram p = 
  		List.fold_left (fun s (id, b) -> s ^ Symbol.name id ^ " {\n" ^ pp_blocks b ^ "}\n\n") "" p
  end