

module I = Ir
module B = Block
module LS = Label.Set
module LM = Label.Map

(* scans till the first goto or if and returns set of jumps from there 
	and the command list till there *)
let rec get_block sofar l = 
	match l with
	| [] -> (LS.empty, sofar, [])

	| instr::[] ->
		(match instr with
		| I.GOTO(lab) -> (LS.singleton lab, sofar @ [instr], [])
		| I.IF(cm, op1, op2, lab1, lab2) -> 
			(LS.add lab1 (LS.singleton lab2), sofar @ [instr], [])

		| _ -> (LS.empty, sofar @ [instr], [])

		)

	| instr::l' -> 
		(match instr with
		| I.GOTO(lab) -> (LS.singleton lab, sofar @ [instr], l')
		| I.IF(cm, op1, op2, lab1, lab2) -> 
			(LS.add lab1 (LS.singleton lab2), sofar @ [instr], l')

		| _ -> get_block (sofar @ [instr]) l'
		)


let rec get_blocks instrlist = 
	match instrlist with
	| instr::l' ->
		(match instr with
		| I.LABEL(l) ->
			let (exit_labels, block_instrs, rem) = get_block [] l' in
			(l, exit_labels, instr::block_instrs)::(get_blocks rem)

		| _ -> ErrorMsg.error None ("Block has to begin with Label!"); raise ErrorMsg.Error
		)

	| [] -> []

let block_convert l = 
	let block_funcs = List.map (fun (id, instrlist) -> (id, get_blocks instrlist)) l in
	block_funcs




