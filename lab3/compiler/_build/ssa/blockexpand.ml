(* Expands out function calls and entry to functions regarding caller-saved registers *)

(* expand out the first block of a function by adding in InitMov (see block.ml) *)
let expand_blockfunc (id, arg_temp_list, blocklist) = 
	match blocklist with
	| [] -> (id, arg_temp_list, blocklist)

	| (l, lset, clist)::rest ->
			let init = List.mapi (fun i arg_temp -> Tree.InitMov(arg_temp, i)) arg_temp_list in
			let init_cmds = List.fold_right (fun cmd l -> cmd::l) init [] in
			(id, arg_temp_list, (l, lset, init_cmds @ clist)::rest)

let expand p = 
	List.map (expand_blockfunc) p