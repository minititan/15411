open Ast
module S = Symbol
module T = TypedAst
module SM = Symbol.Map
module SS = Symbol.Set

let get_type_str = function
  | Bool -> "Bool"
  | Int -> "Int"
  | Void -> "Void"
  | PrId(id) -> "Type " ^ Symbol.name id


let called_tracker = 
object 
  val mutable called_set = SS.empty

  method add_call fname = 
    called_set <- SS.add fname called_set

  method get_call_set = called_set

end

(* to keep track of called and defined functions and typedefs. Is mutable. *)
class globalContext =
object(this)
  val def_set = SS.empty
  val dec_set = SS.empty
  val typemap = SM.empty (* mapping from ident to type for typedef *)
  val fmap = SM.empty (* mapping from function to (return type, argument type list) *)

  method get_def_set = def_set

  method get_dec_set = dec_set

  method add_def name = 
    {< def_set = SS.add name def_set >}

  method is_typedef id = 
    SM.mem id typemap

  method get_functype name = 
    match SM.find' name fmap with
    | Some(p) -> p
    | None -> ErrorMsg.error None "function name not in context"; raise ErrorMsg.Error


  (* adding a function declaration to scope. If previous declaration exists
    check for type compatibility *)
  method add_fundecl fname ptype ptyplist = 

    (* check for typedef names *)
    match SM.find' fname typemap with
    | None -> 
        let rtype = this#get_type ptype in

        let tplist = List.map (fun (t, a) -> match this#get_type t with
                                          | Void -> (ErrorMsg.error None ("Function argument void!"); raise ErrorMsg.Error)
                                          | x -> x ) ptyplist in
        
        let _ = List.fold_left (fun seen (t, a) -> if (SS.mem a seen) then
                                                    (ErrorMsg.error None ("Same name arg"); raise ErrorMsg.Error)
                                                  else if this#is_typedef a then
                                                    (ErrorMsg.error None ("Arg name typedefedz"); raise ErrorMsg.Error)
                                                  else
                                                    SS.add a seen) SS.empty ptyplist
        in

        (match SM.find' fname fmap with
        | None -> {< fmap = SM.add fname (rtype, tplist) fmap; dec_set = SS.add fname dec_set >}

        | Some((ret_type, argtyp_list)) -> 
            if (ret_type = rtype &&
                (List.for_all2 (fun t1 t2 -> t1 = t2) argtyp_list tplist))
            then {<>}

            else (ErrorMsg.error None "function declaration does not match previous declaration"; raise ErrorMsg.Error)
          )

    | Some(_) -> (ErrorMsg.error None "function name taken by typedef"; raise ErrorMsg.Error)


  (* adds a function name to the context and also adds the argument
      identifiers as defined variables *)
  method add_fundefn fname ptype (plist : (Ast.primtype * Ast.ident) list) = 
    if (SS.mem fname def_set) then
      let s = "function " ^ Symbol.name fname ^ " previously defined" in
      (ErrorMsg.error None s; raise ErrorMsg.Error)

    else
      this#add_fundecl fname ptype plist


  (* Checks if all the arguments of the funciton are correct and also returns the return type
    of the function *)
  method check_funccall fname (typexp_list : TypedAst.typexp list) = 
    match SM.find' fname fmap with
    | Some((ret_type, argtyp_list)) ->
        let () = List.iter2 (fun t (t', e) -> 
                    if (t != t') then (ErrorMsg.error None "function argument type mismatch"; raise ErrorMsg.Error)
                    else ()) argtyp_list typexp_list in
        ret_type

    | None -> ErrorMsg.error None "Function call being checked not in context"; raise ErrorMsg.Error


  (* k is ident and v is primtype *)
  method add_typedef k v = 
    match SM.find' k typemap with
    | Some(v') -> ErrorMsg.error None "ident previously used as typedef"; raise ErrorMsg.Error
    | None -> 
        if (SM.mem k fmap) then (ErrorMsg.error None "Typedef name already a function name"; raise ErrorMsg.Error)
      else
        (match v with

          (* if typedefing to another typedef we lookup the default type of that *)
          | PrId(id) -> (match SM.find' id typemap with
                          | None -> ErrorMsg.error None "typedefing ident to ident without previous typedef"; raise ErrorMsg.Error
                          | Some(t) -> {< typemap = SM.add k t typemap >}
                        )

          | Void -> (ErrorMsg.error None ("Cannot typedef Void"); raise ErrorMsg.Error)

          | _ -> {< typemap = SM.add k v typemap >}
        )

  method find_idtype id = 
    match SM.find' id typemap with
    | Some(t) -> t
    | None -> ErrorMsg.error None "Ident not typedefed"; raise ErrorMsg.Error

  (* helper method that given a type returns primitive type *)
  method get_type t = 
    match t with
      | Int -> Int
      | Bool -> Bool
      | Void -> Void
      | PrId(id) -> this#find_idtype id

end


(* keeps track of the magled function names *)
let mangled_name_holder = 
object
    val mutable names = SM.empty

    method add_mangled_name fname mangled_fname = 
      names <- SM.add fname mangled_fname names

    method get_mangled_name fname = 
      SM.find fname names

    method get_mangled_map = names

end


(* object that holds the context within a particulare function *)
class progContext = fun init_set ->
object
  val live_funcs = init_set

  method add_arg (id : Symbol.symbol) = 
    {< live_funcs = SS.remove id live_funcs >}

  method add_func_back (fname : Symbol.symbol) = 
    {< live_funcs = SS.add fname live_funcs >}

  (* checks if a function call is valid under context *)
  method is_live fname = SS.mem fname live_funcs

  method var_declare (global_context : globalContext) id = 
    if (global_context#is_typedef id) then (ErrorMsg.error None "Variable name used as typedef"; raise ErrorMsg.Error)

    else if (SS.mem id live_funcs) then
      (* remove from function context *)
      {< live_funcs = SS.remove id live_funcs >}
    else
      {<>}

  (* checks if variable is valid to use under context *)
  method valid_var_use (global_context : globalContext) id = 
    if (SS.mem id live_funcs || global_context#is_typedef id) then
      (ErrorMsg.error None "Variable name used by function or typedef"; raise ErrorMsg.Error)
    else
      ()

end


(* returns the type of the resulting epressions and the type
    required by each subexp. If subexp type is None then it doesnt
    matter but has to be the same *)
let get_optype = function
  | PLUS -> (Int, Some(Int))
  | MINUS -> (Int, Some(Int))
  | TIMES -> (Int, Some(Int))
  | DIVIDEDBY -> (Int, Some(Int))
  | MODULO -> (Int, Some(Int))
  | BITAND -> (Int, Some(Int))
  | BITOR -> (Int, Some(Int))
  | SAR -> (Int, Some(Int))
  | SAL -> (Int, Some(Int))
  | XOR -> (Int, Some(Int))
  | LOGICAND -> (Bool, Some(Bool))
  | LOGICOR -> (Bool, Some(Bool))
  | EQUALS -> (Bool, None)
  | NOTEQUALS -> (Bool, None)
  | GREATER -> (Bool, Some(Int))
  | GREATEREQUAL -> (Bool, Some(Int))
  | LESS -> (Bool, Some(Int))
  | LESSEQUAL -> (Bool, Some(Int))
  | LOGICNOT -> (Bool, Some(Bool))
  | BITNOT -> (Int, Some(Int))
  | NEGATIVE -> (Int, Some(Int))


let rec tc_exp globl_context prog_context dec exp = 
  match exp with
  | True -> (Bool, T.True)
  | False -> (Bool, T.False)

  | Var(id) -> 
      let _ = prog_context#valid_var_use globl_context id in
      (match SM.find' id dec with
      | Some(t) -> (t, T.Var(id))
      | None -> ErrorMsg.error None ("Undeclared variable " ^ S.name id ^ "."); raise ErrorMsg.Error
    )

  | ConstExp i -> (Int, T.ConstExp(i))

  | BinopExp(oper, inner_exp1, inner_exp2) ->
    let ((t1, e1), (t2, e2)) = (tc_exp globl_context prog_context dec inner_exp1, 
                                tc_exp globl_context prog_context dec inner_exp2) in
    let (res_type, sub_type) = get_optype oper in

    (match (t1, t2) with
    | (Bool, Bool) -> 
              (match sub_type with 
              | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Bool) -> (res_type, T.BinopExp(oper, (t1, e1), (t2, e2)))
              | Some(Int) -> ErrorMsg.error None "Type mismatch: Expected Int got Bool"; raise ErrorMsg.Error
              )

    | (Int, Int) -> (match sub_type with 
              | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Int) -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Bool) -> ErrorMsg.error None "Type mismatch: Expected Bool got Int"; raise ErrorMsg.Error
              )

    | _ -> ErrorMsg.error None "Type mismatch: Mix of types in binop expression"; raise ErrorMsg.Error
    )

  | UnopExp(oper, inner_exp) -> 
    let ((res_type, sub_type), (exp_type, e)) = (get_optype oper, tc_exp globl_context prog_context dec inner_exp) in

    (match (sub_type, exp_type) with
    | (Some(Bool), Bool) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some(Int), Int) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some(Bool), _) -> ErrorMsg.error None "Type mismatch: Unop expected Bool"; raise ErrorMsg.Error
    | (Some(Int), _) -> ErrorMsg.error None "Type mismatch: Unop expected Int"; raise ErrorMsg.Error
    | (None, _) -> ErrorMsg.error None "Equality comparison operator is not a Unop"; raise ErrorMsg.Error
    )

  | Condition(e1, e2, e3) -> 
    let ((t1, te1), (t2, te2), (t3, te3)) =
      (tc_exp globl_context prog_context dec e1, 
        tc_exp globl_context prog_context dec e2, 
          tc_exp globl_context prog_context dec e3) in

    (match (t1) with
    | Bool -> (match (t2, t3) with
          | (Int, Int) -> (Int, T.Condition((t1, te1), (t2, te2), (t3, te3)))
          | (Bool, Bool) -> (Bool, T.Condition((t1, te1), (t2, te2), (t3, te3)))
          | (Void, _) | (_, Void) -> ErrorMsg.error None "Ternary condition cannot have void branch"; raise ErrorMsg.Error
          | _ -> ErrorMsg.error None "Type mismatch: Condition has multiple type paths"; raise ErrorMsg.Error
          )
    | _ -> ErrorMsg.error None "Type mismatch: Condition check exp must have Bool type"; raise ErrorMsg.Error
    )

  | Call(id, args) ->
      (* add function to set of called functions *)
      let () = called_tracker#add_call id in

      let tlist = List.map (fun e -> tc_exp globl_context prog_context dec e) args in

      (* arguments cannot be void *)
      let _ = List.iter (fun (t, ex) -> match t with
                                         | Void  -> (ErrorMsg.error None "Function parameter has void type"; raise ErrorMsg.Error)
                                         | _ -> ()) tlist in

      let t = if (prog_context#is_live id) then globl_context#check_funccall id tlist
              else (ErrorMsg.error None ("Function " ^ Symbol.name id ^ " not live"); raise ErrorMsg.Error) in

      (t, T.Call(id, tlist))


(* HELPERS *)
let dec_set dec =
  let l = List.map (fun (k,_) -> k) (SM.bindings dec) in
  List.fold_left (fun s e -> SS.add e s) SS.empty l

let check_def def x = 
  if SS.mem x def then () else
  (ErrorMsg.error None ("Variable " ^ S.name x ^ " used before defined in return."); raise ErrorMsg.Error)

let rec used_exp : exp -> SS.t = function
  | Var i -> SS.singleton i
  | ConstExp _ -> SS.empty (* def, live *)
  | BinopExp (_, e1, e2) ->
    let live1 = used_exp e1
    and live2 = used_exp e2 in
    SS.union live1 live2
  | UnopExp (_, e) -> used_exp e
  | Condition (e1, e2, e3) ->
    let (live1, live2, live3) = (used_exp e1, used_exp e2, used_exp e3) in
    SS.union (SS.union live1 live2) live3
  | True -> SS.empty
  | False -> SS.empty
  | Call(id, arg_exps) -> List.fold_left (fun s e -> SS.union s (used_exp e)) SS.empty arg_exps 


(* recursively checks the ast. returns a tuple of
  (updated context, typed tree) *)
let rec tc_stm ret_type globl_context prog_context (dec, def, live) s =
  match s with
  | Declare(ptype, id, s') ->
      (match (globl_context#get_type ptype) with
      | Void -> ErrorMsg.error None "Variable cannot have void type"; raise ErrorMsg.Error
      | t -> 
          (match SM.find' id dec with
          | Some(_) -> ErrorMsg.error None "variable already declared"; raise ErrorMsg.Error
          | None -> 
              let new_prog_context = prog_context#var_declare globl_context id in
              let new_dec = SM.add id t dec in
              let (d, def', live', ts) = tc_stm ret_type globl_context new_prog_context (new_dec, def, live) s' in

              if (SS.mem id live') then (ErrorMsg.error None "Variable live at declaration"; raise ErrorMsg.Error)
              else (new_dec, SS.remove id def', SS.remove id live', T.Declare(t, id, ts))
          )
      )

  (* isin tells us if a function witht the same name is called inside and hence the id should 
      not shadow the name *)
  | Assign(id, isin, exp) ->
      let def' = SS.add id def in
      let live' = used_exp exp  in

      (match SM.find' id dec with
      | None -> ErrorMsg.error None "Variable being assigned to without declaration"; raise ErrorMsg.Error
      | Some(t) -> 
          let temp_context = 
              if (isin) then prog_context#add_func_back id else prog_context
          in

          let texp = tc_exp globl_context temp_context dec exp in
          
          if (fst texp = t) then (dec, def', live', T.Assign(id, texp))
        else (ErrorMsg.error None "Types dont match for assignment"; raise ErrorMsg.Error)
      )

  | If(exp, s1, s2) ->
      let texp = tc_exp globl_context prog_context dec exp in
      (match (fst texp) with
      | Bool -> 
          let (_, def1, live1, ts1) = tc_stm ret_type globl_context prog_context (dec, def, live) s1 in
          let (_, def2, live2, ts2) = tc_stm ret_type globl_context prog_context (dec, def, live) s2 in
          let used = used_exp exp in
          let def' = SS.inter def1 def2 in
          let live' = SS.union used (SS.union live1 live2) in
          (dec, def', live', T.If(texp, ts1, ts2))
      | _ -> ErrorMsg.error None "Expression in If not Bool type"; raise ErrorMsg.Error
    )

  | While(exp, s') -> 
    let texp = tc_exp globl_context prog_context dec exp in
    (match (fst texp) with
    | Bool ->
        let used = used_exp exp in
        let (_, _, live', ts) = tc_stm ret_type globl_context prog_context (dec, def, live) s' in
        let live'' = SS.union used live' in
        (dec, def, live'', T.While(texp, ts))
    | _ -> ErrorMsg.error None "Expression in While not Bool type"; raise ErrorMsg.Error
    )

  | Assert(exp) ->
       let texp = tc_exp globl_context prog_context dec exp in
        if (fst texp != Bool) then (ErrorMsg.error None "Assert statement has non-Bool expression"; raise ErrorMsg.Error)
      else
        let live' = used_exp exp in
        let _ = SS.iter (check_def def) live' in
        (dec, def, live', T.Assert(texp))

  | Return(exp) -> 
      let texp = tc_exp globl_context prog_context dec exp in
      if (fst texp != ret_type) then
        (ErrorMsg.error None "Return type invalid"; raise ErrorMsg.Error)
      else

      let live' = used_exp exp in
      let _ = SS.iter (check_def def) live' in
      (* put all declared variables in def *)
      let def' = SS.union def (dec_set dec) in
      (dec, def', live', T.Return(texp))


  | Seq (s1, s2) ->
    let (_, def1, live1, ts1) = tc_stm ret_type globl_context prog_context (dec, def, live) s1 in
    let (_, def2, live2, ts2) = tc_stm ret_type globl_context prog_context (dec, def1, live1) s2 in
    let live' = SS.union live1 (SS.diff live2 def1) in
    (dec, def2, live', T.Seq(ts1, ts2))

  | Expr(exp) ->
    let (t, e) = tc_exp globl_context prog_context dec exp in
    let live' = used_exp exp in
    (dec, def, live', T.Expr((t, e)))

  | Nop -> (dec, def, live, T.Nop)

  | ReturnVoid ->
      if (ret_type != Void) then (ErrorMsg.error None "Return is not void"; raise ErrorMsg.Error)
    else
      let def' = SS.union def (dec_set dec) in
      (dec, def', live, T.ReturnVoid)


(* TODO: this should return the context to be used, adding the variables in *)
(* creates the starting definition and declaration sets for a function*)
let get_func_start plist global_context =
  let init_pc = new progContext global_context#get_dec_set in

  let fold_help (dec, def, c) (ptype, id) = 
    let t = global_context#get_type ptype in
    if (SS.mem id def) then (ErrorMsg.error None "multiple params with same name in function"; raise ErrorMsg.Error)
    else
    (SM.add id t dec, SS.add id def, c#add_arg id)
  in
    List.fold_left fold_help (SM.empty, SS.empty, init_pc) plist


let rec return_check s = 
  match s with
  | Declare(id, t, s') -> return_check s'
  | Assign(id, isin, e) -> false
  | If(e, s1, s2) -> (return_check s1) && (return_check s2)
  | While(e, s') -> false
  | Return(e) -> true
  | Nop -> false
  | Seq(s1, s2) -> (return_check s1) || (return_check s2)
  | Expr(e) -> false
  | ReturnVoid -> true
  | Assert(e) -> false

(* prepends a _c0_ to name if not already there *)
let mangle_name fname = 
  if (Symbol.name fname = "main") then Symbol.symbol "_c0_main"
  else if (Symbol.name fname = "_c0_main") then Symbol.symbol "_c0__c0_main"

  else if (String.length (Symbol.name fname) > 4)
    && (String.sub (Symbol.name fname) 0 4 = "_c0_") then fname 
  else Symbol.symbol (String.concat "" ["_c0_"; (Symbol.name fname)])


let rec tc_topstm is_header gc top = 
  match top with
  | [] -> ([], gc)

  | t::rest -> 
      (match t with

      | Typedef(ptype, id) -> 
          let gc' = gc#add_typedef id ptype in
            let (lst, gc'')  = tc_topstm is_header gc' rest in
            (T.Typedef(ptype, id) :: lst, gc'')

      | Fdecl(ptype, fname, plist) ->

          (* add mapping from function to new name *)
          let () = if (is_header) then 
                      mangled_name_holder#add_mangled_name fname fname
                    else mangled_name_holder#add_mangled_name fname (mangle_name fname)
          in

          (* if its a header declaration then add it as defined *)
          let gc' = if (is_header) then gc#add_def fname else gc

          in

          let gc'' = gc'#add_fundecl fname ptype plist in

          let (lst, ret_gc) = tc_topstm is_header gc'' rest in
          (T.Fdecl(ptype, fname, plist) :: lst , ret_gc)

      (* have to add the variables to the context *)
      | Fdefn(ptype, fname, plist, s') ->

          let () = if (is_header) then 
                      mangled_name_holder#add_mangled_name fname fname
                    else mangled_name_holder#add_mangled_name fname (mangle_name fname)

          in

          (* add function to set of defiend functions *)
          let gc' = gc#add_fundefn fname ptype plist in
          let gc'' = gc'#add_def fname in

          let (decmap, defset, prog_context) = get_func_start plist gc'' in

          let f_ret_type = fst (gc''#get_functype fname) in

          let does_ret = return_check s' in

          let _ = (match f_ret_type with
                    | Void -> ()
                    | _ -> if (not does_ret) then (ErrorMsg.error None "Function does not return"; raise ErrorMsg.Error)
                            else ()
                  )
        in

          let (_,_,_,typed_s) = tc_stm f_ret_type gc'' prog_context (decmap, defset, SS.empty) s' in

          let (lst, ret_gc) = tc_topstm is_header gc'' rest in
          (T.Fdefn(ptype, fname, plist, typed_s) :: lst, ret_gc)
      )


let check (pre_tree, t) = 
  (* by default add in call to main to ensure it is defined *)
  let () = called_tracker#add_call (Symbol.symbol "main") in
  let () = mangled_name_holder#add_mangled_name (Symbol.symbol "main") (Symbol.symbol "_c0_main") in

  let glob_context = new globalContext in

  let (pre_typed_prog, gc) = tc_topstm true glob_context pre_tree in

  let (final_typed_prog, gc') = tc_topstm false gc t in

  (* check that every function called was also defined *)
  if (SS.subset called_tracker#get_call_set gc'#get_def_set) then 
    (final_typed_prog, mangled_name_holder#get_mangled_map)
  else
    (ErrorMsg.error None "Some function called but not defined"; raise ErrorMsg.Error)









