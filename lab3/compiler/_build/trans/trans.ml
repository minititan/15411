open TypedAst
module A = Ast
module T = Tree
module SM = Symbol.Map

let nameMap = 
object
  val mutable nmap = SM.empty

  method addMap m = nmap <- m
  method getName fname = SM.find fname nmap
end

let get_exp e = snd e

let get_res_temp st = 
	match st with
	| None -> Temp.create()
  | Some(stmp) -> stmp

let op_to_binop op = 
	(match op with
	| A.PLUS -> T.ADD
 	| A.MINUS -> T.SUB
 	| A.TIMES -> T.MUL
 	| A.DIVIDEDBY -> T.DIV
 	| A.MODULO -> T.MOD 
 	| A.BITAND -> T.BAND
  | A.BITOR -> T.BOR
  | A.SAR -> T.SAR
  | A.SAL -> T.SAL
  | A.XOR -> T.XOR
  | _ -> ErrorMsg.error None ("Non binop encountered!"); raise ErrorMsg.Error)

let op_to_unop op = 
	(match op with
	| A.NEGATIVE -> T.NEG
  | A.BITNOT -> T.BNOT
  | _ -> ErrorMsg.error None ("Non unop encountered!"); raise ErrorMsg.Error)

let op_to_cmpop op = 
	(match op with
	| A.EQUALS -> T.EQ 
	| A.NOTEQUALS -> T.NOTEQ 
	| A.GREATER -> T.GREATER 
	| A.GREATEREQUAL -> T.GREATEREQ 
	| A.LESS -> T.LESS 
	| A.LESSEQUAL -> T.LESSEQ
	| _ -> ErrorMsg.error None ("Non cmpop encountered!"); raise ErrorMsg.Error)

(* returns sequence of commands that jumps to l1 if true and l2 if false *)
let rec bool_jmp env exp l1 l2 =
  match exp with
  | Var(id) -> 
    (match SM.find' id env with
    | Some(t) -> [T.If(T.Cmp(T.NOTEQ, T.Temp(t), T.Const(Int32.of_int 0)), l1, l2)]
    | None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
    )

  | BinopExp(op, te1, te2) -> 
    (match op with
    | A.LOGICAND ->
      (* optimizing on the values *)
      (match (get_exp te1, get_exp te2) with
      | (_, False) | (False, _) -> [T.Goto(l2)]
      | (True, True) -> [T.Goto(l1)]

      | _ -> 
          let l = Label.create() in
          let (c1, c2) = (bool_jmp env (get_exp te1) l l2, bool_jmp env (get_exp te2) l1 l2) in
          c1 @ (T.Label(l)::c2) 
      )

    | A.LOGICOR -> 
      (* optimizing on values *)
      (match (get_exp te1, get_exp te2) with
        | (_, True) | (True, _) -> [T.Goto(l1)]
        | (False, False) -> [T.Goto(l2)]

        | _ ->
            let l = Label.create() in
            let (c1, c2) = (bool_jmp env (get_exp te1) l1 l, bool_jmp env (get_exp te2) l1 l2) in
            c1 @ (T.Label(l)::c2)
      )

    | A.EQUALS | A.NOTEQUALS | A.GREATER | A.GREATEREQUAL | A.LESS | A.LESSEQUAL ->
      let ((c1, v1), (c2, v2)) = (trans_exp env te1 None, trans_exp env te2 None) in
      let cop = op_to_cmpop op in
      c1 @ (c2 @ [T.If(T.Cmp(cop, v1, v2), l1, l2)])

    | _ -> ErrorMsg.error None ("Boolean binop not found"); raise ErrorMsg.Error
    )

  | UnopExp(op, te1) -> 
    (match op with
    | A.LOGICNOT -> bool_jmp env (get_exp te1) l2 l1
    | _ -> ErrorMsg.error None ("Boolean unop not found"); raise ErrorMsg.Error
    )

  | True -> [T.Goto(l1)]
  | False -> [T.Goto(l2)]

  | Condition(te1, te2, te3) ->
    let lt = Label.create() in
    let lf = Label.create() in
    let (c1, c2, c3) = (bool_jmp env (get_exp te1) lt lf,
              bool_jmp env (get_exp te2) l1 l2,
              bool_jmp env (get_exp te3) l1 l2) in

    c1 @ (T.Label(lt)::c2) @ (T.Label(lf)::c3)

  | ConstExp _ -> ErrorMsg.error None ("ConstExp in boolean type"); raise ErrorMsg.Error

  | Call(id, texp_list) ->
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp env ex None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          let res_t = Temp.create() in
          arg_create_cmds @ [T.Mov(T.Temp(res_t), T.Call(nameMap#getName id, arg_temp_list)); 
                              T.If(T.Cmp(T.NOTEQ, T.Temp(res_t), T.Const(Int32.of_int 0)), l1, l2) ]



(* returns a  pair of (command list, imm containing expression value) *)
and trans_exp env e store = 
  match e with 
  | (t, exp) -> 
    (match t with
    | A.Bool -> 
	(
	  match exp with
      | TypedAst.True ->
      	(match store with
      	| None -> ([], T.Const(Int32.of_int 1))
      	| Some(t) -> ([T.Mov(T.Temp(t), T.Const(Int32.of_int 1))], T.Temp(t))
      	)

      | TypedAst.False -> 
      		(match store with
      			| Some(tmp) -> ([T.Mov(T.Temp(tmp), T.Const(Int32.of_int 0))], T.Temp(tmp))
      			| None -> ([], T.Const(Int32.of_int 0))
      		)

      | Var(id) -> 
      		(match SM.find' id env with
    			| Some(t) -> 
    				(match store with 
    				| None -> ([], T.Temp(t))
    				| Some(tmp) -> ([T.Mov(T.Temp(tmp), T.Temp(t))], T.Temp(tmp))
    				)

    			| None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
    			)

      | Call(id, texp_list) -> 
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp env ex None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          let res_t =  get_res_temp store in
          (arg_create_cmds @ [T.Mov(T.Temp(res_t), T.Call(nameMap#getName id, arg_temp_list))], T.Temp(res_t))

      | UnopExp(A.LOGICNOT,(A.Bool,UnopExp(A.LOGICNOT,e'))) -> trans_exp env e' None

      | _ -> 
      	let tmp = get_res_temp store in
        let l1  = Label.create() in
        let l2 = Label.create() in
        let l3 = Label.create() in
      
        let cmds = bool_jmp env exp l1 l2 in
          ((cmds @ [T.Label(l1); T.Mov(T.Temp(tmp), T.Const(Int32.of_int 1)); T.Goto(l3);
                T.Label(l2); T.Mov(T.Temp(tmp), T.Const(Int32.of_int 0)); T.Goto(l3);  T.Label(l3)]), 
           T.Temp(tmp))
    )
    
    | A.Void -> ErrorMsg.error None ("Non-statement expression hs void type?"); raise ErrorMsg.Error

    | A.Int -> 
      (match exp with
      | Var(id) ->  
        (match (SM.find' id env) with
        | Some(tmp) -> 
          (match store with
          | None -> ([], T.Temp(tmp))
          | Some(t) -> ([T.Mov(T.Temp(t), T.Temp(tmp))], T.Temp(t))
          )
          
        | None -> ErrorMsg.error None ("Variable to temp mapping not found"); raise ErrorMsg.Error
        )

      | ConstExp(i) -> 
        (match store with
        | None -> ([], T.Const(i))
        | Some(t) -> ([T.Mov(T.Temp(t), T.Const(i))], T.Temp(t))
        )

      | BinopExp(op, e1, e2) -> 
        let ((c1, e_res1), (c2, e_res2), con_op) = 
          (match op with
          | A.DIVIDEDBY | A.MODULO -> 
            let divtemp = Temp.create() in 
              (trans_exp env e1 None, trans_exp env e2 (Some divtemp), op_to_binop op)

          | _ -> 
              (trans_exp env e1 None, trans_exp env e2 None, op_to_binop op) 
          )

        in

        let new_t = get_res_temp store in
        let new_comm = [T.Mov(T.Temp(new_t), T.Binop(con_op, e_res1, e_res2))] in
        ((c1 @ c2) @ new_comm, T.Temp(new_t))

      | UnopExp(A.NEGATIVE,(A.Int,UnopExp(A.NEGATIVE,e'))) -> trans_exp env e' store

      | UnopExp(op, e') -> 
        let (cl, res) = trans_exp env e' None in
        let uop = op_to_unop op in
        let res_t = get_res_temp store in

        (cl @ [T.Mov(T.Temp(res_t), T.Unop(uop, res))], T.Temp(res_t))

      | Condition(e1, e2, e3) ->        
        let l1 = Label.create() in
        let l2  = Label.create() in
        let l3 = Label.create() in

        let t_res = get_res_temp store in

        let c1 = bool_jmp env (get_exp e1) l1 l2 in
        let (c2, t2) = trans_exp env e2 None in
        let (c3, t3) = trans_exp env e3 None in

        (c1 @ ([T.Label(l1)] @ c2 @ [T.Mov(T.Temp(t_res), t2); T.Goto(l3)]) @
           ([T.Label(l2)] @ c3 @ [T.Mov(T.Temp(t_res), t3); T.Goto(l3)]) @ 
           [T.Label(l3)], 
        T.Temp(t_res))


      | Call(id, texp_list) -> 
          let t_res = get_res_temp store in
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp env ex None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in

          (arg_create_cmds @ [(T.Mov(T.Temp(t_res), T.Call(nameMap#getName id, arg_temp_list)))], T.Temp(t_res))


      | True | False -> ErrorMsg.error None ("Boolean type in Int expression"); raise ErrorMsg.Error
      )
    )

(* returns a sequence of commands appended to the accumulated commands (in acc) *)
let rec trans_stmt env s acc = 
  match s with 
  | Declare(t, id, s') ->  let tmp = Temp.create() in
              let new_env = (SM.add id tmp env) in
                acc @ (trans_stmt new_env s' [])

  | Assign(id, exp) -> 
    let t = (match (SM.find' id env) with
        | Some(tmp) -> tmp
        | None -> ErrorMsg.error None ("Cannot find mapping for variable to temp in assignment"); raise ErrorMsg.Error
        )
    in
    
    let (cmds, res) = trans_exp env exp (Some t) in
      (acc @ cmds)

  | Seq(s1, s2) -> let (t_s1, t_s2) = (trans_stmt env s1 [], trans_stmt env s2 []) in
            acc @ (t_s1 @ t_s2)

  | Return(exp) -> let (cmds, res) = trans_exp env exp None in
            acc @ (cmds @ [T.Return(res)])

  | ReturnVoid -> acc @ [T.ReturnVoid]

  | Nop -> acc

  | Expr(exp) -> 
     (match exp with
      (* void function call *)
      | (A.Void, Call(id, texp_list)) -> 
          let arg_temp_list = List.map (fun ex -> 
                                          trans_exp env ex None ) texp_list in

          let arg_create_cmds = List.fold_left (fun l (c, t) -> l @ c) [] arg_temp_list in
          let arg_temp_list = List.map (fun (c, t) -> t) arg_temp_list in
          acc @ arg_create_cmds @ [T.VoidCall(nameMap#getName id, arg_temp_list)]

      | _ ->     
          let t = Temp.create() in 
            let (cmds, res) = trans_exp env exp (Some t) in
            acc @ cmds
      )

  | If(bool_exp, s1, s2) -> 
            let l1 = Label.create() in
                let l2 = Label.create() in
                let b_cmds = bool_jmp env (get_exp bool_exp) l1 l2 in

                (* eliminate unnecessary evaluation of statemnet branch if possible *)
               (match b_cmds with 
                | [T.Goto(l_togo)] when (l_togo = l1) -> 
                            let c1 = trans_stmt env s1 [] in
                            acc @ c1
                
                | [T.Goto(l_togo)] when (l_togo = l2) ->
                          let c2 = trans_stmt env s2 [] in
                          acc @ c2

                | _ -> 
                    let (c1, c2) = (trans_stmt env s1 [], trans_stmt env s2 []) in
                    let l3 = Label.create() in
                    acc @ b_cmds @ (T.Label(l1)::c1 @ [T.Goto(l3)]) @
                         T.Label(l2)::c2 @ ([T.Goto(l3)] @ [T.Label(l3)])
                )


  | While(b_exp, s') -> let l1 = Label.create() in
              let l2 = Label.create() in
              let ll = Label.create() in

              let b_cmds = bool_jmp env (get_exp b_exp) l1 l2 in

              (match b_cmds with
              | [T.Goto(l_togo)] when l_togo = l2 -> acc
              
              | _ -> 
                    let cmds = trans_stmt env s' [] in
                    acc @ ([T.Label(ll)] @ (b_cmds @ ([T.Label(l1)] @ (cmds @ [T.Goto(ll); T.Label(l2)]))))

              )


  | Assert(exp) -> let l1 = Label.create() in
                    let l2 = Label.create() in
                    let cmds = bool_jmp env (get_exp exp) l1 l2 in
                    acc @ cmds @ [T.Label(l2); T.VoidCall(Symbol.symbol "abort", []); T.Label(l1)]


let translate_func id_map s = 
  trans_stmt id_map s [T.Label(Label.create())] 


(* map each argument id to a temp and return a list of arguments as temps *)
let build_arg_map args = 
  List.fold_left (fun (m, lst) (t, id) -> 
                      let t = Temp.create() in
                        (SM.add id t m, lst @ [t]) ) (SM.empty, []) args

let rec translate_help l = 
  match l with
  | topstm::l' -> 
      (match topstm with
      | Fdefn(t, id, pms, s) ->
        let temp_arg_map, temp_arg_list = build_arg_map pms in
        let cmd_list = translate_func temp_arg_map s in

        (* stuff a ret at the end of a void function if its not there *)
        let cmd_list' = 
          (if (t = A.Void) then
              (match List.nth cmd_list ((List.length cmd_list) -1) with
                | T.ReturnVoid -> cmd_list
                | T.Return(e) -> cmd_list
                | _ -> cmd_list @ [T.ReturnVoid]
              )
          else cmd_list)

        in

        (t, nameMap#getName id, temp_arg_list, cmd_list')::(translate_help l')

      | Fdecl(t, id, pms) -> translate_help l'

      | Typedef(t, id) -> translate_help l'
    )

  | [] -> []

let translate (l, name_map) = 
  let () = nameMap#addMap name_map in
  translate_help l


