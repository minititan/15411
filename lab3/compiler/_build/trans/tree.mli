type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * exp * exp
  | Unop of unop * exp
  | Call of Symbol.symbol * (exp list)

type cmp = 
  | Cmp of cmpop * exp * exp

type command = 
  | Mov of exp * exp
  | VoidCall of Symbol.symbol * (exp list)
  | If of cmp * Label.label * Label.label (* if true goto Label.label *)
  | Goto of Label.label
  | Label of Label.label
  | Return of exp
  | ReturnVoid

type func = 
  Ast.primtype * Symbol.symbol * Temp.temp list * command list

(* Start line *)
type irprogram = func list

module type PRINT =
  sig
    val pp_unop : unop -> string
    val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_function : func -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT

