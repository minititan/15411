type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * exp * exp
  | Unop of unop * exp
  | Call of Symbol.symbol * (exp list)

type cmp = 
  | Cmp of cmpop * exp * exp

type command = 
  | Mov of exp * exp
  | VoidCall of Symbol.symbol * (exp list)
  | If of cmp * Label.label * Label.label (* if true goto Label.label *)
  | Goto of Label.label
  | Label of Label.label
  | Return of exp
  | ReturnVoid

type func = 
  Ast.primtype * Symbol.symbol * Temp.temp list * command list

(* Start line *)
type irprogram = func list

module type PRINT =
  sig
  	val pp_unop : unop -> string
  	val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_function : func -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT =
  struct
  	let pp_binop op = 
  		match op with
  		| ADD -> "+"
		  | SUB -> "-"
		  | MUL -> "*"
		  | DIV -> "/"
		  | MOD -> "%"
		  | BAND -> "&"
		  | BOR -> "|"
		  | SAR -> ">>"
		  | SAL -> "<<"
		  | XOR -> "^"


	  let pp_unop op = 
		  match op with
			| NEG -> "-"
		  | BNOT -> "~"

    let pp_cmpop op = 
    match op with
      | EQ -> "=="
      | NOTEQ -> "!="
      | GREATER -> ">"
      | GREATEREQ -> ">="
      | LESS -> "<"
      | LESSEQ -> "<="

  	let rec pp_exp e = 
  		match e with
      | Const(i) -> Int32.to_string i
      | Temp(t) -> Temp.name t 
  	 	| Binop(bop, i1, i2) -> (pp_exp i1) ^ " " ^ (pp_binop bop) ^ " " ^ (pp_exp i2)
  	 	| Unop(uop, i1) -> (pp_unop uop) ^ " " ^ (pp_exp i1)
      | Call(id, explist) -> "Call( " ^ (Symbol.name id) ^ ", [" ^ 
                              (List.fold_left (fun s e -> s ^ pp_exp e ^ ",") "" explist) ^ "])"

  	let pp_cmp c = 
  		match c with
  		| Cmp(cop, i1, i2) -> (pp_exp i1) ^ " " ^ (pp_cmpop cop) ^ " " ^ (pp_exp i2)

  	let pp_command c = 
  		match c with
  		| Mov(e1, e2) -> (pp_exp e1) ^ " <- " ^ (pp_exp e2) ^ "\n"
  		| If(c, l1, l2) -> "If (" ^ (pp_cmp c) ^ ") " ^ "Goto " ^ (Label.name l1) ^ " Else Goto " ^ (Label.name l2) ^ "\n"
  		| Goto(l) -> "Goto " ^ (Label.name l) ^ "\n"
  		| Label(l) -> Label.name l ^ ":" ^ "\n"
  		| Return(e) -> "Ret " ^ (pp_exp e) ^ "\n"
      | ReturnVoid -> "RetVoid\n"
      | VoidCall(id, explist) -> "VoidCall( " ^ Symbol.name id ^ ", [" ^ 
                              (List.fold_left (fun s e -> s ^ ", " ^ pp_exp e) "" explist) ^ "])\n"

  	let pp_function = function
      | (t, id, tlist, lst) ->
        let rec pp_funcbody l = 
          (match l with
          | [] -> ""
          | instr::instrs' -> pp_command instr ^ pp_funcbody instrs'
          )

        in

        let func_str = pp_funcbody lst in
        let arg_temps = "(" ^ List.fold_left (fun s t -> s ^ Temp.name t ^ ",") "" tlist ^ ")" in

        Symbol.name id ^ arg_temps ^  " { " ^ func_str ^ "}\n\n"

    let pp_program p = 
      List.fold_left (fun s f -> s ^ pp_function f) "" p

  	end





