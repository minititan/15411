(* L1 Compiler
 * Parse Tree
 * Author: Alex Vaynberg
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Forward compatible fragment of C0
 *
 * Adapted AST to Parse Tree: Ned Williamson
 *)

type ident = Symbol.symbol

type asop =
  | ASSIGN
  | PLUSEQ
  | MINUSEQ
  | STAREQ
  | SLASHEQ
  | PERCENTEQ
  | BITANDEQ
  | BITOREQ
  | XOREQ
  | SAREQ
  | SALEQ

type postop =
  | PLUSPLUS
  | MINUSMINUS

type oper =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDEDBY
  | MODULO
  | NEGATIVE                     (* unary minus *)
  | BITAND
  | BITOR
  | BITNOT
  | SAR
  | SAL
  | XOR
  | LOGICAND
  | LOGICOR
  | LOGICNOT
  | EQUALS
  | NOTEQUALS
  | GREATER
  | GREATEREQUAL
  | LESS
  | LESSEQUAL

type lvalue = Id of ident

type arg_list_follow =
  | ALF of (exp * arg_list_follow) option
and arg_list = AL of (exp * arg_list_follow) option
and exp =
  | ConstExp of Int32.t
  | True
  | False
  | Var of ident
  | UnopExp of oper * exp
  | BinopExp of oper * exp * exp
  | Condition of exp * exp * exp
  | Call of ident * arg_list

type primtype =
  | INT
  | BOOL
  | PrId of ident
  | VOID

type decl =
  | Decl of primtype * ident
  | DeclAsn of primtype * ident * exp

type simp =
  | SimpAsop of lvalue * asop * exp
  | SimpPostop of lvalue * postop
  | SimpDecl of decl
  | SimpExp of exp

type stmt =
  | StmSimp of simp
  | StmControl of control
  | StmBlock of block
and control =
  | If of exp * stmt * elseopt
  | While of exp * stmt
  | For of simpopt * exp * simpopt * stmt
  | Return of exp
  | ReturnVoid
  | Assert of exp
and block = Block of stmts
and elseopt = ElseOpt of stmt option
and simpopt =
  | SimpOpt of simp option
and stmts =
  | Seq of (stmt * stmts) option

type param = Param of primtype * ident

type param_list_follow = PLF of (param * param_list_follow) option

type param_list = PL of (param * param_list_follow) option

type gdecl =
  | Fdecl of primtype * ident * param_list
  | Fdefn of primtype * ident * param_list * block
  | Typedef of primtype * ident

type program = Program of (gdecl * program) option