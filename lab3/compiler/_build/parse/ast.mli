(* L1 Compiler
 * Abstract Syntax Trees
 * Author: Alex Vaynberg
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Forward compatible fragment of C0
 *)

type ident = Symbol.symbol

type oper =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDEDBY
  | MODULO
  | NEGATIVE                     (* unary minus *)
  | BITAND
  | BITOR
  | BITNOT
  | SAR
  | SAL
  | XOR
  | LOGICAND
  | LOGICOR
  | LOGICNOT
  | EQUALS
  | NOTEQUALS
  | GREATER
  | GREATEREQUAL
  | LESS
  | LESSEQUAL

type primtype =
  | Int
  | Bool
  | Void
  | PrId of ident

type param = primtype * ident

type params = param list

type exp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * exp * exp
  | UnopExp of oper * exp 
  | Condition of exp * exp * exp
  | Call of ident * args
  | True
  | False
and stm =
  | Declare of primtype * ident * stm
  | Assign of ident * bool * exp
  | If of exp * stm * stm
  | While of exp * stm
  | Assert of exp
  | Return of exp
  | ReturnVoid
  | Seq of stm * stm
  | Expr of exp
  | Nop
and args = exp list

type gdecl =
  | Fdefn of primtype * ident * params * stm
  | Fdecl of primtype * ident * params
  | Typedef of primtype * ident

type program = gdecl list

(* print as source, with redundant parentheses *)
module type PRINT =
  sig
    val pp_exp : exp -> string
    val pp_stm : stm -> string
    val pp_program : program -> string
  end

module Print : PRINT
