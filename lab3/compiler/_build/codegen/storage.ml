type reg =
  | EAX (* 32 bit *)
  | AL (* 8 bit version of eax *)
  | EBX
  | ECX
  | CL (* 8 bit version of ECX *)
  | EDX
  | EBP
  | ESP
  | ESI
  | EDI
  | R8D
  | R9D
  | R10D
  | R11D
  | R12D
  | R13D
  | R14D
  | R15D
  | RAX (* 64 bit *)
  | RBX
  | RCX
  | RDX
  | RBP
  | RSP
  | RSI
  | RDI
  | R8
  | R9
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15

exception TranslateByte

let reg_to_64 = function
  | EAX | RAX -> RAX
  | EBX | RBX -> RBX
  | ECX | RCX -> RCX
  | EDX | RDX -> RDX
  | EBP | RBP -> RBP
  | ESP | RSP -> RSP
  | ESI | RSI -> RSI
  | EDI | RDI -> RDI
  | R8D | R8 -> R8
  | R9D | R9 -> R9
  | R10D | R10 -> R10
  | R11D | R11 -> R11
  | R12D | R12 -> R12
  | R13D | R13 -> R13
  | R14D | R14 -> R14
  | R15D | R15 -> R15
  | CL | AL -> raise TranslateByte

let reg_to_32 = function
  | EAX | RAX -> EAX
  | EBX | RBX -> EBX
  | ECX | RCX -> ECX
  | EDX | RDX -> EDX
  | EBP | RBP -> EBP
  | ESP | RSP -> ESP
  | ESI | RSI -> ESI
  | EDI | RDI -> EDI
  | R8D | R8 -> R8D
  | R9D | R9 -> R9D
  | R10D | R10 -> R10D
  | R11D | R11 -> R11D
  | R12D | R12 -> R12D
  | R13D | R13 -> R13D
  | R14D | R14 -> R14D
  | R15D | R15 -> R15D
  | CL | AL -> raise TranslateByte

type offset = int

type str = REG of reg | STACK of offset

let format_reg = function
  | EAX -> "%eax"
  | EBX -> "%ebx"
  | ECX -> "%ecx"
  | CL -> "%cl"
  | AL -> "%al"
  | EDX -> "%edx"
  | EBP -> "%ebp"
  | ESP -> "%esp"
  | ESI -> "%esi"
  | EDI -> "%edi"
  | R8D -> "%r8d"
  | R9D -> "%r9d"
  | R10D -> "%r10d"
  | R11D -> "%r11d"
  | R12D -> "%r12d"
  | R13D -> "%r13d"
  | R14D -> "%r14d"
  | R15D -> "%r15d"
  | RAX -> "%rax"
  | RBX -> "%rbx"
  | RCX -> "%rcx"
  | RDX -> "%rdx"
  | RBP -> "%rbp"
  | RSP -> "%rsp"
  | RSI -> "%rsi"
  | RDI -> "%rdi"
  | R8 -> "%r8"
  | R9 -> "%r9"
  | R10 -> "%r10"
  | R11 -> "%r11"
  | R12 -> "%r12"
  | R13 -> "%r13"
  | R14 -> "%r14"
  | R15 -> "%r15"

let format = function
  | REG r -> format_reg r
  | STACK o -> string_of_int o ^ "(%rsp)"
