module IR = Ir
module S = Storage
module IM = Regalloc_util.IntMap
open Llir
open Regalloc_util


exception WrongColor

let color_to_storage c =
  if (c >= 0) then raise WrongColor
  else if c < stack_min_temp then
    S.STACK(stack_min_temp - c)
  else
    S.REG(temp_to_reg c)

let get_store t storage_map =
  let c = IM.find (Temp.get_int t) storage_map in
  color_to_storage c

let munch_operand storage_map : IR.operand -> operand = function
  | IR.TEMP t ->
    (match get_store t storage_map with
    | S.STACK i -> STACK i
    | S.REG r -> REG r
    )
  | IR.REG r -> REG r
  | IR.IMM i -> IMM i
  | IR.ARGTOP i -> ARGTOP i
  | IR.ARGBOTTOM i -> ARGBOTTOM i

exception DestinationImm
exception ArginBinop

let munch_binop storage_map binop d t1 t2 =
  let t1 = munch_operand storage_map t1
  and t2 = munch_operand storage_map t2
  and d = munch_operand storage_map d in
  (match binop with
   | Tree.DIV -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EAX)]
   | Tree.MOD -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EDX)]

   | Tree.ADD ->
      if d = t2 then 
        (match t1 with
        | IMM (i) -> if (i = Int32.one) then [UNOP(INC, d)]
                    else [BINOP (ADD, d, t1)]
        | _ -> [BINOP (ADD, d, t1)] 
        )

      else if d = t1 then 
        (match t2 with
          | IMM (i) -> if (i = Int32.one) then [UNOP(INC, d)]
                        else [BINOP (ADD, d, t2)] 
          | _ -> [BINOP (ADD, d, t2)] 
        )

      else [MOV (d, t1); BINOP (ADD, d, t2)] (* split up 3 addr *)

   | Tree.SUB -> 
      if d = t2 then [BINOP (SUB, d, t1); UNOP(NEG, d)] 
      
      else if d = t1 then 
        (match t2 with
        | IMM (i) -> if (i = Int32.one) then [UNOP(DEC, d)]
                      else [BINOP (SUB, d, t2)]
        | _ -> [BINOP (SUB, d, t2)]
        )

      else [MOV (d, t1); BINOP (SUB, d, t2)]

   | Tree.MUL -> 
      if d = t2 then [BINOP (MUL, d, t1)] 
      else if d = t1 then [BINOP (MUL, d, t2)] 
      else [MOV (d, t1); BINOP (MUL, d, t2)]

   | Tree.BAND -> 
      if d = t2 then [BINOP (BAND, d, t1)] 
      else if d = t1 then [BINOP (BAND, d, t2)] 
      else [MOV (d, t1); BINOP (BAND, d, t2)]

   | Tree.BOR -> 
      if d = t2 then [BINOP (BOR, d, t1)] 
      else if d = t1 then [BINOP (BOR, d, t2)] 
      else [MOV (d, t1); BINOP (BOR, d, t2)]

   | Tree.SAR ->
   (* if d is stack
    t1 in r14
    t2 in ecx
    shift r14 by cl
    store r14 into d

    if d is reg
    t1 in d
    t2 in ecx
    shift d by cl

    if d is imm
    fail *)
      (match d with
        | STACK _ -> [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAR, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        | REG _ ->
        if d = t2 then
          [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAR, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        else
          [MOV (d, t1); MOV (REG S.ECX, t2); BINOP (SAR, d, REG S.CL)]
        | IMM _ -> raise DestinationImm
        | ARGTOP _ | ARGBOTTOM _ -> raise ArginBinop
      )

   | Tree.SAL ->
      (match d with
        | STACK _ -> [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAL, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        | REG _ ->
        if d = t2 then
          [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAL, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        else
          [MOV (d, t1); MOV (REG S.ECX, t2); BINOP (SAL, d, REG S.CL)]
        | IMM _ -> raise DestinationImm
        | ARGTOP _ | ARGBOTTOM _ -> raise ArginBinop
      )
   | Tree.XOR -> if d = t2 then [BINOP (XOR, d, t1)] else if d = t1 then [BINOP (XOR, d, t2)] else [MOV (d, t1); BINOP (XOR, d, t2)]
 )

let munch_unop storage_map u d t1 =
  let (d, t1) = (munch_operand storage_map d, munch_operand storage_map t1) in
  (match u with
  | Tree.NEG -> [MOV(d, t1); UNOP (NEG, d)]
  | Tree.BNOT -> [MOV(d, t1); UNOP (BNOT, d)]
  )

let munch_if storage_map cop o1 o2 l1 l2 =
  let o1' = munch_operand storage_map o1 in
  let o2' = munch_operand storage_map o2 in
  let cop' = (match cop with
                | Tree.EQ -> EQ
                | Tree.NOTEQ -> NOTEQ
                | Tree.GREATER -> GREATER
                | Tree.GREATEREQ -> GREATEREQ
                | Tree.LESS -> LESS
                | Tree.LESSEQ -> LESSEQ
                ) in 

              [IF (cop', o1', o2', l1); GOTO l2]

let munch_instr storage_map = function
  | IR.BINOP (b, d, t1, t2) -> munch_binop storage_map b d t1 t2
  | IR.UNOP (u, d, t1) -> munch_unop storage_map u d t1
  | IR.MOV (d, s) ->
    (let d' = munch_operand storage_map d
    and s' = munch_operand storage_map s in
    [MOV (d', s')])
  | IR.DIRECTIVE s -> [DIRECTIVE s]
  | IR.COMMENT s -> [COMMENT s]
  | IR.IF (cop, o1, o2, l1, l2) -> munch_if storage_map cop o1 o2 l1 l2
  | IR.GOTO l -> [GOTO l]
  | IR.LABEL l -> [LABEL l]
  | IR.RET -> [RET]

  | IR.CALL(fname) -> [CALL(fname)]

let block_llir irlist storage_map =
    List.flatten (List.map (munch_instr storage_map) irlist)

let func_llir blist storage_map =
  List.flatten (List.map (fun (l, lset, block) -> block_llir block storage_map) blist)

let llirgen flist (storage_map : int Regalloc_util.IntMap.t Symbol.Map.t) =
  List.map (fun (fname, blist) -> (fname, func_llir blist (Symbol.Map.find fname storage_map))) flist

