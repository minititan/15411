(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 * llir.ml based on codegen.ml
 *)

type operand =
  | IMM of Int32.t
  | REG of Storage.reg 
  | ARGTOP of int
  | ARGBOTTOM of int
  | STACK of int

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label (* if true goto Label.label *)
  | CALL of Symbol.symbol
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET

type func = Symbol.symbol * (instr list)

type program = func list

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_operand = function
    | STACK s -> "Stack (" ^ (string_of_int s) ^ ")"
    | IMM i -> Int32.to_string i
    | REG r -> Storage.format_reg r
    | ARGTOP i -> "argtop" ^ string_of_int i
    | ARGBOTTOM i -> "argbottom" ^ string_of_int i

    let pp_binop = function
    | ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "MUL"
    | BAND -> "BAND"
    | BOR -> "BOR"
    | SAR -> "SAR"
    | SAL -> "SAL"
    | XOR -> "XOR"

    let pp_unop = function
    | IDIV -> "IDIV"
    | NEG -> "NEG"
    | BNOT -> "BNOT"
    | INC -> "INC"
    | DEC -> "DEC"

    let pp_label = Label.name

    let pp_cmpop = function
    | EQ -> "EQ"
    | NOTEQ -> "NEQ"
    | GREATER -> "GT"
    | GREATEREQ -> "GTE"
    | LESS -> "LT"
    | LESSEQ -> "LTE"

    let pp_instr = function
    | BINOP (oper, op1, op2) -> pp_binop oper ^ " " ^ pp_operand op1 ^ " <- " ^ pp_operand op1 ^ "," ^ pp_operand op2
    | UNOP (oper, op1) -> pp_unop oper ^ " " ^ pp_operand op1
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ " <- " ^ pp_operand s
    | DIRECTIVE s -> "DIRECTIVE: " ^ s
    | COMMENT s -> "COMMENT: " ^ s
    | CLTD -> "CLTD"
    | RET -> "RET"
    | IF (cop, o1, o2, l) -> "IF (" ^ pp_operand o1 ^ " " ^ pp_cmpop cop ^ " " ^ pp_operand o2 ^ ") GOTO " ^ pp_label l
    | GOTO l -> "GOTO " ^ pp_label l
    | LABEL l -> "LABEL " ^ pp_label l
    | CALL id -> "CALL " ^ Symbol.name id 

    let rec pp_instrs = function
    | [] -> ""
    | instr::instrs' -> pp_instr instr ^ "\n" ^ pp_instrs instrs'

    let pp_program p =
      let sl = List.map (fun (fname, irlist) -> Symbol.name fname ^ " {\n" ^ pp_instrs irlist ^ "}\n") p in
      List.fold_left (fun s s' -> s ^ s' ^ "\n\n") "" sl
  end
