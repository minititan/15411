BEGIN {
S["LTLIBOBJS"]=""
S["LIBOBJS"]=""
S["NATIVE_DYNLINK"]="yes"
S["INCLUDEGTK2"]=""
S["OBJEXT"]=".o"
S["LIBEXT"]=".a"
S["EXE"]=""
S["OCAMLWIN32"]="no"
S["LABLGNOMECANVAS"]="no"
S["LABLGTK2"]="no"
S["OCAMLLIB"]="/usr/local/lib/ocaml"
S["OCAMLVERSION"]="4.01.0"
S["OCAMLBEST"]="opt"
S["OCAMLFIND"]="ocamlfind"
S["OCAMLWEB"]="true"
S["OCAMLDOCOPT"]="ocamldoc.opt"
S["OCAMLDOC"]="ocamldoc.opt"
S["OCAMLYACC"]="ocamlyacc"
S["OCAMLLEXDOTOPT"]="ocamllex.opt"
S["OCAMLLEX"]="ocamllex.opt"
S["OCAMLDEP"]="ocamldep"
S["OCAMLOPTDOTOPT"]="ocamlopt.opt"
S["OCAMLCDOTOPT"]="ocamlc.opt"
S["OCAMLOPT"]="ocamlopt.opt"
S["OCAMLC"]="ocamlc.opt"
S["target_alias"]=""
S["host_alias"]=""
S["build_alias"]=""
S["LIBS"]=""
S["ECHO_T"]=""
S["ECHO_N"]=""
S["ECHO_C"]="\\c"
S["DEFS"]="-DPACKAGE_NAME=\\\"\\\" -DPACKAGE_TARNAME=\\\"\\\" -DPACKAGE_VERSION=\\\"\\\" -DPACKAGE_STRING=\\\"\\\" -DPACKAGE_BUGREPORT=\\\"\\\" -DPACKAGE_URL=\\\"\\\""
S["mandir"]="${datarootdir}/man"
S["localedir"]="${datarootdir}/locale"
S["libdir"]="${exec_prefix}/lib"
S["psdir"]="${docdir}"
S["pdfdir"]="${docdir}"
S["dvidir"]="${docdir}"
S["htmldir"]="${docdir}"
S["infodir"]="${datarootdir}/info"
S["docdir"]="${datarootdir}/doc/${PACKAGE}"
S["oldincludedir"]="/usr/include"
S["includedir"]="${prefix}/include"
S["localstatedir"]="${prefix}/var"
S["sharedstatedir"]="${prefix}/com"
S["sysconfdir"]="${prefix}/etc"
S["datadir"]="${datarootdir}"
S["datarootdir"]="${prefix}/share"
S["libexecdir"]="${exec_prefix}/libexec"
S["sbindir"]="${exec_prefix}/sbin"
S["bindir"]="${exec_prefix}/bin"
S["program_transform_name"]="s,x,x,"
S["prefix"]="/usr/local"
S["exec_prefix"]="${prefix}"
S["PACKAGE_URL"]=""
S["PACKAGE_BUGREPORT"]=""
S["PACKAGE_STRING"]=""
S["PACKAGE_VERSION"]=""
S["PACKAGE_TARNAME"]=""
S["PACKAGE_NAME"]=""
S["PATH_SEPARATOR"]=":"
S["SHELL"]="/bin/sh"
  for (key in S) S_is_set[key] = 1
  FS = ""

}
{
  line = $ 0
  nfields = split(line, field, "@")
  substed = 0
  len = length(field[1])
  for (i = 2; i < nfields; i++) {
    key = field[i]
    keylen = length(key)
    if (S_is_set[key]) {
      value = S[key]
      line = substr(line, 1, len) "" value "" substr(line, len + keylen + 3)
      len += length(value) + length(field[++i])
      substed = 1
    } else
      len += 1 + keylen
  }

  print line
}

