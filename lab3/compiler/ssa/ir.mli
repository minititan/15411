(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

type binop = Tree.binop
type cmpop = Tree.cmpop
type unop = Tree.unop

type operand =
  | IMM of Int32.t
  | REG of Storage.reg
  | TEMP of Temp.temp
  | ARGTOP of int
  | ARGBOTTOM of int

type instr =
  | BINOP of binop * operand * operand * operand
  | UNOP of unop * operand * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label * Label.label (* if true goto Label.label *)
  | CALL of Symbol.symbol
  | GOTO of Label.label
  | LABEL of Label.label
  | RET

type irfunction = Symbol.symbol * instr list

type irprogram = irfunction list

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_cmpop : cmpop -> string
    val pp_instr : instr -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT



