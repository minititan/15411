

type multemp = int * int
type t = multemp

let sub_counter = Hashtbl.create 128

let from_temp t =
	let () = Hashtbl.add sub_counter t 1 in
	(t, 0)

let compare (a, a') (b, b') = 
	a = b && a' = b'

let get_next_subtemp t = 
	match t with
	| (t1, t2) -> 
		let curr_sub = Hashtbl.find sub_counter t1 in
		let () = Hashtbl.replace sub_counter t1 (curr_sub + 1) in
		(t1, curr_sub)

let name (t, t') = "t" ^ string_of_int t ^ "," ^ string_of_int t'

let format ff t = Format.fprintf ff "%s" (name t)

module S =
  struct
    type t = temp
    let compare = compare
    let format = format
  end

module type SET = Printable.SET with type elt = t
module Set = Printable.MakeSet(S)

module type MAP = Printable.MAP with type key = t
module Map = Printable.MakeMap(S)
		