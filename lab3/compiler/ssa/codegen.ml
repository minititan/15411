(* Goes from the transated ast tree to an intermediate representation
  consisting of straight line instructions. This is then converted to a block
  format later for optimizations.
 *)

module T = Tree
module S = Storage
module I = Ir

let get_operand exp =
  match exp with
  | T.Const(i) -> I.IMM(i)
  | T.Temp(t) -> I.TEMP(t)
  | T.Call(id, elist) -> ErrorMsg.error None ("Cannot have call in operand"); raise ErrorMsg.Error
  | T.Binop _ | T.Unop _ -> ErrorMsg.error None ("Cannot match expression to operand"); raise ErrorMsg.Error

let rec munch_exp e d =
  match e with
  | T.Const(i) -> [I.MOV(d, get_operand e)]
  | T.Temp(t) -> [I.MOV(d, get_operand e)]
  | T.Binop(op, e1, e2) -> [I.BINOP(op, d, get_operand e1, get_operand e2)]
  | T.Unop(op, e1) -> [I.UNOP(op, d, get_operand e1)]
  | T.Call(id, elist) -> 
      let irlst = List.flatten (List.mapi (fun i e -> 
                                              let movein = I.ARGBOTTOM(i) in
                                                munch_exp e movein) elist) in
      irlst @ [I.CALL(id); I.MOV(d, I.REG(S.EAX))]

let munch_command c =
  match c with
  | T.Mov(e1, e2) -> munch_exp e2 (get_operand e1)

  | T.If(T.Cmp(cmpop, e1, e2), l1, l2) -> [I.IF(cmpop, get_operand e1, get_operand e2, l1, l2)]

  | T.Goto(l) -> [I.GOTO(l)]

  | T.Label(l) -> [I.LABEL(l)]

  | T.ReturnVoid -> [I.RET]

  | T.VoidCall(id, elist) -> 
        let irlst = List.flatten (List.mapi (fun i e -> 
                                                munch_exp e (I.ARGBOTTOM i) ) elist) in
        irlst @ [I.CALL(id)]

  | T.Return(e) -> 
      let d = I.REG(S.EAX) in
      munch_exp e d @ [I.RET]


let create_arg_moves temp_arg_list = 
  List.mapi (fun i t -> I.MOV((I.TEMP t), (I.ARGTOP i))) temp_arg_list

let munch_func' (t, id, temp_arg_list, cmdlist) = 
  let arg_moves = create_arg_moves temp_arg_list in
  match cmdlist with
  | cmd::restcmds -> (id, (munch_command cmd) @ arg_moves @ (List.flatten (List.map (munch_command) restcmds)) )
  | [] -> (id, [])

let hack_operand num_args offset_opt = function
  | I.IMM i -> I.IMM i
  | I.REG r -> I.REG r
  | I.TEMP t ->
    let t_value = Temp.get_int t in
    (match offset_opt with
     | Some offset when (t_value-offset) > 6 && (t_value-offset) < num_args ->
         I.ARGTOP (t_value-offset) (* hack goes here *)
     | _ -> I.TEMP t
    )
  | I.ARGTOP i -> I.ARGTOP i
  | I.ARGBOTTOM i -> I.ARGBOTTOM i

let hack_command num_args offset_opt = function
  | I.BINOP (b, t1, t2, t3) ->
    I.BINOP (b, hack_operand num_args offset_opt t1, hack_operand num_args offset_opt t2, hack_operand num_args offset_opt t3)
  | I.UNOP (u, t1, t2) -> I.UNOP (u, hack_operand num_args offset_opt t1, hack_operand num_args offset_opt t2)
  | I.MOV (t1, t2) -> I.MOV (hack_operand num_args offset_opt t1, hack_operand num_args offset_opt t2)
  | I.DIRECTIVE s -> I.DIRECTIVE s
  | I.COMMENT s -> I.COMMENT s
  | I.IF (c, t1, t2, l1, l2) -> I.IF (c, hack_operand num_args offset_opt t1, hack_operand num_args offset_opt t2, l1, l2)
  | I.CALL s -> I.CALL s
  | I.GOTO l -> I.GOTO l
  | I.LABEL l -> I.LABEL l
  | I.RET -> I.RET

let commandlist_argoffset ir =
  let get_offset = function
  | I.MOV (I.TEMP t, I.ARGTOP a) -> Some (Temp.get_int t - a)
  | _ -> None
  in
  let arg_temps = List.map get_offset ir in
  let combine x y =
  (match (x,y) with
   | (Some x, Some y) when x = y -> Some x
   | (None, Some x) -> Some x
   | (Some x, None) -> Some x
   | _ -> None
  )
  in
  List.fold_left combine None arg_temps

let hack_commandlist ir num_args =
  let offset_opt = commandlist_argoffset ir in
  List.map (hack_command num_args offset_opt) ir
  (* for i in [6,num_temps_needed-1], map temp (i+1) to i) *)

let munch_func (t, id, temp_arg_list, cmdlist) =
  let (_, original_ir) = munch_func' (t, id, temp_arg_list, cmdlist) in
  let num_args = List.length temp_arg_list in
  let hacked_ir = hack_commandlist original_ir num_args in
  (id, hacked_ir)

let codegen flist =
  List.map (munch_func) flist




