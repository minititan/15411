type ident = Ast.ident

type oper = Ast.oper

type primtype = Ast.primtype

type param = primtype * ident

type params = param list

type simpexp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * typexp * typexp
  | UnopExp of oper * typexp 
  | Condition of typexp * typexp * typexp
  | True
  | False
  | Call of ident * typexp list
and typexp = 
  primtype * simpexp
and typstm =
  | Declare of primtype * ident * typstm
  | Assign of ident * typexp
  | If of typexp * typstm * typstm
  | While of typexp * typstm
  | Return of typexp
  | Seq of typstm * typstm
  | Expr of typexp
  | Nop
  | Assert of typexp
  | ReturnVoid
and typtopstm = 
  | Fdefn of primtype * ident * params * typstm
  | Fdecl of primtype * ident * params
  | Typedef of primtype * ident

type typprogram = typtopstm list

