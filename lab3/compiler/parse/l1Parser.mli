type token =
  | EOF
  | TYPEDEF
  | IF
  | ELSE
  | WHILE
  | FOR
  | STRUCT
  | NULL
  | ALLOC
  | ALLOCARRY
  | ASSERT
  | TRUE
  | FALSE
  | BOOL
  | VOID
  | CHAR
  | STRING
  | SEMI
  | DECCONST of (Int32.t)
  | HEXCONST of (Int32.t)
  | IDENT of (Symbol.symbol)
  | RETURN
  | INT
  | PLUS
  | MINUS
  | STAR
  | SLASH
  | PERCENT
  | BITAND
  | BITOR
  | XOR
  | BITNOT
  | LOGICNOT
  | LOGICAND
  | LOGICOR
  | PLUSPLUS
  | MINUSMINUS
  | SAR
  | SAL
  | EQUALS
  | NOTEQUALS
  | LESSEQUAL
  | GREATEREQUAL
  | LESS
  | GREATER
  | QUESTION
  | COLON
  | ASSIGN
  | PLUSEQ
  | MINUSEQ
  | STAREQ
  | SLASHEQ
  | PERCENTEQ
  | BITANDEQ
  | BITOREQ
  | XOREQ
  | SAREQ
  | SALEQ
  | LBRACE
  | RBRACE
  | LPAREN
  | RPAREN
  | COMMA
  | CONTINUE
  | BREAK
  | NEGATE

val program :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Parsetree.program
