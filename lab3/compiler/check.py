import subprocess
import glob

compiler_loc = "/Users/nedwilliamson/compilers/15411/lab3/compiler/bin/l3c"

for fn in glob.glob("/Users/nedwilliamson/compilers/15411/lab3/tests0/*.l3"):
    print "-------------------------------------------------------------"
    print fn
    with open(fn, 'r') as f:
        print f.read()
    subprocess.call([compiler_loc,"--dump-ast",fn])
