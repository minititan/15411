(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(*
TODO: include unsigned jumps?
*)

module S = Storage

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADDL
  | ADDQ
  | SUBL
  | SUBQ
  | MUL
  | ANDL
  | ORL
  | SARL
  | SALL
  | XORL

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type operand =
  | IMM of Int32.t
  | REG of Storage.reg
  | STACK of int

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | CMP of operand * operand
  | JUMP of cmpop * Label.label (* conditional jump *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET
  | DIRECTIVE of string
  | COMMENT of string
  | FUNCHEADER of string
  | PUSH of operand
  | POP of operand
  | CALL of Symbol.symbol
  | AND of operand * operand

type program = instr list

(* functions that format assembly output *)

let format_reg = S.format_reg

let format_unop = function
  | IDIV -> "IDIVL"
  | NEG -> "NEGL"
  | BNOT -> "NOTL"
  | INC -> "INCL"
  | DEC -> "DECL"

let format_binop = function
  | ADDL -> "ADDL"
  | ADDQ -> "ADDQ"
  | SUBL -> "SUBL"
  | SUBQ -> "SUBQ"
  | MUL -> "IMULL" (* ?? *)
  | ANDL -> "ANDL"
  | ORL -> "ORL"
  | SARL -> "SARL"
  | SALL -> "SHLL" (* no "arirthmetic" shift left *)
  | XORL -> "XORL"

let base_reg = S.ESP

let format_storage = S.format

let format_operand ~is32 = function
  | IMM n  -> "$" ^ Int32.to_string n
  | STACK s -> string_of_int s ^ "(%rsp)"
  | REG r ->
    (match r with
    | S.AL -> "%al"
    | S.CL -> "%cl"
    | _ -> 
    (let r' = if is32 then S.reg_to_32 r else S.reg_to_64 r in
    S.format_reg r')
  )

let format_label = Label.name

let is32_binop = function
  | ADDL -> true
  | ADDQ -> false
  | SUBL -> true
  | SUBQ -> false
  | MUL -> true
  | ANDL -> true
  | ORL -> true
  | SARL -> true
  | SALL -> true
  | XORL -> true

let format = function
    BINOP (oper, d, s) ->
      let is32 = is32_binop oper in
      "\t" ^ format_binop oper
      ^ "\t" ^ format_operand ~is32 s
      ^ ", " ^ format_operand ~is32 d ^ "\n"
      (* ^ "," ^ format_operand s ^ "\n" *)
  | UNOP (oper, d) ->
      "\t" ^ format_unop oper
      ^ "\t" ^ format_operand ~is32:true d ^ "\n"
  | MOV (d, s) ->
      "\t" ^ "MOVL"
      ^ "\t" ^ format_operand ~is32:true s
      ^ ", " ^ format_operand ~is32:true d ^ "\n"

  | PUSH op -> 
      "\t" ^ "PUSHQ" 
      ^ "\t" ^ (format_operand ~is32:false op) ^ "\n"

  | POP op ->
      "\t" ^ "POPQ" 
      ^ "\t" ^ (format_operand ~is32:false op) ^ "\n"

  | CALL func ->
      "\tCALL\t" ^ Symbol.name func ^ "\n"

  | DIRECTIVE str ->
      "\t" ^ str ^ "\n"
  | COMMENT str ->
      "\t" ^ "/* " ^ str ^ "*/\n"
  | FUNCHEADER name ->
      let name' = name in
      "\t.global\t" ^ name' ^ "\n"
      ^ name' ^ ":\n"
  | CLTD ->
      "\t" ^ "CLTD" ^ "\n"
  | RET ->
      "\t" ^ "RET" ^ "\n"
  | GOTO l ->
      "\t" ^ "JMP " ^ format_label l ^ "\n"
  | LABEL l ->
      format_label l ^ ":" ^ "\n"
  | CMP (o1, o2) ->
      "\t" ^ "CMPL" ^
      "\t" ^ format_operand ~is32:true o2 ^ "," ^ format_operand ~is32:true o1 ^ "\n"
  | JUMP (cop, l) ->
    let jinst = (match cop with
      | EQ -> "JE"
      | NOTEQ -> "JNE"
      | GREATER -> "JG"
      | GREATEREQ -> "JGE"
      | LESS -> "JL"
      | LESSEQ -> "JLE") in
    "\t" ^ jinst ^ " " ^ format_label l ^ "\n"

  | AND(op1, op2) -> "\t" ^ "AND" ^ "\t" ^ format_operand ~is32:false op2 ^ "," ^ format_operand ~is32:false op1 ^ "\n"

module type PRINT =
  sig
    val pp_instr : instr -> string
    val pp_instrs : instr list -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct
    let pp_instr = format

    let rec pp_instrs = function
      [] -> ""
    | instr::instrs' -> pp_instr instr ^ pp_instrs instrs'

    let pp_program instrs = "Assembly code {\n" ^ pp_instrs instrs ^ "}\n"
  end
