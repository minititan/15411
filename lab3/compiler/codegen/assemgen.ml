module AS = Assem
module LL = Llir
module Alloc = Allocator
module S = Storage
module SM = Symbol.Map
module IM = Regalloc_util.IntMap
module IS = Regalloc_util.IntSet
open Regalloc_util


(* When the stack is used we use R15 for loading and storing to stack *)
let munch_binop = function
  | LL.ADD -> AS.ADDL
  | LL.SUB -> AS.SUBL
  | LL.MUL -> AS.MUL
  | LL.BAND -> AS.ANDL
  | LL.BOR -> AS.ORL
  | LL.SAR -> AS.SARL
  | LL.SAL -> AS.SALL
  | LL.XOR -> AS.XORL

let munch_unop = function
  | LL.IDIV -> AS.IDIV
  | LL.NEG -> AS.NEG
  | LL.BNOT -> AS.BNOT
  | LL.INC -> AS.INC
  | LL.DEC -> AS.DEC

let munch_cmpop = function
  | LL.EQ -> AS.EQ
  | LL.NOTEQ -> AS.NOTEQ
  | LL.GREATER -> AS.GREATER
  | LL.GREATEREQ -> AS.GREATEREQ
  | LL.LESS -> AS.LESS
  | LL.LESSEQ -> AS.LESSEQ

let spot_size = 8 (* 64 bit registers -> 8 *)

let stack_reg = AS.REG S.R15

let is_shift = function
  | AS.SARL | AS.SALL -> true
  | _ -> false

let is_memsafe = function
  | AS.ADDQ | AS.ADDL | AS.SUBQ | AS.SUBL | AS.ANDL | AS.XORL | AS.ORL -> true
  | _ -> false

let color_to_storage c =
  if c < stack_min_temp then
    S.STACK(stack_min_temp - c)
  else
    S.REG(temp_to_reg c)

let get_store t storage_map =
  let c = IM.find (Temp.get_int t) storage_map in
  color_to_storage c

let get_arg ~is_argbottom wholestack = function
  | 0 -> AS.REG S.EDI (* still ints *)
  | 1 -> AS.REG S.ESI
  | 2 -> AS.REG S.EDX
  | 3 -> AS.REG S.ECX
  | 4 -> AS.REG S.R8D
  | 5 -> AS.REG S.R9D
  | a -> 
      if (is_argbottom) then AS.STACK (word_size * (a - 6))
    else AS.STACK (word_size * (wholestack + 1 + a - 6))

(*exception ArgNotMove*)

(* returns a list of assem instuctions and stack space used *)
let llir_to_assem_list inst storage_map arg_stack var_stack reg_stack extra_stack epilogue = 
  let wholestack = arg_stack + var_stack + reg_stack + extra_stack in
  let unwrap_temp mov_to mov_out = function
    | LL.REG r -> AS.REG r
    | LL.IMM i -> AS.IMM i
    | LL.STACK i -> AS.STACK (word_size * (arg_stack + i - 1))
    | LL.ARGTOP a -> get_arg ~is_argbottom:false wholestack a
    | LL.ARGBOTTOM a -> get_arg ~is_argbottom:true wholestack a
  in

  let unwrap_temps is_mov (t1,t2) = 
    if (is_mov) then
    (unwrap_temp true false t1, unwrap_temp false true t2)
  else
    (unwrap_temp false false t1, unwrap_temp false false t2)
  in
  match inst with
    | LL.BINOP (oper, d, s) ->
      let oper = munch_binop oper in
      if is_memsafe oper then
      (match unwrap_temps false (d, s) with (* shift case, needs refactoring *)
          | (AS.REG r1, AS.REG r2) -> [AS.BINOP(oper, AS.REG r1, AS.REG r2)]

          | (AS.REG r, AS.STACK offset) -> [AS.BINOP(oper, AS.REG r, AS.STACK offset)]

          | (AS.STACK offset, AS.REG r) -> [AS.BINOP(oper, AS.STACK offset, AS.REG r)]

          | (AS.STACK off1, AS.STACK off2) -> [AS.MOV(stack_reg, AS.STACK off2); (* difference is in these two lines *)
                                         AS.BINOP(oper, AS.STACK off1, stack_reg)]
      
          | (AS.STACK offset, AS.IMM i) -> [AS.BINOP(oper, AS.STACK offset, AS.IMM i)]
          
          | (AS.REG r, AS.IMM i) -> [AS.BINOP(oper, AS.REG r, AS.IMM i)]

          | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )
      else if is_shift oper then
      (match unwrap_temps false (d, s) with (* shift case, needs refactoring *)
          | (AS.REG r1, AS.REG r2) -> [AS.BINOP(oper, AS.REG r1, AS.REG r2)]

          | (AS.REG r, AS.STACK offset) -> [AS.MOV(stack_reg, AS.STACK offset);
                                      AS.BINOP(oper, AS.REG r, stack_reg)]

          | (AS.STACK offset, AS.REG r) -> [AS.MOV(stack_reg, AS.STACK offset);
                                      AS.BINOP(oper, stack_reg, AS.REG r);
                                      AS.MOV(AS.STACK offset, stack_reg)]

          | (AS.STACK off1, AS.STACK off2) -> [AS.MOV(stack_reg, AS.STACK off2); (* difference is in these two lines *)
                                         AS.BINOP(oper, AS.STACK off1, stack_reg)]
                                       

          | (AS.STACK offset, AS.IMM i) -> [AS.MOV(stack_reg, AS.STACK offset);
                               AS.BINOP(oper, stack_reg, AS.IMM i);
                               AS.MOV(AS.STACK offset, stack_reg)]

          | (AS.REG r, AS.IMM i) -> [AS.BINOP(oper, AS.REG r, AS.IMM i)]
          

          | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )
    else
      (match unwrap_temps false (d, s) with (* non shift case *)
          | (AS.REG r1, AS.REG r2) -> [AS.BINOP(oper, AS.REG r1, AS.REG r2)]

          | (AS.REG r, AS.STACK offset) -> [AS.MOV(stack_reg, AS.STACK offset);
                                      AS.BINOP(oper, AS.REG r, stack_reg)]

          | (AS.STACK offset, AS.REG r) -> [AS.MOV(stack_reg, AS.STACK offset);
                                      AS.BINOP(oper, stack_reg, AS.REG r);
                                      AS.MOV(AS.STACK offset, stack_reg)]

          | (AS.STACK off1, AS.STACK off2) -> [AS.MOV(stack_reg, AS.STACK off1);
                                         AS.BINOP(oper, stack_reg, AS.STACK off2);
                                         AS.MOV(AS.STACK off1, stack_reg)]
                                       

          | (AS.STACK offset, AS.IMM i) -> [AS.MOV(stack_reg, AS.STACK offset);
                               AS.BINOP(oper, stack_reg, AS.IMM i);
                               AS.MOV(AS.STACK offset, stack_reg)]
          | (AS.REG r, AS.IMM i)  -> [AS.BINOP(oper, AS.REG r, AS.IMM i)]
      

          | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )

  | LL.MOV(d, s) ->
    (match unwrap_temps true (d, s) with

        | (AS.REG r1, AS.REG r2) -> [AS.MOV(AS.REG r1, AS.REG r2)]

        | (AS.REG r1, AS.STACK(offset)) -> [AS.MOV(AS.REG r1, AS.STACK offset)]

        | (AS.STACK offset, AS.REG r2) -> [AS.MOV(AS.STACK offset, AS.REG r2)]

        | (AS.STACK off1, AS.STACK off2) -> [AS.MOV(stack_reg, AS.STACK off2);
                                             AS.MOV(AS.STACK off1, stack_reg)]
                                         
        | (AS.STACK s, AS.IMM i) -> [AS.MOV(AS.STACK s, AS.IMM i)]
        
        | (AS.REG r, AS.IMM i) -> [AS.MOV(AS.REG r, AS.IMM i)]
        
        | (AS.IMM _, _) -> raise (Failure "cannot store into IMM for MOV")
    )

  | LL.UNOP (oper, dst) ->
    let opert = munch_unop oper in
    (match unwrap_temp false false dst with
        | AS.REG r -> [AS.UNOP(opert, AS.REG r)]
        | AS.STACK s -> [AS.UNOP(opert, AS.STACK s)]
        
        | AS.IMM _ -> raise (Failure "cannot store into IMM for UNOP")
    )

  | LL.IF (cop, o1, o2, l) -> (* CMPL is happening twice, swapped MOV *)
    let jump_inst = AS.JUMP (munch_cmpop cop, l) in
    (match unwrap_temps false (o1, o2) with
        | (AS.REG r1, AS.REG r2) -> [AS.CMP(AS.REG r1, AS.REG r2); jump_inst]

        | (AS.REG r1, AS.STACK(offset)) -> [AS.CMP(AS.REG r1, AS.STACK offset); jump_inst]

        | (AS.STACK offset, AS.REG r2) -> [AS.CMP(AS.STACK offset, AS.REG r2); jump_inst]

        | (AS.STACK off1, AS.STACK off2) -> [AS.MOV(stack_reg, AS.STACK off2);
            AS.CMP(AS.STACK off1, stack_reg); jump_inst]

        | (AS.REG r, AS.IMM i) -> [AS.CMP(AS.REG r, AS.IMM i); jump_inst]
        | (AS.STACK s, AS.IMM i) -> [AS.MOV (stack_reg, AS.STACK s); AS.CMP(stack_reg, AS.IMM i); jump_inst]
      
        | (AS.IMM i, AS.STACK s) -> [AS.MOV(stack_reg, AS.IMM i); AS.CMP(stack_reg, AS.STACK s); jump_inst]
        | (AS.IMM i, AS.REG r) -> [AS.MOV(stack_reg, AS.IMM i); AS.CMP(stack_reg, AS.REG r); jump_inst]

        | (AS.IMM i1, AS.IMM i2) -> [AS.MOV (stack_reg, AS.IMM i1); AS.CMP(stack_reg, AS.IMM i2); jump_inst]
    )
  | LL.LABEL l -> [AS.LABEL l]
  | LL.GOTO l -> [AS.GOTO l]
  | LL.CALL f -> [AS.AND(AS.REG(S.AL), AS.IMM Int32.zero); AS.CALL f]
  | LL.CLTD -> [AS.CLTD]
  | LL.RET -> epilogue @ [AS.RET]
  | LL.DIRECTIVE str -> [AS.DIRECTIVE str]
  | LL.COMMENT str -> [AS.COMMENT str] 

(*
let rec llir_to_assem max_arg_stack_used llir_list storage_map stack_set sofar = 
  match llir_list with
  | inst::rest ->
      let (stack_set', assemlist) = get_assem_inst_info inst storage_map max_arg_stack_used in
      llir_to_assem max_arg_regs_used rest storage_map
          (IM.union stack_set stack_set') sofar @ assemlist
  | [] -> (stack_set, sofar)
*)

let callee_saved_registers_as_temps =
  let intlist_to_intset int_list =
    List.fold_left (fun s x -> IS.add x s) IS.empty int_list
  in
  let callee_saved_temps =
    List.map reg_to_temp
      [S.RBX; S.RBP; S.R12; S.R13; S.R14; S.R15]
  in
  intlist_to_intset callee_saved_temps

(* returns the amount of stack space required by the largest function
 * call made in the function
 *)
let get_max_arg_stack_used llir_list =
  let rec get_max_args_used llir_list' maxsofar =
    match llir_list' with
    | ins::rest -> 
      (match ins with
      | LL.MOV (LL.ARGBOTTOM i, _) when i > maxsofar -> get_max_args_used rest i
      | _ -> get_max_args_used rest maxsofar
      )
    | [] -> maxsofar
  in
  let max_args_used = get_max_args_used llir_list 0 in
  let num_argument_registers = 5 in
  (max (max_args_used - num_argument_registers) 0)

let storage_map_to_temp_set storage_map =
  IntMap.fold
  (fun _ temp_used temp_set -> IS.add temp_used temp_set)
  storage_map
  IS.empty

let get_max_stack_spot storage_map =
  let temp_set = storage_map_to_temp_set storage_map in
  let minimum_temp =
    IS.fold
    (fun i1 i2 -> if i1 < i2 then i1 else i2)
    temp_set
    0
  in

  if (minimum_temp = 0) then 0 else
  match color_to_storage minimum_temp with
  | S.REG _ -> 0
  | S.STACK i -> i

(* Applies the register mapping to the llir and generates the relevant
 * assembly as well. Does not take care of stack manipulation assembly instructions 
 *)
let translate_llir llir_list arg_stack var_stack callee_stack align_stack storage_map epilogue = 
  let inst_llist =
    List.map
    (fun llir -> llir_to_assem_list llir storage_map arg_stack var_stack callee_stack align_stack epilogue)
    llir_list
  in
  List.concat inst_llist

let callee_registers_to_pro_ep callee_set =
  let callee_list = IS.elements callee_set in
  let registers = List.map temp_to_reg callee_list in
  let registers_64 = List.map S.reg_to_64 registers in
  let push_instructions = List.map (fun reg -> AS.PUSH (AS.REG reg)) registers_64 in
  let pop_instructions = List.rev (List.map (fun reg -> AS.POP (AS.REG reg)) registers_64) in
  (push_instructions, pop_instructions)


(* returns the number of spots needed after aligning rsp to 16 bytes *)
let get_extra_space_needed var_and_arg_spots callee_spots = 
    if ((var_and_arg_spots + callee_spots) mod 2 = 0) then 1
    else 0


(* *)
let func_to_assem llir_list storage_map =
  let storage_temps_used = storage_map_to_temp_set storage_map in
  let callee_registers_used =
    IS.inter callee_saved_registers_as_temps storage_temps_used
  in

  let num_callee_spots = IS.cardinal callee_registers_used in

  let arg_stack_spots = get_max_arg_stack_used llir_list in

  let local_var_stack_spots_used = get_max_stack_spot storage_map in

  let extra_spot = get_extra_space_needed (arg_stack_spots + local_var_stack_spots_used) num_callee_spots in

  let (alignpush, alignpop) = if (extra_spot = 0) then ([], []) else 
                                  ([AS.BINOP(AS.SUBQ, AS.REG S.RSP, AS.IMM
                                  (Int32.of_int (word_size * 1)))], 
                                      [AS.BINOP(AS.ADDQ, AS.REG S.RSP, AS.IMM
                                      (Int32.of_int (word_size * 1)))])

  in

  let space_needed = local_var_stack_spots_used + arg_stack_spots in

  let (alloc_instr,dealloc_instr) =
    if space_needed = 0 then
      ([],[])
    else
      let stack_shift = AS.IMM(Int32.of_int(space_needed * word_size)) in
      ([AS.BINOP(AS.SUBQ, AS.REG S.RSP, stack_shift)],
       [AS.BINOP(AS.ADDQ, AS.REG S.RSP, stack_shift)])
  in
  let (callee_pushes, callee_pops) = callee_registers_to_pro_ep callee_registers_used in
  let epilogue = dealloc_instr @ callee_pops @ alignpop in
  let assem_list = translate_llir llir_list 
                      arg_stack_spots local_var_stack_spots_used num_callee_spots extra_spot storage_map epilogue in

  alignpush @ callee_pushes @ alloc_instr @ assem_list

(* plan from Sharman:
 * first get the maximum argument function and based on that calculate
 * initial stack space (i think i just passed the max number of args as
 * initial stack space so subtract the number of arg regs from that)
 *
 * next recurse through the llir list and accumulate the callee-save
 * regs and stack spots along the way
 *
 * using callee-save reg info we have to append push and pop of those
 * regs before and after the assem list
 *
 * we also use stack spots to calculate total stack space as such:
 * (max_args - (number of arg regs) + stack spots used
 * by temps + no. of callee-save regs used)
 *)

let assembly_gen prog storage_map =
  let process_function (f_name,llir_list) =
    let func_storage_map = SM.find f_name storage_map in
    let func_assem = func_to_assem llir_list func_storage_map in
    (Symbol.name f_name, func_assem)
  in
  let functions_with_assem = List.map process_function prog in
  let assem_with_names =
    List.map (fun (name,ins) -> (AS.FUNCHEADER name)::ins) functions_with_assem
  in
  List.concat assem_with_names




