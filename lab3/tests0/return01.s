	.file	"../tests0/return01.l3"
	.text
	.global	_c0_sum
_c0_sum:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	MOVL	$0, %esi
L4:
	CMPL	%edi,%eax
	JLE L2
	JMP L3
L2:
	ADDL	%eax, %esi
	INCL	%eax
	JMP L4
L3:
	MOVL	%esi, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	PUSHQ	%rbx
L5:
	MOVL	$100, %ebx
	MOVL	$1, %edi
	MOVL	$100, %esi
	AND	$0,%al
	CALL	_c0_sum
	MOVL	%eax, %edi
	MOVL	%ebx, %esi
	ADDL	$1, %esi
	MOVL	%ebx, %ecx
	IMULL	%esi, %ecx
	MOVL	$2, %esi
	MOVL	%ecx, %eax
	CLTD
	IDIVL	%esi
	MOVL	%eax, %esi
	MOVL	%edi, %eax
	SUBL	%esi, %eax
	MOVL	%eax, %eax
	POPQ	%rbx
	RET
	.ident	"15-411 L3 compiler"
