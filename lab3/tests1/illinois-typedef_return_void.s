	.file	"../tests1/illinois-typedef_return_void.l3"
	.text
	.global	_c0_test
_c0_test:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %edi
	CMPL	$0,%edi
	JNE L5
	JMP L6
L5:
	ADDQ	$8, %rsp
	RET
	JMP L7
L6:
	ADDQ	$8, %rsp
	RET
	JMP L7
L7:
L4:
	JMP L2
L2:
	JMP L4
L3:
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L8:
	MOVL	$1, %edi
	AND	$0,%al
	CALL	_c0_test
	MOVL	$0, %edi
	AND	$0,%al
	CALL	_c0_test
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
