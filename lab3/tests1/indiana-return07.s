	.file	"../tests1/indiana-return07.l3"
	.text
	.global	_c0_food
_c0_food:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_a1
_c0_a1:
	SUBQ	$8, %rsp
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$64, %rsp
L2:
	MOVL	%edi, %ebx
	MOVL	%esi, %ebp
	MOVL	%edx, %r12d
	MOVL	%ecx, %r13d
	MOVL	%r8d, 128(%rsp)
	MOVL	%r9d, 136(%rsp)
	MOVL	128(%rsp), %r15d
	MOVL	%r15d, 144(%rsp)
	MOVL	136(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	144(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	152(%rsp), %r15d
	MOVL	%r15d, 48(%rsp)
	MOVL	%ebp, %edi
	MOVL	%r12d, %esi
	MOVL	%r13d, %edx
	MOVL	128(%rsp), %ecx
	MOVL	136(%rsp), %r8d
	MOVL	144(%rsp), %r9d
	MOVL	32(%rsp), %r15d
	MOVL	%r15d, 0(%rsp)
	MOVL	40(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	48(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	%ebx, 24(%rsp)
	AND	$0,%al
	CALL	_c0_b2
	MOVL	%eax, 56(%rsp)
	MOVL	%r12d, %edi
	MOVL	%r13d, %esi
	MOVL	128(%rsp), %edx
	MOVL	136(%rsp), %ecx
	MOVL	144(%rsp), %r8d
	MOVL	32(%rsp), %r9d
	MOVL	40(%rsp), %r15d
	MOVL	%r15d, 0(%rsp)
	MOVL	48(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	%ebx, 16(%rsp)
	MOVL	%ebp, 24(%rsp)
	AND	$0,%al
	CALL	_c0_b2
	MOVL	%eax, %edi
	MOVL	56(%rsp), %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%edi, %eax
	ADDQ	$64, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	ADDQ	$8, %rsp
	RET
	.global	_c0_b2
_c0_b2:
	PUSHQ	%r15
	PUSHQ	%r14
	PUSHQ	%r13
	PUSHQ	%r12
	PUSHQ	%rbp
	PUSHQ	%rbx
	SUBQ	$56, %rsp
L3:
	MOVL	%edi, %ebx
	MOVL	%esi, %ebp
	MOVL	%edx, %r12d
	MOVL	%ecx, %r13d
	MOVL	%r8d, 0(%rsp)
	MOVL	%r9d, 8(%rsp)
	MOVL	112(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	120(%rsp), %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	128(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	136(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	%r13d, %edi
	ADDL	$1, %edi
	CMPL	%edi,%r13d
	JE L11
	JMP L12
L11:
	MOVL	%r13d, %edi
	JMP L13
L12:
	MOVL	0(%rsp), %edi
	JMP L13
L13:
	MOVL	%r12d, %ecx
	ADDL	%edi, %ecx
	MOVL	16(%rsp), %edi
	MOVL	8(%rsp), %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %esi
	MOVL	24(%rsp), %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%ecx, 48(%rsp)
	ADDL	%edi, 48(%rsp)
	MOVL	32(%rsp), %edi
	MOVL	40(%rsp), %esi
	AND	$0,%al
	CALL	_c0_food
	MOVL	%eax, %edi
	ADDL	48(%rsp), %edi
	CMPL	$32,%edi
	JL L7
	JMP L5
L7:
	MOVL	%r13d, %edi
	ADDL	$1, %edi
	CMPL	%edi,%r13d
	JE L8
	JMP L9
L8:
	MOVL	%r13d, %edi
	JMP L10
L9:
	MOVL	0(%rsp), %edi
	JMP L10
L10:
	MOVL	%r12d, %ecx
	ADDL	%edi, %ecx
	MOVL	16(%rsp), %edi
	MOVL	8(%rsp), %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %esi
	MOVL	24(%rsp), %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%ecx, 48(%rsp)
	ADDL	%edi, 48(%rsp)
	MOVL	32(%rsp), %edi
	MOVL	40(%rsp), %esi
	AND	$0,%al
	CALL	_c0_food
	MOVL	%eax, %edi
	ADDL	48(%rsp), %edi
	CMPL	$0,%edi
	JGE L4
	JMP L5
L4:
	MOVL	%ebp, %edi
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%edi
	MOVL	%edx, %ebp
	MOVL	%r13d, %edi
	ADDL	$1, %edi
	CMPL	%edi,%r13d
	JE L14
	JMP L15
L14:
	MOVL	%r13d, %edi
	JMP L16
L15:
	MOVL	0(%rsp), %edi
	JMP L16
L16:
	MOVL	%r12d, %ecx
	ADDL	%edi, %ecx
	MOVL	16(%rsp), %edi
	MOVL	8(%rsp), %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %esi
	MOVL	24(%rsp), %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%ecx, %ebx
	ADDL	%edi, %ebx
	MOVL	32(%rsp), %edi
	MOVL	40(%rsp), %esi
	AND	$0,%al
	CALL	_c0_food
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	%ebp, %r14d
	MOVL	%eax, %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, %eax
	MOVL	%eax, %eax
	JMP L6
L5:
	MOVL	$0, %edi
	MOVL	$42, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	MOVL	%edi, %eax
	JMP L6
L6:
	MOVL	%eax, %eax
	ADDQ	$56, %rsp
	POPQ	%rbx
	POPQ	%rbp
	POPQ	%r12
	POPQ	%r13
	POPQ	%r14
	POPQ	%r15
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
	SUBQ	$32, %rsp
L17:
	MOVL	$1, %edi
	MOVL	$2, %esi
	MOVL	$3, %edx
	MOVL	$4, %ecx
	MOVL	$5, %r8d
	MOVL	$6, %r9d
	MOVL	$7, 0(%rsp)
	MOVL	$8, 8(%rsp)
	MOVL	$9, 16(%rsp)
	MOVL	$10, 24(%rsp)
	AND	$0,%al
	CALL	_c0_a1
	MOVL	%eax, %eax
	CMPL	$2,%eax
	JE L18
	JMP L19
L18:
	MOVL	$1, %edi
	MOVL	$2, %esi
	MOVL	$3, %edx
	MOVL	$4, %ecx
	MOVL	$5, %r8d
	MOVL	$6, %r9d
	MOVL	$7, 0(%rsp)
	MOVL	$8, 8(%rsp)
	MOVL	$9, 16(%rsp)
	MOVL	$10, 24(%rsp)
	AND	$0,%al
	CALL	_c0_a1
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	JMP L20
L19:
	MOVL	$6, %edi
	MOVL	$2, %esi
	MOVL	$3, %edx
	MOVL	$4, %ecx
	MOVL	$5, %r8d
	MOVL	$6, %r9d
	MOVL	$7, 0(%rsp)
	MOVL	$8, 8(%rsp)
	MOVL	$9, 16(%rsp)
	MOVL	$10, 24(%rsp)
	AND	$0,%al
	CALL	_c0_a1
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	JMP L20
L20:
	MOVL	%eax, %eax
	ADDQ	$32, %rsp
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
