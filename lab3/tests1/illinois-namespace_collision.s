	.file	"../tests1/illinois-namespace_collision.l3"
	.text
	.global	_c0_done
_c0_done:
	SUBQ	$8, %rsp
L1:
	MOVL	$0, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0___c0_main
_c0___c0_main:
	SUBQ	$8, %rsp
L2:
	AND	$0,%al
	CALL	_c0_done
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0__c0_main
_c0__c0_main:
	SUBQ	$8, %rsp
L3:
	AND	$0,%al
	CALL	_c0___c0_main
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_c0_main
_c0_c0_main:
	SUBQ	$8, %rsp
L4:
	AND	$0,%al
	CALL	_c0__c0_main
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0__main
_c0__main:
	SUBQ	$8, %rsp
L5:
	AND	$0,%al
	CALL	_c0_c0_main
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L6:
	AND	$0,%al
	CALL	_c0__main
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
