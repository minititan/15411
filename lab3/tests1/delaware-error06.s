	.file	"../tests1/delaware-error06.l3"
	.text
	.global	_c0_world
_c0_world:
	SUBQ	$8, %rsp
L1:
	MOVL	$5, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_hello
_c0_hello:
	SUBQ	$8, %rsp
L2:
	AND	$0,%al
	CALL	_c0_world
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L3:
	AND	$0,%al
	CALL	_c0_hello
	MOVL	%eax, %eax
	AND	$0,%al
	CALL	_c0_hello
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
