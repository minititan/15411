	.file	"../tests2/aluminium-return14.l3"
	.text
	.global	_c0_plus
_c0_plus:
	SUBQ	$8, %rsp
L1:
	MOVL	%edi, %eax
	MOVL	%esi, %edi
	ADDL	%edi, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L2:
	MOVL	$1, %edi
	MOVL	$1, %esi
	AND	$0,%al
	CALL	_c0_plus
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
