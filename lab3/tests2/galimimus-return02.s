	.file	"../tests2/galimimus-return02.l1"
	.text
	.global	_c0_main
_c0_main:
	SUBQ	$8, %rsp
L1:
	MOVL	$2, %edi
	MOVL	%edi, %esi
	NEGL	%esi
	MOVL	$4, %ecx
	MOVL	%esi, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %ecx
	MOVL	%ecx, %ecx
	NEGL	%ecx
	IMULL	$2, %ecx
	ADDL	%edi, %ecx
	MOVL	$3, %edi
	MOVL	%esi, %eax
	CLTD
	IDIVL	%edi
	MOVL	%edx, %edi
	MOVL	%ecx, %esi
	ADDL	%edi, %esi
	MOVL	$3, %edi
	MOVL	$1, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	ADDL	%edi, %esi
	MOVL	$2, %edi
	MOVL	$3, %eax
	CLTD
	IDIVL	%edi
	MOVL	%edx, %edi
	MOVL	%edi, %eax
	NEGL	%eax
	ADDL	%esi, %eax
	MOVL	%eax, %eax
	ADDQ	$8, %rsp
	RET
	.ident	"15-411 L3 compiler"
