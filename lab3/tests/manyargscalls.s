	.file	"../tests/manyargscalls.l3"
	.text
	.global	_c0_call1
_c0_call1:
L1:
	MOVL	%edi, %eax
	MOVL	$0, %eax
	MOVL	$1, %edi
	MOVL	$2, %esi
	MOVL	$3, %edx
	MOVL	$4, %ecx
	MOVL	$5, %r8d
	MOVL	%eax, %r9d
	ADDL	%edi, %r9d
	MOVL	%eax, %r10d
	ADDL	%edi, %r10d
	ADDL	%esi, %r10d
	ADDL	%edx, %r10d
	ADDL	%edi, %eax
	ADDL	%esi, %eax
	ADDL	%edx, %eax
	ADDL	%ecx, %eax
	ADDL	%r8d, %eax
	ADDL	%r9d, %eax
	ADDL	%r10d, %eax
	MOVL	%r9d, %edi
	MOVL	%r10d, %esi
	MOVL	%eax, %edx
	AND	$0,%al
	CALL	_c0_call2
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	RET
	.global	_c0_call2
_c0_call2:
	PUSH	%r15
	PUSH	%r14
	PUSH	%r13
	PUSH	%r12
	PUSH	%rbp
	PUSH	%rbx
	SUBQ	$96, %rsp
L2:
	MOVL	%edi, %ebx
	MOVL	%esi, %ebp
	MOVL	%edx, %r12d
	MOVL	$0, %r13d
L5:
	CMPL	%ebx,%r13d
	JL L3
	JMP L4
L3:
	MOVL	$0, 80(%rsp)
L8:
	CMPL	%ebp,80(%rsp)
	JL L6
	JMP L7
L6:
	MOVL	$0, 88(%rsp)
L11:
	CMPL	%r12d,88(%rsp)
	JL L9
	JMP L10
L9:
	MOVL	%r13d, %edi
	MOVL	80(%rsp), %esi
	MOVL	88(%rsp), %edx
	MOVL	%r13d, %ecx
	MOVL	80(%rsp), %r8d
	MOVL	88(%rsp), %r9d
	MOVL	%r13d, 0(%rsp)
	MOVL	80(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	88(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	%r13d, 24(%rsp)
	MOVL	80(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	88(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	%r13d, 48(%rsp)
	MOVL	80(%rsp), %r15d
	MOVL	%r15d, 56(%rsp)
	MOVL	88(%rsp), %r15d
	MOVL	%r15d, 64(%rsp)
	AND	$0,%al
	CALL	_c0_call3
	INCL	88(%rsp)
	JMP L11
L10:
	INCL	80(%rsp)
	JMP L8
L7:
	INCL	%r13d
	JMP L5
L4:
	MOVL	$42, %eax
	ADDQ	$96, %rsp
	POP	%rbx
	POP	%rbp
	POP	%r12
	POP	%r13
	POP	%r14
	POP	%r15
	RET
	.global	_c0_call3
_c0_call3:
L12:
	MOVL	%edi, %edi
	MOVL	%esi, %esi
	MOVL	%edx, %r10d
	MOVL	%ecx, %ecx
	MOVL	%r8d, %ecx
	MOVL	%r9d, %ecx
	MOVL	8(%rsp), %ecx
	MOVL	16(%rsp), %ecx
	MOVL	24(%rsp), %ecx
	MOVL	32(%rsp), %ecx
	MOVL	40(%rsp), %ecx
	MOVL	48(%rsp), %ecx
	MOVL	56(%rsp), %ecx
	MOVL	64(%rsp), %ecx
	MOVL	72(%rsp), %ecx
	ADDL	%edi, %esi
	MOVL	$15, %edi
	MOVL	%r10d, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edi
	SUBL	%esi, %edi
	NEGL	%edi
	RET
	.global	_c0_main
_c0_main:
L13:
	MOVL	$2, %edi
	AND	$0,%al
	CALL	_c0_call1
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L3 compiler"
