	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp2:
	.cfi_def_cfa_offset 16
Ltmp3:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp4:
	.cfi_def_cfa_register %rbp
	movl	$0, -4(%rbp)
	movl	$1, -8(%rbp)
	movl	$2, -12(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	LBB0_2
## BB#1:
	movl	$0, -4(%rbp)
	jmp	LBB0_3
LBB0_2:
	movl	$1, -4(%rbp)
LBB0_3:
	movl	-4(%rbp), %eax
	popq	%rbp
	ret
	.cfi_endproc


.subsections_via_symbols
