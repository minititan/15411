{- L1 Compiler
   Author: Matthew Maurer <mmaurer@andrew.cmu.edu>
   Modified by: Ryan Pearl <rpearl@andrew.cmu.edu>
                Rokhini Prabhu <rokhinip@andrew.cmu.edu>

   Currently just a pseudolanguage with 3-operand instructions and arbitrarily many temps.
-}
module Compile.CodeGen where

import Compile.Types
import qualified Data.Map as Map

type Alloc = (Map.Map String Int, Int)

-- Get the decls from   
getDecls :: [Stmt] -> [Decl]
getDecls stmts = 
  map (\(Decl d) -> d) $ filter (\stmt -> (case stmt of 
                                            (Decl _) -> True 
                                            _ -> False)) stmts
codeGen :: AST -> [AAsm]
codeGen (Block stmts _) = 
  let
    decls = getDecls stmts
    temps = Map.fromList $ zip (map var decls) [0..]
  in concatMap (genStmt (temps, (length decls))) stmts

genStmt :: Alloc -> Stmt -> [AAsm]
genStmt alloc (Decl d) = genDecl alloc d
genStmt alloc (Simp simp) = genSimp alloc simp
genStmt alloc (Ret e) = genExp alloc e (AReg 0) ++ [ARet (ALoc $ AReg 0)] 

genDecl :: Alloc -> Decl -> [AAsm]
genDecl alloc (JustDecl _ _) = []
genDecl alloc (DeclAsgn v e p) = genSimp alloc (Asgn (Var v) Equal e p)

genSimp :: Alloc -> Simp -> [AAsm]
genSimp (allocmap, n) (Asgn (Var v) asnop exp pos) =
  let
    tmpNum = ATemp $ allocmap Map.! v
    exp' = case asnop of
             Equal -> exp
             AsnOp binop -> Binop binop (Ident v pos) exp pos
  in genExp (allocmap, n) exp' tmpNum

genExp :: Alloc -> Exp -> ALoc -> [AAsm]
genExp _ (Int n _) dest = [AAsm [dest] ANop [AImm $ fromIntegral $ num n]]
genExp (allocmap, _) (Ident var _) dest = [AAsm [dest] ANop [ALoc $ ATemp $ allocmap Map.! var]]
genExp (allocmap, n) (Binop binop exp1 exp2 _) dest = let
  cogen1 = genExp (allocmap, n + 1) exp1 (ATemp n)
  cogen2 = genExp (allocmap, n + 2) exp2 (ATemp $ n + 1)
  combine  = [AAsm [dest] (genBinOp binop) [ALoc $ ATemp n, ALoc $ ATemp $ n + 1]]
  in cogen1 ++ cogen2 ++ combine
genExp (allocmap, n) (Unop unop exp _) dest = let
  cogen = genExp (allocmap, n + 1) exp (ATemp n)
  assign  = [AAsm [dest] ASub [AImm 0, ALoc $ ATemp n]]
  in cogen ++ assign

genBinOp :: Binop -> AOp
genBinOp Add = AAdd
genBinOp Sub = ASub
genBinOp Div = ADiv
genBinOp Mul = AMul
genBinOp Mod = AMod
