{- L1 Compiler
   Author: Matthew Maurer <mmaurer@andrew.cmu.edu>
   Modified by: Ryan Pearl <rpearl@andrew.cmu.edu>
                Rokhini Prabhu <rokhinip@andrew.cmu.edu>

   Defines the AST we parse to
-}
module Compile.Types.AST where

import Text.ParserCombinators.Parsec.Pos (SourcePos)
import Numeric as Num
import Compile.Types.Ops

type Ident = String

data AST = Block [Stmt] SourcePos
data Stmt = Decl Decl 
          | Simp Simp 
          | Ret Exp  
data Decl = JustDecl {var :: Ident, pos :: SourcePos}
          | DeclAsgn {var :: Ident, e :: Exp, pos :: SourcePos}
data Simp = Asgn LValue Asnop Exp SourcePos 
data LValue = Var Ident
data ParseInt = Num {num :: Integer}
data Exp = Int ParseInt SourcePos 
         | Ident Ident SourcePos 
         | Binop Binop Exp Exp SourcePos 
         | Unop Unop Exp SourcePos 

-- Note to the student: You will probably want to write a new pretty printer
-- using the module Text.PrettyPrint.HughesPJ from the pretty package
-- This is a quick and dirty pretty printer.
-- Once that is written, you may find it helpful for debugging to switch
-- back to the deriving Show instances.

instance Show AST where
  show (Block stmts _) =
    "int main () {\n" ++ (unlines $ (map (\stmt ->"\t" ++ show stmt) stmts)) ++ "}\n"

instance Show Stmt where
  show (Decl d) = show d 
  show (Simp simp) = show simp 
  show (Ret e) = "return " ++ (show e) ++ ";"

instance Show Decl where
  show (JustDecl i _) = "int " ++ i ++ ";"
  show (DeclAsgn x e _) = "int " ++ x ++ " = " ++ (show e) ++ ";"

instance Show LValue where
  show (Var x) = x

instance Show Simp where
  show (Asgn lval asnop exp _) = (show lval) ++ " " ++ (show asnop) ++ " " ++ (show exp) ++ ";" 

instance Show ParseInt where
  show (Num n) = show n

instance Show Exp where
  show (Int x _) = show x
  show (Ident var _) = var
  show (Binop binop exp1 exp2 _) = (show exp1) ++ " " ++ (show binop) ++ " " ++ (show exp2)
  show (Unop unop exp _) = (show unop) ++ (show exp) 
