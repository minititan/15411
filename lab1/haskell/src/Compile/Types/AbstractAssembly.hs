{- L1 Compiler
   Author: Matthew Maurer <mmaurer@andrew.cmu.edu>
   Modified by: Ryan Pearl <rpearl@andrew.cmu.edu>

   Defines a flat abstract assembly.
-}
module Compile.Types.AbstractAssembly where

import Compile.Types.Ops

data AAsm = AAsm {aAssign :: [ALoc]
                 ,aOp     :: AOp
                 ,aArgs   :: [AVal]
                 }
          | ARet AVal
          | AComment String 

data AVal = ALoc ALoc
          | AImm Int 

data ALoc = AReg Int
          | ATemp Int 
          | AMem Int

instance Show AAsm where
  show (AAsm dest ANop [src]) = (show dest) ++ " <-- " ++ (show src) ++ "\n"
  show (AAsm dest asnop [src1, src2]) = (show dest) ++ " <-- " ++ (show src1) ++ " " 
                                        ++ (show asnop) ++ " " ++ (show src2) ++ "\n"
  show (ARet retval) = "ret " ++ show retval ++ "\n"

instance Show AVal where
  show (ALoc loc) = show loc
  show (AImm n) = show n

instance Show ALoc where
  show (AReg n) = "%r" ++ (show n)
  show (ATemp n) = "%t" ++ (show n)
  show (AMem n) = "M[" ++ (show n) ++ "]"
