	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/kansas-test1.l1"
	MOVL	$1, %eax
	MOVL	$17, %ebx
	MOVL	$3, %ecx
	MOVL	$4, %ebp
	MOVL	$5, %edi
	MOVL	$6, %edx
	MOVL	%eax, %r8d
	IMULL	%edx, %r8d
	MOVL	%ebx, %eax
	MOVL	$7, %ebx
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %ebx
	MOVL	%ecx, %eax
	MOVL	$2, %ecx
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %eax
	MOVL	%r8d, %ecx
	MOVL	%ebx, %edx
	MOVL	%ecx, %ebx
	IMULL	%edx, %ebx
	MOVL	%ebp, %ecx
	CLTD
	IDIVL	%ecx
	SUBL	%eax, %ebx
	MOVL	%edi, %eax
	MOVL	$3, %ecx
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %ecx
	MOVL	%ebx, %eax
	SUBL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
