	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/delaware-grant-return02.l1"
	MOVL	$4, %eax
	MOVL	$5, %ebx
	MOVL	%ebx, %ecx
	MOVL	%eax, %edx
	MOVL	%ecx, %eax
	ADDL	%edx, %eax
	MOVL	%eax, %ecx
	MOVL	%ebx, %edx
	MOVL	%ecx, %ebx
	ADDL	%edx, %ebx
	MOVL	%ebx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	IMULL	%ecx, %eax
	MOVL	%eax, %eax
	MOVL	$3, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
