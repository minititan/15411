	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/colorado-exception02.l1"
	MOVL	$1, %eax
	MOVL	$0, %ebx
	MOVL	$0, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%edx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
