	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/illinois-exception01.l1"
	MOVL	$0, %eax
	MOVL	$5, %ebx
	MOVL	%eax, %ecx
	IMULL	%ecx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%ecx
	RET
	.ident	"15-411 L1 reference compiler"
