	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/indiana-divTest1.l1"
	MOVL	$1, %ebx
	MOVL	$2, %ecx
	MOVL	$3, %ebp
	MOVL	$5, %edi
	MOVL	$7, %eax
	MOVL	$9, %r8d
	MOVL	$11, %r9d
	MOVL	%ebx, %edx
	MOVL	%ebp, %r10d
	ADDL	%r10d, %edx
	MOVL	%edi, %r10d
	ADDL	%r10d, %edx
	MOVL	%eax, %r10d
	MOVL	%edx, %eax
	ADDL	%r10d, %eax
	MOVL	%r8d, %edx
	ADDL	%edx, %eax
	MOVL	%r9d, %edx
	ADDL	%edx, %eax
	MOVL	%ecx, %edx
	MOVL	%ebp, %r10d
	IMULL	%r10d, %edx
	MOVL	%ebp, %r11d
	MOVL	%edx, %r10d
	IMULL	%r11d, %r10d
	CLTD
	IDIVL	%r10d
	MOVL	%eax, %r10d
	MOVL	%ebp, %eax
	MOVL	%ecx, %r11d
	CLTD
	IDIVL	%r11d
	MOVL	%edx, %eax
	ADDL	%eax, %r10d
	MOVL	%ecx, %eax
	MOVL	%ebp, %ecx
	ADDL	%ecx, %eax
	MOVL	%r8d, %ecx
	ADDL	%ecx, %eax
	MOVL	%r9d, %ecx
	ADDL	%ecx, %eax
	MOVL	%edi, %ecx
	IMULL	%ecx, %ebx
	MOVL	%edi, %ecx
	IMULL	%ecx, %ebx
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %ebx
	MOVL	%r10d, %eax
	SUBL	%ebx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
