	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/connecticut-result2.l1"
	MOVL	$1, %eax
	MOVL	$2, %eax
	MOVL	$48, %eax
	MOVL	$3840, %eax
	MOVL	$4, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %ecx
	MOVL	%eax, %eax
	MOVL	%ecx, %ecx
	ADDL	%eax, %ecx
	MOVL	%ebx, %eax
	SUBL	%ecx, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %eax
	MOVL	$3, %edx
	MOVL	%eax, %ecx
	IMULL	%edx, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	MOVL	$3, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%edx, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
