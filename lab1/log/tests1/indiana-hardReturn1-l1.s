	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/indiana-hardReturn1.l1"
	MOVL	$2, %eax
	MOVL	$0, %ebx
	MOVL	%ebx, %ecx
	IMULL	%ecx, %eax
	MOVL	%ebx, %ecx
	MOVL	%eax, %ebx
	ADDL	%ecx, %ebx
	MOVL	$10, %eax
	MOVL	$4, %ecx
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %eax
	ADDL	%eax, %ebx
	MOVL	$18, %ecx
	MOVL	$18, %ebp
	MOVL	$3, %edi
	MOVL	$2, %eax
	MOVL	$1, %r8d
	CLTD
	IDIVL	%r8d
	ADDL	%eax, %edi
	MOVL	%ebp, %eax
	CLTD
	IDIVL	%edi
	IMULL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
