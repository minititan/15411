	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/arkansas-exception01.l1"
	MOVL	$1, %eax
	MOVL	$0, %ebx
	MOVL	$1, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	MOVL	%ebx, %ebx
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	$0, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
