	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/indiana-easyException1.l1"
	MOVL	$0, %eax
	MOVL	$2, %ebx
	IMULL	%ebx, %eax
	MOVL	$10, %ebx
	MOVL	$2, %ecx
	MOVL	%eax, %ebp
	MOVL	%ecx, %eax
	CLTD
	IDIVL	%ebp
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
