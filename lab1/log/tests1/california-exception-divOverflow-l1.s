	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/california-exception-divOverflow.l1"
	MOVL	$-2147483648, %eax
	MOVL	$0, %ebx
	MOVL	$1, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
