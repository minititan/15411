	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/delaware-grant-return03.l1"
	MOVL	$1, %eax
	MOVL	$3, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	MOVL	$4, %ebx
	MOVL	%eax, %eax
	IMULL	%ebx, %eax
	MOVL	$0, %ebx
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
