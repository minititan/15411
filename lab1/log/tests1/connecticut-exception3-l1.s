	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/connecticut-exception3.l1"
	MOVL	$1, %eax
	MOVL	$0, %ebx
	MOVL	%ebx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	SUBL	%ecx, %eax
	MOVL	$-2147483648, %ebx
	MOVL	%ebx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
