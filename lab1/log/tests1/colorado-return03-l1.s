	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/colorado-return03.l1"
	MOVL	$5, %eax
	MOVL	%eax, %eax
	MOVL	$4, %ebx
	MOVL	%eax, %eax
	IMULL	%ebx, %eax
	MOVL	$7, %ebx
	MOVL	%eax, %eax
	MOVL	%ebx, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %eax
	MOVL	%ebx, %ebx
	MOVL	$2, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	MOVL	$0, %ecx
	MOVL	$30, %edx
	MOVL	%ecx, %ecx
	SUBL	%edx, %ecx
	MOVL	%eax, %eax
	IMULL	%ecx, %eax
	MOVL	%eax, %eax
	MOVL	%ebx, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	MOVL	%ebx, %ebx
	MOVL	%eax, %eax
	SUBL	%ebx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
