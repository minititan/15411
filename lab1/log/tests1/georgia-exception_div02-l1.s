	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/georgia-exception_div02.l1"
	MOVL	$-2147483648, %eax
	MOVL	$0, %ebx
	MOVL	$1, %ecx
	SUBL	%ecx, %ebx
	CLTD
	IDIVL	%ebx
	RET
	.ident	"15-411 L1 reference compiler"
