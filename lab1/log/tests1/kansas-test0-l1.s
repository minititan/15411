	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/kansas-test0.l1"
	MOVL	$1, %eax
	MOVL	$2, %ebx
	MOVL	$3, %ecx
	MOVL	$4, %edx
	MOVL	$5, %ebp
	MOVL	$6, %edi
	ADDL	%edi, %eax
	MOVL	$7, %edi
	SUBL	%edi, %ebx
	MOVL	$0, %edi
	MOVL	%ecx, %r8d
	MOVL	%edi, %ecx
	SUBL	%r8d, %ecx
	ADDL	%ebx, %eax
	MOVL	%ecx, %ebx
	SUBL	%ebx, %eax
	MOVL	%edx, %ebx
	ADDL	%ebx, %eax
	MOVL	%ebp, %ebx
	SUBL	%ebx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
