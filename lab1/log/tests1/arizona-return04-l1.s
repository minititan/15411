	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/arizona-return04.l1"
	MOVL	$42, %eax
	MOVL	$2, %ebx
	MOVL	$15, %ecx
	MOVL	$4, %ebp
	MOVL	%eax, %eax
	MOVL	%ebx, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%edx, %ebx
	MOVL	%ecx, %eax
	MOVL	%ebp, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%edx, %eax
	MOVL	%ebx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
