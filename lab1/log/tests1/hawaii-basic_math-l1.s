	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/hawaii-basic_math.l1"
	MOVL	$14, %eax
	MOVL	$2, %ebx
	CLTD
	IDIVL	%ebx
	MOVL	$5, %ebx
	MOVL	$2, %ecx
	IMULL	%ecx, %ebx
	ADDL	%ebx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
