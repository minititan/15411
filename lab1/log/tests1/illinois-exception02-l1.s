	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/illinois-exception02.l1"
	MOVL	$10, %eax
	MOVL	$5, %ebx
	CLTD
	IDIVL	%ebx
	MOVL	$10, %ebx
	CLTD
	IDIVL	%ebx
	MOVL	$5, %ebx
	MOVL	$5, %ecx
	MOVL	%eax, %ebp
	MOVL	%ecx, %eax
	CLTD
	IDIVL	%ebp
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
