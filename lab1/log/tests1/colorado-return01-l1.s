	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/colorado-return01.l1"
	MOVL	$0, %eax
	MOVL	%eax, %eax
	MOVL	$1, %ebx
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	$2, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	IMULL	%ecx, %eax
	MOVL	$3, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	SUBL	%ecx, %eax
	MOVL	$0, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
