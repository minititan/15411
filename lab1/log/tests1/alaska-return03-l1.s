	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/alaska-return03.l1"
	MOVL	$1, %eax
	MOVL	$5, %eax
	MOVL	$4, %ebx
	MOVL	%eax, %eax
	IMULL	%ebx, %eax
	MOVL	$8, %ecx
	MOVL	%eax, %ebx
	ADDL	%ecx, %ebx
	MOVL	$2, %eax
	MOVL	$3, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	SUBL	%ecx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
