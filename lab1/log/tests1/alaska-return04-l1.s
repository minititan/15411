	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/alaska-return04.l1"
	SUBQ	$112, %rsp
	MOVL	$12, %ebx
	MOVL	%ebx, %ecx
	MOVL	$1234, %ebp
	MOVL	$8123, %edi
	MOVL	$11142, %r8d
	MOVL	$4899, %r9d
	MOVL	$999, %r10d
	MOVL	$-559038737, %r11d
	MOVL	$12385, %r12d
	MOVL	%r9d, %eax
	MOVL	%r8d, %edx
	MOVL	%eax, %r13d
	ADDL	%edx, %r13d
	MOVL	%ebx, %eax
	MOVL	%ebx, %r14d
	MOVL	%eax, %eax
	CLTD
	IDIVL	%r14d
	MOVL	%eax, %r14d
	MOVL	$881075, 4(%rsp)
	MOVL	%r11d, %eax
	MOVL	4(%rsp), %edx
	MOVL	%eax, 8(%rsp)
	MOVL	8(%rsp), %r15d
	SUBL	%edx, %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	4(%rsp), %eax
	MOVL	%r11d, %edx
	MOVL	%eax, 12(%rsp)
	MOVL	12(%rsp), %r15d
	SUBL	%edx, %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	8(%rsp), %eax
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	%eax, %eax
	CLTD
	IDIVL	16(%rsp)
	MOVL	%eax, 16(%rsp)
	MOVL	4(%rsp), %eax
	MOVL	%r11d, 20(%rsp)
	MOVL	%eax, %eax
	CLTD
	IDIVL	20(%rsp)
	MOVL	%edx, 20(%rsp)
	MOVL	%r8d, %eax
	MOVL	%edi, %edx
	MOVL	%eax, %eax
	SUBL	%edx, %eax
	MOVL	%ebx, %edx
	MOVL	%eax, 24(%rsp)
	MOVL	24(%rsp), %r15d
	SUBL	%edx, %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	%r14d, %eax
	MOVL	%r13d, 28(%rsp)
	MOVL	%eax, %eax
	CLTD
	IDIVL	28(%rsp)
	MOVL	%eax, %eax
	MOVL	%ebx, %ebx
	MOVL	%ecx, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%ebp, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%edi, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r8d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r9d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r10d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r11d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r12d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r13d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%r14d, %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	4(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	8(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	12(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	16(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	20(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	24(%rsp), %ecx
	MOVL	%ebx, %ebx
	ADDL	%ecx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	ADDQ	$112, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
