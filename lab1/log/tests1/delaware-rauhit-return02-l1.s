	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/delaware-rauhit-return02.l1"
	MOVL	$3, %eax
	MOVL	%eax, %eax
	MOVL	$3, %ecx
	MOVL	%eax, %ebx
	ADDL	%ecx, %ebx
	MOVL	%ebx, %eax
	MOVL	$3, %edx
	MOVL	%eax, %ecx
	ADDL	%edx, %ecx
	MOVL	%ebx, %eax
	MOVL	%ecx, %ebp
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebp
	MOVL	%eax, %ebp
	MOVL	%ecx, %eax
	MOVL	%ebx, %edi
	MOVL	%eax, %eax
	CLTD
	IDIVL	%edi
	MOVL	%eax, %edx
	MOVL	%ebp, %eax
	IMULL	%edx, %eax
	MOVL	%ebx, %edx
	MOVL	%eax, %eax
	ADDL	%edx, %eax
	MOVL	%ecx, %edx
	MOVL	%eax, %eax
	SUBL	%edx, %eax
	MOVL	%eax, %edx
	MOVL	%ebx, %ebp
	MOVL	%edx, %ebx
	IMULL	%ebp, %ebx
	MOVL	%eax, %eax
	MOVL	%ecx, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	ADDL	%ecx, %eax
	RET
	.ident	"15-411 L1 reference compiler"
