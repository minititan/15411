	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/florida-test_exception2.l1"
	MOVL	$0, %eax
	MOVL	$1, %ebx
	MOVL	%eax, %eax
	SUBL	%ebx, %eax
	MOVL	$0, %ebx
	MOVL	$-2147483648, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%ebx, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
