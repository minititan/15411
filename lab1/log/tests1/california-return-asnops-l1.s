	.globl	_c0_main
	.align	4, 0x90
_c0_main:
	.file	"../tests1/california-return-asnops.l1"
	MOVL	$0, %eax
	MOVL	%eax, %eax
	MOVL	$1, %ebx
	MOVL	%eax, %eax
	ADDL	%ebx, %eax
	MOVL	%eax, %eax
	MOVL	$2, %ebx
	MOVL	%eax, %eax
	SUBL	%ebx, %eax
	MOVL	%eax, %eax
	MOVL	$6, %ebx
	MOVL	%eax, %eax
	IMULL	%ebx, %eax
	MOVL	%eax, %eax
	MOVL	$3, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	MOVL	$2, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%edx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
