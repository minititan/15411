	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/alabama-test4.l1"
	MOVL	$1, %eax
	MOVL	$2, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	MOVL	%eax, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %eax
	MOVL	$0, %ebx
	MOVL	%eax, %ecx
	MOVL	%ebx, %eax
	IMULL	%ecx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
