	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/exception03.l1"
	MOVL	$0, %eax
	MOVL	$-2147483648, %ebx
	MOVL	%eax, %eax
	SUBL	%ebx, %eax
	MOVL	%eax, %eax
	MOVL	$0, %ebx
	MOVL	$1, %ecx
	MOVL	%ebx, %ebx
	SUBL	%ecx, %ebx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ebx
	MOVL	%eax, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
