	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/return01.l1"
	SUBQ	$208, %rsp
	MOVL	$0, 4(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 48(%rsp)
	MOVL	$1, 52(%rsp)
	MOVL	48(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	52(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	$2, 40(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 44(%rsp)
	MOVL	40(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	IMULL	44(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	$3, 32(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	32(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	SUBL	36(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	$0, 24(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 28(%rsp)
	MOVL	24(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	SUBL	28(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	16(%rsp), %eax
	ADDQ	$208, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
