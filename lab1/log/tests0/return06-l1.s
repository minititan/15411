	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/return06.l1"
	MOVL	$2, %ebx
	MOVL	%ebx, %eax
	MOVL	%ebx, %ecx
	MOVL	%eax, %eax
	IMULL	%ecx, %eax
	MOVL	%eax, %ecx
	MOVL	%eax, %edx
	MOVL	%ecx, %eax
	IMULL	%edx, %eax
	MOVL	%eax, %ecx
	MOVL	%eax, %edx
	MOVL	%ecx, %eax
	IMULL	%edx, %eax
	MOVL	%eax, %ecx
	MOVL	%eax, %edx
	MOVL	%ecx, %eax
	IMULL	%edx, %eax
	MOVL	%eax, %eax
	MOVL	%ebx, %ecx
	MOVL	%eax, %eax
	CLTD
	IDIVL	%ecx
	MOVL	%eax, %eax
	MOVL	%eax, %ecx
	MOVL	%eax, %edx
	MOVL	%ecx, %eax
	IMULL	%edx, %eax
	MOVL	%eax, %eax
	MOVL	%ebx, %ebx
	MOVL	%eax, %eax
	IMULL	%ebx, %eax
	MOVL	%eax, %eax
	RET
	.ident	"15-411 L1 reference compiler"
