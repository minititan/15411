	.file	"../tests0/return01.l1"
	movl	$0, %eax
	movl	$3, %ecx
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
	movl	-8(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -12(%rbp)
	movl	-12(%rbp), %edx
	shll	$1, %edx
	movl	%edx, -16(%rbp)
	subl	-16(%rbp), %ecx
	movl	%ecx, -20(%rbp)
	subl	-20(%rbp), %eax
	movl	%eax, -24(%rbp)
	movl	-20(%rbp), %eax
	ret
	.ident	"15-411 L1 reference compiler"