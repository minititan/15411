(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 * llir.ml based on codegen.ml
 *)

module IR = Ir
module S = Storage

type operand =
    IMM of Int32.t
  | REG of Storage.reg 
  | TEMP of Temp.t

(* DIV s becomes [CTLD; IDIVL s] in x86_64 *)
type binop = ADD | SUB | MUL

type unop = IDIV

type instr =
    BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | CLTD
  | RET

type program = instr list

let munch_operand : IR.operand -> operand = function
    IR.TEMP t -> TEMP t
  | IR.REG r -> REG r
  | IR.IMM i -> IMM i

let munch_binop binop d t1 t2 =
    let t1 = munch_operand t1
    and t2 = munch_operand t2
    and d = munch_operand d in
    (match binop with
     IR.DIV -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EAX)]
   | IR.MOD -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EDX)]
   | IR.ADD -> [MOV (d, t1); BINOP (ADD, d, t2)] (* split up 3 addr *)
   | IR.SUB -> [MOV (d, t1); BINOP (SUB, d, t2)]
   | IR.MUL -> [MOV (d, t1); BINOP (MUL, d, t2)]
    )

let munch_ir = function
    IR.BINOP (binop, d, t1, t2) -> munch_binop binop d t1 t2
  | IR.MOV (t1, t2) ->
    let t1 = munch_operand t1
    and t2 = munch_operand t2 in
    [MOV (t1, t2)]
  | IR.DIRECTIVE str -> [DIRECTIVE str]
  | IR.COMMENT str -> [DIRECTIVE str]
  | IR.RET -> [RET]

(* llirgen: Ir.instr list -> instr list *)

let rec llirgen = function
    [] -> []
  | ir::irs -> munch_ir ir @ llirgen irs

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_operand = function
      IMM i -> Int32.to_string i
    | REG r -> S.format_reg r
    | TEMP t -> Temp.name t

    let pp_binop = function
      ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "MUL"

    let pp_unop = function
      IDIV -> "IDIV"

    let pp_instr = function
      BINOP (oper, op1, op2) -> pp_binop oper ^ " " ^ pp_operand op1 ^ " <- " ^ pp_operand op1 ^ "," ^ pp_operand op2
    | UNOP (oper, op1) -> pp_unop oper ^ " " ^ pp_operand op1
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ "," ^ pp_operand s
    | DIRECTIVE s -> "DIRECTIVE: " ^ s
    | COMMENT s -> "COMMENT: " ^ s
    | CLTD -> "CLTD"
    | RET -> "RET"
    
    let rec pp_instrs = function
      [] -> ""
    | instr::instrs' -> pp_instr instr ^ "\n" ^ pp_instrs instrs'

    let pp_program instrs = "LLIR code {\n" ^ pp_instrs instrs ^ "}\n"
  end
