module AS = Assem
module LLIR = Llir
module Alloc = Allocator
module S = Storage
open Regalloc_util

exception Unimplemented

let stack_only = true

(* When the stack is used we use R15 for loading and storing to stack *)
let munch_binop = function
    LLIR.ADD -> AS.ADDL
  | LLIR.SUB -> AS.SUBL
  | LLIR.MUL -> AS.MUL

let munch_unop = function
    LLIR.IDIV -> AS.IDIV

(* wrap REG or STACK with storage *)
let wreg r = AS.STR (S.REG r)
let wstk s = AS.STR (S.STACK s)

let stack_reg = wreg S.R15

let get_store t color_map = IntMap.find t color_map

let get_stack_store_only t = 
  if (t < 0) then raise Unimplemented else
    S.STACK (word_size * t)

(* apply_color / make_asm, turns llir into asm w/ coloring *)
(* type of colors is Temp.map *)
let apply_color color_map =

  let unwrap_temp = function
      LLIR.TEMP t -> if (stack_only) then AS.STR (get_stack_store_only (Temp.get_int t))
                    else AS.STR (get_store (Temp.get_int t) color_map)
    | LLIR.REG r -> wreg r
    | LLIR.IMM i -> AS.IMM i in

  let unwrap_temps (t1,t2) = (unwrap_temp t1, unwrap_temp t2) in
  
  function
    LLIR.BINOP (oper, d, s) ->
      let oper = munch_binop oper in
      (match unwrap_temps (d, s) with
        (AS.STR s1, AS.STR s2) -> (match (s1,s2) with
          (S.REG r1, S.REG r2) -> [AS.BINOP(oper, wreg r1, wreg r2)]
        | (S.REG r, S.STACK offset) -> [AS.MOV(stack_reg, wstk offset);
                                    AS.BINOP(oper, wreg r, stack_reg)]

        | (S.STACK offset, S.REG r) -> [AS.MOV(stack_reg, wstk offset);
                                    AS.BINOP(oper, stack_reg, wreg r);
                                    AS.MOV(wstk offset, stack_reg)]

        | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off1);
                                       AS.BINOP(oper, stack_reg, wstk off2);
                                       AS.MOV(wstk off1, stack_reg)]
      )
      | (AS.STR s, AS.IMM i) -> 
        (match s with
          S.STACK offset -> [AS.MOV(stack_reg, wstk offset);
                           AS.BINOP(oper, stack_reg, AS.IMM i);
                           AS.MOV(wstk offset, stack_reg)]

        | S.REG r -> [AS.BINOP(oper, wreg r, AS.IMM i)]
        )

      | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
    )
  | LLIR.MOV(d, s) ->
    (match unwrap_temps (d, s) with
      (AS.STR s1, AS.STR s2) -> (match (s1, s2) with
          (S.REG r1, S.REG r2) -> [AS.MOV(wreg r1, wreg r2)]

        | (S.REG r1, S.STACK(offset)) -> [AS.MOV(wreg r1, wstk offset)]

        | (S.STACK offset, S.REG r2) -> [AS.MOV(wstk offset, wreg r2)]

        | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off2);
                                             AS.MOV(wstk off1, stack_reg)]
      )

    | (AS.STR s, AS.IMM i) -> [AS.MOV(AS.STR s, AS.IMM i)]

    | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
    )

  | LLIR.UNOP (oper, dst) ->
    let oper = munch_unop oper in
    (match unwrap_temp dst with
        AS.STR dst -> (match dst with
          S.REG r -> [AS.UNOP(oper, wreg r)]
        | S.STACK s -> [AS.UNOP(oper, wstk s)]
        )
      | AS.IMM i -> raise (Failure "cannot store into IMM")
    )

  | LLIR.CLTD -> [AS.CLTD]
  | LLIR.RET -> [AS.RET]
  | LLIR.DIRECTIVE str -> [AS.DIRECTIVE str]
  | LLIR.COMMENT str -> [AS.COMMENT str] 


(* Calculate the amount of stack space needed for variables *)
let get_stack_space storage_map = 
  let helper k store (space, seen) = 
    match store with 
    | S.STACK(offset) -> if (IntSet.mem offset seen) then (space, seen)
                        else (space+1, IntSet.add offset seen)
    | S.REG(r) -> (space, seen)
  in
    let (stack_spots, seen) = IntMap.fold helper storage_map (0, IntSet.empty)
  in
    stack_spots * word_size


(* Helper function to perform list fold and get largest temp in llir list *)
let bigger_temp curr_max instr =
  match instr with
  | LLIR.BINOP(_, dest, op) -> 
      (match (dest, op) with
        | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
          max (max (Temp.get_int d) (Temp.get_int t)) curr_max

        | (LLIR.REG(r), LLIR.TEMP(t)) ->
            max curr_max (Temp.get_int t)

        | (LLIR.TEMP(d), LLIR.IMM(_)) -> 
            max curr_max (Temp.get_int d)

        | (LLIR.REG(r), LLIR.IMM(_)) ->
          curr_max

        | (LLIR.REG r1, LLIR.REG r2) ->
          curr_max

        | (LLIR.TEMP t, LLIR.REG r) ->
          max curr_max (Temp.get_int t)

        | (LLIR.IMM _, _) -> ErrorMsg.error None ("IMM cannot be a destination for a BINOP");
               raise ErrorMsg.Error)
  
  | LLIR.MOV(dest, src) -> 
      (match (dest, src) with 
        | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
          max curr_max (max (Temp.get_int d) (Temp.get_int t))

        | (LLIR.REG r, LLIR.TEMP t) ->
          max curr_max (Temp.get_int t)

        | (LLIR.REG r1, LLIR.REG r2) ->
          curr_max

        | (LLIR.TEMP d, LLIR.IMM _) -> 
          max curr_max (Temp.get_int d)

        | (LLIR.REG r, LLIR.IMM _) ->
          curr_max

        | (LLIR.TEMP t, LLIR.REG r) ->
          max curr_max (Temp.get_int t)

        | (LLIR.IMM _, _) -> ErrorMsg.error None ("IMM cannot be a destination for a MOV");
               raise ErrorMsg.Error
      )

  | LLIR.UNOP(operation, op) ->
      (match operation with
        | LLIR.IDIV -> match op with
                  | LLIR.TEMP(t) -> 
                    max curr_max (Temp.get_int t)
                  | _ -> ErrorMsg.error None ("Not supposed to get a non-temp in IDIV");
                         raise ErrorMsg.Error)

  | LLIR.CLTD ->  curr_max

  (* RET defines nothing, uses EAX, has no successor *)
  | LLIR.RET -> curr_max

  | _ -> ErrorMsg.error None ("Instr that is not binop, unop, cltd, or ret in instr list!");
         raise ErrorMsg.Error

(* finds largest temp and returns that * word size *)
let get_only_stack_space llir = 
  let max_temp = List.fold_left bigger_temp 0 llir in
  word_size * max_temp


let assembly_gen llir storage_map = 
  let stack_space = if (stack_only) then get_only_stack_space llir
                    else get_stack_space storage_map in

  let instr = (List.flatten (List.map (apply_color storage_map) llir)) in

  if (stack_space = 0) then 
    instr
  else
    (* move esp down by how much space we need *)

    let pre_instr = [AS.BINOP(AS.SUBQ, AS.STR (S.REG S.ESP), AS.IMM(Int32.of_int(stack_space * word_size)))] 

    in

    let post_instr = [AS.BINOP(AS.ADDQ, AS.STR (S.REG S.ESP), AS.IMM(Int32.of_int (stack_space * word_size)));
                      AS.RET]

    in
      pre_instr @ (List.filter (fun x -> match x with 
                                          AS.RET -> false
                                        | _ -> true) instr) @ post_instr
