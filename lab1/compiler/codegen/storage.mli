type reg = EAX
        |  EBX
        |  ECX
        |  EDX
        |  EBP
        |  ESP
        |  ESI
        |  EDI
        |  R8
        |  R9
        |  R10
        |  R11
        |  R12
        |  R13
        |  R14
        |  R15

type offset = int

type str = REG of reg | STACK of offset

val format_reg: reg -> string

val format: str -> string
