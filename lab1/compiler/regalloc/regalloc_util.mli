module type IntSet_type = Printable.SET with type elt = int

module IntSet : IntSet_type

module type IntMap_type = Printable.MAP with type key = int

module IntMap : IntMap_type

val min_temp : int

val word_size : int

val stack_min_temp : int

val reg_to_temp : Storage.reg -> int

val temp_to_reg : int -> Storage.reg
