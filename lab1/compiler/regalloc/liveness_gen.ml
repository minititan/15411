(* Implementation of register allocator *)

open Regalloc_util
module LLIR = Llir
module S = Storage


exception WrongPattern of string

(* (Definition, Use set, Successor set) *)
type predicates = IntSet.t * IntSet.t * IntSet.t

let rec list_to_set lst = 
  match lst with
  | [] -> IntSet.empty
  | x::l' -> IntSet.add x (list_to_set l')

(* Generates predicates for an instruction *)
let predicate_gen line instr =
	let succ_set = IntSet.singleton (line+1) in
			match instr with
			| LLIR.BINOP(_, dest, op) -> 
          (match (dest, op) with
            | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
                (IntSet.singleton (Temp.get_int d), list_to_set [Temp.get_int d; Temp.get_int t], succ_set)

            | (LLIR.REG(r), LLIR.TEMP(t)) ->
                (IntSet.singleton (reg_to_temp r), list_to_set [Temp.get_int t; reg_to_temp r], succ_set)

            | (LLIR.TEMP(d), LLIR.IMM(_)) -> 
                (IntSet.singleton (Temp.get_int d), IntSet.singleton (Temp.get_int d), succ_set)

            | (LLIR.REG(r), LLIR.IMM(_)) ->
                (IntSet.singleton (reg_to_temp r), IntSet.singleton (reg_to_temp r), succ_set)

            | (LLIR.REG r1, LLIR.REG r2) ->
                (IntSet.singleton (reg_to_temp r1), list_to_set [reg_to_temp r1; reg_to_temp r2], succ_set)

            | (LLIR.TEMP t, LLIR.REG r) ->
                (IntSet.singleton (Temp.get_int t), list_to_set [Temp.get_int t; reg_to_temp r], succ_set)

            | (LLIR.IMM _, _) -> ErrorMsg.error None ("IMM cannot be a destination for a BINOP");
                   raise ErrorMsg.Error)

            (*| _ -> ErrorMsg.error None ("Operand and destination are not of type reg or temp in BINOP!");
                   raise ErrorMsg.Error)*)
      
      | LLIR.MOV(dest, src) -> 
          (match (dest, src) with 
            | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
                (IntSet.singleton (Temp.get_int d), IntSet.singleton (Temp.get_int t), succ_set)

            | (LLIR.REG r, LLIR.TEMP t) ->
                (IntSet.singleton (reg_to_temp r), IntSet.singleton (Temp.get_int t), succ_set)

            | (LLIR.REG r1, LLIR.REG r2) ->
                (IntSet.singleton (reg_to_temp r1), IntSet.singleton (reg_to_temp r2), succ_set)

            | (LLIR.TEMP d, LLIR.IMM _) -> 
                (IntSet.singleton (Temp.get_int d), IntSet.empty, succ_set)

            | (LLIR.REG r, LLIR.IMM _) ->
                (IntSet.singleton (reg_to_temp r), IntSet.empty, succ_set)

            | (LLIR.TEMP t, LLIR.REG r) ->
                (IntSet.singleton (Temp.get_int t), IntSet.singleton (reg_to_temp r), succ_set)

            | (LLIR.IMM _, _) -> ErrorMsg.error None ("IMM cannot be a destination for a MOV");
                   raise ErrorMsg.Error

            (*| _ -> ErrorMsg.error None ("Operand and destination are not of type reg or temp in MOV!");
                   raise ErrorMsg.Error *)
          )

      | LLIR.UNOP(operation, op) ->
          (match operation with
            | LLIR.IDIV -> match op with
                      | LLIR.TEMP(t) -> 
                          (list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX],
                           list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX; Temp.get_int t],
                           succ_set)
                      | _ -> ErrorMsg.error None ("Not supposed to get a non-temp in IDIV");
                             raise ErrorMsg.Error)

            (* | _ -> raise (WrongPattern "Not handling a unop that is not IDIV") *)
            (* Commented for now; the compiler knows no such unop exists. *)

      | LLIR.CLTD -> (IntSet.singleton (reg_to_temp S.EDX), IntSet.singleton (reg_to_temp S.EAX), succ_set)

      (* RET defines nothing, uses EAX, has no successor *)
      | LLIR.RET -> (IntSet.empty, IntSet.singleton (reg_to_temp S.EAX), IntSet.empty)

      | _ -> ErrorMsg.error None ("Instr that is not binop, unop, cltd, or ret in instr list!");
             raise ErrorMsg.Error

(* Generates a sequence conataining a set of live variables
   at each line corresponding to the 2 address sequence 
*)
let rec helper pred_list live_set =
	match pred_list with
  | [] -> raise (WrongPattern "predicate list is empty!")
	| [first_line] -> [IntSet.empty]
	| (def_set, use_set, succ_set)::rem_list ->
          let new_liveset = IntSet.union (IntSet.diff live_set def_set) (use_set) in
						new_liveset::(helper rem_list new_liveset)

(* For now it does a linear scan starting from the end to back *)
let liveness_generator instr_list = 
	let pred_list = List.mapi predicate_gen instr_list in
	(List.rev (helper (List.rev pred_list) IntSet.empty), pred_list)

let print_set s = 
  let mid_s =
    match IntSet.elements s with
        [] -> "{"
      | [x] -> "{" ^ (string_of_int x)
      | x::xs -> "{" ^ (string_of_int x) ^ List.fold_left (fun s i -> let new_s = Printf.sprintf ",%d" i in s ^ new_s ) "" xs
  in
    mid_s ^ "}"

module type PRINT = 
  sig
    val pp_livelist : IntSet.t list -> string 
  end

module Print : PRINT = 
  struct
    let pp_livelist l =
      "Live list {\n" ^
      List.fold_left (fun s liveset -> let set_s = print_set liveset in
                                            s ^ set_s ^ "\n") "" l
      ^ "}\n"
    end

