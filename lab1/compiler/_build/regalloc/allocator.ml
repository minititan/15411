(* module to implement register or stack allocation *)

open Llir
open Regalloc_util

module L = Liveness_gen
module C = Coloring

let color_to_storage need_stack c =
  if (need_stack && (c < stack_min_temp)) then
      Storage.STACK(word_size*(stack_min_temp - c))
  else Storage.REG(temp_to_reg c)


let get_storage_map color_map = 
  let (k, some_v) = IntMap.choose color_map in
  let min_color = IntMap.fold (fun k v min_c -> if (v < min_c) then v else min_c) color_map some_v in
  if (min_color < min_temp) then
    IntMap.map (color_to_storage true) color_map
  else
    IntMap.map (color_to_storage false) color_map

let format k v = 
  let s = Printf.sprintf "t%d -> " k in
  match v with 
  | Storage.STACK(o) -> let s2 = Printf.sprintf "STACK(%d)\n" o in s ^ s2
  | Storage.REG(r) -> s ^ Storage.format_reg r ^ "\n" 

module type PRINT =
  sig
    val pp_storagemap : Storage.str IntMap.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_storagemap m =
      "Register mapping {\n" ^
      IntMap.fold (fun k v s -> s ^ (format k v)) m ""
      ^ "}\n"
  end

(*
let get_storage_map instr_list = 
  let liveness_list = L.liveness_generator instr_list in 
  let color_map = C.get_color_map liveness_list in
  let (k, min_color) = IntMap.min_binding color_map in

  if (min_color < min_temp) then
    (* since we need a register to do the stack moves *)
    let stack_spots = min_temp - min_color in

    (* We don't have enough registers so first find
      stack_spots number of color class
      with the least elements *)
    let color_to_vertex = collect_vals color_map in

    let color_cmp (c1, vxs1) (c2, vxs2) = 
      (* if the color class contains register it must always lose comparison *)
      let vxs1_has_reg = BatList.exists (fun x -> x < 0) vxs1 in
      let vxs2_has_reg = BatList.exists (fun x -> x < 0) vxs2 in
      let vxs1_l = List.length vxs1 in
      let vxs2_l = List.length vxs2 in
      if (vxs1_has_reg and vxs2_has_reg) then 0
      else if (vxs1_has_reg) then 1
      else if (vxs2_has_reg) then -1
      else if (vxs1_l > vxs2_l) then 1
      else if (vxs2_l > vxs1_l) then -1
      else 0
    in

    (* We sort it to get the sizes of the color classes to pick
      the smallest color classes that dont contain a register 
     to store on the stack *)
    let rev_color_sizes = List.sort color_cmp (IntMap.bindings color_to_vertex)

    in

    (* We now have a list of (color, temp list) sorted by sizes of temp list
      So we now replace the smallest ones by stack storage *)
    let (stack_list)
    in storage_map
  else
    IntMap.empty
*)