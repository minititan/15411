(* Holds utility stuff for register allocation phase *)

module LLIR = Llir
module S = Storage


module type IntSet_type = Printable.SET with type elt = int

(* Using printable sets for debugging *)
module IntSet = Printable.MakeSet(struct 
                                    type t = int 
                                    let compare = compare 
                                    let format ff e = Format.fprintf ff "%d" (e)
                                  end)

module type IntMap_type = Printable.MAP with type key = int

(* Using printable sets for debugging *)
module IntMap = Printable.MakeMap(struct 
                                    type t = int 
                                    let compare = compare 
                                    let format ff e = Format.fprintf ff "%d" (e)
                                  end)

exception WrongPattern of string

let min_temp = -14
let stack_min_temp = -13
let word_size = 4

(* Function that maps from registers to negative temps *)
let reg_to_temp = function
    S.EAX -> -1
  | S.EBX -> -2
  | S.ECX -> -3
  | S.EDX -> -4
  | S.EBP -> -5
  | S.EDI -> -6
  | S.R8  -> -7
  | S.R9  -> -8
  | S.R10 -> -9
  | S.R11 -> -10
  | S.R12 -> -11
  | S.R13 -> -12
  | S.R14 -> -13
  | S.R15 -> -14
  | _ -> raise (WrongPattern "Reserved register being used!")


let temp_to_reg = function
    -1 -> S.EAX
  | -2 -> S.EBX
  | -3 -> S.ECX
  | -4 -> S.EDX
  | -5 -> S.EBP
  | -6 -> S.EDI
  | -7 -> S.R8
  | -8 -> S.R9
  | -9 -> S.R10
  | -10 -> S.R11
  | -11 -> S.R12
  | -12 -> S.R13
  | -13 -> S.R14
  | -14 -> S.R15
  | x -> let s = Printf.sprintf "Integer %d not assigned to register!" x in 
              raise (WrongPattern s)



