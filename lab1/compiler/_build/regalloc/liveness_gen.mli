(* Module to implement liveness gneration
*)

open Regalloc_util

val liveness_generator : Llir.instr list -> IntSet.t list * ((IntSet.t * IntSet.t * IntSet.t) list)

module type PRINT = 
  sig
    val pp_livelist : IntSet.t list -> string 
  end

module Print : PRINT
