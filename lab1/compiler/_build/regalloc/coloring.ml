(* Module that gives a coloring of a list of vertices *)

open Graph
open Regalloc_util

module Int = struct
  type t = int
  let compare = compare
  let hash = Hashtbl.hash
  let equal = (=)
  let default = 0
end

module G = Imperative.Graph.Concrete(Int)
module CQT = Cliquetree.CliqueTree(G)

let g = G.create()

let connect_v v v' = 
	G.add_edge g v v'


(* Connects 2 sets of vertices completely *)
let connect_all s1 s2 = 
	let l1 = IntSet.elements s1 in
	let l2 = IntSet.elements s2 in
	List.iter (fun v -> List.iter (fun v' -> connect_v v v' ) l2) l1

(* Add all defined vertices *)
let add_all_vertex pred_list = 
	let iter_help (def_set, use_set, succ_set) = 
		List.iter (fun v -> G.add_vertex g v) (IntSet.elements def_set)
	in
		List.iter (iter_help) pred_list


(* creates the graph using the list of live variables and returns it *)
let create_graph liveset_arr pred_list =
	let _ = add_all_vertex pred_list in
	let l = List.length pred_list in
	(* adds the edge b/w v and v' to g*)
	List.iteri (fun i (def, _, _) -> 
					if (i = l-1) then ()
					else connect_all def liveset_arr.(i+1)) pred_list

(* Use Ocaml's CliqueTree module to get a perfect elimination
   ordering from the graph 
 *)
let get_ordering () = 
	(*if (CQT.is_chordal g) then
		let _ = prerr_endline "IM HERE" in 
		let (ordering, x, y) = CQT.mcs_clique g in
			ordering
	else*)
		IntSet.elements (G.fold_vertex (IntSet.add) g IntSet.empty)

(* Searches for lowest color not taken by neighbors in g not in l 
   If vertex is register then simply returns number since register
   is color itself. 
*)
let get_color v color_map =
	match v with 
	| _ when (v < 0) -> v

	| _ ->
		let neighbors = G.succ g v in 

		let fold_helper color_set v' = 
			if (IntMap.mem v' color_map) then
				IntSet.add (IntMap.find v' color_map) color_set
			else
				color_set
		in

		let neighbor_colors = List.fold_left fold_helper IntSet.empty neighbors in

		let rec get_available_color c_set c = 
			if (IntSet.mem c c_set) then get_available_color c_set (c-1)
			else c
		in
			get_available_color neighbor_colors (-1)


(* Returns a mapping from vertex to color assigned in a 
   greedy coloring based on ordering given
 *)
let get_coloring v_ordering =
	let rec color_helper vlist c_map = 
		match vlist with
		|  [] -> c_map
		| 	v::l -> let c = get_color v c_map in 
						color_helper l (IntMap.add v c c_map)
	
	in
	color_helper v_ordering IntMap.empty

let get_color_map live_list pred_list = 
	let () = create_graph live_list pred_list in
	let ordering = get_ordering () in
	let mapping = get_coloring ordering in
	mapping

let format k v  = 
	Printf.sprintf "%d -> %d\n" k v


module type PRINT =
  sig
    val pp_colormap : int IntMap.t -> string
  end

module Print : PRINT = 
  struct 
    let pp_colormap m =
    	let (k, mincolor) = IntMap.min_binding m in
		let info = Printf.sprintf "Min binding: %d, %d\n" k mincolor in
      "Color mapping {\n" ^
      IntMap.fold (fun k v s -> s ^ (format k v)) m ""
      ^ info ^ "}\n"
  end
