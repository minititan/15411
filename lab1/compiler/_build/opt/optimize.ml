module AS = Assem

open Storage

let rec optimize = function
    [] -> []
  | inst::insts ->
    match inst with
        AS.MOV (AS.STR (REG r1), AS.STR (REG r2)) ->
          if r1 = r2 then optimize insts
          else inst::(optimize insts)
      | _ -> inst::(optimize insts)