(* L1 Compiler
 * Top Level Environment
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

open Regalloc_util

let say = print_endline
let newline = print_newline

let flag_verbose = Flag.flag "verbose"
let flag_ast = Flag.flag "ast"
let flag_ir = Flag.flag "ir"
let flag_assem = Flag.flag "assem"
let flag_parse = Flag.flag "parse"
let flag_irinst = Flag.flag "irinst"
let flag_llir = Flag.flag "llir"
let flag_coloring = Flag.flag "coloring"
let flag_optimize = Flag.flag "optimize"
let flag_livelist = Flag.flag "livelist"

let reset_flags () =
  List.iter Flag.unset [flag_verbose; flag_ast; flag_ir; flag_assem;
  flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist]

let set flag = Arg.Unit (fun () -> Flag.set flag)

let set_all () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir; flag_assem;
  flag_parse; flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist])

let set_code () =
  Arg.Unit (fun () -> List.iter Flag.set [flag_verbose; flag_ast; flag_ir; flag_assem;
  flag_irinst; flag_llir; flag_coloring; flag_optimize; flag_livelist])

let options =
  [("--verbose", set flag_verbose, "verbose message");
   ("-v", set flag_verbose, "verbose message");
   ("--debug-parse", set flag_parse, "debug the parser");
   ("--dump-ast", set flag_ast, "pretty print the AST");
   ("--dump-ir", set flag_ir, "pretty print the IR");
   ("--dump-irinst", set flag_irinst, "pretty print the IR instructions");
   ("--dump-llir", set flag_llir, "pretty print the LLIR instructions");
   ("--dump-coloring", set flag_coloring, "pretty print the coloring map");
   ("--dump-assem", set flag_assem, "pretty print the assembly");
   ("--dump-opt", set flag_optimize, "pretty print the optimized assembly");
   ("--dump-livelist", set flag_livelist, "pretty print the live list");
   ("--all", set_all() , "set all print flags (super verbose mode)");
   ("--code", set_code(), "set all flags related to code generation")
 ]

exception EXIT

let stem s =
  try
    let dot = String.rindex s '.' in
    String.sub s 0 dot
  with Not_found -> s

let (@@) f g x = f (g x)

let main args =
  try
    let header = "Usage: compile [OPTION...] SOURCEFILE\nwhere OPTION is" in
    let usageinfo () = Arg.usage options header in
    let errfn msg = say (msg ^ "\n"); usageinfo (); raise EXIT in

    let _ = Temp.reset () in
    let _ = reset_flags () in

    let _ = if Array.length args < 2 then (usageinfo (); raise EXIT) in

    (* Parse the arguments.  Non-keyword arguments accumulate in fref,
       and then is assigned to files. *)
    let files =
      let fref = ref []
      and current = ref 0 in
      let () = Arg.parse_argv ~current args options
          (fun s -> fref := s::!fref) header in
      List.rev !fref in

    let source = match files with
      [] -> errfn "Error: no input file"
    | [filename] -> filename
    | _ -> errfn "Error: more than one input file" in

    (* Parse *)
    let _ = Flag.guard flag_verbose say ("Parsing... " ^ source) in
    let _ = Flag.guard flag_parse
        (fun _ -> ignore (Parsing.set_trace true)) () in
    let ast = Parse.parse source in
    let _ = Flag.guard flag_ast
        (fun () -> say (Ast.Print.pp_program ast)) () in

    (* Typecheck *)
    let _ = Flag.guard flag_verbose say "Checking..." in
    let _ = TypeChecker.typecheck ast in

    (* Translate *)
    let _ = Flag.guard flag_verbose say "Translating..." in
    let irtree = Trans.translate ast in
    let _ = Flag.guard flag_ir (fun () -> say (Tree.Print.pp_program irtree)) () in

    (* IR *)
    let _ = Flag.guard flag_verbose say "Generating IR..." in
    let irinst = Codegen.codegen irtree in
    let _ = Flag.guard flag_irinst (fun () -> say (Ir.Print.pp_program irinst)) () in

    let _ = Flag.guard flag_verbose say "Generating LLIR..." in
    let llir = Llir.llirgen irinst in
    let _ = Flag.guard flag_llir (fun () -> say (Llir.Print.pp_program llir)) () in

    (*let _ = Flag.guard flag_verbose say "Generating live list..." in
    let (live_list, pred_list) = Liveness_gen.liveness_generator llir in
    let _ = Flag.guard flag_livelist (fun () -> say (Liveness_gen.Print.pp_livelist live_list)) () in

    let _ = Flag.guard flag_verbose say "Generating color map..." in
    let color_map = Coloring.get_color_map (Array.of_list live_list) pred_list in
    let _ = Flag.guard flag_coloring (fun () -> say (Coloring.Print.pp_colormap color_map)) () in

    let _ = Flag.guard flag_verbose say "Generating storage map..." in
    let storage_map = Allocator.get_storage_map color_map in
    let _ = Flag.guard flag_coloring (fun () -> say (Allocator.Print.pp_storagemap storage_map)) () in
    *)
    
    let _ = Flag.guard flag_verbose say "Generating assembly..." in
    let assem = Assemgen.assembly_gen llir IntMap.empty (*storage_map*) in
    let _ = Flag.guard flag_assem
        (fun () -> say (Assem.Print.pp_program assem)) () in

(*
    let _ = Flag.guard flag_verbose say "Optimizing.." in
    let assem_opt = Optimize.optimize assem in
    let _ = Flag.guard flag_verbose
        (fun () -> say (Assem.Print.pp_program assem_opt)) () in
*)
    (* Add assembly header and footer *)
    let assem =
      [Assem.DIRECTIVE(".file\t\"" ^ source ^ "\"")]
      @ assem
      @ [Assem.DIRECTIVE ".ident\t\"15-411 L1 reference compiler\""] in

    let pre_code = Headers_gen.get_headers_string in
    let code = pre_code ^ (String.concat "" (List.map Assem.format assem)) in

    (* Output assembly *)
    let afname = stem source ^ ".s" in
    let _ = Flag.guard flag_verbose
        say ("Writing assembly to " ^ afname ^ " ...") in
    let _ = SafeIO.withOpenOut afname
        (fun afstream -> output_string afstream code) in
    (* Return success status *)
    0

  with
    ErrorMsg.Error -> say "Compilation failed"; 1
  | EXIT -> 1
  | Arg.Help x -> prerr_string x; 1
  | e -> prerr_string (Printexc.to_string e); 1


let test s =
  main (Array.of_list (""::(Str.split (Str.regexp "[ \t\n]+") s)))
