(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(* Ned: from assem.ml
 * We want 3-addr for our IR and 2-addr for assem,
 * so we use this to define a different instruction
 * type.
 *
 * Therefore we have these instruction types, each
 * progressively lower lever: IR, LLIR, AS.
 *)

module S = Storage

type operand =
    IMM of Int32.t
  | REG of S.reg
  | TEMP of Temp.temp

type operation = ADD | SUB | MUL | DIV | MOD

type instr =
    BINOP of operation * operand * operand * operand
  | MOV of operand * operand
  | RET
  | DIRECTIVE of string
  | COMMENT of string

type program = instr list

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_operation : operation -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_operand = function
      IMM i -> Int32.to_string i
    | REG r -> S.format_reg r
    | TEMP t -> Temp.name t

    let pp_operation = function
      ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "MUL"
    | DIV -> "DIV"
    | MOD -> "MOD"

    let pp_instr = function
      BINOP (oper, op1, op2, op3) -> pp_operation oper ^ " " ^ pp_operand op1 ^ " <- " ^ pp_operand op2 ^ "," ^ pp_operand op3
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ "," ^ pp_operand s
    | DIRECTIVE s -> "DIRECTIVE: " ^ s
    | COMMENT s -> "COMMENT: " ^ s
    | RET -> "RET"
    
    let rec pp_instrs = function
      [] -> ""
    | instr::instrs' -> pp_instr instr ^ "\n" ^ pp_instrs instrs'

    let pp_program instrs = "IR code {\n" ^ pp_instrs instrs ^ "}\n"
  end
