type reg = EAX
        |  EBX
        |  ECX
        |  EDX
        |  EBP
        |  ESP
        |  ESI
        |  EDI
        |  R8
        |  R9
        |  R10
        |  R11
        |  R12
        |  R13
        |  R14
        |  R15

type offset = int

type str = REG of reg | STACK of offset

let format_reg = function
  | EAX -> "%eax"
  | EBX -> "%ebx"
  | ECX -> "%ecx"
  | EDX -> "%edx"
  | EBP -> "%ebp"
  | ESP -> "%esp"
  | ESI -> "%esi"
  | EDI -> "%edi"
  | R8 -> "%r8"
  | R9 -> "%r9"
  | R10 -> "%r10"
  | R11 -> "%r11"
  | R12 -> "%r12"
  | R13 -> "%r13"
  | R14 -> "%r14"
  | R15 -> "%r15"

let format = function
    REG r -> format_reg r
  | STACK o -> "STACK (" ^ string_of_int o ^ ")"
