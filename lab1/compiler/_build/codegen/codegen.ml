(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 *)

module T = Tree
module IR = Ir
module S = Storage

let munch_op = function
    T.ADD -> IR.ADD
  | T.SUB -> IR.SUB
  | T.MUL -> IR.MUL
  | T.DIV -> IR.DIV
  | T.MOD -> IR.MOD

(* munch_exp : IR.operand -> T.exp -> IR.instr list *)
(* munch_exp d e
 * generates instructions to achieve d <- e
 * d must be TEMP(t) or REG(r)
 *)
let rec munch_exp d = function
    T.CONST n -> [IR.MOV (d, IR.IMM n)]
  | T.TEMP t -> [IR.MOV(d, IR.TEMP t)]
  | T.BINOP (binop, e1, e2) ->
      munch_binop d (binop, e1, e2)

(* munch_binop : IR.operand -> T.binop * T.exp * T.exp -> IR.instr list *)
(* munch_binop d (binop, e1, e2)
 * generates instruction to achieve d <- e1 binop e2
 * d must be TEMP(t) or REG(r)
 *)
and munch_binop d (binop, e1, e2) =
    let operator = munch_op binop
    and t1 = IR.TEMP (Temp.create ())
    and t2 = IR.TEMP (Temp.create ()) in
    munch_exp t1 e1
    @ munch_exp t2 e2
    @ [IR.BINOP (operator, d, t1, t2)]

(* munch_stm : T.stm -> IR.instr list *)
(* munch_stm stm generates code to execute stm *)
let munch_stm = function
    T.MOVE (T.TEMP t1, e2) -> munch_exp (IR.TEMP t1) e2
  | T.RETURN e ->
      (* return e is implemented as %eax <- e *)
      munch_exp (IR.REG S.EAX) e @ [IR.RET]
  | _ -> assert false

let rec codegen = function
    [] -> []
  | stm::stms -> munch_stm stm @ codegen stms

