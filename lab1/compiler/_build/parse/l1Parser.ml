type token =
  | EOF
  | STRUCT
  | TYPEDEF
  | IF
  | ELSE
  | WHILE
  | FOR
  | CONTINUE
  | BREAK
  | ASSERT
  | TRUE
  | FALSE
  | NULL
  | ALLOC
  | ALLOCARRY
  | BOOL
  | VOID
  | CHAR
  | STRING
  | SEMI
  | DECCONST of (Int32.t)
  | HEXCONST of (Int32.t)
  | IDENT of (Symbol.symbol)
  | RETURN
  | INT
  | MAIN
  | PLUS
  | MINUS
  | STAR
  | SLASH
  | PERCENT
  | ASSIGN
  | PLUSEQ
  | MINUSEQ
  | STAREQ
  | SLASHEQ
  | PERCENTEQ
  | LBRACE
  | RBRACE
  | LPAREN
  | RPAREN
  | UNARY
  | ASNOP

open Parsing;;
let _ = parse_error;;
# 2 "parse/l1Parser.mly"
(* L1 Compiler
 * L1 grammar
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Now conforms to the L1 fragment of C0
 *
 * Modified: Maxime Serrano <mserrano@andrew.cmu.edu> Fall 2014
 * Should be more up-to-date with 2014 spec
 *
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

module A = Ast

let ploc (left, right) =
  Parsing.rhs_start left, Parsing.rhs_end right
let mark e (left, right) = (* for expressions *)
  A.Marked (Mark.mark' (e, ParseState.ext (ploc (left, right))))
let marks e (left, right) = (* for statements *)
  A.Markeds (Mark.mark' (e, ParseState.ext (ploc (left, right))))

(* expand_asnop (id, "op=", exp) region = "id = id op exps"
 * or = "id = exp" if asnop is "="
 * syntactically expands a compound assignment operator
 *)
let expand_asnop a b =
  match a, b with
    (id, None, exp), (left, right) ->
      A.Assign(id, exp)
  | (id, Some oper, exp), (left, right) ->
      A.Assign(id, mark (A.OpExp (oper, [A.Var(id); exp])) (left, right))

# 84 "parse/l1Parser.ml"
let yytransl_const = [|
    0 (* EOF *);
  257 (* STRUCT *);
  258 (* TYPEDEF *);
  259 (* IF *);
  260 (* ELSE *);
  261 (* WHILE *);
  262 (* FOR *);
  263 (* CONTINUE *);
  264 (* BREAK *);
  265 (* ASSERT *);
  266 (* TRUE *);
  267 (* FALSE *);
  268 (* NULL *);
  269 (* ALLOC *);
  270 (* ALLOCARRY *);
  271 (* BOOL *);
  272 (* VOID *);
  273 (* CHAR *);
  274 (* STRING *);
  275 (* SEMI *);
  279 (* RETURN *);
  280 (* INT *);
  281 (* MAIN *);
  282 (* PLUS *);
  283 (* MINUS *);
  284 (* STAR *);
  285 (* SLASH *);
  286 (* PERCENT *);
  287 (* ASSIGN *);
  288 (* PLUSEQ *);
  289 (* MINUSEQ *);
  290 (* STAREQ *);
  291 (* SLASHEQ *);
  292 (* PERCENTEQ *);
  293 (* LBRACE *);
  294 (* RBRACE *);
  295 (* LPAREN *);
  296 (* RPAREN *);
  297 (* UNARY *);
  298 (* ASNOP *);
    0|]

let yytransl_block = [|
  276 (* DECCONST *);
  277 (* HEXCONST *);
  278 (* IDENT *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\003\000\003\000\004\000\004\000\
\004\000\004\000\005\000\007\000\007\000\007\000\006\000\006\000\
\006\000\006\000\006\000\006\000\006\000\006\000\006\000\006\000\
\006\000\006\000\009\000\009\000\008\000\008\000\008\000\008\000\
\008\000\008\000\000\000"

let yylen = "\002\000\
\008\000\000\000\002\000\002\000\002\000\003\000\002\000\004\000\
\002\000\004\000\003\000\001\000\001\000\003\000\003\000\001\000\
\001\000\001\000\003\000\003\000\003\000\003\000\003\000\003\000\
\005\000\002\000\001\000\001\000\001\000\001\000\001\000\001\000\
\001\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\035\000\000\000\000\000\000\000\000\000\
\012\000\000\000\000\000\013\000\000\000\000\000\000\000\000\000\
\000\000\000\000\027\000\028\000\018\000\017\000\000\000\000\000\
\000\000\016\000\000\000\000\000\000\000\000\000\003\000\004\000\
\005\000\029\000\030\000\031\000\032\000\033\000\034\000\000\000\
\000\000\000\000\026\000\000\000\006\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\014\000\001\000\000\000\000\000\
\000\000\015\000\000\000\000\000\021\000\022\000\023\000\000\000\
\000\000\000\000\025\000"

let yydgoto = "\002\000\
\004\000\014\000\015\000\016\000\017\000\044\000\018\000\040\000\
\026\000"

let yysindex = "\005\000\
\005\255\000\000\251\254\000\000\243\254\250\254\012\255\236\254\
\000\000\252\254\002\255\000\000\032\255\017\255\236\254\040\255\
\041\255\054\255\000\000\000\000\000\000\000\000\023\255\252\254\
\053\255\000\000\030\255\038\255\028\255\076\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\252\254\
\023\255\031\255\000\000\037\255\000\000\252\254\252\254\252\254\
\252\254\252\254\252\254\252\254\000\000\000\000\065\255\000\000\
\023\255\000\000\045\255\045\255\000\000\000\000\000\000\065\255\
\065\255\044\255\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\063\255\
\000\000\000\000\000\000\000\000\000\000\000\000\063\255\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\059\255\083\255\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\084\255\238\254\
\000\000\000\000\244\254\006\255\000\000\000\000\000\000\085\255\
\086\255\070\255\000\000"

let yygindex = "\000\000\
\000\000\091\000\000\000\000\000\000\000\246\255\094\000\000\000\
\000\000"

let yytablesize = 107
let yytable = "\025\000\
\024\000\009\000\010\000\011\000\012\000\001\000\019\000\024\000\
\024\000\026\000\026\000\026\000\043\000\019\000\019\000\019\000\
\020\000\021\000\013\000\005\000\022\000\024\000\023\000\027\000\
\020\000\006\000\028\000\019\000\003\000\055\000\056\000\020\000\
\020\000\007\000\024\000\059\000\060\000\061\000\062\000\063\000\
\064\000\065\000\019\000\020\000\021\000\020\000\066\000\022\000\
\008\000\041\000\019\000\020\000\021\000\009\000\030\000\022\000\
\012\000\057\000\032\000\033\000\051\000\042\000\046\000\047\000\
\048\000\049\000\050\000\053\000\052\000\024\000\013\000\045\000\
\048\000\049\000\050\000\054\000\058\000\007\000\046\000\047\000\
\048\000\049\000\050\000\067\000\034\000\035\000\036\000\037\000\
\038\000\039\000\046\000\047\000\048\000\049\000\050\000\026\000\
\026\000\026\000\026\000\026\000\002\000\009\000\011\000\008\000\
\010\000\031\000\029\000"

let yycheck = "\010\000\
\019\001\022\001\023\001\024\001\025\001\001\000\019\001\026\001\
\027\001\028\001\029\001\030\001\023\000\026\001\027\001\020\001\
\021\001\022\001\039\001\025\001\025\001\040\001\027\001\022\001\
\019\001\039\001\025\001\040\001\024\001\040\000\041\000\026\001\
\027\001\040\001\039\001\046\000\047\000\048\000\049\000\050\000\
\051\000\052\000\020\001\021\001\022\001\040\001\057\000\025\001\
\037\001\027\001\020\001\021\001\022\001\022\001\038\001\025\001\
\025\001\027\001\019\001\019\001\031\001\039\001\026\001\027\001\
\028\001\029\001\030\001\040\001\031\001\039\001\039\001\019\001\
\028\001\029\001\030\001\000\000\040\001\019\001\026\001\027\001\
\028\001\029\001\030\001\040\001\031\001\032\001\033\001\034\001\
\035\001\036\001\026\001\027\001\028\001\029\001\030\001\026\001\
\027\001\028\001\029\001\030\001\038\001\019\001\019\001\019\001\
\019\001\015\000\013\000"

let yynames_const = "\
  EOF\000\
  STRUCT\000\
  TYPEDEF\000\
  IF\000\
  ELSE\000\
  WHILE\000\
  FOR\000\
  CONTINUE\000\
  BREAK\000\
  ASSERT\000\
  TRUE\000\
  FALSE\000\
  NULL\000\
  ALLOC\000\
  ALLOCARRY\000\
  BOOL\000\
  VOID\000\
  CHAR\000\
  STRING\000\
  SEMI\000\
  RETURN\000\
  INT\000\
  MAIN\000\
  PLUS\000\
  MINUS\000\
  STAR\000\
  SLASH\000\
  PERCENT\000\
  ASSIGN\000\
  PLUSEQ\000\
  MINUSEQ\000\
  STAREQ\000\
  SLASHEQ\000\
  PERCENTEQ\000\
  LBRACE\000\
  RBRACE\000\
  LPAREN\000\
  RPAREN\000\
  UNARY\000\
  ASNOP\000\
  "

let yynames_block = "\
  DECCONST\000\
  HEXCONST\000\
  IDENT\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _6 = (Parsing.peek_val __caml_parser_env 2 : 'stmts) in
    Obj.repr(
# 74 "parse/l1Parser.mly"
                                                 ( _6 )
# 278 "parse/l1Parser.ml"
               : Ast.stm list))
; (fun __caml_parser_env ->
    Obj.repr(
# 78 "parse/l1Parser.mly"
                                ( [] )
# 284 "parse/l1Parser.ml"
               : 'stmts))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'stmt) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'stmts) in
    Obj.repr(
# 79 "parse/l1Parser.mly"
                                ( _1::_2 )
# 292 "parse/l1Parser.ml"
               : 'stmts))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'decl) in
    Obj.repr(
# 83 "parse/l1Parser.mly"
                                 ( marks (A.Declare _1) (1, 1) )
# 299 "parse/l1Parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'simp) in
    Obj.repr(
# 84 "parse/l1Parser.mly"
                                 ( marks _1 (1, 1) )
# 306 "parse/l1Parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'exp) in
    Obj.repr(
# 85 "parse/l1Parser.mly"
                                 ( marks (A.Return _2) (1, 2) )
# 313 "parse/l1Parser.ml"
               : 'stmt))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : Symbol.symbol) in
    Obj.repr(
# 89 "parse/l1Parser.mly"
                                ( A.NewVar _2 )
# 320 "parse/l1Parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : Symbol.symbol) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 90 "parse/l1Parser.mly"
                                ( A.Init (_2, _4) )
# 328 "parse/l1Parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    Obj.repr(
# 91 "parse/l1Parser.mly"
                                ( A.NewVar (Symbol.symbol "main") )
# 334 "parse/l1Parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 92 "parse/l1Parser.mly"
                                ( A.Init (Symbol.symbol "main", _4) )
# 341 "parse/l1Parser.ml"
               : 'decl))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'lvalue) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'asnop) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 96 "parse/l1Parser.mly"
                                 ( expand_asnop (_1, _2, _3) (1, 3) )
# 350 "parse/l1Parser.ml"
               : 'simp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Symbol.symbol) in
    Obj.repr(
# 100 "parse/l1Parser.mly"
                                ( _1 )
# 357 "parse/l1Parser.ml"
               : 'lvalue))
; (fun __caml_parser_env ->
    Obj.repr(
# 101 "parse/l1Parser.mly"
                                ( Symbol.symbol "main" )
# 363 "parse/l1Parser.ml"
               : 'lvalue))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'lvalue) in
    Obj.repr(
# 102 "parse/l1Parser.mly"
                                ( _2 )
# 370 "parse/l1Parser.ml"
               : 'lvalue))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'exp) in
    Obj.repr(
# 106 "parse/l1Parser.mly"
                                 ( _2 )
# 377 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'intconst) in
    Obj.repr(
# 107 "parse/l1Parser.mly"
                                 ( _1 )
# 384 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    Obj.repr(
# 108 "parse/l1Parser.mly"
                                 ( mark (A.Var (Symbol.symbol "main")) (1, 1) )
# 390 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Symbol.symbol) in
    Obj.repr(
# 109 "parse/l1Parser.mly"
                                 ( mark (A.Var _1) (1, 1) )
# 397 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 110 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.PLUS, [_1; _3])) (1, 3) )
# 405 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 111 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.MINUS, [_1; _3])) (1, 3) )
# 413 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 112 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.TIMES, [_1; _3])) (1, 3) )
# 421 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 113 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.DIVIDEDBY, [_1; _3]))
                                     (1, 3) )
# 430 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 115 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.MODULO, [_1; _3])) (1, 3) )
# 438 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 116 "parse/l1Parser.mly"
                                 ( mark _3 (1, 3) )
# 445 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'exp) in
    Obj.repr(
# 117 "parse/l1Parser.mly"
                                 ( mark  _4 (1, 5) )
# 452 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'exp) in
    Obj.repr(
# 118 "parse/l1Parser.mly"
                                 ( mark (A.OpExp (A.NEGATIVE, [_2])) (1, 2) )
# 459 "parse/l1Parser.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Int32.t) in
    Obj.repr(
# 122 "parse/l1Parser.mly"
                     ( A.ConstExp _1 )
# 466 "parse/l1Parser.ml"
               : 'intconst))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : Int32.t) in
    Obj.repr(
# 123 "parse/l1Parser.mly"
                     ( A.ConstExp _1 )
# 473 "parse/l1Parser.ml"
               : 'intconst))
; (fun __caml_parser_env ->
    Obj.repr(
# 127 "parse/l1Parser.mly"
                                ( None )
# 479 "parse/l1Parser.ml"
               : 'asnop))
; (fun __caml_parser_env ->
    Obj.repr(
# 128 "parse/l1Parser.mly"
                                ( Some A.PLUS )
# 485 "parse/l1Parser.ml"
               : 'asnop))
; (fun __caml_parser_env ->
    Obj.repr(
# 129 "parse/l1Parser.mly"
                                ( Some A.MINUS )
# 491 "parse/l1Parser.ml"
               : 'asnop))
; (fun __caml_parser_env ->
    Obj.repr(
# 130 "parse/l1Parser.mly"
                                ( Some A.TIMES )
# 497 "parse/l1Parser.ml"
               : 'asnop))
; (fun __caml_parser_env ->
    Obj.repr(
# 131 "parse/l1Parser.mly"
                                ( Some A.DIVIDEDBY )
# 503 "parse/l1Parser.ml"
               : 'asnop))
; (fun __caml_parser_env ->
    Obj.repr(
# 132 "parse/l1Parser.mly"
                                ( Some A.MODULO )
# 509 "parse/l1Parser.ml"
               : 'asnop))
(* Entry program *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let program (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.stm list)
;;
# 136 "parse/l1Parser.mly"

# 536 "parse/l1Parser.ml"
