	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests2/barium-type-error05.l2"
	SUBQ	$48, %rsp
	MOVL	$1, 4(%rsp)
	MOVL	$1, 8(%rsp)
	MOVL	8(%rsp), %r15d
	CMPL	%r15d,4(%rsp)
	JG L1
	JMP L2
L1:
	MOVL	$1, 12(%rsp)
	JMP L3
L2:
	MOVL	$0, 12(%rsp)
	JMP L3
L3:
	MOVL	$1, %eax
	MOVL	%eax, %eax
	ADDQ	$48, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
