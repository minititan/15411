	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests2/faramir-while_overflow.l2"
	SUBQ	$64, %rsp
	MOVL	$30, %r15d
	CMPL	$32,%r15d
	JL L7
	JMP L5
L7:
	MOVL	$30, %r15d
	CMPL	$0,%r15d
	JGE L4
	JMP L5
L4:
	MOVL	$17, %r14d
	MOVL	$30, %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L6
L5:
	MOVL	$0, 12(%rsp)
	MOVL	$42, %eax
	CLTD
	IDIVL	12(%rsp)
	MOVL	%eax, 16(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L6
L6:
L3:
	MOVL	4(%rsp), %r15d
	CMPL	$0,%r15d
	JGE L1
	JMP L2
L1:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L3
L2:
	MOVL	$5, %eax
	MOVL	%eax, %eax
	ADDQ	$64, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
