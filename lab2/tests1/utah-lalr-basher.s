	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/utah-lalr-basher.l2"
	SUBQ	$64, %rsp
	MOVL	$0, 4(%rsp)
	MOVL	$1, 8(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 4(%rsp)
L6:
	CMPL	$5,4(%rsp)
	JL L4
	JMP L5
L4:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L6
L5:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	ADDL	$4, %r15d
	MOVL	%r15d, 16(%rsp)
L3:
	CMPL	$5,8(%rsp)
	JL L1
	JMP L2
L1:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L3
L2:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	SUBL	8(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$64, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
