	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/return01.l2"
	SUBQ	$48, %rsp
	MOVL	$100, 4(%rsp)
	MOVL	$0, 8(%rsp)
	MOVL	$0, 12(%rsp)
L3:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,12(%rsp)
	JLE L1
	JMP L2
L1:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	12(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 12(%rsp)
	JMP L3
L2:
	MOVL	8(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$48, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
