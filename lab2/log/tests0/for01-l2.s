	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/for01.l2"
	SUBQ	$144, %rsp
	MOVL	$254, 4(%rsp)
	MOVL	$1, 8(%rsp)
	MOVL	$0, 12(%rsp)
L3:
	MOVL	4(%rsp), %r15d
	CMPL	$0,%r15d
	JG L1
	JMP L2
L1:
	MOVL	$2, 32(%rsp)
	MOVL	4(%rsp), %eax
	CLTD
	IDIVL	32(%rsp)
	MOVL	%edx, 36(%rsp)
	MOVL	36(%rsp), %r15d
	CMPL	$0,%r15d
	JE L10
	JMP L11
L10:
	MOVL	$0, 16(%rsp)
	JMP L12
L11:
	MOVL	$1, 16(%rsp)
	JMP L12
L12:
	MOVL	8(%rsp), %r15d
	CMPL	$0,%r15d
	JL L7
	JMP L8
L7:
	MOVL	$0, 24(%rsp)
	MOVL	$1, %eax
	CLTD
	IDIVL	24(%rsp)
	MOVL	%eax, 28(%rsp)
	MOVL	28(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$144, %rsp
	RET
	JMP L9
L8:
	JMP L9
L9:
	MOVL	16(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L5
	JMP L4
L4:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	IMULL	$10, %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L6
L5:
	MOVL	12(%rsp), %r15d
	ADDL	8(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	IMULL	$10, %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L6
L6:
	MOVL	$2, 20(%rsp)
	MOVL	4(%rsp), %eax
	CLTD
	IDIVL	20(%rsp)
	MOVL	%eax, 4(%rsp)
	JMP L3
L2:
	MOVL	12(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$144, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
