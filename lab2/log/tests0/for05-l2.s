	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/for05.l2"
	SUBQ	$176, %rsp
	MOVL	$0, 16(%rsp)
	MOVL	$0, 20(%rsp)
	MOVL	$0, 24(%rsp)
	MOVL	$21, 4(%rsp)
	MOVL	$0, 8(%rsp)
	MOVL	$2, 12(%rsp)
L6:
	MOVL	24(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L8
	JMP L7
L7:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,12(%rsp)
	JL L4
	JMP L5
L8:
	JMP L5
L4:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	12(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	$17, 40(%rsp)
	MOVL	12(%rsp), %eax
	CLTD
	IDIVL	40(%rsp)
	MOVL	%edx, 44(%rsp)
	MOVL	44(%rsp), %r15d
	CMPL	$0,%r15d
	JE L12
	JMP L13
L12:
	MOVL	$1, 24(%rsp)
	JMP L14
L13:
	MOVL	$1, 16(%rsp)
	JMP L14
L14:
	MOVL	24(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L10
	JMP L9
L9:
	MOVL	$1, 20(%rsp)
	JMP L11
L10:
	JMP L11
L11:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	36(%rsp), %r15d
	ADDL	16(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	36(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	ADDL	20(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	JMP L6
L5:
	MOVL	$17, 28(%rsp)
	MOVL	12(%rsp), %eax
	CLTD
	IDIVL	28(%rsp)
	MOVL	%edx, 32(%rsp)
	MOVL	32(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L1
	JMP L2
L1:
	MOVL	8(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$176, %rsp
	RET
	JMP L3
L2:
	MOVL	$0, %eax
	MOVL	%eax, %eax
	ADDQ	$176, %rsp
	RET
	JMP L3
L3:
	.ident	"15-411 L1 reference compiler"
