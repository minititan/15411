	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/shift01.l2"
	SUBQ	$128, %rsp
	MOVL	$1, 4(%rsp)
	MOVL	4(%rsp), %r14d
	MOVL	4(%rsp), %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 4(%rsp)
	MOVL	$32, 28(%rsp)
	MOVL	$33, %eax
	CLTD
	IDIVL	28(%rsp)
	MOVL	%edx, 32(%rsp)
	MOVL	4(%rsp), %r14d
	MOVL	32(%rsp), %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 4(%rsp)
	MOVL	4(%rsp), %r14d
	MOVL	$1, %ecx
	SARL	%cl, %r14d
	MOVL	%r14d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	NOTL	4(%rsp)
	MOVL	$32, 20(%rsp)
	MOVL	$33, %eax
	CLTD
	IDIVL	20(%rsp)
	MOVL	%edx, 24(%rsp)
	MOVL	4(%rsp), %r14d
	MOVL	24(%rsp), %ecx
	SARL	%cl, %r14d
	MOVL	%r14d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ORL	$1, %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	$1, %r14d
	MOVL	$31, %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 16(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ANDL	16(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	$1, %r14d
	MOVL	$31, %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 8(%rsp)
	MOVL	8(%rsp), %r14d
	MOVL	$31, %ecx
	SARL	%cl, %r14d
	MOVL	%r14d, 12(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	XORL	12(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$128, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
