	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/block03.l2"
	SUBQ	$48, %rsp
	MOVL	$-1, 4(%rsp)
	MOVL	$0, 12(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	12(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	$2, 8(%rsp)
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	8(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$48, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
