	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/return02.l2"
	SUBQ	$80, %rsp
	MOVL	$9, 4(%rsp)
	MOVL	$1, 8(%rsp)
	MOVL	$1, 12(%rsp)
	MOVL	$3, 16(%rsp)
L6:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,16(%rsp)
	JLE L4
	JMP L5
L4:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	ADDL	8(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	20(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 16(%rsp)
	JMP L6
L5:
	MOVL	16(%rsp), %r15d
	CMPL	$0,%r15d
	JG L1
	JMP L2
L1:
	MOVL	12(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$80, %rsp
	RET
	JMP L3
L2:
	MOVL	$0, %eax
	MOVL	%eax, %eax
	ADDQ	$80, %rsp
	RET
	JMP L3
L3:
	.ident	"15-411 L1 reference compiler"
