	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/for04.l2"
	SUBQ	$160, %rsp
	MOVL	$3, 4(%rsp)
	MOVL	$0, 8(%rsp)
	MOVL	$1, 12(%rsp)
L12:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,12(%rsp)
	JLE L10
	JMP L11
L10:
	MOVL	$1, 16(%rsp)
L15:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,16(%rsp)
	JLE L13
	JMP L14
L13:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	40(%rsp), %r15d
	IMULL	16(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	40(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	16(%rsp), %r15d
	CMPL	%r15d,12(%rsp)
	JNE L16
	JMP L17
L16:
	JMP L18
L17:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	36(%rsp), %r15d
	IMULL	16(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	36(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L18
L18:
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 16(%rsp)
	JMP L15
L14:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 12(%rsp)
	JMP L12
L11:
	MOVL	$1, 12(%rsp)
L3:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,12(%rsp)
	JLE L1
	JMP L2
L1:
	MOVL	$1, 16(%rsp)
L6:
	MOVL	4(%rsp), %r15d
	CMPL	%r15d,16(%rsp)
	JLE L4
	JMP L5
L4:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	32(%rsp), %r15d
	IMULL	16(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	32(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	16(%rsp), %r15d
	CMPL	%r15d,20(%rsp)
	JNE L7
	JMP L8
L7:
	JMP L9
L8:
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	24(%rsp), %r15d
	SUBL	$1, %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 28(%rsp)
	MOVL	28(%rsp), %r15d
	IMULL	24(%rsp), %r15d
	MOVL	%r15d, 28(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	28(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L9
L9:
	JMP L6
L5:
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 12(%rsp)
	JMP L3
L2:
	MOVL	8(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$160, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
