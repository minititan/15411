	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/if01.l2"
	SUBQ	$176, %rsp
	MOVL	$0, 4(%rsp)
	MOVL	$0, 8(%rsp)
L3:
	MOVL	8(%rsp), %r15d
	CMPL	$8,%r15d
	JL L1
	JMP L2
L1:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	ANDL	$1, %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L4
	JMP L5
L4:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	36(%rsp), %r15d
	ANDL	$2, %r15d
	MOVL	%r15d, 36(%rsp)
	MOVL	36(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L16
	JMP L17
L16:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 44(%rsp)
	MOVL	44(%rsp), %r15d
	ANDL	$4, %r15d
	MOVL	%r15d, 44(%rsp)
	MOVL	44(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L22
	JMP L23
L22:
	MOVL	$7, 12(%rsp)
	JMP L24
L23:
	MOVL	$3, 12(%rsp)
	JMP L24
L24:
	JMP L18
L17:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	40(%rsp), %r15d
	ANDL	$4, %r15d
	MOVL	%r15d, 40(%rsp)
	MOVL	40(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L19
	JMP L20
L19:
	MOVL	$5, 12(%rsp)
	JMP L21
L20:
	MOVL	$1, 12(%rsp)
	JMP L21
L21:
	JMP L18
L18:
	JMP L6
L5:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	24(%rsp), %r15d
	ANDL	$2, %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	24(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L7
	JMP L8
L7:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	32(%rsp), %r15d
	ANDL	$4, %r15d
	MOVL	%r15d, 32(%rsp)
	MOVL	32(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L13
	JMP L14
L13:
	MOVL	$6, 12(%rsp)
	JMP L15
L14:
	MOVL	$2, 12(%rsp)
	JMP L15
L15:
	JMP L9
L8:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 28(%rsp)
	MOVL	28(%rsp), %r15d
	ANDL	$4, %r15d
	MOVL	%r15d, 28(%rsp)
	MOVL	28(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L10
	JMP L11
L10:
	MOVL	$4, 12(%rsp)
	JMP L12
L11:
	MOVL	$0, 12(%rsp)
	JMP L12
L12:
	JMP L9
L9:
	JMP L6
L6:
	MOVL	$10, 16(%rsp)
	MOVL	16(%rsp), %r15d
	IMULL	4(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	12(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L3
L2:
	MOVL	4(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$176, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
