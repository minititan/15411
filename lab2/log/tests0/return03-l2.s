	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests0/return03.l2"
	SUBQ	$96, %rsp
	MOVL	$1, 4(%rsp)
	MOVL	4(%rsp), %r15d
	CMPL	$0,%r15d
	JNE L1
	JMP L2
L1:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	24(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 24(%rsp)
	MOVL	24(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$96, %rsp
	RET
	JMP L3
L2:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %r15d
	SUBL	$1, %r15d
	MOVL	%r15d, 20(%rsp)
	MOVL	20(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$96, %rsp
	RET
	JMP L3
L3:
	MOVL	8(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	12(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	16(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$96, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
