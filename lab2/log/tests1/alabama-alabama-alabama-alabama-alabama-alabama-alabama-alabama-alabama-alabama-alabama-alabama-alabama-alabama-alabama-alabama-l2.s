	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama.l2"
	SUBQ	$64, %rsp
	MOVL	$0, 4(%rsp)
	MOVL	$1, 12(%rsp)
	MOVL	12(%rsp), %r15d
	IMULL	$10, %r15d
	MOVL	%r15d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	IMULL	$100, %r15d
	MOVL	%r15d, 16(%rsp)
	MOVL	16(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	MOVL	8(%rsp), %r15d
	IMULL	$1000, %r15d
	MOVL	%r15d, 8(%rsp)
L3:
	MOVL	8(%rsp), %r15d
	CMPL	%r15d,4(%rsp)
	JL L1
	JMP L2
L1:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	ADDL	$1, %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L3
L2:
	MOVL	4(%rsp), %eax
	MOVL	%eax, %eax
	ADDQ	$64, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
