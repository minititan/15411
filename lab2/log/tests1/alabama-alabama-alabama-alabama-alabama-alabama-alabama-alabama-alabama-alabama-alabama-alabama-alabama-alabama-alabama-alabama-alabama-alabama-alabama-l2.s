	.globl	__c0_main
	.align	4, 0x90
__c0_main:
	.file	"../tests1/alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama-alabama.l2"
	SUBQ	$80, %rsp
	MOVL	$1, 4(%rsp)
	MOVL	$0, 8(%rsp)
L3:
	JMP L1
L1:
	MOVL	4(%rsp), %r15d
	CMPL	$32,%r15d
	JL L7
	JMP L5
L7:
	MOVL	4(%rsp), %r15d
	CMPL	$0,%r15d
	JGE L4
	JMP L5
L4:
	MOVL	8(%rsp), %r14d
	MOVL	4(%rsp), %ecx
	SHLL	%cl, %r14d
	MOVL	%r14d, 12(%rsp)
	MOVL	12(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L6
L5:
	MOVL	$0, 16(%rsp)
	MOVL	$42, %eax
	CLTD
	IDIVL	16(%rsp)
	MOVL	%eax, 20(%rsp)
	MOVL	20(%rsp), %r15d
	MOVL	%r15d, 8(%rsp)
	JMP L6
L6:
	MOVL	4(%rsp), %r15d
	MOVL	%r15d, 4(%rsp)
	MOVL	4(%rsp), %r15d
	IMULL	$2, %r15d
	MOVL	%r15d, 4(%rsp)
	JMP L3
L2:
	MOVL	$0, %eax
	MOVL	%eax, %eax
	ADDQ	$80, %rsp
	RET
	.ident	"15-411 L1 reference compiler"
