open TypedAst
module A = Ast
module T = Tree
module SM = Symbol.Map

let get_exp e = 
  match e with
  | (t, exp) -> exp

let get_res_temp st = 
	match st with
	| None -> Temp.create()
    | Some(stmp) -> stmp

let op_to_binop op = 
	(match op with
	| A.PLUS -> T.ADD
 	| A.MINUS -> T.SUB
 	| A.TIMES -> T.MUL
 	| A.DIVIDEDBY -> T.DIV
 	| A.MODULO -> T.MOD 
 	| A.BITAND -> T.BAND
  | A.BITOR -> T.BOR
  | A.SAR -> T.SAR
  | A.SAL -> T.SAL
  | A.XOR -> T.XOR
  | _ -> ErrorMsg.error None ("Non binop encountered!"); raise ErrorMsg.Error)

let op_to_unop op = 
	(match op with
	| A.NEGATIVE -> T.NEG
  | A.BITNOT -> T.BNOT
  | _ -> ErrorMsg.error None ("Non unop encountered!"); raise ErrorMsg.Error)

let op_to_cmpop op = 
	(match op with
	| A.EQUALS -> T.EQ 
	| A.NOTEQUALS -> T.NOTEQ 
	| A.GREATER -> T.GREATER 
	| A.GREATEREQUAL -> T.GREATEREQ 
	| A.LESS -> T.LESS 
	| A.LESSEQUAL -> T.LESSEQ
	| _ -> ErrorMsg.error None ("Non cmpop encountered!"); raise ErrorMsg.Error)

(* returns sequence of commands that jumps to l1 if true and l2 if false *)
let rec bool_jmp env exp l1 l2 =
  match exp with
  | Var(id) -> 
    (match SM.find' id env with
    | Some(t) -> [T.If(T.Cmp(T.NOTEQ, T.Temp(t), T.Const(Int32.of_int 0)), l1); T.Goto(l2)]
    | None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
    )

  | BinopExp(op, te1, te2) -> 
    (match op with
    | A.LOGICAND ->
      let l = Label.create() in
      let (c1, c2) = (bool_jmp env (get_exp te1) l l2, bool_jmp env (get_exp te2) l1 l2) in
        c1 @ (T.Label(l)::c2)

    | A.LOGICOR -> 
      let l = Label.create() in
      let (c1, c2) = (bool_jmp env (get_exp te1) l1 l, bool_jmp env (get_exp te2) l1 l2) in
      c1 @ (T.Label(l)::c2)

    | A.EQUALS | A.NOTEQUALS | A.GREATER | A.GREATEREQUAL | A.LESS | A.LESSEQUAL ->
      let ((c1, v1), (c2, v2)) = (trans_exp env te1 None, trans_exp env te2 None) in
        let cop = op_to_cmpop op in
        c1 @ (c2 @ [T.If(T.Cmp(cop, v1, v2), l1); T.Goto(l2)])

    | _ -> ErrorMsg.error None ("Boolean binop not found"); raise ErrorMsg.Error
    )

  | UnopExp(op, te1) -> 
    (match op with
    | A.LOGICNOT -> bool_jmp env (get_exp te1) l2 l1
    | _ -> ErrorMsg.error None ("Boolean unop not found"); raise ErrorMsg.Error
    )

  | True -> [T.Goto(l1)]
  | False -> [T.Goto(l2)]

  | Condition(te1, te2, te3) ->
    let lt = Label.create() in
    let lf = Label.create() in
    let (c1, c2, c3) = (bool_jmp env (get_exp te1) lt lf,
              bool_jmp env (get_exp te2) l1 l2,
              bool_jmp env (get_exp te3) l1 l2) in

    c1 @ (T.Label(lt)::c2) @ (T.Label(lf)::c3)

  | ConstExp _ -> ErrorMsg.error None ("ConstExp in boolean type"); raise ErrorMsg.Error


(* returns a  pair of (command list, imm containing expression value) *)
and trans_exp env e store = 
  match e with 
  | (t, exp) -> 
    (match t with
    | A.Bool -> 
	(
	  match exp with
      | TypedAst.True ->
      	(match store with
      	| None -> ([], T.Const(Int32.of_int 1))
      	| Some(t) -> ([T.Mov(T.Temp(t), T.Const(Int32.of_int 1))], T.Temp(t))
      	)

      | TypedAst.False -> 
      		(match store with
      			| Some(tmp) -> ([T.Mov(T.Temp(tmp), T.Const(Int32.of_int 0))], T.Temp(tmp))
      			| None -> ([], T.Const(Int32.of_int 0))
      		)

      | Var(id) -> 
      		(match SM.find' id env with
			| Some(t) -> 
				(match store with 
				| None -> ([], T.Temp(t))
				| Some(tmp) -> ([T.Mov(T.Temp(tmp), T.Temp(t))], T.Temp(tmp))
				)

			| None -> ErrorMsg.error None ("Temp mapping not found"); raise ErrorMsg.Error
			)

      | _ -> 
      	let tmp = get_res_temp store in
        let l1  = Label.create() in
        let l2 = Label.create() in
        let l3 = Label.create() in
      
        let cmds = bool_jmp env exp l1 l2 in
          ((cmds @ [T.Label(l1); T.Mov(T.Temp(tmp), T.Const(Int32.of_int 1)); T.Goto(l3);
                T.Label(l2); T.Mov(T.Temp(tmp), T.Const(Int32.of_int 0)); T.Goto(l3);  T.Label(l3)]), 
           T.Temp(tmp))
    )

    | A.Int -> 
      (match exp with
      | Var(id) ->  
        (match (SM.find' id env) with
        | Some(tmp) -> 
          (match store with
          | None -> ([], T.Temp(tmp))
          | Some(t) -> ([T.Mov(T.Temp(t), T.Temp(tmp))], T.Temp(t))
          )
          
        | None -> raise ErrorMsg.Error
        )

      | ConstExp(i) -> 
        (match store with
        | None -> ([], T.Const(i))
        | Some(t) -> ([T.Mov(T.Temp(t), T.Const(i))], T.Temp(t))
        )

      | BinopExp(op, e1, e2) -> 
        let ((c1, e_res1), (c2, e_res2), con_op) = 
          (match op with
          | A.DIVIDEDBY | A.MODULO -> 
            let divtemp = Temp.create() in 
              (trans_exp env e1 None, trans_exp env e2 (Some divtemp), op_to_binop op)

          | _ -> 
              (trans_exp env e1 None, trans_exp env e2 None, op_to_binop op) 
          )

        in

        let new_t = get_res_temp store in
        let new_comm = [T.Mov(T.Temp(new_t), T.Binop(con_op, e_res1, e_res2))] in
        ((c1 @ c2) @ new_comm, T.Temp(new_t))

      | UnopExp(op, e') -> 
        let (cl, res) = trans_exp env e' None in
        let uop = op_to_unop op in
        let res_t = get_res_temp store in

        (cl @ [T.Mov(T.Temp(res_t), T.Unop(uop, res))], T.Temp(res_t))

      | Condition(e1, e2, e3) ->        
        let l1 = Label.create() in
        let l2  = Label.create() in
        let l3 = Label.create() in

        let t_res = get_res_temp store in

        let c1 = bool_jmp env (get_exp e1) l1 l2 in
        let (c2, t2) = trans_exp env e2 None in
        let (c3, t3) = trans_exp env e3 None in

        (c1 @ ([T.Label(l1)] @ c2 @ [T.Mov(T.Temp(t_res), t2); T.Goto(l3)]) @
           ([T.Label(l2)] @ c3 @ [T.Mov(T.Temp(t_res), t3); T.Goto(l3)]) @ 
           [T.Label(l3)], 
        T.Temp(t_res))

      | True | False -> ErrorMsg.error None ("Boolean type in Int expression"); raise ErrorMsg.Error
      )
    )

(* returns a sequence of commands appended to the accumulated commands (in acc) *)
let rec trans_stmt env s acc = 
  match s with 
  | Declare(id, t, s') ->  let tmp = Temp.create() in
              let new_env = (SM.add id tmp env) in
                acc @ (trans_stmt new_env s' [])

  | Assign(id, exp) -> 
    let t = (match (SM.find' id env) with
        | Some(tmp) -> tmp
        | None -> raise ErrorMsg.Error)
    in
    
    let (cmds, res) = trans_exp env exp (Some t) in
      (acc @ cmds)

  | Seq(s1, s2) -> let (t_s1, t_s2) = (trans_stmt env s1 [], trans_stmt env s2 []) in
            acc @ (t_s1 @ t_s2)

  | Return(exp) -> let (cmds, res) = trans_exp env exp None in
            acc @ (cmds @ [T.Return(res)])

  | Nop -> acc

  | Expr(exp) -> let t = Temp.create() in 
            let (cmds, res) = trans_exp env exp (Some t) in
            acc @ cmds

  | If(bool_exp, s1, s2) -> let l1 = Label.create() in
                let l2 = Label.create() in
                let l3 = Label.create() in
                let b_cmds = bool_jmp env (get_exp bool_exp) l1 l2 in

                let (c1, c2) = (trans_stmt env s1 [], trans_stmt env s2 []) in

                acc @ b_cmds @ (T.Label(l1)::c1 @ [T.Goto(l3)]) @
                     T.Label(l2)::c2 @ ([T.Goto(l3)] @ [T.Label(l3)])

  | While(b_exp, s') -> let l1 = Label.create() in
              let l2 = Label.create() in
              let ll = Label.create() in

              let b_cmds = bool_jmp env (get_exp b_exp) l1 l2 in

              let cmds = trans_stmt env s' [] in

              acc @ ([T.Label(ll)] @ (b_cmds @ ([T.Label(l1)] @ (cmds @ [T.Goto(ll); T.Label(l2)] ) ) ) )


let translate s = 
  trans_stmt SM.empty s [] 


