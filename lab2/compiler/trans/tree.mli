type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * exp * exp
  | Unop of unop * exp

type cmp = 
  | Cmp of cmpop * exp * exp

type command = 
  | Mov of exp * exp
  | If of cmp * Label.label (* if true goto Label.label *)
  | Goto of Label.label
  | Label of Label.label
  | Return of exp

(* Start line *)
type irprogram = command list

module type PRINT =
  sig
    val pp_unop : unop -> string
    val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT

