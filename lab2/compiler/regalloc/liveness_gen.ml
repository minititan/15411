(* Implementation of register allocator *)

open Regalloc_util
module LLIR = Llir
module S = Storage
module LM = Label.Map

exception WrongPattern of string

let raise_error s = ErrorMsg.error None s; raise ErrorMsg.Error

(* (Definition, Use set, Successor set) *)
type predicates = IntSet.t * IntSet.t * IntSet.t

let rec list_to_set lst = 
  match lst with
  | [] -> IntSet.empty
  | x::l' -> IntSet.add x (list_to_set l')

let enumerate l =
  List.mapi (fun i x -> (i,x)) l

let label_map_from_instructions instructions =
  List.fold_left (fun lm (i,l) -> match l with LLIR.LABEL l' -> LM.add l' i lm | _ -> lm) LM.empty (enumerate instructions)

(* Generates predicates for an instruction *)
let predicate_gen line instr =
  let succ_set = IntSet.singleton (line+1) in
      match instr with
      | LLIR.BINOP(_, dest, op) -> 
          (match (dest, op) with
            | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
                (IntSet.singleton (Temp.get_int d), list_to_set [Temp.get_int d; Temp.get_int t], succ_set)

            | (LLIR.REG(r), LLIR.TEMP(t)) ->
                (IntSet.singleton (reg_to_temp r), list_to_set [Temp.get_int t; reg_to_temp r], succ_set)

            | (LLIR.TEMP(d), LLIR.IMM(_)) -> 
                (IntSet.singleton (Temp.get_int d), IntSet.singleton (Temp.get_int d), succ_set)

            | (LLIR.REG(r), LLIR.IMM(_)) ->
                (IntSet.singleton (reg_to_temp r), IntSet.singleton (reg_to_temp r), succ_set)

            | (LLIR.REG r1, LLIR.REG r2) ->
                (IntSet.singleton (reg_to_temp r1), list_to_set [reg_to_temp r1; reg_to_temp r2], succ_set)

            | (LLIR.TEMP t, LLIR.REG r) ->
                (IntSet.singleton (Temp.get_int t), list_to_set [Temp.get_int t; reg_to_temp r], succ_set)

            | (LLIR.IMM _, _) -> raise_error "IMM cannot be a destination for a BINOP")
      
      | LLIR.MOV(dest, src) -> 
          (match (dest, src) with 
            | (LLIR.TEMP(d), LLIR.TEMP(t)) ->
                (IntSet.singleton (Temp.get_int d), IntSet.singleton (Temp.get_int t), succ_set)

            | (LLIR.REG r, LLIR.TEMP t) ->
                (IntSet.singleton (reg_to_temp r), IntSet.singleton (Temp.get_int t), succ_set)

            | (LLIR.REG r1, LLIR.REG r2) ->
                (IntSet.singleton (reg_to_temp r1), IntSet.singleton (reg_to_temp r2), succ_set)

            | (LLIR.TEMP d, LLIR.IMM _) -> 
                (IntSet.singleton (Temp.get_int d), IntSet.empty, succ_set)

            | (LLIR.REG r, LLIR.IMM _) ->
                (IntSet.singleton (reg_to_temp r), IntSet.empty, succ_set)

            | (LLIR.TEMP t, LLIR.REG r) ->
                (IntSet.singleton (Temp.get_int t), IntSet.singleton (reg_to_temp r), succ_set)

            | (LLIR.IMM _, _) -> raise_error "IMM cannot be a destination for a MOV"
          )

      | LLIR.UNOP(operation, op) ->
          (
            match operation with
            | LLIR.IDIV | LLIR.NEG | LLIR.BNOT -> (match op with
              | LLIR.TEMP(t) -> 
                  (list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX],
                   list_to_set [reg_to_temp S.EAX; reg_to_temp S.EDX; Temp.get_int t],
                   succ_set)
              | LLIR.IMM _ | LLIR.REG _ -> raise_error "Not supposed to get a non-temp in IDIV"
          )
        )

      | LLIR.CLTD -> (IntSet.singleton (reg_to_temp S.EDX), IntSet.singleton (reg_to_temp S.EAX), succ_set)

      (* RET defines nothing, uses EAX, has no successor *)
      | LLIR.RET -> (IntSet.empty, IntSet.singleton (reg_to_temp S.EAX), IntSet.empty)

      | LLIR.LABEL _ -> (IntSet.empty, IntSet.empty, succ_set)
      | LLIR.GOTO l -> (IntSet.empty, IntSet.empty, IntSet.singleton (Label.get_int l))
      | LLIR.IF (_, _, _, l) ->
        (*let succ_set' = try IntSet.add (LM.find l label_map) succ_set with Not_found -> succ_set in*)
        (IntSet.empty, IntSet.empty, succ_set)
      | LLIR.DIRECTIVE _ | LLIR.COMMENT _ -> raise_error "Instr that is not binop, unop, cltd, or ret in instr list!"

(* Generates a sequence conataining a set of live variables
   at each line corresponding to the 2 address sequence 
*)
let rec helper pred_list live_set =
  match pred_list with
  | [] -> raise (WrongPattern "predicate list is empty!")
  | [first_line] -> [IntSet.empty]
  | (def_set, use_set, succ_set)::rem_list ->
          let new_liveset = IntSet.union (IntSet.diff live_set def_set) (use_set) in
            new_liveset::(helper rem_list new_liveset)

(* For now it does a linear scan starting from the end to back *)
let liveness_generator instr_list = 
  let pred_list = List.mapi predicate_gen instr_list in
  (List.rev (helper (List.rev pred_list) IntSet.empty), pred_list)

let print_set s = 
  let mid_s =
    match IntSet.elements s with
        [] -> "{"
      | [x] -> "{" ^ (string_of_int x)
      | x::xs -> "{" ^ (string_of_int x) ^ List.fold_left (fun s i -> let new_s = Printf.sprintf ",%d" i in s ^ new_s ) "" xs
  in
    mid_s ^ "}"

module type PRINT = 
  sig
    val pp_livelist : IntSet.t list -> string 
  end

module Print : PRINT = 
  struct
    let pp_livelist l =
      let combine s liveset = let set_s = print_set liveset in s ^ set_s ^ "\n" in
      "Live list {\n" ^ List.fold_left combine "" l ^ "}\n"
    end
