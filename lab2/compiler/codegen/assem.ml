(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(*
TODO: include unsigned jumps?
*)

module S = Storage

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADDL
  | ADDQ
  | SUBL
  | SUBQ
  | MUL
  | ANDL
  | ORL
  | SARL
  | SALL
  | XORL

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type operand =
  | IMM of Int32.t
  | STR of S.str

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | CMP of operand * operand
  | JUMP of cmpop * Label.label (* conditional jump *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET
  | DIRECTIVE of string
  | COMMENT of string
  | PUSH of operand
  | POP of operand

type program = instr list

(* functions that format assembly output *)

let format_reg = function
  | S.EAX -> "%eax"
  | S.EBX -> "%ebx"
  | S.ECX -> "%ecx"
  | S.CL -> "%cl"
  | S.EDX -> "%edx"
  | S.EBP -> "%ebp" (* these 2 are different since they operate on stack addresses *)
  | S.ESP -> "%rsp"
  | S.ESI -> "%esi"
  | S.EDI -> "%edi"
  | S.R8 -> "%r8d"
  | S.R9 -> "%r9d"
  | S.R10 -> "%r10d"
  | S.R11 -> "%r11d"
  | S.R12 -> "%r12d"
  | S.R13 -> "%r13d"
  | S.R14 -> "%r14d"
  | S.R15 -> "%r15d"
  | S.RSP -> "%rsp"

let format_unop = function
  | IDIV -> "IDIVL"
  | NEG -> "NEGL"
  | BNOT -> "NOTL"
  | INC -> "INCL"
  | DEC -> "DECL"

let format_binop = function
  | ADDL -> "ADDL"
  | ADDQ -> "ADDQ"
  | SUBL -> "SUBL"
  | SUBQ -> "SUBQ"
  | MUL -> "IMULL" (* ?? *)
  | ANDL -> "ANDL"
  | ORL -> "ORL"
  | SARL -> "SARL"
  | SALL -> "SHLL" (* no "arirthmetic" shift left *)
  | XORL -> "XORL"

let base_reg = S.ESP

let format_storage = S.format

let format_operand = function
  | IMM n  -> "$" ^ Int32.to_string n
  | STR s -> format_storage s

let format_label = Label.name

let format = function
    BINOP (oper, d, s) ->
      "\t" ^ format_binop oper
      ^ "\t" ^ format_operand s
      ^ ", " ^ format_operand d ^ "\n"
      (* ^ "," ^ format_operand s ^ "\n" *)
  | UNOP (oper, d) ->
      "\t" ^ format_unop oper
      ^ "\t" ^ format_operand d ^ "\n"
  | MOV (d, s) ->
      "\t" ^ "MOVL"
      ^ "\t" ^ format_operand s
      ^ ", " ^ format_operand d ^ "\n"

  | PUSH(op) -> 
      "\t" ^ "PUSHL" 
      ^ "\t" ^ (format_operand op) ^ "\n"

  | POP(op) ->
      "\t" ^ "POPL" 
      ^ "\t" ^ (format_operand op) ^ "\n"

  | DIRECTIVE str ->
      "\t" ^ str ^ "\n"
  | COMMENT str ->
      "\t" ^ "/* " ^ str ^ "*/\n"
  | CLTD ->
      "\t" ^ "CLTD" ^ "\n"
  | RET ->
      "\t" ^ "RET" ^ "\n"
  | GOTO l ->
      "\t" ^ "JMP " ^ format_label l ^ "\n"
  | LABEL l ->
      format_label l ^ ":" ^ "\n"
  | CMP (o1, o2) ->
      "\t" ^ "CMPL" ^
      "\t" ^ format_operand o2 ^ "," ^ format_operand o1 ^ "\n"
  | JUMP (cop, l) ->
    let jinst = (match cop with
      | EQ -> "JE"
      | NOTEQ -> "JNE"
      | GREATER -> "JG"
      | GREATEREQ -> "JGE"
      | LESS -> "JL"
      | LESSEQ -> "JLE") in
    "\t" ^ jinst ^ " " ^ format_label l ^ "\n"

module type PRINT =
  sig
    val pp_instr : instr -> string
    val pp_instrs : instr list -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct
    let pp_instr = format

    let rec pp_instrs = function
      [] -> ""
    | instr::instrs' -> pp_instr instr ^ pp_instrs instrs'

    let pp_program instrs = "Assembly code {\n" ^ pp_instrs instrs ^ "}\n"
  end
