(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 *)

module T = Tree
module S = Storage
module I = Ir

let get_operand exp =
  match exp with
  | T.Const(i) -> I.IMM(i)
  | T.Temp(t) -> I.TEMP(t)
  | T.Binop _ | T.Unop _ -> ErrorMsg.error None ("Cannot match expression to operand"); raise ErrorMsg.Error

let munch_exp e d =
  match e with
  | T.Const(i) -> I.MOV(d, get_operand e)
  | T.Temp(t) -> I.MOV(d, get_operand e)
  | T.Binop(op, e1, e2) -> I.BINOP(op, d, get_operand e1, get_operand e2)
  | T.Unop(op, e1) -> I.UNOP(op, d, get_operand e1)

let munch_command c =
  match c with
  | T.Mov(e1, e2) -> [munch_exp e2 (get_operand e1)]
  | T.If(T.Cmp(cmpop, e1, e2), l) -> [I.IF(cmpop, get_operand e1, get_operand e2, l)]
  | T.Goto(l) -> [I.GOTO(l)]
  | T.Label(l) -> [I.LABEL(l)]
  | T.Return(e) -> 
      let d = I.REG(S.EAX) in
      [munch_exp e d; I.RET d]

let codegen clist =
  List.flatten (List.map (munch_command) clist)
