(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 *)

type operand =
    IMM of Int32.t
  | REG of Storage.reg 
  | TEMP of Temp.t

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT
  | INC
  | DEC

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label (* if true goto Label.label *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET

type program = instr list

val llirgen: Ir.program -> program

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT
