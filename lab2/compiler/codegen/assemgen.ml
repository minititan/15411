module AS = Assem
module LLIR = Llir
module Alloc = Allocator
module S = Storage
open Regalloc_util

exception Unimplemented

let stack_only = true

(* When the stack is used we use R15 for loading and storing to stack *)
let munch_binop = function
  | LLIR.ADD -> AS.ADDL
  | LLIR.SUB -> AS.SUBL
  | LLIR.MUL -> AS.MUL
  | LLIR.BAND -> AS.ANDL
  | LLIR.BOR -> AS.ORL
  | LLIR.SAR -> AS.SARL
  | LLIR.SAL -> AS.SALL
  | LLIR.XOR -> AS.XORL

let munch_unop = function
  | LLIR.IDIV -> AS.IDIV
  | LLIR.NEG -> AS.NEG
  | LLIR.BNOT -> AS.BNOT
  | LLIR.INC -> AS.INC
  | LLIR.DEC -> AS.DEC

let munch_cmpop = function
  | LLIR.EQ -> AS.EQ
  | LLIR.NOTEQ -> AS.NOTEQ
  | LLIR.GREATER -> AS.GREATER
  | LLIR.GREATEREQ -> AS.GREATEREQ
  | LLIR.LESS -> AS.LESS
  | LLIR.LESSEQ -> AS.LESSEQ

(* wrap REG or STACK with storage *)
let wreg r = AS.STR (S.REG r)
let wstk s = AS.STR (S.STACK s)

let stack_reg = wreg S.R15

let get_store t color_map = IntMap.find t color_map


let get_stack_store_only t = 
  if (t < 0) then raise Unimplemented else
    
  if      t = 0 then S.REG S.EBX
  else if t = 1 then S.REG S.EBP
  else if t = 2 then S.REG S.ESI
  else if t = 3 then S.REG S.EDI
  else if t = 4 then S.REG S.R8
  else if t = 5 then S.REG S.R9
  else if t = 6 then S.REG S.R10
  else if t = 7 then S.REG S.R11
  else if t = 8 then S.REG S.R12
  else if t = 9 then S.REG S.R13
  else S.STACK (word_size * (t - 9))

(*let get_stack_store_only t = 
  if (t < 0) then raise Unimplemented else
    S.STACK (word_size * t)*)

let is_shift = function
  | AS.SARL | AS.SALL -> true
  | _ -> false

let is_memsafe = function
  | AS.ADDQ | AS.ADDL | AS.SUBQ | AS.SUBL | AS.ANDL | AS.XORL | AS.ORL -> true
  | _ -> false

(* apply_color / make_asm, turns llir into asm w/ coloring *)
(* type of colors is Temp.map *)
let apply_color color_map space_to_pop =
  let unwrap_temp = function
    | LLIR.TEMP t -> if (stack_only) then AS.STR (get_stack_store_only (Temp.get_int t))
                    else AS.STR (get_store (Temp.get_int t) color_map)
    | LLIR.REG r -> wreg r
    | LLIR.IMM i -> AS.IMM i in
  let unwrap_temps (t1,t2) = (unwrap_temp t1, unwrap_temp t2) in
  function
    | LLIR.BINOP (oper, d, s) ->
      let oper = munch_binop oper in
      if is_memsafe oper then
      (match unwrap_temps (d, s) with (* shift case, needs refactoring *)
        | (AS.STR s1, AS.STR s2) -> (match (s1,s2) with
          | (S.REG r1, S.REG r2) -> [AS.BINOP(oper, wreg r1, wreg r2)]

          | (S.REG r, S.STACK offset) -> [AS.BINOP(oper, wreg r, wstk offset)]

          | (S.STACK offset, S.REG r) -> [AS.BINOP(oper, wstk offset, wreg r)]

          | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off2); (* difference is in these two lines *)
                                         AS.BINOP(oper, wstk off1, stack_reg)]
                                       )
        | (AS.STR s, AS.IMM i) -> 
          (match s with
            | S.STACK offset -> [AS.BINOP(oper, wstk offset, AS.IMM i)]
            | S.REG r -> [AS.BINOP(oper, wreg r, AS.IMM i)]
          )

        | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )
      else if is_shift oper then
      (match unwrap_temps (d, s) with (* shift case, needs refactoring *)
        | (AS.STR s1, AS.STR s2) -> (match (s1,s2) with
          | (S.REG r1, S.REG r2) -> [AS.BINOP(oper, wreg r1, wreg r2)]

          | (S.REG r, S.STACK offset) -> [AS.MOV(stack_reg, wstk offset);
                                      AS.BINOP(oper, wreg r, stack_reg)]

          | (S.STACK offset, S.REG r) -> [AS.MOV(stack_reg, wstk offset);
                                      AS.BINOP(oper, stack_reg, wreg r);
                                      AS.MOV(wstk offset, stack_reg)]

          | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off2); (* difference is in these two lines *)
                                         AS.BINOP(oper, wstk off1, stack_reg)]
                                       )
        | (AS.STR s, AS.IMM i) -> 
          (match s with
            | S.STACK offset -> [AS.MOV(stack_reg, wstk offset);
                               AS.BINOP(oper, stack_reg, AS.IMM i);
                               AS.MOV(wstk offset, stack_reg)]
            | S.REG r -> [AS.BINOP(oper, wreg r, AS.IMM i)]
          )

        | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )
    else
      (match unwrap_temps (d, s) with (* non shift case *)
        | (AS.STR s1, AS.STR s2) -> (match (s1,s2) with
          | (S.REG r1, S.REG r2) -> [AS.BINOP(oper, wreg r1, wreg r2)]

          | (S.REG r, S.STACK offset) -> [AS.MOV(stack_reg, wstk offset);
                                      AS.BINOP(oper, wreg r, stack_reg)]

          | (S.STACK offset, S.REG r) -> [AS.MOV(stack_reg, wstk offset);
                                      AS.BINOP(oper, stack_reg, wreg r);
                                      AS.MOV(wstk offset, stack_reg)]

          | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off1);
                                         AS.BINOP(oper, stack_reg, wstk off2);
                                         AS.MOV(wstk off1, stack_reg)]
                                       )
        | (AS.STR s, AS.IMM i) -> 
          (match s with
            | S.STACK offset -> [AS.MOV(stack_reg, wstk offset);
                               AS.BINOP(oper, stack_reg, AS.IMM i);
                               AS.MOV(wstk offset, stack_reg)]
            | S.REG r -> [AS.BINOP(oper, wreg r, AS.IMM i)]
          )

        | (AS.IMM _, _) -> raise (Failure "cannot store into IMM")
      )
  | LLIR.MOV(d, s) ->
    (match unwrap_temps (d, s) with
      | (AS.STR s1, AS.STR s2) -> (match (s1, s2) with
        | (S.REG r1, S.REG r2) -> [AS.MOV(wreg r1, wreg r2)]
        | (S.REG r1, S.STACK(offset)) -> [AS.MOV(wreg r1, wstk offset)]
        | (S.STACK offset, S.REG r2) -> [AS.MOV(wstk offset, wreg r2)]
        | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off2);
                                             AS.MOV(wstk off1, stack_reg)]
                                         )
      | (AS.STR s, AS.IMM i) -> [AS.MOV(AS.STR s, AS.IMM i)]
      | (AS.IMM _, _) -> raise (Failure "cannot store into IMM for MOV")
    )
  | LLIR.UNOP (oper, dst) ->
    let opert = munch_unop oper in
    (match unwrap_temp dst with
        AS.STR dst -> (match dst with
          S.REG r -> [AS.UNOP(opert, wreg r)]
        | S.STACK s -> [AS.UNOP(opert, wstk s)]
        )
      | AS.IMM _ -> raise (Failure "cannot store into IMM for UNOP")
    )
  | LLIR.IF (cop, o1, o2, l) -> (* CMPL is happening twice, swapped MOV *)
    let jump_inst = AS.JUMP (munch_cmpop cop, l) in
    (match unwrap_temps (o1, o2) with
      | (AS.STR s1, AS.STR s2) -> (match (s1, s2) with
        | (S.REG r1, S.REG r2) -> [AS.CMP(wreg r1, wreg r2); jump_inst]
        | (S.REG r1, S.STACK(offset)) -> [AS.CMP(wreg r1, wstk offset); jump_inst]
        | (S.STACK offset, S.REG r2) -> [AS.CMP(wstk offset, wreg r2); jump_inst]
        | (S.STACK off1, S.STACK off2) -> [AS.MOV(stack_reg, wstk off2);
            AS.CMP(wstk off1, stack_reg); jump_inst]
          )
      | (AS.STR s, AS.IMM i) -> (
        match s with
        | S.REG r -> [AS.CMP(AS.STR s, AS.IMM i); jump_inst]
        | S.STACK o -> [AS.MOV (stack_reg, AS.STR s); AS.CMP(stack_reg, AS.IMM i); jump_inst]
      )
      | (AS.IMM i, AS.STR s) -> [AS.MOV(stack_reg, AS.IMM i); AS.CMP(stack_reg, AS.STR s); jump_inst]
      | (AS.IMM i1, AS.IMM i2) -> [AS.MOV (stack_reg, AS.IMM i1); AS.CMP(stack_reg, AS.IMM i2); jump_inst]
    )
  | LLIR.LABEL l -> [AS.LABEL l]
  | LLIR.GOTO l -> [AS.GOTO l]
  | LLIR.CLTD -> [AS.CLTD]
  | LLIR.RET -> 
    (match space_to_pop with
      | None -> [AS.RET]
      | Some(space) -> [AS.BINOP(AS.ADDQ, AS.STR(S.REG S.RSP), AS.IMM(space)); AS.RET]
    )
  | LLIR.DIRECTIVE str -> [AS.DIRECTIVE str]
  | LLIR.COMMENT str -> [AS.COMMENT str] 

(* Calculate the amount of stack space needed for variables *)
let get_stack_space storage_map = 
  let helper k store (space, seen) = 
    match store with 
    | S.STACK(offset) -> if (IntSet.mem offset seen) then (space, seen)
                        else (space+1, IntSet.add offset seen)
    | S.REG(r) -> (space, seen)
  in
    let (stack_spots, seen) = IntMap.fold helper storage_map (0, IntSet.empty)
  in
    stack_spots * word_size

let bigger_temp_pair curr_max = function
  | (LLIR.TEMP d, LLIR.TEMP t) -> max (max (Temp.get_int d) (Temp.get_int t)) curr_max
  | (LLIR.REG _, LLIR.TEMP t) -> max curr_max (Temp.get_int t)
  | (LLIR.TEMP d, LLIR.IMM _) -> max curr_max (Temp.get_int d)
  | (LLIR.REG r, LLIR.IMM _) -> curr_max
  | (LLIR.REG _, LLIR.REG _) -> curr_max
  | (LLIR.TEMP t, LLIR.REG r) -> max curr_max (Temp.get_int t)
  | (LLIR.IMM _, LLIR.REG r) -> curr_max
  | (LLIR.IMM _, LLIR.TEMP t) -> max curr_max (Temp.get_int t)
  | (LLIR.IMM _, LLIR.IMM _) -> curr_max (* This case is not applicable for BINOP and MOV. If something is breaking look at this. *)

(* Helper function to perform list fold and get largest temp in llir list *)
let bigger_temp curr_max instr =
  match instr with
  | LLIR.BINOP(_, dest, op) -> bigger_temp_pair curr_max (dest, op)
  | LLIR.MOV(dest, src) ->  bigger_temp_pair curr_max (dest, src)
  | LLIR.UNOP(_, op) ->
      (match op with
      | LLIR.TEMP t -> max curr_max (Temp.get_int t)
      | LLIR.IMM _ | LLIR.REG _ -> curr_max
      )
  (* RET defines nothing, uses EAX, has no successor *)
  | LLIR.IF (cop, o1, o2, l) -> bigger_temp_pair curr_max (o1, o2)
  | LLIR.CLTD | LLIR.RET | LLIR.GOTO _ | LLIR.LABEL _ | LLIR.DIRECTIVE _ | LLIR.COMMENT _ -> curr_max

(* finds largest temp and returns that * word size *)
let get_only_stack_space llir = 
  let max_temp = List.fold_left bigger_temp 0 llir in
  word_size * max_temp


let assembly_gen llir storage_map = 
  let stack_space = if (stack_only) then get_only_stack_space llir
                    else get_stack_space storage_map in

  if (stack_space = 0)
    then (List.flatten (List.map (apply_color storage_map None) llir))

  else
    (* move esp down by how much space we need *)
    let rsp = AS.STR (S.REG S.RSP) in
    let stack_shift = AS.IMM(Int32.of_int(stack_space * word_size)) in
    let pre_instr = [AS.BINOP(AS.SUBQ, rsp, stack_shift)] in
    let instr = (List.flatten (List.map 
                  (apply_color storage_map (Some(Int32.of_int (stack_space * word_size))) ) llir)) in
    pre_instr @ instr



