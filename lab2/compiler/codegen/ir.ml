(* L1 Compiler
 * Assembly language
 * Author: Kaustuv Chaudhuri <kaustuv+@andrew.cmu.edu>
 * Modified By: Alex Vaynberg <alv@andrew.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Currently just a pseudo language with 3-operand
 * instructions and arbitrarily many temps
 *
 * We write
 *
 * BINOP  operand1 <- operand2,operand3
 * MOV    operand1 <- operand2
 *
 *)

(* Ned: from assem.ml
 * We want 3-addr for our IR and 2-addr for assem,
 * so we use this to define a different instruction
 * type.
 *
 * Therefore we have these instruction types, each
 * progressively lower lever: IR, LLIR, AS.
 *)

module S = Storage
module T = Tree

type binop = T.binop
type cmpop = T.cmpop
type unop = T.unop

type operand =
    IMM of Int32.t
  | REG of Storage.reg
  | TEMP of Temp.temp

type instr =
  | BINOP of binop * operand * operand * operand
  | UNOP of unop * operand * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label (* if true goto Label.label *)
  | GOTO of Label.label
  | LABEL of Label.label
  | RET of operand

type program = instr list

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_cmpop : cmpop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_operand = function
      IMM i -> Int32.to_string i
    | REG r -> S.format_reg r
    | TEMP t -> Temp.name t

    let pp_binop op = 
      match op with
      | T.ADD -> "ADD"
      | T.SUB -> "SUB"
      | T.MUL -> "MUL"
      | T.DIV -> "DIV"
      | T.MOD -> "MOD"
      | T.BAND -> "AND"
      | T.BOR -> "OR"
      | T.SAR -> "SAR"
      | T.SAL -> "SAL"
      | T.XOR -> "XOR"

    let pp_unop op = 
      match op with
      | T.NEG -> "NEG"
      | T.BNOT -> "NOT"

    let pp_cmpop op = 
    match op with
      | T.EQ -> "EQ"
      | T.NOTEQ -> "NEQ"
      | T.GREATER -> "GREAT"
      | T.GREATEREQ -> "GREATEQ"
      | T.LESS -> "LESS"
      | T.LESSEQ -> "LESSEQ"

    let pp_label = Label.name

    let pp_instr = function
    | BINOP (op, op1, op2, op3) -> pp_binop op ^ " " ^ pp_operand op1 ^ " <- " ^ pp_operand op2 ^ "," ^ pp_operand op3
    | UNOP (op, d, op1) -> pp_unop op ^ " " ^ pp_operand d ^ " <- " ^ pp_operand op1
    | IF (op, op1, op2, l) -> "CMP " ^ pp_cmpop op ^ " (" ^ pp_operand op1 ^ ", " ^ pp_operand op2 ^ "): " ^ 
                              "GOTO " ^ (Label.name l) 
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ "," ^ pp_operand s
    | DIRECTIVE s -> "DIRECTIVE: " ^ s
    | COMMENT s -> "COMMENT: " ^ s
    | RET(op) -> "RET " ^ " " ^ (pp_operand op)
    | GOTO l -> "GOTO " ^ pp_label l
    | LABEL l -> "LABEL " ^ pp_label l
    
    let rec pp_program = function
    | [] -> ""
    | instr::instrs' -> pp_instr instr ^ "\n" ^ pp_program instrs'

  end
