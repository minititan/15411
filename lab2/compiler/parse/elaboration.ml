(* elaboration.ml
 * Ned Williamson
 * Parse Tree -> AST
 *)

module P = Parsetree
module A = Ast

let trans_primtype = function
  | P.Bool -> A.Bool
  | P.Int -> A.Int

let trans_oper = function
  | P.PLUS -> A.PLUS
  | P.MINUS -> A.MINUS
  | P.TIMES -> A.TIMES
  | P.DIVIDEDBY -> A.DIVIDEDBY
  | P.MODULO -> A.MODULO
  | P.NEGATIVE -> A.NEGATIVE
  | P.BITAND -> A.BITAND
  | P.BITOR -> A.BITOR
  | P.BITNOT -> A.BITNOT
  | P.SAR -> A.SAR
  | P.SAL -> A.SAL
  | P.XOR -> A.XOR
  | P.LOGICAND -> A.LOGICAND
  | P.LOGICOR -> A.LOGICOR
  | P.LOGICNOT -> A.LOGICNOT
  | P.EQUALS -> A.EQUALS
  | P.NOTEQUALS -> A.NOTEQUALS
  | P.GREATER -> A.GREATER
  | P.GREATEREQUAL -> A.GREATEREQUAL
  | P.LESS -> A.LESS
  | P.LESSEQUAL -> A.LESSEQUAL




let unwrap_decl = function
  | P.SimpNone -> None
  | P.SimpOpt s ->
    (match s with
      | P.SimpDecl d -> Some d
      | P.SimpAsop _ | P.SimpPostop _ | P.SimpExp _ -> None)

exception DeclInStep

let rec trans_control = function
  | P.If (e, s, elo) ->
      A.If (trans_exp e, trans_stm s, trans_elseopt elo)
  | P.While (e, s) -> A.While (trans_exp e, trans_stm s)
  | P.For (init, e, step, body) ->
    if (match unwrap_decl step with Some _ -> true | None -> false) then raise DeclInStep else
    (match unwrap_decl init with
      | None -> A.Seq(trans_simpopt init, A.While(trans_exp e, A.Seq(trans_stm body, trans_simpopt step)))
      | Some d -> (match d with
        | P.Decl (p, i) -> A.Declare(i, trans_primtype p, A.While(trans_exp e, A.Seq(trans_stm body, trans_simpopt step)))
        | P.DeclAsn (p, i, e2) -> A.Declare(i, trans_primtype p, A.Seq(A.Assign (i, trans_exp e2),A.While(trans_exp e, A.Seq(trans_stm body, trans_simpopt step))))
        )
      )
  | P.Return e -> A.Return (trans_exp e)
and trans_elseopt = function
  | P.Else s -> trans_stm s
  | P.ElseNone -> A.Nop
and trans_simp = function
  | P.SimpAsop (l, o, e) ->
    let i = (match l with P.Id i -> i)
    and e' = trans_exp e in
    (match o with
      | P.ASSIGN -> A.Assign (i, e')
      | P.PLUSEQ -> A.Assign (i, (A.BinopExp (A.PLUS, A.Var i, e')))
      | P.MINUSEQ -> A.Assign (i, (A.BinopExp (A.MINUS, A.Var i, e')))
      | P.STAREQ -> A.Assign (i, (A.BinopExp (A.TIMES, A.Var i, e')))
      | P.SLASHEQ -> A.Assign (i, (A.BinopExp (A.DIVIDEDBY, A.Var i, e')))
      | P.PERCENTEQ -> A.Assign (i, (A.BinopExp (A.MODULO, A.Var i, e')))
      | P.BITANDEQ -> A.Assign (i, (A.BinopExp (A.BITAND, A.Var i, e')))
      | P.BITOREQ -> A.Assign (i, (A.BinopExp (A.BITOR, A.Var i, e')))
      | P.XOREQ -> A.Assign (i, (A.BinopExp (A.XOR, A.Var i, e')))

      | P.SAREQ -> A.Assign (i, construct_shift_tree P.SAR (P.Var i) e)
      | P.SALEQ -> A.Assign (i, construct_shift_tree P.SAL (P.Var i) e)
    )

  | P.SimpPostop (l, p) ->
    let i = (match l with P.Id i -> i) in
    (match p with
      | P.PLUSPLUS -> A.Assign (i, (A.BinopExp (A.PLUS, A.Var i, A.ConstExp Int32.one)))
      | P.MINUSMINUS -> A.Assign (i, (A.BinopExp (A.MINUS, A.Var i, A.ConstExp Int32.one)))
    )
    
  | P.SimpDecl d -> trans_decl d
  | P.SimpExp e -> A.Expr (trans_exp e)

and construct_shift_tree op e1 e2 = 
    let (te1, te2) = (trans_exp e1, trans_exp e2) in
    A.Condition(A.BinopExp(A.LOGICAND, A.BinopExp(A.LESS, te2, A.ConstExp(Int32.of_int 32)),
                                       A.BinopExp(A.GREATEREQUAL, te2, A.ConstExp(Int32.of_int 0))),

                A.BinopExp(trans_oper op, te1, te2), 

                A.BinopExp(A.DIVIDEDBY, A.ConstExp(Int32.of_int 42), A.ConstExp(Int32.of_int 0)) )

and trans_exp = function
  | P.ConstExp i -> A.ConstExp i
  | P.True -> A.True
  | P.False -> A.False
  | P.Var i -> A.Var i
  | P.UnopExp (o, e) -> A.UnopExp (trans_oper o, trans_exp e)
  | P.BinopExp (o, e1, e2) ->
      (match trans_oper o with
        | A.LOGICAND -> A.Condition(trans_exp e1, trans_exp e2, A.False)
        | A.LOGICOR -> A.Condition(trans_exp e1, A.True, trans_exp e2)
        | A.SAR | A.SAL -> construct_shift_tree o e1 e2

        | o' -> A.BinopExp (o', trans_exp e1, trans_exp e2))
  | P.Condition (e1, e2, e3) ->
      let e1' = trans_exp e1
      and e2' = trans_exp e2
      and e3' = trans_exp e3 in
      A.Condition (e1', e2', e3')
and trans_decl = function
  | P.Decl (p, i) -> A.Declare (i, trans_primtype p, A.Nop)
  | P.DeclAsn (p, i, e) -> A.Declare (i, trans_primtype p, A.Assign(i, trans_exp e))
and trans_stm : P.stmt -> A.stm = function
  | P.StmSimp s -> trans_simp s
  | P.StmControl c -> trans_control c
  | P.StmBlock b -> trans_block b
and trans_simpopt = function
  | P.SimpNone -> A.Nop
  | P.SimpOpt s -> trans_simp s
and trans_block = function
  | P.Block b -> trans_stms b
and trans_stms = function
  | P.Seq (s, ss) ->
    (match s with
      | P.StmSimp smp ->
        (match smp with
          | P.SimpDecl dcl -> (match dcl with
            | P.Decl (p, i) -> A.Declare (i, trans_primtype p, trans_stms ss)
            | P.DeclAsn (p, i, e) -> A.Declare (i, trans_primtype p, A.Seq(A.Assign(i, trans_exp e), trans_stms ss)))
          | P.SimpAsop _ | P.SimpPostop _ | P.SimpExp _ -> A.Seq (trans_stm s, trans_stms ss))
      | P.StmControl _ | P.StmBlock _ -> A.Seq (trans_stm s, trans_stms ss))
  | P.StmtsNone -> A.Nop

let trans_lvalue = function
  | P.Id i -> A.Var i

let trans_program = function
  | P.Prog b -> trans_block b

(*


let trans_prim = function
  | P.Int -> A.Int
  | P.Bool -> A.Bool

let rec trans_exp = function
  | P.Var i -> A.Var i
  | P.ConstExp i -> A.ConstExp i
  | P.BinopExp (o, e1, e2) -> A.BinopExp (trans_oper o, trans_exp e1, trans_exp e2)
  | P.UnopExp (o, e) -> A.UnopExp (trans_oper o, trans_exp e)
  | P.Condition (e1, e2, e3) -> A.Condition (trans_exp e1, trans_exp e2, trans_exp e3)
  | P.True -> A.True
  | P.False -> A.False

let rec trans_stm = function
  | P.Declare (i, t) -> A.Declare (i, trans_prim t)
  | P.Assign (i, e) -> A.Assign (i, trans_exp e)
  | P.If (e, s1, s2) -> A.If (trans_exp e, trans_stm s1, trans_stm s2)
  | P.For (s1, e, s2, s3) ->
    (match s1 with
    | P.Declare (i, t, s) -> A.Declare (i, trans_prim t, A.Seq(trans_stm s, A.While(trans_exp e, A.Seq(trans_stm s3, trans_stm s2))))
    | _ -> A.Seq(trans_stm s1, A.While(trans_exp e, A.Seq(trans_stm s3, trans_stm s2))))
  | P.While (e, s) -> A.While (trans_exp e, trans_stm s)
  | P.Return e -> A.Return (trans_exp e)
  | P.Seq (s1, s2) -> A.Seq (trans_stm s1, trans_stm s2)
  | P.Expr e -> A.Expr (trans_exp e)
  | P.Nop -> A.Nop
*)
