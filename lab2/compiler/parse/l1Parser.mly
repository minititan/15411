%{
(* L1 Compiler
 * L1 grammar
 * Author: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Now conforms to the L1 fragment of C0
 *
 * Modified: Maxime Serrano <mserrano@andrew.cmu.edu> Fall 2014
 * Should be more up-to-date with 2014 spec
 *
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *)

module P = Parsetree

(* expand_asnop (id, "op=", exp) region = "id = id op exps"
 * or = "id = exp" if asnop is "="
 * syntactically expands a compound assignment operator
 *)
(*
let expand_asnop = function
  | (id, None, exp) ->
      P.Assign(id, exp)
  | (id, Some oper, exp) ->
      P.Assign(id, P.BinopExp (oper, P.Var(id), exp))

let expand_postop = function
    (id, oper) ->
      P.Assign(id, (P.BinopExp (oper, P.Var(id), P.ConstExp(Int32.one))))
*)

let convert_lvalue l = 
  match l with
  | P.Id(id) -> P.Var(id)

%}

%token EOF
%token STRUCT TYPEDEF IF ELSE WHILE FOR CONTINUE BREAK
%token ASSERT TRUE FALSE NULL ALLOC ALLOCARRY
%token BOOL VOID CHAR STRING 
%token SEMI
%token <Int32.t> DECCONST 
%token <Int32.t> HEXCONST
%token <Symbol.symbol> IDENT
%token RETURN
%token INT
%token MAIN
%token PLUS MINUS STAR SLASH PERCENT 
%token BITAND BITOR XOR BITNOT LOGICNOT LOGICAND LOGICOR
%token PLUSPLUS MINUSMINUS
%token SAR SAL EQUALS NOTEQUALS LESSEQUAL GREATEREQUAL LESS GREATER
%token QUESTION COLON
%token ASSIGN PLUSEQ MINUSEQ STAREQ SLASHEQ PERCENTEQ 
%token BITANDEQ BITOREQ XOREQ SAREQ SALEQ
%token LBRACE RBRACE
%token LPAREN RPAREN
%token UNARY ASNOP
/* UNARY and ASNOP are dummy terminals.
 * We need dummy terminals if we wish to assign a precedence
 * to a rule that does not correspond to the precedence of
 * the rightmost terminal in that rule.
 * Implicit in this is that precedence can only be infered
 * terminals. Therefore, don't try to assign precedence to "rules"
 */

%type <Parsetree.program> program

%right ASNOP
%right QUESTION COLON
%left LOGICOR
%left LOGICAND
%left BITOR
%left XOR
%left BITAND
%left EQUALS NOTEQUALS
%left LESS LESSEQUAL GREATER GREATEREQUAL
%left SAL SAR
%left PLUS MINUS
%left STAR SLASH PERCENT
%right LOGICNOT BITNOT UNARY MINUSMINUS PLUSPLUS
%left LPAREN

%start program

%%

program :
  INT MAIN LPAREN RPAREN block EOF { P.Prog $5 }
  ;

block :
  LBRACE stmts RBRACE           { P.Block $2 }
  ;

typ :
  INT                           { P.Int }
  | BOOL                        { P.Bool }
  ;

decl :
 typ IDENT                           { P.Decl ($1, $2) }
 | typ IDENT ASSIGN exp              { P.DeclAsn ($1, $2, $4) }
 | typ MAIN                          { P.Decl ($1, (Symbol.symbol "main")) }
 | typ MAIN ASSIGN exp               { P.DeclAsn ($1, Symbol.symbol "main", $4) }

stmts :
  /* empty */                         { P.StmtsNone }
 | stmt stmts                         { P.Seq ($1, $2) }
 ;

stmt :
 simp SEMI                       { P.StmSimp $1 }
 | control                       { P.StmControl $1 }
 | block                         { P.StmBlock $1 }
 ;

simp :
  lvalue asnop exp %prec ASNOP     { P.SimpAsop ($1, $2, $3) }
  | lvalue postop                  { P.SimpPostop ($1, $2) }
  | decl                           { P.SimpDecl $1 }
  | exp                            { P.SimpExp $1 }
  ;

simpopt : 
  /* empty */                     { P.SimpNone }
  | simp                          { P.SimpOpt $1 }
  ;

lvalue :
 MAIN                       { P.Id (Symbol.symbol "main") }
 | IDENT                    { P.Id $1 }
 | LPAREN lvalue RPAREN     { $2 }
 ;

control :
  IF LPAREN exp RPAREN stmt ELSE stmt                      { P.If($3, $5, P.Else $7) }
  | IF LPAREN exp RPAREN stmt                              { P.If($3, $5, P.ElseNone) }
  | WHILE LPAREN exp RPAREN stmt                           { P.While($3, $5) }
  | FOR LPAREN simpopt SEMI exp SEMI simpopt RPAREN stmt   { P.For ($3, $5, $7, $9) }
  | RETURN exp SEMI                                        { P.Return $2 }
  ;

/* expressions are organized into blocks by precedence */
exp :
 MAIN                            { P.Var (Symbol.symbol "main") }
 | LPAREN exp RPAREN             { $2 }
 | lvalue                        { convert_lvalue $1 }
 | intconst                      { $1 }

 | TRUE                          { P.True } 
 | FALSE                         { P.False }
 | IDENT                         { P.Var $1 }
 
 | LOGICNOT exp %prec UNARY      { P.UnopExp (P.LOGICNOT, $2) }
 | BITNOT exp %prec UNARY        { P.UnopExp (P.BITNOT, $2) }
 | MINUS exp %prec UNARY         { P.UnopExp (P.NEGATIVE, $2) }

 | exp STAR exp                  { P.BinopExp (P.TIMES, $1, $3) }
 | exp SLASH exp                 { P.BinopExp (P.DIVIDEDBY, $1, $3) }
 | exp PERCENT exp               { P.BinopExp (P.MODULO, $1, $3) }
 | exp PLUS exp                  { P.BinopExp (P.PLUS, $1, $3) }
 | exp MINUS exp                 { P.BinopExp (P.MINUS, $1, $3) }

 | exp SAL exp                   { P.BinopExp (P.SAL, $1, $3) }
 | exp SAR exp                   { P.BinopExp (P.SAR, $1, $3) }

 | exp LESS exp                  { P.BinopExp (P.LESS, $1, $3)}
 | exp LESSEQUAL exp             { P.BinopExp (P.LESSEQUAL, $1, $3)}
 | exp GREATER exp               { P.BinopExp (P.GREATER, $1, $3)}
 | exp GREATEREQUAL exp          { P.BinopExp (P.GREATEREQUAL, $1, $3)}

 | exp EQUALS exp                { P.BinopExp (P.EQUALS, $1, $3) }
 | exp NOTEQUALS exp             { P.BinopExp (P.NOTEQUALS, $1, $3) }

 | exp BITAND exp                { P.BinopExp (P.BITAND, $1, $3) }

 | exp XOR exp                   { P.BinopExp (P.XOR, $1, $3) }

 | exp BITOR exp                 { P.BinopExp (P.BITOR, $1, $3) }

 | exp LOGICAND exp              { P.BinopExp (P.LOGICAND, $1, $3) }

 | exp LOGICOR exp               { P.BinopExp (P.LOGICOR, $1, $3) }

 | exp QUESTION exp COLON exp    { P.Condition($1, $3, $5) }

 ;

intconst :
  DECCONST           { P.ConstExp $1 }
 | HEXCONST          { P.ConstExp $1 }
 ;

postop :
    PLUSPLUS                    { P.PLUSPLUS }
  | MINUSMINUS                  { P.MINUSMINUS } 
  ;

asnop :
   ASSIGN                       { P.ASSIGN }
 | PLUSEQ                       { P.PLUSEQ }
 | MINUSEQ                      { P.MINUSEQ }
 | STAREQ                       { P.STAREQ }
 | SLASHEQ                      { P.SLASHEQ }
 | PERCENTEQ                    { P.PERCENTEQ }
 | BITANDEQ                     { P.BITANDEQ }
 | BITOREQ                      { P.BITOREQ }
 | XOREQ                        { P.XOREQ }
 | SAREQ                        { P.SAREQ }
 | SALEQ                        { P.SALEQ }
 ;

%%

