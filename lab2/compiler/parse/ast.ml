(* L1 Compiler
 * Abstract Syntax Trees
 * Author: Alex Vaynberg
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Forward compatible fragment of C0
 *)

(* Consider using pretty printing *)

type ident = Symbol.symbol

type oper =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDEDBY
  | MODULO
  | NEGATIVE                     (* unary minus *)
  | BITAND
  | BITOR
  | BITNOT
  | SAR
  | SAL
  | XOR
  | LOGICAND
  | LOGICOR
  | LOGICNOT
  | EQUALS
  | NOTEQUALS
  | GREATER
  | GREATEREQUAL
  | LESS
  | LESSEQUAL

type primtype = Int | Bool

type exp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * exp * exp
  | UnopExp of oper * exp 
  | Condition of exp * exp * exp
  | True
  | False
and stm =
  | Declare of ident * primtype * stm
  | Assign of ident * exp
  | If of exp * stm * stm
  | While of exp * stm
  | Return of exp
  | Seq of stm * stm
  | Expr of exp
  | Nop

type program = stm

module type PRINT =
  sig
    val pp_exp : exp -> string
    val pp_stm : stm -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_ident id = Symbol.name id

    let pp_type = function
      | Int -> "int"
      | Bool -> "bool"

    let pp_oper = function
      | PLUS -> "+"
      | MINUS -> "-"
      | TIMES -> "*"
      | DIVIDEDBY -> "/"
      | MODULO -> "%"
      | NEGATIVE -> "-"
      | BITAND -> "&"
      | BITOR -> "|"
      | BITNOT -> "~"
      | SAR -> ">>"
      | SAL -> "<<"
      | XOR -> "^"
      | LOGICAND -> "&&"
      | LOGICOR -> "||"
      | LOGICNOT -> "!"
      | EQUALS -> "=="
      | NOTEQUALS -> "!="
      | GREATER -> ">"
      | GREATEREQUAL -> ">="
      | LESS -> "<"
      | LESSEQUAL -> "<="

    let rec pp_exp = function
      | Var id     -> pp_ident id
      | ConstExp c -> Int32.to_string c
      | UnopExp (op, e) -> pp_oper op ^ "(" ^ pp_exp e ^ ")"
      | BinopExp (op, e1, e2) ->
          "(" ^ pp_exp e1 ^ " " ^ pp_oper op ^ " " ^ pp_exp e2 ^ ")"
      | Condition (e1, e2, e3) -> "(" ^ pp_exp e1 ^ " ? (" ^ pp_exp e2 ^ ") : (" ^ pp_exp e3 ^ "))"
      | True -> "true"
      | False -> "false"

    let rec pp_stm = function
      | If (e, s1, s2) -> "if (" ^ pp_exp e ^ ") {" ^ pp_stm s1 ^ "} else {" ^ pp_stm s2 ^ "}\n" 
      | While (e, s) -> "while (" ^ pp_exp e ^ ") {" ^ pp_stm s ^ "}\n"
      | Seq (s1, s2) -> pp_stm s1 ^ "\n" ^ pp_stm s2
      | Declare (i, t, s) -> pp_type t ^ " " ^ pp_ident i ^ ";{\n" ^ pp_stm s ^ "}\n"
      | Assign (id, e) -> pp_ident id ^ " = " ^ pp_exp e ^ ";\n"
      | Expr e -> pp_exp e ^ "\n"
      | Return e -> "return " ^ pp_exp e ^ ";\n"
      | Nop -> "NOP;"

    let pp_program stm = "\nInput {\n" ^ pp_stm stm ^ "\n}\n"
  end
