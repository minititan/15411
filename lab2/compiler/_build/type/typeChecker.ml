open Ast
module S = Symbol
module T = TypedAst
module SM = Symbol.Map
module SS = Symbol.Set

let get_type_str = function
  | Bool -> "Bool"
  | Int -> "Int"

(* returns the type of the resulting epressions and the type
    required by each subexp. If subexp type is None then it doesnt
    matter but has to be the same *)
let get_optype = function
  | PLUS -> (Int, Some(Int))
  | MINUS -> (Int, Some(Int))
  | TIMES -> (Int, Some(Int))
  | DIVIDEDBY -> (Int, Some(Int))
  | MODULO -> (Int, Some(Int))
  | BITAND -> (Int, Some(Int))
  | BITOR -> (Int, Some(Int))
  | SAR -> (Int, Some(Int))
  | SAL -> (Int, Some(Int))
  | XOR -> (Int, Some(Int))
  | LOGICAND -> (Bool, Some(Bool))
  | LOGICOR -> (Bool, Some(Bool))
  | EQUALS -> (Bool, None)
  | NOTEQUALS -> (Bool, None)
  | GREATER -> (Bool, Some(Int))
  | GREATEREQUAL -> (Bool, Some(Int))
  | LESS -> (Bool, Some(Int))
  | LESSEQUAL -> (Bool, Some(Int))
  | LOGICNOT -> (Bool, Some(Bool))
  | BITNOT -> (Int, Some(Int))
  | NEGATIVE -> (Int, Some(Int))

  
let rec tc_exp context exp = 
  match exp with
  | True -> (Bool, T.True)
  | False -> (Bool, T.False)

  | Var id -> 
    (match (SM.find' id context) with
    | Some t -> (t, T.Var(id))
    | None -> 
      ErrorMsg.error None ("Undeclared variable " ^ S.name id ^ ".");
      raise ErrorMsg.Error
    )

  | ConstExp i -> (Int, T.ConstExp(i))

  | BinopExp(oper, inner_exp1, inner_exp2) ->
    let ((t1, e1), (t2, e2)) = (tc_exp context inner_exp1, tc_exp context inner_exp2) in
    let (res_type, sub_type) = get_optype oper in
    (match (t1, t2) with
    | (Bool, Bool) -> (match sub_type with 
              | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Bool) -> (res_type, T.BinopExp(oper, (t1, e1), (t2, e2)))
              | Some(Int) -> ErrorMsg.error None
                ("Type mismatch: Expected Int got Bool"); raise ErrorMsg.Error
              )
    | (Int, Int) -> (match sub_type with 
              | None -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Int) -> (res_type, T.BinopExp(oper, (t1,e1), (t2,e2)))
              | Some(Bool) -> 
                  ErrorMsg.error None ("Type mismatch: Expected Bool got Int"); raise ErrorMsg.Error
              )
    | (Bool, Int) -> ErrorMsg.error None
        ("Type mismatch: Mix of types in binop expression"); raise ErrorMsg.Error
    | (Int, Bool) -> ErrorMsg.error None
        ("Type mismatch: Mix of types in binop expression"); raise ErrorMsg.Error
    )

  | UnopExp(oper, inner_exp) -> 
    let ((res_type, sub_type), (exp_type, e)) = (get_optype oper, tc_exp context inner_exp) in

    (match (sub_type, exp_type) with
    | (Some(Bool), Bool) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some(Int), Int) -> (res_type, T.UnopExp(oper, (exp_type, e)))
    | (Some(Bool), Int) -> ErrorMsg.error None
        ("Type mismatch: Unop expected Bool got Int exp"); raise ErrorMsg.Error
    | (Some(Int), Bool) -> ErrorMsg.error None
        ("Type mismatch: Unop expected Int got Bool exp"); raise ErrorMsg.Error
    | (None, _) -> ErrorMsg.error None
        ("Equality comparison operator is not a Unop"); raise ErrorMsg.Error
    )

  | Condition(e1, e2, e3) -> 
    let ((t1, te1), (t2, te2), (t3, te3)) =
      (tc_exp context e1, tc_exp context e2, tc_exp context e3) in
    (match (t1) with
    | Bool -> (match (t2, t3) with
          | (Int, Int) -> (Int, T.Condition((t1, te1), (t2, te2), (t3, te3)))
          | (Bool, Bool) -> (Bool, T.Condition((t1, te1), (t2, te2), (t3, te3)))
          | (Int, Bool) | (Bool, Int) ->
            ErrorMsg.error None
              ("Type mismatch: Condition has Int and Bool paths");
              raise ErrorMsg.Error
          )
    | Int -> ErrorMsg.error None
        ("Type mismatch: Condition check exp must have Bool type but has Int");
        raise ErrorMsg.Error
    )

let rec used_exp : exp -> SS.t = function
  | Var i -> SS.singleton i
  | ConstExp _ -> SS.empty (* def, live *)
  | BinopExp (_, e1, e2) ->
    let live1 = used_exp e1
    and live2 = used_exp e2 in
    SS.union live1 live2
  | UnopExp (_, e) -> used_exp e
  | Condition (e1, e2, e3) ->
    let (live1, live2, live3) = (used_exp e1, used_exp e2, used_exp e3) in
    SS.union (SS.union live1 live2) live3
  | True -> SS.empty
  | False -> SS.empty

let dec_set dec =
  let l = List.map (fun (k,_) -> k) (SM.bindings dec) in
  List.fold_left (fun s e -> SS.add e s) SS.empty l

let rec tc_stm (dec, def, live) t =
  match t with
  | Declare(id, typ, s') -> 
    (match (SM.find' id dec) with
    | Some _ -> ErrorMsg.error None
        ("Variable " ^ S.name id ^ " already declared."); raise ErrorMsg.Error
    | None ->
      (let dec' = SM.add id typ dec in
       let (_, def',live', typ_s') = tc_stm (dec', def, live) s' in
       if SS.mem id live'
       then (ErrorMsg.error None
         ("Variable " ^ S.name id ^ " used before defined in declaration.");
         raise ErrorMsg.Error)
       else
         let def'' = SS.remove id def' in
         let live'' = SS.remove id live' in
         (dec',def'',live'', T.Declare(id, typ, typ_s')) ))

  | Assign (id, exp) ->
    let def' = SS.add id def in
    let live' = used_exp exp in
    (match (SM.find' id dec) with
    | Some(t) -> 
      let (t', te) = tc_exp dec exp in 
      (match (t, t') with
      | (Bool, Bool) -> (dec, def', live', T.Assign(id, (t', te)))
      | (Int, Int) -> (dec, def', live', T.Assign(id, (t', te)))
      | (Int, Bool) | (Bool, Int) -> 
          ErrorMsg.error None
          ("Assigned variable " ^ S.name id ^ " has type " ^ 
          (get_type_str t) ^ " but exp has different type");
          raise ErrorMsg.Error)
    | None -> ErrorMsg.error None
      ("Variable " ^ S.name id ^ " undeclared and being assigned to");
        raise ErrorMsg.Error)

  | If (exp, s1, s2) -> 
    let (t', te) = tc_exp dec exp in
    (match (t') with
    | Bool ->
      let (_, def1, live1, ts1) = tc_stm (dec, def, live) s1 in
      let (_, def2, live2, ts2) = tc_stm (dec, def, live) s2 in
      let used = used_exp exp in
      let def' = SS.inter def1 def2 in
      let live' = SS.union used (SS.union live1 live2) in
      (dec, def', live', T.If((t', te), ts1, ts2))
    | Int -> ErrorMsg.error None ("Type mismatch: If condition has type Int");
             raise ErrorMsg.Error)

  | While (exp, s) -> 
    let (t', te) = tc_exp dec exp in
    (match (t') with
    | Bool ->
        let used = used_exp exp in
        let (_, _, live', ts) = tc_stm (dec, def, live) s in
        (*let def' = SS.empty in (* body may not be executed *)*)
        let live'' = SS.union used live' in
        (dec, def, live'', T.While((t', te), ts))
    | Int -> ErrorMsg.error None 
              ("Type mismatch: While condition has type Int.");
              raise ErrorMsg.Error)

  | Return exp ->
    let check_def x = if SS.mem x def then () else
      (ErrorMsg.error None
        ("Variable " ^ S.name x ^ " used before defined in return.");
        raise ErrorMsg.Error) in

    let (t', te) = tc_exp dec exp in 
    (match (t') with
    | Bool -> ErrorMsg.error None
      ("Type mismatch: Return has type Bool."); raise ErrorMsg.Error
    | Int ->
      let live' = used_exp exp in
      let _ = SS.iter check_def live' in
      (* put all declared variables in def *)
      let def' = SS.union def (dec_set dec) in
      (dec, def', live', T.Return((t', te)))
    )

  | Seq (s1, s2) ->
    let (_, def1, live1, ts1) = tc_stm (dec, def, live) s1 in
    let (_, def2, live2, ts2) = tc_stm (dec, def1, live1) s2 in
    let live' = SS.union live1 (SS.diff live2 def1) in
    (dec, def2, live', T.Seq(ts1, ts2))

  | Expr(exp) ->
    let (t, e) = tc_exp dec exp in
    let live' = used_exp exp in
    (dec, def, live', T.Expr((t, e)))

  | Nop -> (dec, def, live, T.Nop)

let rec return_check s = 
  match s with
  | Declare(id, t, s') -> return_check s'
  | Assign(id, e) -> false
  | If(e, s1, s2) -> (return_check s1) && (return_check s2)
  | While(e, s') -> false
  | Return(e) -> true
  | Nop -> false
  | Seq(s1, s2) -> (return_check s1) || (return_check s2)
  | Expr(e) -> false

let typecheck t = 
  let (_,_,_,typed_ast) = tc_stm (SM.empty, SS.empty, SS.empty) t in
  let return_possible = return_check t in
  if return_possible then typed_ast else (ErrorMsg.error None
        ("Return not possible."); raise ErrorMsg.Error)
