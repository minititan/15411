val typecheck : Ast.program -> TypedAst.typprogram
val tc_stm : (Ast.primtype Symbol.Map.t * Symbol.Set.t * Symbol.Set.t) -> 
					Ast.program -> 
						(Ast.primtype Symbol.Map.t * Symbol.Set.t * Symbol.Set.t * TypedAst.typprogram)

