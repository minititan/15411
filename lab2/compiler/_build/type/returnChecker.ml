open Ast
module SS = Symbol.Set

let rec returnCheck s = 
	match s with
	| Declare(id, t, s') -> returnCheck s'
	| Assign(id, e) -> false
	| If(e, s1, s2) -> (returnCheck s1) && (returnCheck s2)
	| While(e, s') -> false
	| Return(e) -> true
	| Nop -> false
	| Seq(s1, s2) -> (returnCheck s1) || (returnCheck s2)
	| Expr(e) -> false

(* Returns a set of used variables in the expression *)
let rec getUsedVar exp = 
	match exp with
	| Var(id) -> SS.singleton id 
	| ConstExp(i) -> SS.empty
	| BinopExp(o, e1, e2) -> SS.union (getUsedVar e1) (getUsedVar e2)
	| UnopExp(o, e) -> getUsedVar e
	| Condition(e1, e2, e3) -> SS.union (getUsedVar e3) (SS.union (getUsedVar e1) (getUsedVar e2))
	| True | False -> SS.empty


(* returns a tuple of what is defined and live at the statement *)
let rec varInitCheck s in_scope = 
	match s with 
	| Declare(id, t, s') -> 
			let (def_set, live_set) = varInitCheck s' (SS.add id in_scope) in
			(match (SS.mem id live_set) with
			| true -> ErrorMsg.error None ("Variable is live in a Declare"); raise ErrorMsg.Error
			| false -> (SS.remove id def_set, live_set)
			)

	| Assign(id, exp) -> (SS.singleton id, getUsedVar exp)

	| If(exp, s1, s2) -> let ((def_set1, live_set1), (def_set2, live_set2)) = 
								(varInitCheck s1 in_scope, varInitCheck s2 in_scope) in
							let e_used = getUsedVar exp in
								(SS.inter def_set1 def_set2, SS.union e_used (SS.union live_set1 live_set2))

	| While(exp, s') -> let (def, live) = varInitCheck s' in_scope in
						 (SS.empty, SS.union (getUsedVar exp) (live))

	| Return(exp) -> (in_scope, getUsedVar exp)

	| Seq(s1, s2) -> let ((def_set1, live_set1), (def_set2, live_set2)) = 
								(varInitCheck s1 in_scope, varInitCheck s2 in_scope) in

					(SS.union (def_set1) (def_set2),
						SS.union (live_set1) (SS.diff (live_set2) (def_set1)))

	| Nop -> (SS.empty, SS.empty)

	| Expr(exp) -> (SS.empty, getUsedVar exp)
