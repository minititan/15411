type token =
  | EOF
  | STRUCT
  | TYPEDEF
  | IF
  | ELSE
  | WHILE
  | FOR
  | CONTINUE
  | BREAK
  | ASSERT
  | TRUE
  | FALSE
  | NULL
  | ALLOC
  | ALLOCARRY
  | BOOL
  | VOID
  | CHAR
  | STRING
  | SEMI
  | DECCONST of (Int32.t)
  | HEXCONST of (Int32.t)
  | IDENT of (Symbol.symbol)
  | RETURN
  | INT
  | MAIN
  | PLUS
  | MINUS
  | STAR
  | SLASH
  | PERCENT
  | BITAND
  | BITOR
  | XOR
  | BITNOT
  | LOGICNOT
  | LOGICAND
  | LOGICOR
  | PLUSPLUS
  | MINUSMINUS
  | SAR
  | SAL
  | EQUALS
  | NOTEQUALS
  | LESSEQUAL
  | GREATEREQUAL
  | LESS
  | GREATER
  | QUESTION
  | COLON
  | ASSIGN
  | PLUSEQ
  | MINUSEQ
  | STAREQ
  | SLASHEQ
  | PERCENTEQ
  | BITANDEQ
  | BITOREQ
  | XOREQ
  | SAREQ
  | SALEQ
  | LBRACE
  | RBRACE
  | LPAREN
  | RPAREN
  | UNARY
  | ASNOP

val program :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Parsetree.program
