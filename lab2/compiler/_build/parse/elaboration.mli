(* elaboration.ml
 * Ned Williamson
 * Parse Tree -> AST
 *)

(*
val trans_primtype : P.primtype -> A.primtype

val trans_oper : P.oper -> A.oper

val unwrap_decl : P.Simp -> P.Decl option

exception DeclInStep
*)

val trans_program : Parsetree.program -> Ast.program