(* L1 Compiler
 * Parse Tree
 * Author: Alex Vaynberg
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 *
 * Modified: Anand Subramanian <asubrama@andrew.cmu.edu> Fall 2010
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Forward compatible fragment of C0
 *
 * Adapted AST to Parse Tree: Ned Williamson
 *)

type ident = Symbol.symbol

type oper =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDEDBY
  | MODULO
  | NEGATIVE                     (* unary minus *)
  | BITAND
  | BITOR
  | BITNOT
  | SAR
  | SAL
  | XOR
  | LOGICAND
  | LOGICOR
  | LOGICNOT
  | EQUALS
  | NOTEQUALS
  | GREATER
  | GREATEREQUAL
  | LESS
  | LESSEQUAL

type primtype = Int | Bool

type exp =
  | ConstExp of Int32.t
  | True
  | False
  | Var of ident
  | UnopExp of oper * exp
  | BinopExp of oper * exp * exp
  | Condition of exp * exp * exp

type decl =
  | Decl of primtype * ident
  | DeclAsn of primtype * ident * exp

type lvalue =
  | Id of ident

type asop =
  | ASSIGN
  | PLUSEQ
  | MINUSEQ
  | STAREQ
  | SLASHEQ
  | PERCENTEQ
  | BITANDEQ
  | BITOREQ
  | XOREQ
  | SAREQ
  | SALEQ

type postop =
  | PLUSPLUS
  | MINUSMINUS

type simp =
  | SimpAsop of lvalue * asop * exp
  | SimpPostop of lvalue * postop
  | SimpDecl of decl
  | SimpExp of exp

type simpopt =
  | SimpOpt of simp
  | SimpNone

type stmts =
  | Seq of stmt * stmts
  | StmtsNone
and stmt =
  | StmSimp of simp
  | StmControl of control
  | StmBlock of block
and control =
  | If of exp * stmt * elseopt
  | While of exp * stmt
  | For of simpopt * exp * simpopt * stmt
  | Return of exp
and elseopt =
  | Else of stmt
  | ElseNone
and block = Block of stmts

type program = Prog of block

(*
module type PRINT =
  sig
    val pp_exp : exp -> string
    val pp_stm : stm -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_ident id = Symbol.name id

    let pp_oper = function
      | PLUS -> "+"
      | MINUS -> "-"
      | TIMES -> "*"
      | DIVIDEDBY -> "/"
      | MODULO -> "%"
      | NEGATIVE -> "-"
      | BITAND -> "&"
      | BITOR -> "|"
      | BITNOT -> "~"
      | SAR -> ">>"
      | SAL -> "<<"
      | XOR -> "^"
      | LOGICAND -> "&&"
      | LOGICOR -> "||"
      | LOGICNOT -> "!"
      | EQUALS -> "=="
      | NOTEQUALS -> "!="
      | GREATER -> ">"
      | GREATEREQUAL -> ">="
      | LESS -> "<"
      | LESSEQUAL -> "<="

    let rec pp_exp = function
      | Var id     -> pp_ident id
      | ConstExp c -> Int32.to_string c
      | BinopExp (o, e1, e2) -> pp_exp e1 ^ " <- " ^ pp_exp e1 ^ " " ^ pp_oper o ^ " e2"
      | UnopExp (o, e) -> pp_oper o ^ " " ^ pp_exp e
      | Condition (e1, e2, e3) -> pp_exp e1 ^ " ? " ^ pp_exp e2 ^ " : " ^ pp_exp e3
      | True -> "true"
      | False -> "false"

    let pp_type = function
      | Int -> "int"
      | Bool -> "bool"

    let rec pp_stm = function
      | Declare (id, t)         -> pp_type t ^ " " ^ pp_ident id ^ ";"
      | Assign  (id, e)         -> pp_ident id ^ " = " ^ pp_exp e ^ ";"
      | If      (e, s1, s2)     -> "if (" ^ pp_exp e ^ "){" ^ pp_stm s1 ^ "} else {" ^ pp_stm s2 ^ "}\n"
      | While   (e, s)          -> "while (" ^ pp_exp e ^ ") {" ^ pp_stm s ^ "}"
      | Return   e              -> "return " ^ pp_exp e ^ ";"
      | For     (s1, e, s2, s3) -> "for (" ^ pp_stm s1 ^ "; " ^ pp_exp e ^ "; " ^ pp_stm s2 ^ ") {" ^ pp_stm s3 ^ "}"
      | Expr e                  -> pp_exp e
      | Nop                     -> "NOP"

    let rec pp_stms = function
        [] -> ""
      | stm::stms -> pp_stm stm ^ "\n" ^ pp_stms stms

    let pp_program stms = "\nInput {\n" ^ pp_stms stms ^ "}\n"
  end
*)
