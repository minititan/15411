val get_storage_map : int Regalloc_util.IntMap.t -> Storage.str Regalloc_util.IntMap.t

val format : int -> Storage.str -> string

module type PRINT =
  sig
    val pp_storagemap : Storage.str Regalloc_util.IntMap.t -> string
  end

module Print : PRINT
