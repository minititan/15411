type ident = Ast.ident

type oper = Ast.oper

type primtype = Ast.primtype


type simpexp =
  | Var of ident
  | ConstExp of Int32.t
  | BinopExp of oper * typexp * typexp
  | UnopExp of oper * typexp 
  | Condition of typexp * typexp * typexp
  | True
  | False
and typexp = 
  primtype * simpexp
and typstm =
  | Declare of ident * primtype * typstm
  | Assign of ident * typexp
  | If of typexp * typstm * typstm
  | While of typexp * typstm
  | Return of typexp
  | Seq of typstm * typstm
  | Expr of typexp
  | Nop

type typprogram = typstm