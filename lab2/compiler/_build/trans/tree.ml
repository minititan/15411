type binop = 
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type cmpop = 
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type unop = 
  | NEG
  | BNOT

type exp = 
  | Const of Int32.t
  | Temp of Temp.temp
  | Binop of binop * exp * exp
  | Unop of unop * exp

type cmp = 
  | Cmp of cmpop * exp * exp

type command = 
  | Mov of exp * exp
  | If of cmp * Label.label (* if true goto Label.label *)
  | Goto of Label.label
  | Label of Label.label
  | Return of exp

(* Start line *)
type irprogram = command list

module type PRINT =
  sig
  	val pp_unop : unop -> string
  	val pp_binop : binop -> string
    val pp_exp : exp -> string
    val pp_cmp : cmp -> string
    val pp_command : command -> string
    val pp_program : irprogram -> string
  end

module Print : PRINT =
  struct
  	let pp_binop op = 
  		match op with
  		| ADD -> "+"
		  | SUB -> "-"
		  | MUL -> "*"
		  | DIV -> "/"
		  | MOD -> "%"
		  | BAND -> "&"
		  | BOR -> "|"
		  | SAR -> ">>"
		  | SAL -> "<<"
		  | XOR -> "^"


	  let pp_unop op = 
		  match op with
			| NEG -> "-"
		  | BNOT -> "~"

    let pp_cmpop op = 
    match op with
      | EQ -> "=="
      | NOTEQ -> "!="
      | GREATER -> ">"
      | GREATEREQ -> ">="
      | LESS -> "<"
      | LESSEQ -> "<="

  	let rec pp_exp e = 
  		match e with
      | Const(i) -> Int32.to_string i
      | Temp(t) -> Temp.name t 
  	 	| Binop(bop, i1, i2) -> (pp_exp i1) ^ " " ^ (pp_binop bop) ^ " " ^ (pp_exp i2)
  	 	| Unop(uop, i1) -> (pp_unop uop) ^ " " ^ (pp_exp i1)

  	let pp_cmp c = 
  		match c with
  		| Cmp(cop, i1, i2) -> (pp_exp i1) ^ " " ^ (pp_cmpop cop) ^ " " ^ (pp_exp i2)

  	let pp_command c = 
  		match c with
  		| Mov(e1, e2) -> (pp_exp e1) ^ " <- " ^ (pp_exp e2) ^ "\n"
  		| If(c, l) -> "If (" ^ (pp_cmp c) ^ ") " ^ "Goto " ^ (Label.name l) ^ "\n"
  		| Goto(l) -> "Goto " ^ (Label.name l) ^ "\n"
  		| Label(l) -> Label.name l ^ ":" ^ "\n"
  		| Return(e) -> "Ret " ^ (pp_exp e) ^ "\n"

  	let rec pp_program = function
    | [] -> ""
    | instr::instrs' -> pp_command instr ^ pp_program instrs'

  	end





