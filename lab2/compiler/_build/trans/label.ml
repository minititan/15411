type label = int
type t = label

let counter = ref 1

let reset () = counter := 1
let create () =
  let t = !counter in
  let () = counter := !counter + 1 in
  t

let name t = "L" ^ string_of_int t
let get_int t = t
let get_label i = i
let compare a b = compare a b

let format ff t = Format.fprintf ff "%s" (name t)

module S =
  struct
    type t = label
    let compare = compare
    let format = format
  end

module type SET = Printable.SET with type elt = t
module Set = Printable.MakeSet(S)

module type MAP = Printable.MAP with type key = t
module Map = Printable.MakeMap(S)