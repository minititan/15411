type reg =
  | EAX
  | EBX
  | ECX
  | CL (* 8 bit version of ECX *)
  | EDX
  | EBP
  | ESP
  | RSP
  | ESI
  | EDI
  | R8
  | R9
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15

type offset = int

type str = REG of reg | STACK of offset

let format_reg = function
  | EAX -> "%eax"
  | EBX -> "%ebx"
  | ECX -> "%ecx"
  | CL -> "%cl"
  | EDX -> "%edx"
  | EBP -> "%ebp"
  | ESP -> "%esp"
  | RSP -> "%rsp"
  | ESI -> "%esi"
  | EDI -> "%edi"
  | R8 -> "%r8d"
  | R9 -> "%r9d"
  | R10 -> "%r10d"
  | R11 -> "%r11d"
  | R12 -> "%r12d"
  | R13 -> "%r13d"
  | R14 -> "%r14d"
  | R15 -> "%r15d"

let format = function
  | REG r -> format_reg r
  | STACK o -> string_of_int o ^ "(%rsp)"
