(* L1 Compiler
 * Assembly Code Generator for FAKE assembly
 * Author: Alex Vaynberg <alv@andrew.cmu.edu>
 * Based on code by: Kaustuv Chaudhuri <kaustuv+@cs.cmu.edu>
 * Modified: Frank Pfenning <fp@cs.cmu.edu>
 * Converted to OCaml by Michael Duggan <md5i@cs.cmu.edu>
 *
 * Implements a "convenient munch" algorithm
 * llir.ml based on codegen.ml
 *)

module IR = Ir
module S = Storage

type operand =
  | IMM of Int32.t
  | REG of Storage.reg 
  | TEMP of Temp.t

type cmpop =
  | EQ
  | NOTEQ
  | GREATER
  | GREATEREQ
  | LESS
  | LESSEQ

type binop =
  | ADD
  | SUB
  | MUL
  | BAND
  | BOR
  | SAR
  | SAL
  | XOR

type unop =
  | IDIV
  | NEG
  | BNOT

type instr =
  | BINOP of binop * operand * operand
  | UNOP of unop * operand
  | MOV of operand * operand
  | DIRECTIVE of string
  | COMMENT of string
  | IF of cmpop * operand * operand * Label.label (* if true goto Label.label *)
  | GOTO of Label.label
  | LABEL of Label.label
  | CLTD
  | RET

type program = instr list

let munch_operand : IR.operand -> operand = function
  | IR.TEMP t -> TEMP t
  | IR.REG r -> REG r
  | IR.IMM i -> IMM i

exception DestinationImm

let munch_binop binop d t1 t2 =
  let t1 = munch_operand t1
  and t2 = munch_operand t2
  and d = munch_operand d in
  (match binop with
   | Tree.DIV -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EAX)]
   | Tree.MOD -> [MOV (REG S.EAX, t1); CLTD; UNOP (IDIV, t2); MOV (d, REG S.EDX)]
   | Tree.ADD -> if d = t2 then [BINOP (ADD, d, t1)] else [MOV (d, t1); BINOP (ADD, d, t2)] (* split up 3 addr *)
   | Tree.SUB -> if d = t2 then [BINOP (SUB, d, t1); UNOP(NEG, d)] else [MOV (d, t1); BINOP (SUB, d, t2)]
   | Tree.MUL -> if d = t2 then [BINOP (MUL, d, t1)] else [MOV (d, t1); BINOP (MUL, d, t2)]
   | Tree.BAND -> if d = t2 then [BINOP (BAND, d, t1)] else [MOV (d, t1); BINOP (BAND, d, t2)]
   | Tree.BOR -> if d = t2 then [BINOP (BOR, d, t1)] else [MOV (d, t1); BINOP (BOR, d, t2)]
   | Tree.SAR ->
   (* if d is stack
    t1 in r14
    t2 in ecx
    shift r14 by cl
    store r14 into d

    if d is reg
    t1 in d
    t2 in ecx
    shift d by cl

    if d is imm
    fail *)
      (match d with
        | TEMP _ -> [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAR, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        | REG _ -> [MOV (d, t1); MOV (REG S.ECX, t2); BINOP (SAR, d, REG S.CL)]
        | IMM _ -> raise DestinationImm
      )
      (*match (t1,t2) with
      | (TEMP _, TEMP _) -> [MOV (REG S.R14, t2); MOV (d, t1); BINOP (SAR, d, REG S.R14)]
      | (TEMP _, REG _) -> [MOV (d, t1); BINOP (SAR, d, t2)]
      | (TEMP _, IMM _) -> [MOV (d, t1); BINOP (SAR, d, t2)]
      | (REG _, TEMP _) -> [MOV (d, t1); BINOP (SAR, d, t2)]
      | (REG _, IMM _) -> [MOV (d, t1); BINOP (SAR, d, t2)]
      | (IMM _, IMM _) -> [MOV (d, t1); BINOP (SAR, d, t2)]
      )*)
   | Tree.SAL ->
      (match d with
        | TEMP _ -> [MOV (REG S.R14, t1); MOV (REG S.ECX, t2); BINOP (SAL, REG S.R14, REG S.CL); MOV (d, REG S.R14)]
        | REG _ -> [MOV (d, t1); MOV (REG S.ECX, t2); BINOP (SAL, d, REG S.CL)]
        | IMM _ -> raise DestinationImm
      )
   | Tree.XOR -> if d = t2 then [BINOP (XOR, d, t1)] else [MOV (d, t1); BINOP (XOR, d, t2)]
 )

let munch_unop u d t1 =
  let (d, t1) = (munch_operand d, munch_operand t1) in
  (match u with
  | Tree.NEG -> [MOV(d, t1); UNOP (NEG, d)]
  | Tree.BNOT -> [MOV(d, t1); UNOP (BNOT, d)]
  )

let munch_if cop o1 o2 l =
  let o1' = munch_operand o1 in
  let o2' = munch_operand o2 in
  let cop' = (match cop with
  | Tree.EQ -> EQ
  | Tree.NOTEQ -> NOTEQ
  | Tree.GREATER -> GREATER
  | Tree.GREATEREQ -> GREATEREQ
  | Tree.LESS -> LESS
  | Tree.LESSEQ -> LESSEQ
  ) in [IF (cop', o1', o2', l)]

let munch_instr = function
  | IR.BINOP (b, d, t1, t2) -> munch_binop b d t1 t2
  | IR.UNOP (u, d, t1) -> munch_unop u d t1
  | IR.MOV (d, s) ->
    (let d' = munch_operand d
    and s' = munch_operand s in
    [MOV (d', s')])
  | IR.DIRECTIVE s -> [DIRECTIVE s]
  | IR.COMMENT s -> [COMMENT s]
  | IR.IF (cop, o1, o2, l) -> munch_if cop o1 o2 l
  | IR.GOTO l -> [GOTO l]
  | IR.LABEL l -> [LABEL l]
  | IR.RET e -> let e' = munch_operand e in
      [MOV (REG S.EAX, e'); RET]

let rec llirgen = function
  | [] -> []
  | i::is -> (munch_instr i) @ (llirgen is)

(*
  | IR.BINOP (binop, d, t1, t2) -> munch_binop binop d t1 t2
  | IR.MOV (t1, t2) ->
    let t1 = munch_operand t1
    and t2 = munch_operand t2 in
    [MOV (t1, t2)]
  | IR.DIRECTIVE str -> [DIRECTIVE str]
  | IR.COMMENT str -> [DIRECTIVE str]
  | IR.RET(_) -> [RET] (* to fix *)
*)

module type PRINT =
  sig
    val pp_operand : operand -> string
    val pp_binop : binop -> string
    val pp_unop : unop -> string
    val pp_instr : instr -> string
    val pp_program : program -> string
  end

module Print : PRINT =
  struct

    let pp_operand = function
    | IMM i -> Int32.to_string i
    | REG r -> S.format_reg r
    | TEMP t -> Temp.name t

    let pp_binop = function
    | ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "MUL"
    | BAND -> "BAND"
    | BOR -> "BOR"
    | SAR -> "SAR"
    | SAL -> "SAL"
    | XOR -> "XOR"

    let pp_unop = function
    | IDIV -> "IDIV"
    | NEG -> "NEG"
    | BNOT -> "BNOT"

    let pp_label = Label.name

    let pp_cmpop = function
    | EQ -> "EQ"
    | NOTEQ -> "NEQ"
    | GREATER -> "GT"
    | GREATEREQ -> "GTE"
    | LESS -> "LT"
    | LESSEQ -> "LTE"

    let pp_instr = function
    | BINOP (oper, op1, op2) -> pp_binop oper ^ " " ^ pp_operand op1 ^ " <- " ^ pp_operand op1 ^ "," ^ pp_operand op2
    | UNOP (oper, op1) -> pp_unop oper ^ " " ^ pp_operand op1
    | MOV (d, s) -> "MOV " ^ pp_operand d ^ "," ^ pp_operand s
    | DIRECTIVE s -> "DIRECTIVE: " ^ s
    | COMMENT s -> "COMMENT: " ^ s
    | CLTD -> "CLTD"
    | RET -> "RET"
    | IF (cop, o1, o2, l) -> "IF (" ^ pp_operand o1 ^ " " ^ pp_cmpop cop ^ " " ^ pp_operand o2 ^ ") GOTO " ^ pp_label l
    | GOTO l -> "GOTO " ^ pp_label l
    | LABEL l -> "LABEL " ^ pp_label l

    let rec pp_instrs = function
    | [] -> ""
    | instr::instrs' -> pp_instr instr ^ "\n" ^ pp_instrs instrs'

    let pp_program instrs = "LLIR code {\n" ^ pp_instrs instrs ^ "}\n"
  end
